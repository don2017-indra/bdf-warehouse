 $(function() {

          $('.select_date').daterangepicker({
              autoUpdateInput: false,
              locale: {
                  cancelLabel: 'Clear'
              }
          });

          $('.select_date').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
          });

          $('.select_date').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });



            $(document).on("click", "#submitUpdate", function(e) {
                var id = $(this).data('id');
                var form = '#update-status-'+id;
                $(form).submit();
            });
        });

        $(document).on('click', 'th.sorting, th.sorting_desc, th.sorting_asc', function(e) {
            e.preventDefault();
            var requestUrl = window.location.href;

            var segments = requestUrl.split('/');
            var lastSegment = segments[segments.length-1].split('?');

            if(lastSegment.length == 1)
            {
                requestUrl+='?';
            }

            var url = requestUrl;
            var sortby = $(this).data('sortby');
            var cssclass = $(this).attr('class');
            if ((cssclass == 'sorting') || (cssclass == 'sorting_asc')){
               
                if(lastSegment.length == 1)
                {
                    url +=  "sortby=" + sortby + "&order=ASC";
               
                } else {
                   
                    url +=  "&sortby=" + sortby + "&order=ASC";
                }
                   
            } else {
               
                if(lastSegment.length == 1)
                {
                    url +=  "sortby=" + sortby + "&order=DESC";
                
                }else{
                
                    url +=  "&sortby=" + sortby + "&order=DESC";
               }
                
            }

            window.location.href = url;

        });

        $(document).on("click", ".sort", function(e) {
            e.preventDefault();

            var requestUrl = window.location.href;

            var segments = requestUrl.split('/');
            var lastSegment = segments[segments.length-1].split('?');

            if(lastSegment.length == 1)
            {
                requestUrl+='?';
            }          

            var url = requestUrl;
            var sortby = $(this).data('sortby');
            var cssclass = $(this).data('class');
            if ((cssclass === 'sorting') || (cssclass === 'sorting_desc')){
               
                if(lastSegment.length == 1)
                {
                    url +=  "sortby=" + sortby + "&order=ASC";
               
                }else{
                   
                    url +=  "&sortby=" + sortby + "&order=ASC";
                }
                   
            }else{
               
                if(lastSegment.length == 1)
                {
                    url +=  "sortby=" + sortby + "&order=DESC";
                
                }else{
                
                    url +=  "&sortby=" + sortby + "&order=DESC";
               }
                
            }

            window.location.href = url;

        });