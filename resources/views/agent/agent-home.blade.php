@extends('agent.layouts.app-agent')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label style="font: bold 15pt Calibri ">Welcome to Agent Portal</label>
                </div>
                <div class="panel-body">
                    Hello {{ Session::get('AgentName') }}!
                </div>
            </div>
        </div>
    </div>
@endsection
