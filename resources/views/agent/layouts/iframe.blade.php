<!DOCTYPE HTML>
<html lang="en">
<head>
    @include('agent.partials.head-agent')
    @include('agent.partials.javascripts-agent')
</head>
<body class="iframe-popup">
<div class="layout">
        <div class="main-wrapper-iframe" style="height:100%;">
            <div class="container-fluid">
            	@if (Session::has('flash_success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <i class="icon-ok-sign"></i><strong>Success</strong> {{ Session::get('flash_success') }}
                    </div>
                @endif
                @if (Session::has('flash_notice'))
                    <div class="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <i class="icon-exclamation-sign"></i><strong>Warning!</strong> {{ Session::get('flash_notice') }}
                    </div>
                @endif
                @if (Session::has('flash_error'))
                    <div class="alert alert-error">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <i class="icon-minus-sign"></i><strong>Warning!</strong> {{ Session::get('flash_error') }}
                    </div>
                @endif
                @yield('content')
            </div>
        </div>
</div>
@section('footerasset')
@show
</body>
</html>