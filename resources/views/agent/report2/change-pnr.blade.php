<input type="hidden" value="{{ $agentID }}" name="user_id">
<input type="hidden" value="{{ $flight_date }}" name="flight_date">
<input type="hidden" value="{{ $flight_no }}" name="flight_no">
<input type="hidden" value="{{ $carrier_code }}" name="carrier_code">
<input type="hidden" value="pnr" name="updatefield" id="updatefield"> 
<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-3 control-label">Existing PNR : </label>
        <div class="col-md-9">
            <label class="control-label">{{ $pnr }}</label>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-3 control-label" for="order_status">New PNR : </label>
        <div class="col-md-6">
            <input id="new_pnr" name="new_pnr" type="text" placeholder="" class="form-control" value="{{ old('new_pnr') }}">
            &nbsp;
        </div>
        <div class="col-md-3">
            <button type="button" class="btn btn-primary" id="loadnewpnr">Select</button>
        </div>
    </div>
</div>
<div class="col-md-12">
    <span id="loading-t1" style="display: none;"><img src="{{ asset('images/loading.gif') }}">Refreshing...</span>
    &nbsp;
    <div class="newPnr">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-3 control-label" for="order_status">Flight : </label>
                <div class="col-md-9">
                    <select name="flight" id="flight" class="form-control" width="100%"></select>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-3 control-label" for="order_status">Pax : </label>
                <div class="col-md-9">
                    <select name="pax" id="pax" class="form-control" width="100%"></select>
                </div>
            </div>
        </div>
    </div>
</div>
&nbsp;
<span id="loading-update" style="display: none;"><img src="{{ asset('images/loading.gif') }}">Refreshing...</span>
&nbsp;
<div class="col-md-6">
    <div class="form-actions pull-right ">
        <button id="submit_update_pnr" type="submit" class="btn btn-primary product_submit" data-id="{{ $order->order_id }}" onclick="javascript:startUpdate();">Submit</button>
        &nbsp;
        <input type="button" id="backtochoose" class="btn btn-default" value="Back"  data-id="{{ $order->order_id }}">
    </div>
</div>