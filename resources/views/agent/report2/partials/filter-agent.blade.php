<form method="get" action="" class="form-horizontal">
	<div class="col-md-12">
						<div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('flight_date', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="flight_date">Flight Date</label>
                                    <div class="col-md-9">
                                        <input id="flight_date" name="flight_date" type="text" placeholder="" class="form-control select_date" value="{{ old('flight_date',$request->has('flight_date') != null ? $request->input('flight_date') : '') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('sales_date', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="sales_date">Sales Date</label>
                                    <div class="col-md-9">
                                        <input id="sales_date" name="sales_date" type="text" placeholder="" class="form-control select_date" value="{{ old('sales_date',$request->has('sales_date') != null ? $request->input('sales_date') : '') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('flight_type', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="flight_type">Flight Type : </label>
                                    <div class="col-md-9">
                                        <select name="flight_type" id="flight_type" class="form-control select2">
                                            <option value="">Select</option>
                                            <option value="all" {{ $request->input('flight_type') == 'all' ? 'selected' : ''  }}>All</option>
                                            <option value="domestic" {{ $request->input('flight_type') == 'domestic' ? 'selected' : ''  }}>Domestic</option>
                                            <option value="international" {{ $request->input('flight_type') == 'international' ? 'selected' : ''  }}>International</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('order_no', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="order_no">Order No. : </label>
                                    <div class="col-md-9">
                                        <input id="order_no" name="order_no" type="text" placeholder="" class="form-control" value="{{ old('order_no') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('flight_no', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="flight_no">Flight No. : </label>
                                    <div class="col-md-9">
                                        <input id="flight_no" name="flight_no" type="text" placeholder="" class="form-control" value="{{ old('flight_no') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('aoc', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="aoc">AOC : </label>
                                    <div class="col-md-9">
                                        <select name="aoc" id="aoc" class="form-control select2">
                                            <option value="">Select</option>
                                            <option value="All">All</option>
                                            <option value="AOC">AOC</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="row">
                            
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('pnc', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="pnc">PNR : </label>
                                    <div class="col-md-9">
                                        <input id="pnc" name="pnc" type="text" placeholder="" class="form-control" value="{{ old('pnc') }}">
                                    </div>
                                </div>
                            </div>
							<div class="col-md-8">
                                <div class="form-group {{ $errors->first('order_status', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="order_status">Status : </label>
                                    <div class="col-md-9">
                                        <select name="order_status" id="order_status" class="form-control select2" multiple="multiple">
                                            <option value="">Select</option>
                                            <option value="ordered">Ordered</option><!--paid,reserved,reprint_pack_slip,cart_loaded-->
                                    
                                            <option value="delivered">Delivered</option>
                                            <option value="not_delivered">Not Delivered</option>
                                            <option value="order_cancelled">Order Cancelled</option>
                                            <option value="req_refund">Requested Refund</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="row">
							
                        </div>
					</div>
					
                    <div class="col-md-12">
                        <div class="form-actions pull-left ">
                            <button type="submit" class="btn btn-primary product_submit">Submit</button>
                                        &nbsp;
                            <input type="reset" class="btn btn-default" value="Reset">
                        </div>
                    </div>
</form>