<input type="hidden" value="{{ $agentID }}" name="user_id">
<input type="hidden" value="{{ $orderId }}" name="order_id">
<input type="hidden" value="{{ $flight_date }}" name="flight_date">
<input type="hidden" value="{{ $flight_no }}" name="flight_no">
<input type="hidden" value="{{ $carrier_code }}" name="carrier_code">
<input type="hidden" value="pax" name="updatefield" id="updatefield"> 
<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-3 control-label" for="order_status">Existing Pax : </label>
        <div class="col-md-9">
            <label class="control-label">{{ $recipient }}</label>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-3 control-label" for="order_status">New Pax : </label>
        <div class="col-md-9">
            <select name="pax" id="pax" class="form-control" width="100%">
                <?php
                if(!empty($paxData)){
                    foreach ($paxData as $key => $passenger) {
                        $fullname = $passenger['firstName'].' '.$passenger['lastName'];
                        $params = $passenger['firstName'].'|'.$passenger['lastName'].'|'.$passenger['seatNo'];
                        echo '<option value="'.$params.'">'.$fullname.'</option>';
                    }
                }              
                ?>
            </select>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-actions pull-left ">
        <button id="submit_update_pax" type="submit" class="btn btn-primary product_submit" data-id="{{ $order->order_id }}" onclick="javascript:startUpdate();">Submit</button>
                &nbsp;
        <span id="loading-update" style="display: none;"><img src="{{ asset('images/loading.gif') }}">Refreshing...</span>
        &nbsp;
        <input type="button" id="backtochoose" class="btn btn-default" value="Back"  data-id="{{ $order->order_id }}">
    </div>
</div>