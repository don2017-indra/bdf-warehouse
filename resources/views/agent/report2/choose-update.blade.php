
    <div class="modal-body clearfix">
    	<div class="col-md-12">
                    <div class="form-group">
                        <label>
                            <input type="radio" name="type" value="PNR">
                            PNR
                        </label>
                    </div>
                </div>
          
            
                <div class="col-md-12">
                    <div class="form-group">
                        <label>
                            <input type="radio" name="type" value="Flight">
                            Flight
                        </label>
                    </div>
                </div>

            
                <div class="col-md-12">
                    <div class="form-group">
                        <label>
                            <input type="radio" name="type" value="Pax">
                            Pax
                        </label>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>
                            <input type="radio" name="type" value="Status">
                            Status
                        </label>
                    </div>
                </div>
	</div>
    <div class="modal-footer">	
        <div class="col-md-12">
            <div class="form-actions pull-right ">
                <button type="button" class="btn btn-primary" id="selectType" data-id="{{ $order->order_id }}">Select</button>
                &nbsp;
                <button type="button" class="btn btn-default" data-dismiss="modal" id="closeUpdate">Close</button>
                
            </div>
        </div>
    </div>
