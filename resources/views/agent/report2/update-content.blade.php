{{-- Page content --}}
@section('content')
    <section class="content paddingleft_right15">
        <div class="row">
                <div class="panel-body">
                    <div class="row">
                    @if($order_result)
                    @foreach($order_result->rows as $key => $order)
                    <?php
                    $incrementId = $order->order_id;
                            $orderId = $order->order_id;
                            $pnr = $order->pnr;
                            $flight = $order->carrier_code.$order->flight_number;
                            $sales_date = $order->sales_date;
                            $flight_date = $order->flight_date;
                            $flight = $order->departure_station.' - '.$order->arrival_station;
                            $recipient = $order->first_name.' '.$order->last_name;
                            $flight_no = $order->flight_number;
                            $carrier_code = $order->carrier_code;
                            $email = '';
                    ?>
                    <div class="panel panel-primary ">
                        <div class="panel-heading clearfix">
                            <h4 class="panel-title pull-left"> <i class="livicon" data-name="sitemap" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                Order no : {{ $order->increment_id }}
                            </h4>
                        </div>
                        <br />
                        <div class="panel-body">
                            <div id="defaultModal" class="row">
                                @include('agent.report.choose-update')
                            </div>
                            <div id="pnrModal" class="row">
                                <form method="post" action="{{ route('agent.report.update-pnr',$order->order_id) }}" class="form-horizontal" id="form-update-pnr-{{ $order->order_id }}">
                                    @include('agent.report.change-pnr')
                                </form>
                            </div>
                            <div id="flightModal" class="row">
                                <form method="post" action="{{ route('agent.report.update-flight',$order->order_id) }}" class="form-horizontal" id="form-update-flight-{{ $order->order_id }}">
                                    @include('agent.report.change-flight')
                                </form>
                            </div>
                            <div id="paxModal" class="row">
                                <form method="post" action="{{ route('agent.report.update-pax',$order->order_id) }}" class="form-horizontal" id="form-update-pax-{{ $order->order_id }}">
                                    @include('agent.report.change-pax')
                                </form>
                            </div>
                            <div id="statusModal" class="row">
                                <form method="post" action="{{ route('agent.report.update-status',$order->order_id) }}" class="form-horizontal" id="form-update-status-{{ $order->order_id }}">
                                        @include('agent.report.change-status')
                                </form>
                            </div>
                            
                        </div>
                    </div>
                    @endforeach
                    @endif
                    </div>
                </div>
        </div>    <!-- row-->
        <!-- row-->
    </section>
    <script type="text/javascript">

        function startUpdate(){
            document.getElementById('updatefield').value = 'true';
            setTimeout("parent.$.fancybox.close()", 5000);
        }

    $(document).ready(function() {
        $('#defaultModal').show();
        $('#flightModal').hide();
        $('#paxModal').hide();
        $('#statusModal').hide();
        $('#pnrModal').hide();
        $(document).on("click", "#selectType", function(e) {
                
                var id = $(this).data('id');
                var type = $("input[name='type']:checked").val();
                if(type == 'PNR'){
                    $('#defaultModal').hide();
                    $('#flightModal').hide();
                    $('#paxModal').hide();
                    $('#statusModal').hide();
                    $('#pnrModal').show();
                }
                else if(type == 'Flight'){
                    $('#defaultModal').hide();
                    $('#flightModal').show();
                    $('#paxModal').hide();
                    $('#statusModal').hide();
                    $('#pnrModal').hide();
                }
                else if(type == 'Pax'){
                   $('#defaultModal').hide();
                    $('#flightModal').hide();
                    $('#paxModal').show();
                    $('#statusModal').hide();
                    $('#pnrModal').hide();
                }
                else if(type == 'Status'){
                    $('#defaultModal').hide();
                    $('#flightModal').hide();
                    $('#paxModal').hide();
                    $('#statusModal').show();
                    $('#pnrModal').hide();
                } else {
                    $('#defaultModal').show();
                    $('#flightModal').hide();
                    $('#paxModal').hide();
                    $('#statusModal').hide();
                    $('#pnrModal').hide();
                }
                
            });

        $(document).on("click", "#backtochoose", function(e) {
                $('#defaultModal').show();
                    $('#flightModal').hide();
                    $('#paxModal').hide();
                    $('#statusModal').hide();
                    $('#pnrModal').hide();
                
            });

        $(document).on("click", "#loadnewpnr", function(e) {
                var pnr = $('#new_pnr').val();
                var url = "{{ url('agent/change-pnr') }}"+'/'+pnr;
                //alert(pnr);
                //alert(url);
                $("#loading-t1").fadeIn();
                $.ajax
                        ({
                            type: "GET",
                            url: url,
                            cache: false,
                            success: function(html)
                            {
                                //alert(html);
                                $(".newPnr").empty();
                                $(".newPnr").html(html);
                                $("#loading-t1").fadeOut('fast');
                            }
                        });
        });

        $(document).on("click", "#submit_update_flight", function(e) {
                $("#loading-update").fadeIn();
                var id = $(this).data('id');
                var form = '#form-update-flight-'+id;
                $(form).submit();
        });

        $(document).on("click", "#submit_update_pax", function(e) {
                var id = $(this).data('id');
                var form = '#form-update-pax-'+id;
                $(form).submit();
                $("#loading-update").fadeIn();
        });

        $(document).on("click", "#submit_update_pnr", function(e) {
                var id = $(this).data('id');
                var form = '#form-update-pnr-'+id;
                $(form).submit();
                $("#loading-update").fadeIn();
        });

        $(document).on("click", "#submit_update_status", function(e) {
                var id = $(this).data('id');
                var form = '#form-update-status-'+id;
                $(form).submit();
                $("#loading-update").fadeIn();
        });

        $(document).on("click", "#closeUpdate", function(e) {
            return parent.$.fancybox.close();
        });
    });
</script>
@stop


