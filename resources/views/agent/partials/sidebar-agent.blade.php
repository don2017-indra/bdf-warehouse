@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            <li class="{{ $request->segment(2) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/agent/home') }}">
                    <i class="fa fa-tachometer"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>
			<li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span class="title">Reports</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ $request->segment(1) == 'agent' && $request->segment(2) == 'report' ? 'active active-sub' : '' }}">
                        <a href="{{ route('agent.report') }}">
                            <i class="fa  fa-list-alt"></i>
                            <span class="title">
                                Agent Reports
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(1) == 'agent2' ? 'active active-sub' : '' }}">
                        <a href="{{ route('agent2.report') }}">
                            <i class="fa  fa-list-alt"></i>
                            <span class="title">
                                Agent Reports 2
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
			
            <li>
                <a href="{{ route('agent.post-logout') }}">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('global.app_logout')</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
