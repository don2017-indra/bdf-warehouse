<input type="hidden" value="{{ $agentID }}" name="user_id">
<input type="hidden" value="status" name="updatefield" id="updatefield"> 
    <div class="col-md-12">
        <div class="form-group {{ $errors->first('order_status', 'has--error') }}">
            <label class="col-md-3 control-label" for="order_status">Status : </label>
            <div class="col-md-9">
                {!! Form::select('new_order_status', $order_status, old('new_order_status') ? old('new_order_status') : '', ['class' => 'form-control', 'width' => '100%']) !!}
            </div>
        </div>
    </div>
	<div class="col-md-6">
            <div class="form-actions pull-left ">
                <button id="submit_update_status" type="submit" class="btn btn-primary product_submit" data-id="{{ $order->order_id }}" onclick="javascript:startUpdate();">Submit</button>
                &nbsp;
                <span id="loading-update" style="display: none;"><img src="{{ asset('images/loading.gif') }}">Refreshing...</span>
                &nbsp;
                <input type="button" id="backtochoose" class="btn btn-default" value="Back"  data-id="{{ $order->order_id }}">
            </div>
    </div>