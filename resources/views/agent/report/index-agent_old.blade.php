@inject('request', 'Illuminate\Http\Request')
@inject('StatusPresenter', 'App\Presenters\StatusPresenter')
@inject('BookingApiPresenter', 'App\Presenters\BookingApiPresenter')
@extends('agent.layouts.app-agent')

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    {{--<link href="{{ asset('css/tables.css') }}" rel="stylesheet" type="text/css" />--}}
    <link href="{{ asset('assets/vendors/sweetalert/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Agent Reports</h1>
        <ol class="breadcrumb">
            <li>
                <a href="">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li><a href="#"> Reports</a></li>
            <li class="active">Agent Reports List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="sitemap" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Agent Reports
                    </h4>
                </div>
                <br />
                <div class="panel-body">
					<div class="row">
					@include('agent.report.partials.filter') 
					</div>
					<div class="col-md-12"><hr></div>
					<div class="row">
                        <div class="panel-body">
                       
						<table class="table table-bordered table-striped table-hover dataTable" id="table">
							<thead>
							<tr>
                                <th width="5%" class="sorting_asc">Order No.</th>
								<th width="5%" class="sorting_asc">PNR</th>
								<th width="5%" class="sorting_asc">Flight No.</th>
								<th width="10%" class="sorting_asc">Sales Date</th>
								<th width="10%" class="sorting_asc">Flight Date</th>
								<th width="20%">Recipient Name</th>
								<th width="10%" class="sorting_asc">Status</th>
								<th width="15%">Actions</th>
							</tr>
							</thead>
							<tbody>
                            @if($order_result)
                            @php
                            $orderId = '';
                            $pnr = '';
                            $flight = '';
                            $sales_date = '';
                            $flight_date = '';
                            $recipient = '';
                            $status = '';
                            @endphp
                            @foreach($order_result['rows'] as $key => $order)
                            @php
                            $orderId = $order['order_id'];
                            $pnr = $order['pnr'];
                            $flight = $order['carrier_code'].$order['flight_number'];
                            $sales_date = $order['sales_date'];
                            $flight_date = $order['flight_date'];
                            $flight = $order['departure_station'].' - '.$order['arrival_station'];
                            $recipient = $order['first_name'].' '.$order['last_name'];
                            $status = $StatusPresenter->getStatusDesc($order['order_status'])
                            @endphp
                            <tr>
                                <td>{{ $orderId }}</td>
                                <td>{{ $pnr }}</td>
                                <td>{{ $flight }}</td>
                                <td>{{ $sales_date }}</td>
                                <td>{{ $flight_date }}</td>
                                <td>{{ $recipient }}</td>
                                <td>{{ $status }}</td>
                                <td>
                                    <div class="text-right">
                                        <a class="btn btn-warning" href="{{ route('agent.report.detail',$order['order_id']) }}" data-toggle="tooltip" data-placement="top" data-title="View order" ><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-success" data-toggle="modal" data-target="#defaultModal-{{ $order['order_id'] }}" data-placement="top" data-title="Update status" data-pnr="$order['pnr']"><i class="fa fa-exchange"></i></a>
                                    </div>
                                    <!-- Modal notify winner -->
                                    <div class="modal fade" class="myModal" id="defaultModal-{{ $order['order_id'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <form method="post" action="" class="form-horizontal">
                                          {{ csrf_field() }}  
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Update - Order ID : {{ $order['order_id'] }}</h4>
                                          </div>
                                          @include('agent.report.choose-update')
                                          </form> 
                                        </div>
                                      </div>
                                    </div>

                                    <!--pnr modal -->
                                    <div class="modal fade" class="myModal" id="pnrModal-{{ $order['order_id'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <form method="post" action="" class="form-horizontal">
                                          {{ csrf_field() }}  
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Update - Order ID : {{ $order['order_id'] }}</h4>
                                          </div>
                                         @include('agent.report.change-pnr')
                                         </form> 
                                        </div>
                                      </div>
                                    </div>

                                    <!--flight modal -->
                                    <div class="modal fade" class="myModal" id="flightModal-{{ $order['order_id'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <form method="post" action="{{ route('agent.report.update-flight',$order['order_id']) }}" class="form-horizontal" id="form-update-flight-{{ $order['order_id'] }}">
                                          {{ csrf_field() }}  
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Update - Order ID : {{ $order['order_id'] }}</h4>
                                          </div>
                                          @include('agent.report.change-flight')
                                          </form> 
                                        </div>
                                      </div>
                                    </div>

                                    <!--pax modal -->
                                    <div class="modal fade" class="myModal" id="paxModal-{{ $order['order_id'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <form method="post" action="" class="form-horizontal">
                                          {{ csrf_field() }}  
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Update - Order ID : {{ $order['order_id'] }}</h4>
                                          </div>
                                          @include('agent.report.change-pax')
                                          </form>
                                        </div>
                                      </div>
                                    </div>

                                    <!--status modal -->
                                    <div class="modal fade" class="myModal" id="statusModal-{{ $order['order_id'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <form method="post" action="" class="form-horizontal">
                                          {{ csrf_field() }}
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Update - Order ID : {{ $order['order_id'] }}</h4>
                                            
                                          </div>
                                          @include('agent.report.change-status') 
                                          </form>
                                        </div>
                                      </div>
                                    </div>

                                    <!-- end Modal -->
                                </td>
                            </tr>
                            @endforeach
                            @endif           
                            </tbody>
						</table>
                        <div class="row">
                            
                        </div>
                        
                        </div>
					</div>
                    
                </div>
            </div>
        </div>    <!-- row-->
    </section>
@stop

{{-- page level scripts --}}
@section('javascript')
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>--}}
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/moment.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script>

        $(function() {

          $('.select_date').daterangepicker({
              autoUpdateInput: false,
              locale: {
                  cancelLabel: 'Clear'
              }
          });

          $('.select_date').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
          });

          $('.select_date').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });



            $(document).on("click", "#selectType", function(e) {
                
                var id = $(this).data('id');
                var type = $("input[name='type']:checked").val();
                if(type == 'PNR'){
                    var modal = '#pnrModal-'+id;
                    $(modal).modal('show');
                    var defaultmodal = '#defaultModal-'+id;
                    $(defaultmodal).modal('hide');
                }
                else if(type == 'Flight'){
                    var modal = '#flightModal-'+id;
                    $(modal).modal('show');
                    var defaultmodal = '#defaultModal-'+id;
                    $(defaultmodal).modal('hide');
                }
                else if(type == 'Pax'){
                    var modal = '#paxModal-'+id;
                    $(modal).modal('show');
                    var defaultmodal = '#defaultModal-'+id;
                    $(defaultmodal).modal('hide');
                }
                else if(type == 'Status'){
                    var modal = '#statusModal-'+id;
                    $(modal).modal('show');
                    var defaultmodal = '#defaultModal-'+id;
                    $(defaultmodal).modal('hide');
                } else {
                    var defaultmodal = '#defaultModal-'+id;
                    $(defaultmodal).modal('show');
                    $(this).modal('hide');
                }
                
            });
            $(document).on("click", "#backtochoose", function(e) {
                var id = $(this).data('id');
                $(this).closest('.modal').modal('hide');
                var defaultmodal = '#defaultModal-'+id;
                $(defaultmodal).modal('show');
                
            });

            $(document).on("keydown", "#new_pnr", function(e) {
                var pnr = $(this).val();
                var url = "{{ url('agent/change-pnr') }}"+'/'+pnr;
                //alert(url);
                $("#loading-t1").fadeIn();
                $.ajax
                        ({
                            type: "GET",
                            url: url,
                            cache: false,
                            success: function(html)
                            {
                                //alert(html);
                                $(".newPnr").empty();
                                $(".newPnr").html(html);
                                $("#loading-t1").fadeOut('fast');
                            }
                        });
            });

            $(document).on("click", "#submit_update_flight", function(e) {
                var id = $(this).data('id');
                var form = '#form-update-flight-'+id;
                $(form).submit();
            });
        });
    </script>



    @include('layouts.flash')
@stop

