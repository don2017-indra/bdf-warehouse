<input type="hidden" value="{{ $agentID }}" name="user_id">
<input type="hidden" value="{{ $orderId }}" name="order_id">
<input type="hidden" value="{{ $flight_date }}" name="flight_date">
<input type="hidden" value="{{ $flight_no }}" name="flight_no">
<input type="hidden" value="{{ $carrier_code }}" name="carrier_code">   
<input type="hidden" value="{{ $email }}" name="user_email">   
<input type="hidden" value="flight" name="updatefield" id="updatefield">    
<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-3 control-label" for="order_status">Existing Flight : </label>
        <label class="col-md-9 control-label">{{ $flight }}</label>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <div class="col-md-3">
                <label class="control-label" for="order_status">New Flight : </label>
        </div>
        <div class="col-md-9">
            <select name="flight_detail" id="flight_detail" class="form-control" width="100%">
                <?php
                    if($flightData) {
                        foreach ($flightData as $key => $flight) {
                            $date = date_create($flight['departure_date']);
                            $departure_date = date_format($date,'d M');
                            $item = $flight['departure'].'-'.$flight['arrival'].' '.$departure_date;
                            $params = $flight['bookingId'].'|'.$flight['flight_date'].'|'.$flight['std'].'|'.$flight['sta'].'|'.$flight['departure'].'|'.$flight['arrival'].'|'.$flight['carrier_code'].'|'.$flight['flight_type'].'|'.$flight['email'].'|'.$flight['flight_no'];
                            
                            echo '<option value="'.$params.'">'.$item.'</option>';
                                        
                        } 
                    }
                ?>
            </select>     
        </div>
    </div>
</div>
&nbsp;
<span id="loading-update" style="display: none;"><img src="{{ asset('images/loading.gif') }}">Refreshing...</span>
&nbsp;
<div class="col-md-6">
    <div class="form-actions pull-left ">
        <button id="submit_update_flight" type="submit" class="btn btn-primary product_submit" data-id="{{ $order->order_id }}" onclick="javascript:startUpdate();">Submit</button>
                &nbsp;
        <input type="button" id="backtochoose" class="btn btn-default" value="Back"  data-id="{{ $order->order_id }}">
    </div>
</div>