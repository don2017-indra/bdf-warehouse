<div class="row">
            @if($order_result)
            @foreach($order_result->rows as $key => $order)
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="sitemap" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Order no : {{ $order->increment_id }}
                    </h4>
                </div>
                <br />
                <div class="panel-body">
                    <div class="row">
                        <div class="panel-body">
                        <table class="table table-bordered " id="table">
                            <tr>
                                <th>Order No.</th>
                                <td>{{ $order->increment_id }}</td>
                            </tr>
                            <tr>
                                <th>PNR</th>
                                <td>{{ $order->pnr }}</td>
                            </tr>
                            <tr>
                                <th>Flight No.</th>
                                <td>{{ $order->carrier_code }}{{ $order->flight_number }}</td>
                            </tr>
                            <tr>
                                <th>Sales Date</th>
                                <td>{{ $order->sales_date }}</td>
                            </tr>
                            <tr>
                                <th>Flight Date</th>
                                <td>{{ $order->flight_date }}</td>
                            </tr>
                            <tr>
                                <th>Recipient Name</th>
                                <td>{{ $order->first_name }} {{ $order->last_name }}</td>
                            </tr>                
                        </table>
                        <hr>
                        <table class="table table-bordered " id="table">
                            <thead>
                            <tr class="filters">
                                <th>Sku</th>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Discounted Price</th>
                                <th>Final Price</th>
                                <th>Quantity</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order->items as $key => $item)    
                            <tr>
                                <td>{{ $item->product_sku }}</td>
                                <td>{{ $item->product_name }}</td>
                                <td>{{ number_format($item->price_per_item,2) }}</td>
                                <td>{{ number_format($item->discounted_price,2) }}</td>
                                <td>{{ number_format($item->final_discounted_price,2) }}</td>
                                <td> {{ number_format($item->qty,0) }} </td>
                            </tr>
                            @endforeach 
                            </tbody>
                        </table>
                        </div>
                    </div>
                    
                </div>
            </div>
            @endforeach
            @endif
        </div> 
<form method="get" action="" class="form-horizontal">
    {{ csrf_field() }}
    <input type="hidden" name="order_id" value="">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
                <div class="form-group {{ $errors->first('order_status', 'has--error') }}">
                    <label class="col-md-3 control-label" for="order_status">Status : </label>
                    <div class="col-md-9">
                        {!! Form::select('order_status', $order_status, old('order_status') ? old('order_status') : '', ['class' => 'form-control select2', ]) !!}
                    </div>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="col-md-12">
                <div class="form-group {{ $errors->first('order_status', 'has--error') }}">
                    <label class="col-md-3 control-label" for="order_status">Comment : </label>
                    <div class="col-md-9">
                        <textarea name="comment" id="comment" rows="10" class="form-control"></textarea>
                    </div>
                </div>
            </div>
        </div>
	</div>
					
    <div class="col-md-6">
        <div class="form-actions pull-left ">
            <button type="submit" class="btn btn-primary product_submit">Submit</button>
            &nbsp;
            <input type="reset" class="btn btn-default" value="Reset" onclick="reset();">
        </div>
    </div>
</form>
