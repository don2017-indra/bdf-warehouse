@extends('agent.layouts.auth-agent')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Agent Portal Login</b></div>
                <div class="panel-body">
                    @if (Session::has('timeout'))
                        <div class="alert alert-danger">
                            <p>{{ Session::get('timeout') }}</p>
                        </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger">
                            <p>{{ Session::get('error') }}</p>
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were problems with input:
                            <br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('agent.post-login') }}">
                        <input type="hidden"
                               name="_token"
                               value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Type</label>

                            <div class="col-md-8">
                                <div class="radio col-md-4">
                                    <label>
                                      <input type="radio" name="type" value="EXT" required="required">
                                      Travel/Corporate Agent
                                    </label>
                                </div>
                                <div class="radio col-md-4">
                                    <label>
                                      <input type="radio" name="type" value="DEF" required="required">
                                      ATSC Agent
                                    </label>
                                </div>
                            </div>
                        </div>       
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">Agent Name</label>

                            <div class="col-md-8">
                                <input type="text" class="form-control" name="agent_name" value="{{ old('agent_name') }}" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-8">
                                <input type="password" class="form-control" name="password" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit"
                                        class="btn btn-primary"
                                        style="margin-right: 15px;">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection