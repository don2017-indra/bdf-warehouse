@extends('seller.layouts.auth-seller')
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Seller Portal Login</b></div>
                <div class="panel-body">
                    @if (Session::has('timeout'))
                        <div class="alert alert-danger">
                            <p>{{ Session::get('timeout') }}</p>
                        </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger">
                            <p>{{ Session::get('error') }}</p>
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were problems with input:
                            <br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('seller.post-login') }}">
                        <input type="hidden"
                               name="_token"
                               value="{{ csrf_token() }}">   
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email</label>

                            <div class="col-md-7">
                                <input type="text" class="form-control" name="seller_email" value="{{ old('seller_name') }}" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-7">
                                <input type="password" class="form-control" name="password" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{ route('seller-password-reset-link') }}">Forgot your password?</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit"
                                        class="btn btn-primary"
                                        style="margin-right: 15px;">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection