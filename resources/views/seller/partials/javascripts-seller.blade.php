<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<script src="{{ url('adminlte/js') }}/bootstrap.min.js"></script>
<script src="{{ url('adminlte/js') }}/select2.full.min.js"></script>
<script src="{{ url('adminlte/js') }}/main.js"></script>

<script src="{{ url('adminlte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ url('adminlte/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ url('adminlte/js/app.min.js') }}"></script>
<script src="{{ url('js/app_extra.js') }}"></script>

{{-- Fancybox --}}
<script src="{{ asset('assets/vendors/fancybox/source/jquery.fancybox.js') }}" type="text/javascript"></script>
<script>
    window._token = '{{ csrf_token() }}';
</script>
<script>
    $(function(){
        // Enables popover
        $("[data-toggle=popover]").popover();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.fancyboxprofile').fancybox();
    });
</script>

@yield('javascript')
