@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar" style="margin-left: -3px">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            <li class="{{ $request->segment(2) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/seller/home') }}">
                    <i class="fa fa-tachometer"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>
            @can('manage_accounts')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-circle-o"></i>
                    <span class="title">@lang('global.user-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                        <a href="{{ route('seller.permissions.index') }}">
                            <i class="fa fa-unlock-alt"></i>
                            <span class="title">
                                @lang('global.permissions.title')
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('seller.roles.index') }}">
                            <i class="fa fa-user-md"></i>
                            <span class="title">
                                @lang('global.roles.title')
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                        <a href="{{ route('seller.users.index') }}">
                            <i class="fa fa-users"></i>
                            <span class="title">
                                @lang('global.users.title')
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            @endcan
            @can('manage_reports')
			<li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span class="title">Reports</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ $request->segment(1) == 'seller' && $request->segment(2) == 'sales' ? 'active active-sub' : '' }}">
                        <a href="{{ route('seller.orders-index') }}">
                            <i class="fa  fa-list-alt"></i>
                            <span class="title">
                                Orders
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            @endcan
        </ul>
    </section>
</aside>
