<!DOCTYPE html>
<html lang="en">
<head>
    @include('seller.partials.head-seller')
</head>
<body class="page-header-fixed">
    <div style="margin-top: 10%;"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">  
                @yield('content')
            </div>
        </div>
    </div>
    <div class="scroll-to-top" style="display: none;">
        <i class="fa fa-arrow-up"></i>
    </div>
    @include('partials.javascripts')
</body>
</html>