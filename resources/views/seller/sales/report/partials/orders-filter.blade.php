<form method="get" action="" class="form-horizontal">
    <input type="hidden" name="search-filter" value="1">
	<div class="col-sm-12">
    	<div class="row">
            <div class="col-sm-5">
                <div class="form-group {{ $errors->first('sales_date', 'has--error') }}">
                    <label class="col-sm-3 control-label" for="sales_date">Sales Date</label>
                    <div class="col-sm-9">
                        <input id="sales_date" autocomplete="off" name="sales_date" type="text" placeholder="" class="form-control select_date" value="{{ old('sales_date',$request->has('sales_date') != null ? $request->input('sales_date') : '') }}">
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group {{ $errors->first('increment_id', 'has--error') }}">
                    <label class="col-sm-3 control-label" for="order_id">Order No.</label>
                    <div class="col-sm-9">
                        <input id="increment_id" name="increment_id" type="text" placeholder="" class="form-control" value="{{ old('increment_id',$request->has('increment_id') != null ? $request->input('increment_id') : '') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group {{ $errors->first('colletion_date', 'has--error') }}">
                    <label class="col-sm-3 control-label" for="collection_date">Collection Date</label>
                    <div class="col-sm-9">
                        <input id="colletion_date" autocomplete="off" name="collection_date" type="text" placeholder="" class="form-control select_date" value="{{ old('collection_date',$request->has('collection_date') != null ? $request->input('collection_date') : '') }}">
                    </div>
                </div>
            </div>
            @if(Session::get('SellerID') == 0)
            <div class="col-sm-5">
                <div class="form-group {{ $errors->first('increment_id', 'has--error') }}">
                    <label class="col-sm-3 control-label" for="order_id">Shop Name</label>
                    <div class="col-sm-9">
                        <input id="shop_name" name="shop_name" type="text" placeholder="" class="form-control" value="{{ old('shop_name',$request->has('shop_name') != null ? $request->input('shop_name') : '') }}">
                    </div>
                </div>
            </div>
            @else
            <div class="col-sm-5">
                <div class="form-group {{ $errors->first('updated_at', 'has--error') }}">
                    <label class="col-sm-3 control-label" for="updated_at">Change Date</label>
                    <div class="col-sm-9">
                        <input id="updated_at" name="updated_at" type="text" placeholder="" class="form-control select_date" value="{{ old('updated_at',$request->has('updated_at') != null ? $request->input('updated_at') : '') }}">
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="row">
            @if(Session::get('SellerID') == 0)
            <div class="col-sm-5">
                <div class="form-group {{ $errors->first('order_status', 'has--error') }}">
                    <label class="col-sm-3 control-label" for="order_status">Status</label>
                    <div class="col-sm-9">
                        {!! Form::select('order_status[]', $order_status, old('order_status') ? old('order_status') : '', ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
                    </div>
                </div>
            </div>
            @else
            <div class="col-sm-5">
                <div class="form-group {{ $errors->first('seller_order_status', 'has--error') }}">
                    <label class="col-sm-3 control-label" for="seller_order_status">Status</label>
                    <div class="col-sm-9">
                        {!! Form::select('seller_order_status[]', $order_status, old('order_status') ? old('order_status') : '', ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
                    </div>
                </div>
            </div>
            @endif

            <div class="col-sm-5">
                <div class="form-group {{ $errors->first('store_location', 'has--error') }}">
                    <label class="col-sm-3 control-label" for="store_location">Store</label>
                    <div class="col-sm-9">
                        {!! Form::select('store_location[]', $store_location, old('store_location') ? old('store_location') : '', ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
                    </div>
                </div>
            </div>
        </div>
        @if(Session::get('SellerID') == 0)
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group {{ $errors->first('updated_at', 'has--error') }}">
                    <label class="col-sm-3 control-label" for="updated_at">Change Date</label>
                    <div class="col-sm-9">
                        <input id="updated_at" name="updated_at" type="text" placeholder="" class="form-control select_date" value="{{ old('updated_at',$request->has('updated_at') != null ? $request->input('updated_at') : '') }}">
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-md-3">
                <br>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary product_submit">Search</button>
                                &nbsp;
                    <a type="reset" class="btn btn-default" value="Reset" onclick="reset();" href="{{ route('seller.orders-index') }}">Reset</a>
                    &nbsp;
                    <a class="btn btn-success" href="{{ route('seller.export',array_merge(request()->all())) }}">Export</a>
                </div>
            </div>
        </div>
       </div>
    </form>

<script type="text/javascript">
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
