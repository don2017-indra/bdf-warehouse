@inject('request', 'Illuminate\Http\Request')
@inject('StatusPresenter', 'App\Presenters\StatusPresenter')
@extends('seller.layouts.app-seller')


{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    {{--<link href="{{ asset('css/tables.css') }}" rel="stylesheet" type="text/css" />--}}
    <link href="{{ asset('assets/vendors/sweetalert/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
@stop
    <!-- Main content -->
    {{-- Page content --}}
@section('content')
 <section class="content-header">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="sitemap" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Orders
                    </h4>
                </div>
                <br />
                <div class="panel-body">
                    <div class="row">
                    @include('seller.sales.report.partials.orders-filter')
                    </div>
                    <div class="col-md-12"><hr></div>
                    <div class="row">
                        <div class="col-md-12">
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-sm" id="tbl_orders2">
                            <thead>
                            <tr>
                                <th width="10%" data-sortby="order_no">Order No</th>
                                <th width="10%" data-sortby="sales_date">Sales Date</th>
                                <th width="18%" data-sortby="collection_date">Collection/Flight Date</th>
                                <th>Collection Time</th>
                                <th width="10%" data-sortby="updated_at">Change Date</th>
                                <th width="10%" data-sortby="recipient">Recipient</th>
                                @if(Auth::user()->hasRole('administrator'))
                                <th data-sortby="shop">Shop Name</th>
                                @endif
                                <th width="6%" data-sortby="order_status">Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($order_result)
                                    @if(empty($request->input('groupby')))
                                        @foreach($order_result->rows as $key => $order)
                                            @php

                                            $recipient_name =$order->first_name.' '. $order->last_name;
                                            $sales_date = date_format(date_create($order->sales_date),'Y-m-d');


                                            $status =  empty($order->seller_order_status) ?  'paid' : $order->seller_order_status;
                                            /* Use seller_order_status as default value for both admin and merchant account */
                                            if(Auth::user()->hasRole('administrator') && !is_null($order->flight_no) && !is_null($order->carrier_code)){
                                                $status =  empty($order->order_status) ?  'paid' : $order->order_status;
                                            } else {  
                                                $status =  empty($order->seller_order_status) ?  'paid' : $order->seller_order_status;
                                             }

                                            $collection_date = '';
                                            $collection_time = '';
                                            if (!is_null($order->pickup_datetime_from) && $order->pickup_datetime_from != '0000-00-00 00:00:00') {
                                                $collection_date = date_format(date_create($order->pickup_datetime_from),'Y-m-d').' - '.date_format(date_create($order->pickup_datetime_to),'Y-m-d');
                                                $collection_time = date_format(date_create($order->pickup_datetime_from),'h:i a');
                                            }
                                            $collection_time = date_format(date_create($order->pickup_datetime_from),'h:ia');

                                            $flight_date = '';
                                            if (!is_null($order->flight_date) && $order->flight_date != '0000-00-00 00:00:00') {
                                                $flight_date = $order->carrier_code ? date_format(date_create($order->flight_date),'Y-m-d') : null;
                                            }

                                            $collection_flight_date = $order->carrier_code ? $flight_date : $collection_date;

                                            $raw_updated_at = date_format(date_create(date('Y-m-d H:i:s', strtotime('+8 hours', strtotime($order->updated_at)))),"Y/m/d");

                                            @endphp
                                            <tr>
                                                <td>{{ $order->increment_id }}</td>       
                                                <td>{{ $sales_date }}</td>
                                                <td>{{ $collection_flight_date }}</td>
                                                <td>{{ $collection_time }}</td>
                                                <td>{{ $raw_updated_at  }}</td>
                                                <td>{{ $recipient_name }}</td>
                                                @if(Auth::user()->hasRole('administrator'))
                                                <td> {{ $order->shop_title ?? null }} </td>
                                                @endif
                                                <td>{{ ucfirst(str_replace('_', ' ', $status)) }}</td>
                                                <td>
                                                    <div class="text-right">
                                                        <a class="btn btn-warning" href="{{ route('seller.order-detail',[$order->order_id,$order->seller_id]) }}" data-toggle="tooltip" data-placement="top" data-title="View Order"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                @endif
                            @endif           
                            </tbody>
                        </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                @if($order_result)
                                <?php
                                $list_class = 'pagination pagination';
                                $this->_total = $order_result->total;
                                $this->_limit = $order_result->limit;
                                $this->_page = $request->input('page') ? $request->input('page') : 1;
                                $links = $order_result->limit;
                                $this->_row_start = ( ( $this->_page - 1 ) * $this->_limit );
                                //return empty result string, no links necessary
                                if ( $order_result->limit == 'all' ) {
                                    return '';
                                }

                                //get the last page number
                                $last = ceil( $this->_total / $this->_limit );
                                
                                //calculate start of range for link printing
                                $start = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
                                
                                //calculate end of range for link printing
                                $end = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;
                                
                                //ul boot strap class - "pagination pagination-sm"
                                $html = '<ul class="' . $list_class . '">';

                                $class = ( $this->_page == 1 ) ? "disabled" : ""; //disable previous page link <<<
                                
                                //$this->_page - 1 = previous page (<<< link )
                                if($this->_page == 1){
                                    $html .= '<a href=""><li class="' . $class . '">&laquo;</a></li>';
                                } else {
                                    $request->request->add(['page' => ($this->_page - 1)]);
                                    $html .= '<li class="' . $class . '"><a href="' . route('seller.orders-index',array_merge(request()->all())) . '">&laquo;</a></li>';
                                }

                                if ( $start > 1 ) { //print ... before (previous <<< link)
                                    $request->request->add(['page' => 1]);
                                    $html .= '<li><a href="'.route('seller.orders-index',array_merge(request()->all())).'">1</a></li>'; //print first page link
                                    $html .= '<li class="disabled"><span>...</span></li>'; //print 3 dots if not on first page
                                }

                                //print all the numbered page links
                                for ( $i = $start ; $i <= $end; $i++ ) {
                                    $request->request->add(['page' => $i]);
                                    $class = ( $this->_page == $i ) ? "active" : ""; //highlight current page
                                    $html .= '<li class="' . $class . '"><a href="' . route('seller.orders-index',array_merge(request()->all())) . '">' . $i . '</a></li>';
                                }

                                if ( $end < $last ) { //print ... before next page (>>> link)
                                    $request->request->add(['page' => $last]);
                                    $html .= '<li class="disabled"><span>...</span></li>'; //print 3 dots if not on last page
                                    $html .= '<li><a href="' . route('seller.orders-index',array_merge(request()->all())) . '">' . $last . '</a></li>'; //print last page link
                                }

                                $class = ( $this->_page == $last ) ? "disabled" : ""; //disable (>>> next page link)
                                
                                //$this->_page + 1 = next page (>>> link)
                                if($this->_page == $last){
                                    $html .= '<a href=""><li class="' . $class . '">&raquo;</a></li>';
                                } else {
                                    $request->request->add(['page' => ($this->_page + 1)]);
                                    $html .= '<li class="' . $class . '"><a href="' . route('seller.orders-index',array_merge(request()->all())) . '">&raquo;</a></li>';
                                }

                                $html .= '</ul>';

                                echo $html;
                                ?>
                                @endif
                            </div>
                        </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>    <!-- row-->
    </section>
 @stop

{{-- page level scripts --}}
@section('javascript')
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>--}}
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/moment.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/airasia_orders.js') }}"></script>
    @include('layouts.flash')
    <script>
        // $('#tbl_orders').dataTable(
        //     {
        //     searching: false,
        //     paging: false,
        //     responsive: true,
        //     bInfo: false,
        //     "columnDefs": [
        //     { "orderable": false, "targets": [6] }
        //     ]
        //     });
        $('#tbl_orders2').dataTable({
            searching: false,
            paging: false,
            bInfo: false,
            "order": [[ {{ $sortingColumn['column'] }}, "{{ $sortingColumn['sort'] }}" ]],
            'columnDefs': [ {
                'targets': [3,8], /* column index */
                'orderable': false, /* true or false */
             }]
        });
    </script>
@stop
