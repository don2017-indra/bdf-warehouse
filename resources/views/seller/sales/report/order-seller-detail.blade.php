@inject('request', 'Illuminate\Http\Request')
@inject('StatusPresenter', 'App\Presenters\StatusPresenter')
@extends('seller.layouts.app-seller')


{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('adminlte/bootstrap/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/sweetalert/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript">

        $(function(){

            $('#select-all-items #chkb_selectAll').change(function() {


                if (this.checked) {

                    $('#item_table tr').each(function (index, row) {

                        var item = $(row).find('#chkb_selectItem');

                        if(!$(item).prop('disabled'))
                        {

                            $(item).prop('checked',true);

                        }

                    });

                } else {

                    $('#item_table tr').each(function (index, row) {

                        var item = $(row).find('#chkb_selectItem');

                        if(!$(item).prop('disabled'))
                        {

                            $(item).prop('checked',false);

                        }

                    });
                }


                updateIDs();
            });

            $('.form-check #chkb_selectItem').change(function() {
                if (!this.checked) {

                    $('#select-all-items #chkb_selectAll').prop('checked',false);

                }

                updateIDs();
            });


            $('#update-item-form #btn_submit').on( "click", function() {

                var status = $( "#update-item-form option:selected" ).text();

                if(getItemIDs() == null || getItemIDs()=='')
                {
                    $('#notice-modal').modal('show');
                    $('#notice-modal .modal-title').text('No Items');
                    $('#notice-modal .modal-body').text('Please select item(s) to update');

                    return;
                }

                if(status!='<Select Status>')
                {
                    $('#update-prompt-modal').modal('show');
                    $('#update-prompt-modal .modal-body').text('Are you sure you want to update the selected item(s) as "'+status+'" ?');

                } else {

                    $('#notice-modal').modal('show');
                    $('#notice-modal .modal-title').text('No Status Selected');
                    $('#notice-modal .modal-body').text('Please select a status to update');
                }

            });

            $('#update-prompt-modal #btn_ok').on( "click", function() {

                $( '[name="update-item-form"]' ).trigger( 'submit' );

            });

            $('#update-prompt-modal #btn_cancel').on( "click", function() {
                $('#update-prompt-modal').modal('hide');
            });

        });

        function updateIDs()
        {

            $('#update-item-form #itemID').val(getItemIDs());
        }


        function getItemIDs()
        {

            var ids = '';
            var rowCount = $('#item_table tbody tr').length;
            $('#item_table > tbody  > tr').each(function(index,row) {

                if($(row).find('#chkb_selectItem').prop('checked')){

                    if(index == (rowCount-1))
                    {
                        ids += $(row).data('itemid');

                    } else {

                        ids += $(row).data('itemid') + ',';
                    }
                }

            });
            return ids;
        }

    </script>
@stop
{{-- Page content --}}
@section('content')
    <section class="content">
        <h4 class="panel-title float-right">
            <span class="glyphicon glyphicon-menu-left"></span>
            <a href='{{ URL::route('seller.orders-index') }}'>Back</a>
        </h4>
        <div class="row">
            @if($order_result)
                <div class="panel panel-primary ">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title pull-left">
                            <i class="livicon" data-name="sitemap" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            <span>Order no : {{ $order_result->increment_id }}</span>
                        </h4>
                    </div>
                    <br />
                    <div class="panel-body col-sm-20">
                        <table class="table table-bordered " id="table" style="margin-bottom: 0;">
                            <tbody>
                            <tr>
                                <th>Order No</th>
                                <td>{{ $order_result->increment_id }}</td>
                            </tr>
                            <tr>
                                <th>Sales Date</th>
                                <td>{{ date_format(date_create($order_result->sales_date),'m/d/Y') }}</td>
                            </tr>
                            <tr>
                                <th>Recipient Name</th>
                                <td>{{ $order_result->first_name }} {{ $order_result->last_name }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{ ucfirst(str_replace('_', ' ', $real_status)) }}</td>
                            </tr>
                            <tr>
                                <th>Order Currency</th>
                                <td>{{ $order_result->order_currency_code }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="item_table">
                                    <thead>
                                    <tr class="filters">
                                        <th>
                                            <div class="form-check" id="select-all-items" data-toggle="tooltip" data-placement="top" data-title="Select All">
                                                <input class="form-check-input" type="checkbox" value="" id="chkb_selectAll" {{ $real_status == 'complete' ||$real_status == 'canceled' ? 'disabled' : '' }} >
                                                <label class="form-check-label" for="defaultCheck1"></label>
                                            </div>
                                        </th>
                                        <th>Sku</th>
                                        <th>Brand Name</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($order_items as $key => $item)
                                        @php
                                            $qty = number_format($item->qty_ordered,0);
                                            $total_price = ($item->price * $qty) - $item->discount_amount;
                                            $item_status = empty($item->item_status) && $order_result->order_status!='pending' ? 'paid' : $item->item_status;

                                            if ($item->store_location == 'INFLIGHT') {
                                                $item_status = $StatusPresenter->getStatusDesc($order_result->seller_order_status);
                                            }
                                        @endphp
                                        <tr data-itemid="{{ $item->item_id }}">
                                            <td>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value="" id="chkb_selectItem" {{ strtolower($item->item_status) =='complete' || strtolower($item->item_status) =='canceled' ? 'disabled' : '' }}>
                                                    <label class="form-check-label" for="defaultCheck1"></label>
                                                </div>
                                            </td>
                                            <td>{{ $item->sku }}</td>
                                            <td>{{ $item->manufacturer }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ number_format($item->price,2) }}</td>
                                            <td>{{ number_format($qty,0) }}</td>
                                            <td>{{ ucfirst($item_status) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-13">
                            <div class="row">
                                @if(!is_null($order_result->flight_no))
                                    <form method="post" action="{{ route('seller.update-order-status',array_merge(request()->all(),[$order_result->order_id, $order_result->seller_id,0])) }}" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <label class="col-sm-4 control-label">Update status:</label>
                                        <div class="col-sm-3">
                                            {!! Form::select('update_status', $update_seller_status,null, ['class' => 'form-control', 'width' => '100%' ]) !!}
                                            <input type="submit" value="Submit" class="btn btn-primary" style="margin-top:5px;float:right">
                                        </div>
                                    </form>
                                @elseif($real_status != 'complete' && $real_status != 'canceled' && is_null($order_result->flight_no))
                                    <form method="post" action="{{ route('seller.update-item-status',array_merge(request()->all())) }}" class="form-horizontal" id="update-item-form" name="update-item-form">
                                        {{ csrf_field() }}
                                        <label class="col-sm-4 control-label">Update item(s) status:</label>
                                        <div class="col-sm-3">
                                            {!! Form::select('update_status', $order_status,$order_result->order_status, ['class' => 'form-control', 'width' => '100%' ]) !!}
                                            <input type="hidden" value="" name="itemID" id="itemID">
                                            <input type="hidden" value="{{ $order_id }}" name="orderID">
                                            <input type="hidden" value="{{ $seller_id }}" name="sellerID">
                                            <input type="hidden" value="{{ $order_result->marketplace_order_id }}" name="sales_market_id">
                                            <input type="button" value="Submit" class="btn btn-primary" id="btn_submit" style="margin-top:5px;float:right">
                                        </div>
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>

        <!-- Confirm Modal -->
        <div class="modal fade" id="update-prompt-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="update-prompt-modal-label">Update Items</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">Are you sure you want to updated the selected items</div>
                    <div class="modal-footer">
                        <button type="button" id="btn_ok" class="btn btn-secondary" data-dismiss="modal">OK</button>
                        <button type="button" id="btn_cancel" class="btn btn-primary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Notice Modal-->
        <div class="modal fade" id="notice-modal">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </section>
@stop
{{-- page level scripts --}}
@section('javascript')
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>--}}
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/moment.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script>
        $(function() {

            $('.select_date').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('.select_date').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('.select_date').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

        });


    </script>
    @include('layouts.flash')
@stop