@extends('seller.layouts.app-seller')

@section('content')
    <div class="row center-block">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading"><label style="font: bold 15pt Calibri ">Welcome to Seller Portal</label></div>
                <div class="panel-body">
                    Hello {{ Auth::user()->name }}!
                </div>
            </div>
        </div>
    </div>
@endsection