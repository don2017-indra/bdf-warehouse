@extends('layouts.auth')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
             <img src="{{ url('images/ourshop_logo.png') }}" style="display: block;margin:0 auto;width: 300px;height:80px">
            <div class="panel login-box">
                <div class="panel-heading-login" style="text-align:center">
                    <label class="panel-title-login"><span class="welcome"></span></label>
                    </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('login') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2" style="margin-top:10px;margin-bottom:3px">
                                <input type="text" placeholder="Email" class=" form-control  @if($errors->has('email'))is-invalid @endif" name="email" value="{{ old('email') }}">
                                <span class="login-error-label" style="color:white">
                                    @if($errors->has('email') && !empty(old('email')))
                                        Email or password is incorrect
                                    @elseif($errors->has('email'))
                                        Email is required
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2" style="margin-bottom:10px">
                                <input type="password" placeholder="Password" class=" form-control @if($errors->has('password'))is-invalid @endif" name="password">
                            @if($errors->has('password'))
                            <span class="login-error-label" style="color:white">
                                Password is required 
                            @endif
                            </div>
                        </div>
                        <div class="form-group login-options">
                            <div class="col-md-10 col-md-offset-3">
                                <label class="remember-me">
                                    <input type="checkbox" name="remember" class="remember-checkbox">&nbsp; Remember me
                                </label>
                                &nbsp;•&nbsp;
                                <a href="{{ route('auth.password.reset') }}">Forgot your password?</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 col-md-offset-7">
                                <button type="submit" class="btn btn-primary login-button">
                                    LOGIN
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection