@extends('layouts.auth')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="padding:20px;margin-top: -100px">
                <div class="panel-heading"><h3>Welcome to Warehouse Portal</h3> <br>Your account has been created!</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were problems with input:
                            <br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal"
                          role="form"
                          method="POST"
                          action="{{ url('directlogin/account/new') }}">
                        <input type="hidden"
                               name="_token"
                               value="{{ csrf_token() }}">
                        <input type="hidden" name="token" value="{{ $token }}">
                        <input type="hidden" name="user_id" value="{{ $user_id }}">
                        <input type="hidden" name="email" value="{{ $email }}">
                        <input type="hidden" name="firstname" value="{{ $firstname }}">
                        <input type="hidden" name="lastname" value="{{ $lastname }}">
                        <input type="hidden" name="luma_password" value="{{ $luma_password }}">
                        <input type="hidden" name="role" value="{{ $role }}">

                        <div class="form-group" style="display: none">
                            <div class="col-md-10">
                                <label><input type="radio" value="luma_password" name="password_usage_option" checked="true"> Use my luma account password</label>
                            </div>
                        </div>

                        <div class="form-group" style="display:none">
                            <div class="col-md-6">
                                <label><input type="radio" value="new_password" name="password_usage_option"> Use a new password</label>    
                            </div>   
                        </div>

                        <div class="new-password-field-block" style="display: none">
                            <div class="form-group">
                                <label class="col-md-4 control-label">New Password</label>

                                <div class="col-md-6">
                                    <input type="password"
                                           class="form-control"
                                           name="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Confirm password</label>

                                <div class="col-md-6">
                                    <input type="password"
                                           class="form-control"
                                           name="password_confirmation">
                                </div>
                            </div>
                        </div>
                         

                        <div class="form-group">
                                <button type="submit"
                                        class="btn btn-primary"
                                        style="float: right;margin-right: 100px">
                                    Continue
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
      crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(function(){
            $('input:radio[name="password_usage_option"]').change(
                function(){
                    if (this.value == 'luma_password') {
                        $('.new-password-field-block').hide();
                    } else if(this.value == 'new_password'){
                        $('.new-password-field-block').show();
                    }
                });
            });
    </script>
@endsection
