@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading"><label class="panel-title"><span class="welcome-home"></span></label></div>
                <div class="panel-body">
                    Hello {{ Auth::user()->name }}!
                    <hr>
                    <div style="margin-top:10px">
                        <ul style="list-style-type: none;"><strong style="font-size:110%">QUICK LINKS:</strong>
                        <li style="margin-top:10px"><a href="{{url('wreport2/warehouse2')}}"><i class="fa fa-file-excel-o" aria-hidden="true"></i> New warehouse reports</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
