
            <html>
            <head> 
            <style>
                html{
                    font-family: 'Montserrat', sans-serif; 
                    font-size: 13px;
                    font-weight: 400;
                    color: #000;
                    height: 100%;
                    width: 100%!important;
                    margin: 0;
                    padding: 0;
                }
				.left {float:left;padding-right:35px}
				.bold {font-weight:500;}
				.clearfix {clear:both}
				.bg-grey-light {background-color:#ebebeb}
				.bg-grey-grey {background-color:#d7d7d7}
				
                .page-header{
                    padding: 10px 0;
                    margin: 0;
                    color: #000;
                    text-transform: uppercase;
                    border-bottom: 1px solid #cbd2b0;
                }
				.logo {float:left;padding-right:30px}
				.address {float:left;}
				.address p {margin:0;}
				.address p.company-name {font-size:11px solid;font-weight: 500;}
				.address p.details {font-size:11px solid;font-weight: 400;}
				
				.invoice_number {font-size:18px;}
				.order_number {font-size:18px;margin-top:10px}
				.passenger_name {margin-top:5px;font-weight:500px;font-size:18px}
				
                .invoice-block{
                    padding: 15px;
                }

                .well{
                    margin-top:15px;
                }
                                
                .table-relative{
                    margin: 10px -15px 0px;
                    border-top: 1px solid #cbd2b0;
                    border-bottom: 1px solid #cbd2b0;
                    padding: 20px 15px 0px; 
                }

                .pre{
                    font-family: 'Montserrat', sans-serif; 
                    line-height: 1em;
                    color: inherit;
                }

                .table{
                    border-collapse: collapse;
                    border-spacing: 0;
                    background: #fff;
                    margin: 10px 0;
                }

                tr:nth-child(odd) {}

                .table > thead > tr > th {
                    color: #000;
                    font-size: 12px;
                    letter-spacing: 1px;
                    text-transform: uppercase;
                    font-weight: 500;
                    padding: 12px 5px;
                    border: none;
                    text-align: left;
                }

                .table > tbody > tr > td{
                    padding:10px 0 10px 5px;
                    font-size:11px;
                    
                }
                tr.product-list{
                    border-bottom: 1px solid #cbd2b0;
                }				

				.special_request {border:1px solid #cbd2b0; padding:10px;margin-top:10px;}
				.invoice-footer	{border-top: 1px solid #cbd2b0;padding:10px 0 ;}		
                @media screen{

                }
                .gst-summary{
                    font-size : 13px;
                }
            </style>
            </head>
            <body>
                <div class="inner-spacer print_area">
                    <div class="invoice-block" style="border:1px dashed black;">
                        <div class="page-header">
                            <div class="logo">
                                @if($carrier_code == 'D7')
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAABuCAYAAADrhcsEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAGYktHRAD/AP8A/6C9p5MAADBuSURBVHhe7V0HgBNV/v4yk2Q3m+2N3osURVHPLnqKWBBPxXJ6Knp2T8R259n1REVs2MVe/9i7ImDvCoIU6X1p20uSTZ3J//u9mWyBBXZxF8Idn4ZsJi8z897ve7/2yjjiBHbhfx6a/b4L/+Noe40gp2/4SsBh/6ORiw71YRd2IFqXCHIq01TvidOaRathLF+K2KoVQHUlTL9fCV/LzITWsQucg/aC3qOXxQXduYsUOwh/jAgNBC+IFa1C7NdfEPvtV8TKA+oYKitglpUC/nIgGkQ8GiMRHHA4XYA3G452XeDIz4cz24PUCy6Ds2t3iwy7NMV2RcuJ0EDw0d/nIDbnNxjLVkOOmOvXwVwyB8bqFTAjlhB5AZY3obGzQ7cFLDBMxA1qDlNDXDSEHoPrqBOhdegEZ/9ecO9/CPTOXayyu0jR5mg+EYQARHTGL4jOmq0Er3r/3F9grFiHuK5DYxmHKw5HmgcOr9cSniLCFi5h9/64zwezOgSTH129u8J10FBoNBlaRircQ/4MvWPnehLtQqtj60SgcOO1tYh89xXiYQPht15FePJUmCJ4w6CgnHBkZsHhdtdri2ZyaxMIIeS/8nKYAROmpsOZ5UbqqEvg3H0QUkecsHORwW4LaWKHEL6hVksyDbdlIvCr6PSfEVu7HoGrLkLMF4WuxeDoUAjNnUrB097b5VoViUYj2cxKOphlAXIsjrwp0+A64AC7UBLDiCEeDMIsKYZZSlKzwzhIYGf3rjxWQie5E7S8fKvsJoQQ8th/bgmtTKTNEsGsKEe8xofyfQfCDDvgbJ8BLSeHlTTqzMR2AzVDPBBAdFUJ2gfD9sHkhTjHkZ9+QPizTxB+9W2ApDBIhIx/XYXwu68h9dxLqd1OUmW1wkKa0jT1t5AnHvAjHgrS7m6mjcVdSkmFw+MB0rx0usX5+uNokgjxcAj+G/8J3+PPwt0lh+o/y+r9rd3zWwL2AGPZUuQX18JBTQEXo45kg+ogcURmTEfZUUcj+85bYCxcgMi0T5X5jC1cAi3XC2NDLQzWwcGyuf/3MtzHHK9+Hvl8CkLvv4Xo15/DKKoE9Yg6noCU1zOpWfbdH6mHD4VrxMlwdpMoi/8LIST83kZsQoR4OIzy3bvBWFMB16A+9A6jO5YADRBnRaMLVsFzzunInPAke0aK/U1yQMyouX4tHAMHwZlfgNjiBfBfOxrxylKSl8R1sDtLW9pmLx4zEGN9HB4NsQgPMZLSMuhwZ2QDGdTAqTS/iabnb0Q28VCAmrqGrwijLp2axgF3x0ykXXkdPAy/VVHpJC30pRoTIRZD+UGDEF+7hrE9VZZu33iygI1hisosLYHhyUfB3DnQqCbRSuqxxaAGMGNRhsZseJpMMxRCyYD+lLGTjq7GaDkC5wH7ITZ7JrSNNZiQgW1rkgyggFWURSI4HKyfEEa+l1dDKFnwxXdG5AS/d5JQNZWMungv3ky4Mp3wjnsA7iOGKb+kuW3TiAj+u29HcMI90Nu1t06QTCSoA3tGNEIHjI2W3RGpp5wA7+hrLXMhr+2I6JzZSoDGqlUIT3oeZnkZ3MNPoXp/E8aShUqVO1JclOdWeqeYFLl31dzNbPMESeSdZIxHGd3JOWp9gDsX8ZxceEYeb7VNMwhRR4R4bQBlewzkjTMkTKUjkpQksGH3JqM2Qo3A8LX3nvCceBw8Z567/bSDaANGNKE3XkHtS5OAiiJ1TO+1G4+XwRGhGhcB89VaDt0WYWuQOE15PMSohTTUUq22cQ/uD++/b7NMxmZQR9XgS8/wH1/yk0Bg35+eTrNQS8dr9nTUPvwY/HTOxLy1CUT1i78k4PWjv82EgzY8NmcWzKXzVa+Tz+bShbwnK70uuYPtQgKBtImQjlpBY7uotgkGYMyZjvBbk1B9xRUkaIVdeFPUESEy9VNo2Qxjkp0EDSEVz8yAnu1FfMM6hF59nmS40wpxWxGS9Qx//B7iMmZCGCtXIDZrOiLffoXY9Blw7b2nimJE6A46eXXCl166IyAypBPhyMhUbSPha/iTdxEYf7ddYFPUESFeVdliTzMpoHoCzVm7bDhIjNrHJiDwzDMwpT6tROo4z29WVYkdVZ/l7+iMnxH5/mvE/X44srItM5BskPuVZFZ6BpwFWQg+PgFmTbX9ZWPUSd6s9e84BrcGlHbIgl6YDd/NNyD4+uuI/T5HpGgXaBkS9l0aU2PPShn+FzqndMIYuhkrlzHs+x3mkkXQunSyHMPtZQK2BayDw2A93Aw3GdI2hToiODw0C2br9KAdBqmw2wVX1wL4rr8OgXvHITpzhjreUsTZk6K//KIigdjSRconiPz4HcKfToZ/7E20w+mIfvslzLVFiNMvUWMJSQ22gSS15s+1PzdGHRG0zp2tHrCzQ7HfgLt3J4TfeQ/VJx2twru6qKyZpJC4v+KoodQsk+C/9nJEPv0IvkvORvX55yuHNLZwAc1Re5XU0pJZGzSEScLKGFETqCOCc8Ae7AVt5HHvCNDDd/btxorpKB7QH2aAnjwFaDYVVTQkh/wt0QE7hWu3zvBffa3KGIaefZKtpcPVr6sqo9EpbC6pkgb+AJx7DrY/NEYdEVLPPh/muuKd209w6KwRe6e8C8SJzMpFym5dUdyuAP777lYOZR1IfDMchrF6lUrfimMVpe2vvmQUTcAnyPniF2jt06F36Ai9e3dokv2TTOJ2Tly1BoSzRsCAs99A+0hj1CeU2AjlvQrpENH73lomLNlA8sb5ii0qUnMYZJ6Eqz+1QQMNF2edzFWlcPQoRN4Pc1RypfaZx1Fz9/3QWT5G4WbdcqPqB77RVwLedGgeDx1Qjcd24s5hw2AUlTrqYmTcxvC6CTRKMQdffQH+yy+F3qPLzqP2hARU5ZG1Fci85Va4DjgIkamTEbyfAu7Vsb4eDI3jwQjMoA/o0BfZT02kek9HxfFHwiH5fskBhFnWSaFLvl/+57ue7lVaQ9sZQ2sbcY2dZPEa5C9eqrRbU2hEBJmDUD64H7TM9OQOhxIQEtTWIlbmR9azL8K9/37QCtvBWFOEqtNOBAKVrEeDtKpNGjMcg965D+P/LIaC8yz7KETx1VrF0iW7KhwiG0QbsIl2Wq1ADRlbtR4Z902AZ9S5Vn2aQCOaazm5SL/7PsSKq9SE0qSGIkGA5PUh4857kDpihPLiRaB6l65Iv/k2xDdOqYpASXCdgjZXLkJs9vS6JJHKQ/C4w5tIsbOsHN+ZScD7lhlRrv0PQOopp2yWBILG0mZBz+lnIe3SK2CUVpMMyesUiXNnVvmQNvpqpF1wgSJAXUX5rvfoSb9nM+Ewhatle1X6tZGQbQLUgd/t1CTw0wymuJF25VUqu7glbNrt6TSl33gjXIccQQfDWoySdKBzZ5bXIOXUv8F7/Y32EG4DAfLv4PNPqVz7ZiHltzHrmPQgCeKhkDKbnvMvQcqRw+wvNo9GPkJDSEhVdeGF9LQXQU9LadzQOxgGSeDa5yBkv/GGGuyJB2vVCiq9oFB9b5ZsQPmgnjQVHeGgo1QHIbWqB4/9N5NAhsCrquA+ejgyJ77UrKH5zXZ3sbNZEyfCkdsJRiiSHEQQdRcIQuvcE1mvv27NW+R9hb/6koRdYRcCQm+RINQSaoaVAhtHtAidRDMkzqI9nPzfBiFBNKJI4NxjL2Q++WKzSCDYrEZQ4FcyBl/arx90J0MoT+qmanh7QUjAMM/0x1GwcKG1joKIrVyJyqFDUbB4serxog5LC3Og9+7Cn9jagLdryNI7Pcv6TOiZDmg7W75kSxAS0Ccyi9dD77Mbcj7/qa6NmoMttwRPrvFkhQsWMCSrVB6ohCM7AuK4RpesQwHvJVFBcRg39O+P3K+/stQ+EZ7yEX0DmR6WMAkkUFUF9J69VD0SL0cV65MMWq61oDsRW7wKev+BJMGPLSKBoFldQgZWCisq4OjeE9G5yxmCtewifxgye3nuMuT/+EP9zOVYFGV79ECqpIA7drKOETVXXQbN9hUEZiyiFpTkTPlW/Va9xGS066DGHv4bENddiLB90s4726qnu+Wzu5utG2UaVvbbnyB93FgEF622xuvrel0bgj3dWLQc3uuugXOgnSeno+e75TrEVxTDe+PtdfdhBoNAmSSRGthF6fSqYViGZi4eiajfgg6lmgEtpi5hIuRd/A55JWO01ATirGuE8siY8AC8d4zfJhIItuwjbAzxGcpKEVu2HBXHDoMz220t3aIj1iaggM3qKmj5HZD17id1PT8ycwaqjh+OeHUZClYX1y0fCz43EYGxN0PLzuEnVou/V2khGU30FsI1qC97zhKgeJkStCylkzEIzWFA69UFxuIitaZTIEv79K68XhIPzat1HsvXIevZ55EyZAjbIY9Ht61ztowIAinOV+jDD+G//nLES3zQe7SNmpUUr7FiA7I//gTug4cowYpQy4ceiXjRMui9eiBn6rfWcd5TxeF/grmUTmOcBJJl+XJcBM57cwRq4ezZgcdlgidPbsbgueRaOpV9EPlyKgKTXkP6WWfBNeQIde3wu5MQ+5Fq1l6OlmyQzG9sTRk19L1IO/W0+tXn24iWEyEB9pTQm/+H0NvvIPL9d9Db5zQe4v2DkDn6sTUlyLj9bqRdeKGlqinQ6tGjEX7tBapAD7z3jkfaOReo8jKTqKx3dzh37wtHXgG0Dt2UryC7siilQGEbM75T09mMNaVIf+B+uI86TuUewp9PZc9ahdTDh8BJj1vqJnMSq04axno1PUizIyECk2GA9Jtuhefsc9RUuj+KbSeCgCZBpm+F3v8QwTdehTMn/Y+Q0oLdi43iSrhPPxtZ99xj2XHCN3YsasfeAddefYCs9sh64UXoHSxzIZNvAw89TK+5txoz0ShAMRlaYXt1ysgP36LmryfAkV+AuD+C/MUrLXLxS2P9eqVWZUq6Gs6eOxuhd99H+OXHoWVlq/MnBXhvcTrJRmWtWt6Wfv2/t9kn2Bh/jAgC9h6ZMVxz402ITn4PehZVaUtzDSIpqWQoiHiQDp3mVBNKsqd+Dt22/+KcVo8ZA51aQd9rAO13D6QcM9z6rYCklKSRfJb1h6avBq4Be9R9H576CXx/P5OCdsE75kqkXXujOq7A34U/fJe9rFx9jMmMpA/fh7Nj7jZa3DaAtE84RJ+pFu4RpyLrkUeanSxqDv44EWzI8G7V6achNvNHaJkea/h3S6cWAfF7JXyDPVH2VTIjcGQw9OvUDWknHw/PuTQJtiiUkMX08DcqRyBkkx5tI/Tee1Zj8fvogrlI2XtvpBxNohCSZKo8/ij6G8vpFA5A3qdT6hpRGjfMz4Gx1yO6io6nHOR19M7UEHINueaOBh1aqUM8GIBz78PqUuutiVYjgkBCs/LDD0N87UJo6VmM2d1KcJtA9V5+JQ6mGabd7gSToZyLzlzqiJPhYk/XZbTMNgnqHHyJH2BW8FVZA3eDDTNklK2kXQEgq4dlylnP3mj/22ybKHRsP/oQNSedBEeHDsieNg2u3fpbP+Q5Q9OmovKM0+HK1GkerPyDmrsZ5T3yb81NwrReE7UcQoJwFGZNFVx77oWsdz5tcbKoOWhVIggUGfbZDfHiMmidOliaeaNLmNXVMOFVoV3aX0+kvfuHtXEWyzokc5kgACGrjIxyqmx6+eEP3kf0sw9R+8NsdKyotEtQ9t98gZpTjmX00ketOcic9CFSjhiqvjPLSlDaqQs0rwbP9Tch/dobeBFLy0h6unTQ7nD36wqHHfVImjYeqKEiSFdE0DNIkHrFs32hNBx9ljXl8Jw8HBmPPN0mJBC0OhEEkvqtvnQUgpPeh3tgd6p86f7WZWR7vXgsgtyZi6l6nVaWj+9KOKKGN2r12omPoOKq6+AU2sgK6HRTefvZr71vlwD8d96G8IsTAQ/JVVaM/KLquvNUjToZ4Vfep5YZgtz3p9WbBBJ2Q3YW3AN4f4m5jTRn0flLoGd7kLfcWt62IScb7t26kKOt3kxbRZz3E/l9BbLuvQtpF13e6uagIdqECALxGWLz5qD04EPhLvRAy6faZYMba9dQUFX1qWJeXjxhWVwTnTWDKv9g6zghhCrOzETKnr3oLPI2SRRRken/GY/UM0fZpYDq04Yj9vs8de68X+ZB72upfoN2dUNuDjxHHYKctyc36k3lQw8E1qy2FvYIXG5EZi+F98KzkX7/o7w/a/5/dNlSVAwcYE2G3V4QTUDqRRYWIX/mr3D1YZQknaUN0WZKT2YJOwfugXbz5yvbHV0oezHaM37kJWv62StjixaietTpWDtoEMPCfexfExS6zJ909u5o+xKiVagxAnSYjjjKLmRBFpsIHDk50Huy0WyUDh6M9DEXIfvpVxqRIE6fwvhhFhxpXuuAkGD+SmTc/R94bx9XRwKBbE2TdtlFVvp6e0BIwLaJLF2LQrads3fbk0DQptZPGl/v1g1Zr32A9HvHK69cbHDFYYfBd8O1qDj8cFSfcSa99i+Qe989ajwjgdDH7yNewvg+xY4++JLRQpl5o4t2SUCOV5TT54wg7ZIxjUKq3JdeQvroa+lkio9R7/0Hn5sIrcCeuiXmYEkR0m+/FZ6zzq1LVycgkUP0uy9IlrYXhpDADIURLSpH3tTPVNttr0nEbUoEBVbOtefe8Iw8FdnPvwTnIYexB89CWARdvJKOYwn0TgXw2M6dAk2I/z9j4SikwIUENmQpm8aIoGHjmDIqSs3iCAfhHn6ifdSCe999AUYvvuuusI8QPEfs97n0J2ia6JHHVq6H5/wL4Dnjb9aopYpoqH1sSFljbZHyZ1oEOr2iyWSCrSSpmgPJlZgbqpD9wktw77efarvthbYngoAV0goK1Exj75hr4T5wf3b5IMNGOnf+IG3+XXB40+3CloMYX7vS2kyqAcSdkRRxQ8gCVVDzyHeyMFURR14UQuT3OfBdew1iP32vNgUX0tQ+9ySi03+yNrVYWwrXIYfQEbvImu9PQYQ+ohOaEADP4/vPrby3DB5qvlBE8Mb6chgl1Wp0MLahUo0NbAly/xIhpd9x1w7ZWHT7Xo2VNYo3qOSIDJKImnfuNRipJ4y0C7AInczaxx6niqbARaANIGraXLtO9Vr1nQxKrednEkHOV/vQQ/Q5FiC2eCH/Ho/a++5DZOonav+CwPjx8N99N83CE4jT4ZRGd7Rrj7QxY6zxBSL0wVvKf1B7DhGxJYsQ/ZxxuwzoNBNyx2ZpNVJO/St9izF8XYW0iy9HnCHzVqHRNBQX23VrXPe2RptFDRtDtpgJ//gLwq9OVFv3y3hArGgDsl58BSnHsQfYCL7yPHxXXQVnl8ZmIQGjuAzeW8chZdhQlY+oncge/iWF7fGQFBVIpQCkQSMfvoY4/Tu9I22+IkyF6qkalYw4ibJhhPfmO5F2/iWq9xlrV9NfOQ2ZL02Cs3sPdW3f1Vcg/P5rLRpvkKl9sngm87nnoRe258V4KvpFZR0z1DzQRmigeaw33ue6CnjvGIfU4cfRZHZime2ktLcLEahyA/fdCd8990FP16DnFdIpCrKBOyD3+x/r1KAItnzP3tCyM9lGTTdAnG1nrCpB2j/+QXu6Ti1917oUSHurENNYa40X6AVpFDgdQok2BAlVK2XWl5AwpyBjwkQWtNLIvvFjEfviW+RMmaoEZJaXoqxHZ+i9uqpzNxeyg0rqZVfSBP7TPgKEP/wAvsvpiOYXWAd4fpl5Lal1IYHseVR3nP5OtIQmYsyVigxO+ld1hGlDtDkRZFZt9NeZCD7ziLLVcYaVIhJj7VpGE++p/QATCL79OnwXncuQbUtrL2WuQZgmgk4i/TetSwceaVB2o17WCCLgmgAcBV2R9803VoKG5SI//4yyIw5D4Zy5DNf6qqKht1+Db8yFJBR7dQsg9XV07KZyHQK5m4pjjoKzh4yC8k7V1oAatAiJ4KXWo+BFdWmJJQNSho5mbNk6pA49jOHsXXD2t2dmtSEh2pwIkVm/opzhoiuNl6FDJiQQrzxe7Uf+ig11PVV6QlmvdvQNcllfu/duDqpBEgJv5qCQkKCJWdDGqpUoGTgAzvZeFCwpqWvsyhOOgrlsEcs1HObld/K1hKLSbHLvUj7RhLwXdQ2GfzGaCBG8xmhD75arNjFXz6coL4HhyoX3tL/AffpZiK9fi5pLL4cjNxWabHOcmBzMsDa2YAWc++6O7El0YJ06He521ndtgDYlgjiDJTk5cPaTqeVywLqUQe8944FHkTryDPVZEJ35C6r+fABVce/6hm1FqGld81agkOZHs7Oa0jvLcrPoNGbBc8Gl8P77VnVcBF3sccE5oGeCbhYcDAlF7rJhOUNc8TXUpBDJjJIU8uCSaJWBtFOPR+SzycrplAeRSAQjQ+uSLk49+hBkv/6RlVqXs7NMbMUKbBi0B1LSTOhd6UcktjDib2IrlqhHFGgdO9OferM+I9vKaBsisDeI07S+IB+pPdtZGz0mLsNGNktlPKCqUcasYsg+iMtGlW0xQ5qNHZmzDDnvvIGUESdbx3gfVeeciui3X8BcV43CIFW6nZ8IT/sENX8bCQcdWglrxfTESYLYvOUwKMCUvp1UFjM2ex5iIQdS+nXmj6jiO9Dn+ehLFd3ITmz+MRcj/A0/U3jhRWtRMGO6yhSKNpL0eeTrz+Eadhw0to18rr3rFoQmvdR4nSI1hFFagnhVjRrEy19QZH/Rumh9IpD9NZeMQvCrn9W+wOw69hf1MEiEnG9m0RfopioqIWNpbibDODZo4z74x0FVZJSXwTn4T1Sx71pjC6xy5JfvUTn0aDhSY9BzCpC3eI39A6CsT1fEN5TCkemC4TNVmjuydD0yx41F6nF/IYE1RRoxZ+KeVA49gD5PFU3dCujtO1gnYZ3KBzH60F2Iri5FwZx5FCQJxE4hv6vYezdEagx0WL3aKk+Ef/0FNacwWiAB67Ui24OkEjGZRUXIW15M55JEkc7VitiKMW4hSILAg/fAc/nVSDn0AFa46fy8lpuP6lNPReBxxv1Ll6D6pGFspG2fgdskxBZRE6gV0ZUhZNz7UN0AU3TRApReOpohbBpc++wP74OPqeMJZL36JrI/+xJZ709DzseTEV1ZjLyp0+A57Uyq6I4wilbBkZdPNd5NhXgS7aSeeYq1h7WA2qb6svMgG2zIEHJdujhBAvpMscWrkf3gA1Z5Qo7XXHixagFjzXref0I0JATroXZU5bUrjjkWNVdeZJGQ7d1aaD2NICS4+zaV0DH9MZhlKwFJ2iScn4agkEx/iIJJgaOwG8zlc6BtlDFsMZQTIu8UvvQgefJLgI4ba5cj071PYE+2yxirVyL82xzoqTIlLhv6wD2sXiagENUgl/zJd9/fzkTKqPPgOWGEOlY+ciRVRoma9Jr9+hvw3XY9aic8grzpM9SGZIIoe3bl8KOp7mPIeuHl+kwh/YqqU05F5NupcPbqg5zPvlU7tSozdepIRL6ZpiIm9xFHI/zBWyo30gjSbrUkAGLQBuyH1EP3V51OmbRE/bcRrUYE/9ib4Ro0GP7rr6aTSEcqlSze0sRK3rg8IUae8ubwuuvsc4shDSA9X5aBV/pgwMoLuHp2pjk4EPoee8GbmAVtQzKbcFO1kiVqtbRMq7MR/vIzhN54WwWkKvf/64/I/fk35dzJ7mrlhx8MZ4f2FHIE7uNHIvzJ+0i/4RakXXSxdS8kT8VJJyIybQoyH3q0fgY2mznw6P2ovf12mkIDWf83SW3iqa459SNUjTiRUYEXmQ8/DaSkwjf6/KYTWVJX3r8EGHqGB9r+RyL9mqsYnvayciLbiNYjwoPj6NAEEX7pcap+2jg24laRYPG23oKQqaoShuGCwwjBuee+cO5nzWdw9u2hhrWdffvVN5A9mCTOanT+74j9Nh16x65I/etZ6rig5u9nIDR5KoWRoho8ffQ/VAZSEJs3GxVHHWzZcKp5GU/Q+w1A3pffWtdgnf133gHf2LtIjhuQcdNNddc26fBV0FwaJSVIH/NPnvPmOnKWDR8G86fv4b3+BqRd8U/UXHYpot9P3WSspQ4J8jP6ikXpvB47Au699yDp/rHNZPjjRODPZTVS7fPPo/a+e+BkKLbNgm0hTJ9f5R20vYeoXfVShh+PFDqACnZj1ZGNQqp9+mmrpzN2j86Zgdh338B7yx1Iu/wqqwxR3od2PtVDjeZRu5mn3zOBYe5frS/jJnw3XoPQKy+o2dUGHcqMR55E6inW9zK1v2LokfCc8/dNZhn7xt6htEHqBRdu8l35yBPh7tNdJaGCbEf/rTdC7yAzqLfSjtIRWESm/mu5afCMJpHOPdfKkSTq3Uz8MSKwcaPsJZEff4L/lpvg6tqO3Wb77D2ghop55x4K0fP3Syw30xa8ivGVV13fGJHvv0HV8OPUJFmNPoTmNGHUBFBQyrAs8XAtvw9lvRnuStgovg3r5zruJGTeO6Gu90r9qi88m2HnV3Bkt0fez7/W9cLK005k2OxAruzdwGsImRIo3q033PQhsvldw0kygticX6lZ9kB48mRUn38unCpl3gKx8PqyX1S0JobMR5+Aa8BucO2+Z4vIsM1EEJsc/e031D52L0IffUZGd1Ih0/aCPDbHdchhyHriBZV1k3az9k2WdY1xuPfbn6Us4ckOIhV/PhDx4vXWk+rEmZTsZjCIfMb3CUR/+xXVRx+q9ppU0UZKmhrtzPn0q0aPGZREVGlWKqML2vmTTrePUsUffDDNxJdqM3AHIyP14C0b/nG3w3v19YjNnKkW8zpkyNyGsWoFtUsJKo872iJBw11emgtbO8iT8FJpajLvf0TtMNdc2DRvGWSbenmSWfmwoYhN/hjuXoydtyMJFNgLZHd09WzFsjK133Jw0isqnV077jYWqK+abKhtzPydzpedBRT/JRSE+6hj7BIWovIQjmAEKWecA9dBh9I0BBnNpJJEh1m/SSAag8HTpJxwin3AgpBAtu2pOuU4Rk+/20ctpP3jKpjr1tHHOBLhLz6HsW6N2gZQ7ts/fiwqhh1F4lAbqYzjNoAsEC0ie1BH3/4QwZfZQVqAFmsEyYAFeOO+u+6Du7+9K0nDRtpe4HXVwzQ7d4a+20AE3vwQupgEqvQCxvV16pdefDF7n3MA4/gG4xKGr0allNPsJ6MJ/P++EsF3XkfB/CJW1FRZQd8lo0iIauQtWUcnUVZZk0PvMGy84jwUrLEfhC5gM0rblO/RE2ZFqZrDkL94g/0lf/PpR6g882y4u+UhuqKMGrQDndEAIuv9cKVEoffpS4JJaPjHISu8Y7+vVOl0ZSKb4UC2jAgsWja4L4wFq+Das/f21wIbg3bbKC9VvdvZtScivy5EoTxIo0H87b/lOgRffBp6riSs6iFmJGPSe0g5mL3dRtWxQ+C59makHGlPjiXBxQyI2VE5fvksxKJm0b0G8teRCKLF+b2xbi1KB+4OVwF9kIJCtQl4ZHU5G1gEQ1nQ1Dj7k4xygD8yqyv4plsRluz82tzBs2ZC9EN48RpkP/YIPGectdUxihYRITp3NqoO/BO0nl0Z91OFtUyZtA2kAaWxIxFqhgHIfm+K/QVBosoKKL1rvqW5GkClub+eAWcva9hZUDawC+SxeXlLVyj5JmYqqU1BiPAH76D6XzdA95jQ3C5Eqw24u+cjtrJMOahamsOabCu/Ew1BBijHVSIE+iSN1H6i7eyybQG6QohvqGb9c5Ej+09v5KQ2RPOIYPeEmlGnIUbfwJHhbbOb3xYoVbikCHniiPXpZx8F4/HzEJkymXbezho2QLymGp7bxiHtb/a2tBRUaY88ONLoR+R1oUBpb4dSM9Bxi/06A9F1lWQKtY+TalyELaYpRI0YCZKH7NnscQ4XhdqQcPK3tJN6lwPbuc14XVkuFw/5Ybbrg4KvGOmIqWh4jza2TgT2qvCUjxF47kXE589Qkz5Vd0kiiPPqPvF0ZD7A+NyupOnzobx3dzjaZ7PTNeiJNtSDu/UMpDO+dw7aE8GJjyD02ssq1SwCjodr1abdStvIM5t1CpodQstIbNFLSEOLeaRfonp7EnWOOsg90kGOx/hnv73U+IbeVR4j3FiIWyaCaIErRyM2dx5ia1ZAT2WFt6BedgTk5s3Va5DzPdV84lkErJLv+qsRfuUVOAqym+St/C5eFYDWoze0Dp0Rm/2DRRhxrORFuy3TyZTpcbmtDiBNlYzCbgZktblRVYv8H2aocFrv0njlVtNE4CFpBP+99yH05ATaODZEofUUtWSD2gfh8KOR9fSLdSwXR7BiUG848ukbbCEml8ms8bIqkkKD5iHJZWY1j5klPEYy6F6XlRRSfsjOSYAERMxSg5QTz0LaRRdA7yhD/vXYVGcSsfnz4HvmaQQevh9aHlVrYY4wxv42eaAqxygh7ap/NlJ1oVcZQ8uA0lZicqmTlp8NvSBLJXgkyWRQS+j9dof72L9YgzuVfkv97+QQZ1k6cu0jD8BkhLMxmmwpGW+vffZZuHrIukM2dk2ADR60PNwkggjIOXAQXAN2t4/wGEPJwP13U4PJNPZmkNcuY/oDDOnkmUd/UlvSZD38MNyHHwmtWx/LRPwXQKIXPT8DtQ9PUJ2oYfs0KdmUYccha/SldKIGA6nZVCkyiOFWexlT19qldjwc4QhSTmqc3ZPnMcLnpzbYehKFZ1D/mhH6A3xpHXsg57XXFQHEu8586mVkPDSBGmIgzECIxTdvZnYKUPBi/sJvvq2G0WXNaAJNS5U933PexYoImS+9BO9tt8Bz6y0wqsPN6mTbC3GGceHJH8FYT1UnYwfUBqGnnoAmu6ds7UZFqAy0ZYNueeiVZ9Qo5H71teUMJzQfyeDaYxCy3/kUhizgFReJ11HYSUkh5tCRn4bYvHmI/PiDfVQd30KLMWpQHrTtJEbmzkblAQdSFdPjTAZGUBjmujVwdO6GrLcnq6ecVg0/Aa7drZVKW4TTReEuQ8yvo52kqiUq2MLkGMmoRhetgTMlCq1rF+VAq4zNTghxiCPLS9BBVonbmnPLRNgYLFr79OOoHX8H4+lNkzQ7BOy9MugUK62lMxSHayBj5C0JiOUlORZZvA6pww9H9ivvWqHhViDjCPFIFP47bkDg0aehsXOo8QulWZIvmtocZFp9dN5ytJNxiAb1bhkRqBZrRl+IyBdT1aPwkgYUhKRzWU32VDK8qSqJwKgFTHmuc9BJM/AVnF26tGioVs4rWwnGq2sQ+XwKqq66BjoMOPv3tMZdWtCUOwTsBLJhiXfcnUgffbV90EKLPL/al55B5MOPNj+FakeBFZSxD+UgNiUM0RrszZFla+E8aAhyp32mbH/DXdybBZJJFu/KjGRxUmV2sufCC9TuJnL+ZPcbZOJN2oV/R9oZ9VPzEmi2RogH/Kg64Uja1SKaheYvE9+hoGCkcsbqElYAyHjuRTg7d4Br8L6KHH8I0mx8GcuXIvL7Avj+PoqhmVd55cmqGWRD9ewp31lrKTcibfNbg2GjbDSBtCTTBk1BEcABo6RcbVKRMmIE0u+yHgnYKiQQSEPyPHqvPuq8cn6z0qdmbm1JMySmyu8QpHkReOyJJu+veS1CGxx6axKjCGthZ1KD5kGeaRQrq6Yzt7vapCLt8ivUIwHVsHJrkKAh2B5yXjl/2uhrYJTVqGynWsy6UVuJ8nVkZiLuYxkhRGvfy1agbkemVjWBrd8JSRCeNhm1D4xXDlFijD7pwPtSG1ZXBeDIzUPK8ScrAmTIowsH71MXJrUZeH559KAs7BUhy33Iuo1GwrbNidZ/dyUVVcaQfSe3k5ags69379Sk6dqyjxA3EfnyM/hvvwnGokXQO7ZXx5IRZq2EdwHogw9D6hGHwHPxaGusoamVVm0JdpaaKy9BbG054svnAmF2Hi/Nqd3M4mu5hp8Ec/limI50mItnskzYCsfbWNuqFWC8v+y3P63fc8HGFokgzyyoPu+viG8ogd61C1s7yRIobDiZmWQaVLl6OrT2mcj55GuLANthb8LNgo0t2qn2yUdQO24cQ9Rsa0KINDXvOVa0Hp5zzkbKiWeg9sE71d6RsRVLocm+j21JBtFCpcVIOW6kWo/RcErB5onAw6UdMtioDJnk4RVJmDQRhzBeUgwjJQ/Zzz8L94EHJ9d8Car8movORujjD9Qq6brJKzQXZvEGxGoMZL/5Bpw9eqNq2EEMa7zQ2vjBaZJVjC1YhbzffoMzsTk50TQRKHQz4EN5uxzo/fskHwlEE9DeRZasR+rxRyD75betHpeM/gu1Q1nfjjAqq+Dq21c53AnNIJFYdP5K9Xf6vWMRfu4ptfC2fkaVlOGrlc1xvKYGrr+MROYDj9tHeJmmiBD96SeUHXEEUiRdKzeeTJAxgtUrEfPFUbBqFfQsqt0kmzW1MeSZEDXnnYna9ybD1T2fJiDdMrOKENQO/F5WKjkKCi0SUPgy/U6my5kVdPA6ZkLLspcSyujvNhODHYi8MhYvh/PgA5A75Tv7OPuQ/V4PMjIw/ja4ehTwFzsw5t0YbAC1/Q3Vms6wsHD+Ajjz8pOeBALZ2znjoSfV5FqjqALRRashz7K2VmEzimAd9PyCOk1g0u9x7n8gPJdeg5zPpkDfrZ86JpGJsawIJnv0tvhAJuUZXboOKSNHWtsVN8AmGiEw/k4E76OD05l+QTJECKIa6fmba4sQDTuRefNNSDn6mCazY0kNNrNMi49Nn47oyuXw/+sykoFcSKOnQx9MTQNMtLeUZd3iQYMCu1RpjPCLT6ldWdKuvByht15BbF0N3H07s+PamkWJsZEoG0HtY12yAVnvfAa9kB2InajhsvtNiBCbPROlBxzAi3Shc2OHXo2LbD+IFmBPiJbWIGX/wfCcPxru/f5E57XDzkWChhD/y+9D9PuvWbc4go/fi/CsBdDiYgIoIBk0YxnJLyASs9ZIUmDxNavg3O9QtV2xsWwJIvMXwn/XWDUKKtPytCyXGgdR/lwT8pKZ2LJfU+aEJ62226j9NvURyMrAxKfgv/UGOLMZ/8pmF0IIvli40UnUwg9JMskqmsSEjdaC08keUMIwLA7vHXfD1a833If+uclK7JSQtiMi9qMGYz98g9CUT6Gxblo7+j3KTFArBKg2eEyIkXbdLXAfcrjaMtioKEfonXdJprBaAxr9bpp6BKCukzz0NeqW99niFTE7sjsi96tv1OeN0aSzKEKVR+uFX3yUTCI7SQLZVNopq75CVDEqScN3/lRPoWMj5ktm/G5lWVUC6qZ4PsmobTKlTI5TFRr+qHpgheuQI5AuG06I8G0b+l8FuwfLQ0tCH38Kk709LMP8dH2U/yNOJX0MaTMhgQybp154GZyybR/bRHwHc9UKaphvEFtTjNjXkxFZRj8KBrULi+Tk8fwmIuU+aIaJwqL69ZgN0TQRCBFS4JZ/0XsNiswh/Ip99RHDyX3gkAds29AMP4nhhTHrWzXgstkUtN2L1RY3JgkgoVG6tTpZjV8I63lNMxCBw6iF89jT4Tn2KGuf5i3MHPqvgVLx9CHmz0PgiYmM3D5XiSaHNw2mTLmnZnaE/Gp3eucpf0NKj27wnHeR/WPCJlTo3dcR+uI7mHN/pbrxM2ylRpF1l0ePgLNTATJu+o/9g8bYLBEUVMxr/SlvgduuV9vMyGTOBIwixsFRAzWXjkK8vKyeCLZwxX4JjeK1If5JtZfphSb7DwRkHyO32pVEtEJcJoeS3Y7+e8OVn4aM516zEjDbO0W8oyHiYIeofexBK9vYsTNiP89SHdFcNAdxPyOG/Q6DY/FsZL/ylvKXGs2roDYXkx1+900SqAShz75BvHiDNR9T5LGZTrVlImwM3qDyF+SECfDnvhv/hdDTj9O2tRP/jjdCdWZKuMcP4tXKZ4OXKewKV9+uaujWLKMa+4FahMKXW4hHnHAW5CHj9Tf5zoq5kj8sbFNIW4toRFNKbyeCQo7lS5Fx/2OofWAc4EmD6+AhcO+7n/q+EZTPxnalNoj+/CP9q/pV302hZUTYDMoG9mFoUgkzIxO6Wa00SdyRCVOWivF7uUD65Rcg7fJr4bvyEvXUVbUCSZJBUlFfDdIffAIpx56w2UWau0AkyCFtJH8LRANvzXcSIm2lTKsQQTaWDDHO9Yy6CBV/6g/ngUPgveyKRiZEIEKOzpmJqr8Mg57fnmQhY9esVlvd5a8s/98zA0mEViGCgqgisfUkhfITGA/LrF+1NwCPB994DXq7QpQfezzcfTshungN7Z4DqXv3Rdbzr0O3n6KyCzsGrUeEjSDLziv2GoC0MbLr2UWIV1fx80DoA/sj9v1PyPlllgo3HSkua/v5/4XIIImxFeOy7ZCERuaTz6j9BqI/fofqc0+H6a9AeNbvSJVH9fTurWYDqyHuXSTY4WgbjcBTBu67C8Hvp0Mr28CPGsLz58DdszPSb7kHzr0Gwdmth114F5IBrU4EyaPLdKjIlE8YVl6P+LoNPKjB+/DDcLbLQ+oJJ/OqjAp2RQZJhVY3DcpRpJCjM2apHIJ7yKHw3nAD0i+4gCQ4yQpjdpEg6dAmpkF2X6v517+gd+8Jz4hjkDJs+C4/IMnRJkSQp6nK8Kn74CHWoNLWEh67sMPRNs6imu1M9b+LADsN2oYIu7DTYVeX3QUC+H+NtRsY/ehg9wAAAABJRU5ErkJggg==" alt="airasialogo"> 
                                @else
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAGYktHRAD/AP8A/6C9p5MAABiNSURBVHhe7Z0HeJRVusff6Zn0ENJIp8MK2NsqqLiKAsoC7nVdG8ULKOLqc33WxqogqIj3WlawgKjYV13bgnTswgoKCoIKCUkIaaRN2vT7/s93JvNNZpJMkklITH7PA8l3Zpj5vvOet56Cxs1QH92GHi0Qd0MDuSqryG238QU/hsFA2qgo0kZGynf0PLq9QKzf76GGrVvJfugQ2fbuIfvBX8hZWixe02i0RDod/9SIa/EkLie58Yd/1UXHkGHIEDKOGkWGYUMp7JxzKWzcWPHe7kq3E4h11y6qfestql//CVl//IG05nDS8KjX8OjX6PVE/EfDQggKl4vcDof4Q/hpqSFnrYX0A9IoYupUMk+4lMInXi7f3D3oFgJp2LGDLC+sIsvqVUIA2rg40phMQgjKsA8hrE1CSFYrC8hCzuoqipg8mSKvv54ipk+XbzpxnFCBVC5ZQhVLHyENmxhd//6kMZvlK10IBARfVFVNzqoKir75Fop/6sngtTDEnBCBlM6aRZYXXyR9UrJijqQP6A646urIcbSAzJdcQgmrVpM+PU2+0jV0qUBKZ8wky0trSJ+WTtrw8NCbo1ABrWGT5jiSy0HABZS07mO+3wj5YufCYUrnU/noMjrMD1m/aRMZhw5jP8GmqbsKA/C9aYxGMvC9OnJzKScikkpuuEG+2Ll0qoY4Cgqo4KTRbI+1wkd0ayG0BA8mV20t2dmUpXyygcIvvUS+EHo6TUPKFiygvPR0FkQ86eLje64wAN87TKxpyFAqnjSJjl00Xr4QekKuIfiw/FR2hDYbafv169mCaAZ3fT3ZWfsz8o6w00+XraEhpBpiP3hA+ArkEMglfovCAAjPjUOGUF5GBlU/95xsDQ0hEwjC2PzhI8jEjlBk1L0ABCjHF9xGxVf9SbZ0nJCYrDK+Kcszz5Bh8ODfrFY0C1sEZ1mZePYBO76Rje2nwwIp+fM1VPfRR6RPTe19wlDhqqkhTWQUpf98QLa0jw4JBMKoX7eOdMnJvVoYHpDlazgaS//5oGxpO+32IbCd0Iw+YXhBaOxmTTl62mmype20SyCWF9dQNfuM3m6mAoHJMWdePhVNmixb2kabBWJndSydNbN3OvAgQcjfsHUbVT76qGwJnjb5ELwReQZC2z5htAL3k40Hb9r+/WQcMUI2tk6bBJKXli6Kbr0lz+gwUigD2zB4gzZZcOIoSfcJow2wIAyZWXR0zCmyoXWCEojj6FGqevop0qE21UebgEWxH/pVBELBEJTJyo3rJyq2Gm27o+TeTRtMV6s9XPnYY0IQfcLoADBdGZlUdNllsqF5WtUQRFUoovVFVR2E+xEpQ2orUVeLAkG+Ub9hkzL/3UeHcTudRC43peccki3+tGiHqtkRaSO6ZnK/N4ClRc6iY1S3ZYts8adZDSmdNZsaNm48MWulfsNAS9DhGRx5BaJZDbG8uFpULvsILdAS15E8sn73nWzxJaBAKpYuFYvYOsuRo0wt1tueaPjxMI/RSlwTcnSpqVSxcKG88iWgQKqWPCxWFIYcxOO//EyR11xDmpgYxcmdINwuF9nZucbcdRe5y8u7VCiaMBPV/vvf8soXP4FYd+zkYePkvgvx8k6ZHKX/+gv1W/YoDfhsO1kPc7QR6u8JAmins7iYsvlnzIJbKXbJEnHdZbDwDWyBAlWD/QRiWbWKtFjUFkq4060sjIwjR8gwaLBo0kZFU7/77iNXdbW47iqgCVjAl2Xxfm/03DlkGDmS3Ha7bOl8NBy9Vj+zQl558RNI9aoXlKWeoYKFgYQIK/70GRmyUSHmjjvIefy4vOoCcC+soZmVFbLBS+Rf/iJWwXcVKNI68vPYn9bKFgUfgVh37yZtWGgjK6zIiJo7L+Dyy7r33iMd+5IuQZrMtD17Aya61q++UvajdBWsqQicLCt913X5CKT2zbdJ2y9OXnUcmABdYiL1X+mvmqDq8f/tnOAhAM7SUor9nzvJOHqUbPGl5s032p5zwf/hTzvRhIVR7YcfyisFH4HUf7JerDoMFfbcHEr7ab+8Yo1QRRbOoiKy8WtdMSrFwEhKpH6PLZMtPPjefUf+xgPjyadIn5gkRm1QsBCclZVC4/CnvdEi+rr+s0/llYKvyfphb2g6CLaaw9vUb7+VDUS2PXvYl/wsr4jK772HDOnsU1SdgAdzVlSK3CBk4F7EwPhJNhCVzplD4RO9ixAqFy1qk6bac3KE/0M5XZTUtdr2CYX/rS7MTPYf98kGlUBse9m2wn8EO0pawGWxUATnGibVcpgjJ59MMbffLq+UlStQWQ9YwOx2uanf48spev58MSkWCuy//kop27fLK8WnNWzeInIB0PDNN+SurQ16esHF9xl57bUUe9ffZAsHBNdf1+6AQMs+tG79enmlEkj9li2kje64PUdY6aqqosTXXpMtRMXTr6JY1YaX6tWrSdcvXl7xv+F8QBMZSRmcqEXxw8XeczeFT53CD2mV72gfELL50kvJPG6cbCHKY9PUb/lj8orv5ZmVws8FDZu/sLPPlhcKVY8s8y8zBetb2CLZ9gfQEOwDx4sdAuaBw8o01RfYf/6Fqthex9x+h2xhX/Luu6SN9ArfzQLst8xr34Ft13dE+o5tvHQU5FPyOq/fKv/738nudlHEH/8oW4hqXn3FR1NbAwOn4oEHqH7rNqr94AM6wpGSLjmJH10RACoAjmPHRN7l4LyrNcHARVh3/kde+ZisHzq8gAFJXtTsm0ifmSlbiI6yqTJodGQcM1q2sENlFfWYDAFHN1gF6aGUPwOZs7gf9QMFO+oYYao4lPWAeL988WKKnebd+mzbt4+0puCFIUC11umi4smTqWzGTHFyhGfHLkw1/F/Kpk00iC1F+rFCdvrsN1u4bwgksIYcOCA25bcXYao48kh44XnZQnT8r7eLL4ycNVO2cODw/fekM/v6Ksy51PFoy+PRdthgpHqOxrDZB/YemgvTg/ejkx35+Yp/aeEhYc/NEyZQ2DnnyBYeGKN5YMTEkpk70kPde6ypcf4LN/As+AzhF1T3iQGnTehPcffdS6azz1KEAd/D9+IoPMZtZ1NmEX6eJd6PRSEoE0FbWkL9JI0CcZSWtH9vNm6ITdUAdpAesCev8sknxNEXERMnyVbuBO54PAiWFOE9CB8dRwuVJUbcbszOFmUFd30D9V+9irKsDWTijoVgMtm0ZdXXUfL2bWwO8uQn+mPnDFhtqmrWvkouzkOcVZUUdcP1spXvZeNmDj2NXuHyT1QOMIlkGjuWTOefL3ZKCeGwn9MNGECpO3dS9K3zKYV9rqOwUHl2HiBw7En/ek/5HKZy8UPiZ+yddwptgTADFzC5zWDigaYEMY0CCd4Y+OOuq6PwK6/0MUuFZ55NxoGDyFFRTuFTrpCt7EQff5yIbbbx1FMp6sYbKHbhfZS8aSMlffiB+BzYYFdFBY+sh4WtxyAJu+BCFkQ9+x1l9jLsjDMobMIl5LbZxLUadFLSW/+UVwql3FlY3mkc7juXXf3F52IqAKUdWAfkFHEP3E9ZbHoSX13LgcmrlM0DQJc9kJz8ubH33CX/paIt0B4MKOxp77/iGfkKUX5GFlnWvCivFISWB4LloTEaWKjKABMzhhiduWxGcFCLWkWDBSV19RIXy8svU9GNN5Ke7XPEVdMpce1a+UrLVK98lkfWYnaKhT6fByGpw1I41aplj5E+zXdTv5hj4WQrXZWMHuNRXvvFF2RKS6e4pQ9R5HWKhiBvUFuEAzzSB+7eTcZTlEVtTtZGT1mnasUKKl9wG2U7vMXHwrPOFkVKCDrtxx9kK3/f739P9ey7UjZuIvMfLhZtmPvA0SGoBATK85wlJdT/lZcp4oorFA2B7Sdd+/wHbHrCSy/LKwVkxoM5p8huqG9RGHCCaioRaXFHIelSA2FUP/UUFZ5+Jh3ijqt+9jnfzZbcBqHZcg5zMuqNWEA0R3eDWbjpbMY8wgBqYTR8/TXFjB/fKAystDkcG0sNX3wprktvuYWSN2wQvwPrf3aSlZNedKRaGNhvaN39HRlYozzCABUPPSQ00TB8GLkC5Cu4FyEDRggEHcj3EDRIjjA6oBnmKVN87DKInj074OfZc3Op4r6FVDThMvqF31C/ebN8hSOvt98hN9tvR0kxxcOsqciNi6eK+x9g+14mFnrroqP5ptnxsvAgVJgastkp6aWX/BZlREz1hrhqav/5DpXNnUv5g4ZQ3rnnUoq8F+HjIiLJPGy42AINyxE2bBiZx18kXgfHLvoDuVxOyij0TV5L+fNcPAhTtnoXMZRey+aSPyP6ttsomaNLVHj9Ogfaz/cvfhV/t2al+AMgNISiEEI4Ryopn34qzErSG6/LN/lj4Q5Sc5xvuJrV3/bjj2Qe+TuffKCEM3t8R9K7XscICk4aRbqoSNIlJDSqO5ys/fBh4fjxoNlOB6Xn5VJkM6ctIGhoei9Ff7qK6j5exxrpIPPvTpKt7J/GX0zO2hohcF1SEv9jByW88aZ8lc3XE0+Qs6aaEl9ZK+7JAzZ+6mPjxGyoPitLtvL72Q9pUUdbvIjKF95P+uQU8dl+yDZFIJyANSsTFgacni5lAPVf+4oQAhyY6YzT5Ru8oKJasWQpHT1DMS2VS5fKVxSs3+4ibXw8ucrLKf4fT8tWvunly0kbbibDmDE+IxrtMImexA1RCrTBdME4Sjt4gEdogShhaLT+0WHtO++KlTM5RhPlpaVyWO8NPXHAGR5cG6Fk14ZM7zwNAod+9y4U5ggDRD90KJlOOVm+yiP+9tspcuJEirruWtnCn8eaWvOOEkioKxTVT/+DdJyDZeTkCFNV+dAi0kK7m8LmltixA+HUEX4eicLpa/6bcGBm0rFBHlvXWsBZVkqHEhIpjEcAOhARUNTMmRTHI8ODWAU5eAg5OYrK5Pd7yJEqnN3kuyFUz14UEX2V878rL5OvNg92d5XNn08GbJ9gR4rMOWXLZjKdeaZ4HWWikilTOYxNEZ8Lzc+qrhKveTh6yqmiGJr49lsUzgIAx2+9lWrYX2aqZhtBKT+nZc0an0CkKblR0ULjAtXMEJLHc4iPQ9XEq8Luuv2rlTANkVdfHVAYDZ99Jn9TKJ48hczszESyxKYFh4OZLvAepwcTg6wY4WIcR1IeLM8/T7Ce6dxpasrmzSMDToSQD4mwMeaOv4rf1dR//oUQsBoIwzhkqDK/wUJ11fC9SGEAaKqGtUNUA/h1dFJtE1OJQqiTs3uPMICLTSSE4SgqEhrkAcVQjzCa3guAn0KSGEgYwMWa6Jk2b3wHK40/HJsjmvAAFS7hSAUhYhgnTh5sBw+S9RvVjBu/7uQbD2d77MG2Z6/oIDs77eh5c2Ur2/I5cyhr3z4/oVc/+6zPzB7KKA3f7JBX3KmchBZwXlEydRrpcGqEpGLRYmVuQwJzomPNVYO5GEdxESV/uk1ME2g5oiqaPo0Hi1dLnMifLp0grxQSVr0gfuZnZHLPec2kiXMqUDBiJFU94l244MjLoyPx3NF2W8vzTNyvBhnCNwpEHxMr1FcNOsHFkU8xq1LprJvosNFI9e+/TymP/598h0IpC0mfld04msVPHFCpQpRmOLwzc/yuZhDH+8aRIxtDTKBMJfvO3uGBrJ9/zuZhNhWeN5YKx47jgXCAUnd5BwyouP/voqTdCD+siRNJNRhkcXffQ6YxJ1MW/MTAgSIxdrJJ9GD96msynez1HR6OnT9WbOwsZcvhAU+dzxqJKjeClmPnnU/5bJrzYTF4sLRYvOTB67ZZScf3ALwCQVKIxKoJeDjbrt3UwNk07DnUX206sBnFxnE5NqZ4wKg0cNioBuYBOYZZpVkATg7mT11ctO1i58+mrymwwQ2bN5HzaIFICs2chOkzvPkItk6otQOIe+HOUmP9+iuKuedu8TsGHcJUmByDjI7QQRb2HTiQU03hOecKa4Cjpuz8M4fD2YIRIxQfyIMZgtKnpIhSCgYlNsY2Z6bUQKCeQLjx3ThKtbnVhBidkDKSl7ilj8hWhbI5c0mfyQ/i0Q7Avzc9OxcH04jk0+w7WlBALBg3zqe0YHn1tWaXseI+YBpxsFj/1atlq0LF3+72j2LQUfG+BUQnC6ni3vvklS+uagsd4bwHRzFhYOBwBJwNmcsa6+SO1rF5AzC/MDNuzh+wXUOdaOL+xLW6T5oBfa7PUrQDNApElE2aEYgH2P/Yu70zZYjOELE0tY+4GbuqfAGcxSUipKxe8axsYYe8fTvlcSwfxg9U8/rrlMdmD6PNybZXrXFNwUOgbmbghM1Dzdq17AvYVGG0quF7gS1Xg4eu4bykaOIkccqdPS+fE9O3qfi/rqbcmGjSDxgg3gdNsH75Jd/zStKlc8TW1PTwdwXb8c0iTKp3ZrVx9XsD2+eiyy5vvJmmIPWPmDaN4lHBlZRyItawbXtAG4kiX/LGDRTGWTDIYZ8CocNPoYM8FV/19+G1YB4QuUnyhk98Aos8hNuslYFMBMr22Zx7gPrPPqeSyVeISSVEkeJkbM6uUWGGVovIrCMd3EZQaYi65RZR0gc+2xGa3S3F7ajpo4yspsXdVfwachjj6DFk+/57MgxUOX0GX9ue5aoQGkLqDI6SPDSwAy4aP77Fw8TsPAiMo04SE3HCVwS65xMAqshJ69dT2PnniWuf4YTTQgP5EYykCB5VaiqWPtzyCnluN2RmcpRWRoZs/w5o79ph4cceeVheKVQ++KBw+C1hyMjgeykX99RdhIFBizKNRxjAR0OOL1hAte/9y29lH0wEIhFMFHnIjYwiHUcUIV+U3QpNS/1AndH3JDD4kZSn/rBXtjTREPPllwtzoEbIi0NDtTCsOzhB40ilq4WBKnPUrNnySqHigQfJ0FzBrpsDy2O+0tfy+AgkfMIEciBbVXU0wtHom2+WVwqWNS+FfoV8MHBUFz7JOx0MkNHDIfdEUIyNmjFDXin4hSSRV2I9lHcSBVGPbb9vCFvz2ustZ5+dBcf3IsFU4WTnLiKzHoYIaji0NwwaJFsU/AVy3fWiBOABOUbD5s1UuXy5uK794CPOKtsXIXUUVA2Oz59PDbK+VsJ5Ayq6PdJccRqBfSlN8XHqHkQ4qy438DWqmM5STu6iY5RC4AnsBJTTnTUWMgxI9Zsh7BFwf2JeByV/bFxSE1AgGIW173/gv48CWtFdRmN3upc2gugKSWza3j2yxUtAgaDpMGe8fQeVdQ4o/4tVJlOmyBYvfj4EwD9EXHaZKG30EVow2KEhgYQBAgoEoJJqP5KrmIY+Qgbml/qvCLyjDAQ0WR6OXXwxOX491GLltY/gQVc7C/LFKszmaFZDQNJHH5GtT0tCBgqJCW++La8C06JAsD06+sYZod1i1ksRc/tJyRRxZcvn+bZosjz45SV9tA3uP0xfZB0v89k5FogWNcQDNqA4cnLkVR9tBVMGsfPmtSoMEJSGAOHgD/6szKj1ETRiIV5JKWflymLq1ghaIOCwVkdGHDHeR3A0mqrjYqFcMARlsjxk5OeJCaK+qCs4sOkzcRV2HAd/3nGbBIL/DSFh1SrFn/QJpUWwZDZs/EUUpdpfGQxtMlkeiq/+MzWwoxf/HV4ffoiSk05H6TgPrI20SyAAq/iwWC3QCsPeDOpUmAlUn8fVFtotEJA/fCS5+Yt75JxEJ4DkDwtCsm3tL8q2yYc0Jf3AfrGVuS+TVzSjo8IAHRIISD/wE+nSUgPui+gtwGfATHVUGKDDAgHYTI9TE7ARtLdFX2K/Ojvw9vqMpoREIAAb/2MXPajsiO0N8MCzc56BI6jaE001R4eceiCwbyJ/+HDxP8v8VudRUA6xHfqVElevFvsoQ0nIBeLh6Kmnic0tYqtW53xF18NagUIh9hqm5x5uUwYeLCEzWU1J3b2L+j+zQpgwkSj1cN+CkBZ1qfBp00WhsDOEATpNQ9QUXT6R6tevI/3AQT1vlSF3j72wUKxFG7BzR6dXJzpNQ9TgqKRUeQgl9mlgtHV3ME4RyuJ4JpxWkc4+oytKRV2iIWrqt22jspv+W5RdUKwUa4S7i49hs4qt366yMjFocGJFc8d1dBZdoiFqzBdeKA7kH7DrWzKMHiXssjjcC1pzgvwMxiT2S8LfaaKjKOGVlymrtqbLhQG6XEMCUblsOVl4NGIdGLY1Y1ZSbCTtrFuTmoBV/jBLCM+j5s0Vh5epD+c8EXQLgXjAAS2WFSup7uOPqW7bVtKZwsT/M6JFPoOtxnrVoZjB3LZ8L+pM2O0qzJHFwtpQQ6bRYyh88iSKnDGTDIO825JPNN1KIE2x7f+J6tetJ9tP+8T/a2Lb96No1xqkgPgPyhY+QsKmUO54CMAla0uGgQPJePrpYgOq+cILGncGd0e6tUCaw3G0gBz5BWLxgKuqkiVnU4TBgsL8jLZ/vDjpAYLoafRIgfx2Ifp/VWcwY1pcwmAAAAAASUVORK5CYII=" alt="airasia_logo">
                                @endif
                                </div>
                                <div class="address">
                                @php 
                                echo $address;
                                @endphp
							</div>
							<div class="clearfix"></div>
                        </div>
                        <div class="well">
                            <div class="row">
                                <div class="column_one left">
                                    <div class="invoice_number bold">
                                    @if($carrier_code == 'QZ' || $carrier_code == 'XT')
                                        Invoice # 
                                    @else 
                                        Tax Invoice #
                                    @endif
                                    <span>{{ $result{0}->invoice_id }}</span>
                                </div>
                                @php
                                    $invoice_date = date('Y-m-d H:i:s', strtotime('+8 hours', strtotime($result{0}->invoice_date)));
                                @endphp
                                    <div class="date_invoice">Date of Invoice <span>{{ date_format(date_create($invoice_date),"d-m-Y H:i:s") }}</span></div> 
                                    <div class="bold order_number">Order #<span>{{ $result{0}->increment_id }}</span></div>
                                    @php
                                        $sales_date = date('Y-m-d H:i:s', strtotime('+8 hours', strtotime($result{0}->sales_date)));

                                    @endphp
                                    <div>Purchased on <span class="date_purchase">{{ date_format(date_create($sales_date),"d-m-Y H:i:s") }}</span></div>
                                </div>
                                <div class="column_two left">
                                    <div>Order will be delivered to:</div>
                                    <div><span class="passenger_name">{{ $result{0}->first_name.' '.$result{0}->last_name  }}</span><br>
                                        <span class="pnr">{{ $result{0}->carrier_code.$result{0}->flight_number }}</span> <br>
                                        <span class="flight_date">{{ $result{0}->flight_date }}</span><br>
                                        <span class="sector">{{ $result{0}->departure_station. ' > '.$result{0}->arrival_station }}</span>
                                    </div>                                
                                </div>
                                <div class="column_three left">

                                    <div>Payment method: <br><span class="payment_method">{{ $result{0}->payment_method }}</span></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="table-relative">
                            <span class="pre">PREORDER INFORMATION</pre>
                            <table class="table table-striped" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th width="50%" colspan="2">Product</th>
                                        <th width="10%">Qty</th>
                                        <th width="10%">Code</th>
                                        <th width="10%" class="bg-grey-light">Unit Price</th>
                                        <th width="10%" class="bg-grey-light">Total</th>
                                        @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                                        <th width="10%" class="bg-grey-grey">Total/{{ $result{0}->base_currency_code }}</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                @foreach($order_items as $item)
                <tr class="product-list">
                    <td><span class="product-name">{{ $item->sku }}</span></div>
                    </td>
                    <td><span class="product-name">{{ $item->name }}</span></td>
                    <td><span class="qty"> {{ number_format($item->qty_ordered) }}</span></td>
                    <td><span class="tax-code"> 
                    {{ $tax_code }}
                    </td>
                    <td class="bg-grey-light">
                        <div class="unit-price">
                            <span class="currency">{{ $result{0}->order_currency_code }}</span>
                            {{ number_format($item->price,2) }}
                        </div>
                    </td>
                     <td class="bg-grey-light">
                        <div class="price-currency">
                            <span class="currency">{{ $result{0}->order_currency_code }}</span>
                             {{ number_format($item->price * $item->qty_ordered,2) }}
                        </div>
                    </td>
                    <td class="bg-grey-grey">
                        @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                        <div class="price-myr"><span class="ringgit">{{ $result{0}->base_currency_code }}</span>
                         {{ number_format(($item->base_price  * $item->qty_ordered),2)  }}
                        </div>
                        @endif
                    </td>
                </tr>
                @endforeach
            <tr style="padding-top:10px;">
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <div class="sub-total">SUB-TOTAL</div>
                </td>
                <td class="bg-grey-light">
                </td>
                <td class="bg-grey-light">
                    <div class="subtotal-currency"><span class="currency">{{ $result{0}->order_currency_code }}</span> {{ number_format($sub_totals,2) }}</div>
                </td>
                <td class="bg-grey-grey">
                    @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                    <div class="subtotal-myr"><span class="currency">{{ $result{0}->base_currency_code }}</span> {{ number_format($base_sub_totals,2) }}</div>
                    @endif
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <div class="discount">DISCOUNT</div>
                </td>
                <td class="bg-grey-light">
                </td>
                <td class="bg-grey-light">
                    <div class="discount-currency"><span class="currency">{{ $result{0}->order_currency_code }}</span> {{ number_format($total_discount,2) }}</div>
                </td>
                <td class="bg-grey-grey">
                    @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                    <div class="discount-myr"><span class="currency">{{ $result{0}->base_currency_code }}</span> {{ number_format($base_total_discount,2) }}</div>
                    @endif
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <div class="shipping">SHIPPING FEES</div>
                </td>
                <td class="bg-grey-light">
                </td>
                <td class="bg-grey-light">
                    <div class="shipping-currency" style="vertical-align:top">
                        <span class="currency">
                            <!--{ $result->rows{0}->order_currency_code }-->
                        </span> <!--{ number_format($shipping_fees,2) }-->
                    </div>
            </td>
            <td class="bg-grey-grey">
                @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                <div class="shipping-myr" style="vertical-align:top">
                    <span class="currency">
                         <!--{ $result->rows{0}->base_currency_code }-->
                    </span> 
                        <!--{ number_format($base_shipping_fees,2) }-->
                    </div>
                @endif
            </td>
            <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <div class="total-price bold">TOTAL <br>
                        @if($carrier_code!=='Z2')
                        inclusive {{ $tax_label }} {{ $tax_rate * 100 }}%<br> [if applicable]
                        @endif
                    </div>
                </td>
                <td class="bg-grey-light">
                </td>
                <td class="bg-grey-light">
                    <div class="total-currency"><span class="currency">{{ $result{0}->order_currency_code }}</span> {{ number_format($total,2) }}</div>
                </td>
                <td class="bg-grey-grey">
                    @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                    <div class="total-myr"><span class="currency">{{ $result{0}->base_currency_code }}</span> {{ number_format($base_total,2) }}</div>
                    @endif
                </td>
                <td></td>
            </tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <tr>
            </tr>
             @if($carrier_code==='Z2')
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Vatable Sale</td>
                <td></td>
                <td>
                    @if($result{0}->flight_type == 'domestic') 
                        
                        <span class="currency">
                            {{ $result{0}->order_currency_code }}
                        </span> 
                            {{ number_format(($total-$tax),2) }}
                        
                    @endif
                </td>
                <td>
                    @if($total_dom_base_cur!=0)
                        @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                            <span class="currency">
                                {{ $result{0}->base_currency_code }}
                            </span> 
                                {{ number_format(($base_total-$base_tax),2) }}
                        @endif
                    @endif
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>VAT-Exempt Sale</td>
                <td></td>
                <td>
                    @if($flight_bound_z2=='INBOUND' AND $result{0}->flight_type=='international')
                    
                        <span class="currency">{{ $result{0}->order_currency_code }}
                        </span> 
                        {{ number_format($total,2) }}
                    @endif
                    </td>
                <td>
                    @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                        @if($flight_bound_z2=='INBOUND' AND $result{0}->flight_type=='international')
                            <span class="currency">{{ $result{0}->base_currency_code }}
                            </span> 
                            {{ number_format($base_total,2) }}
                        
                        @endif
                    @endif

                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Zero-rated Sale</td>
                <td></td>
                <td>@if($flight_bound_z2=='OUTBOUND' AND $result{0}->flight_type=='international')
                        <span class="currency">{{ $result{0}->order_currency_code }}
                        </span> 
                        {{ number_format($total,2) }}
                  @endif
                    </td>
                <td>
                    @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                        @if($flight_bound_z2=='OUTBOUND' AND $result{0}->flight_type=='international')
                        
                            <span class="currency">{{ $result{0}->base_currency_code }}
                            </span> 
                            {{ number_format($base_total,2) }}
                       
                        @endif
                    @endif
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total Sale</td>
                <td></td>
                <td>   
                    <span class="currency">
                        {{ $result{0}->order_currency_code }}
                    </span>
                    @if($result{0}->flight_type=='domestic')
                        {{ number_format($total-$tax,2) }}
                    @else
                        {{ number_format($total,2) }}
                    @endif
                    
                </td>
                <td>
                    @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                        <span class="currency">
                            {{ $result{0}->base_currency_code }}
                        </span>
                        @if($result{0}->flight_type=='domestic')
                            {{ number_format($base_total,2) }}
                        @else
                            {{ number_format($base_total-$base_tax,2) }}
                        @endif
                    @endif

                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>12% VAT</td>
                <td></td>
                <td>
                
                    @if($result{0}->flight_type=='domestic')
                    <span class="currency">
                        {{ $result{0}->order_currency_code }}
                    </span>
                        {{ number_format($tax,2) }}
                    @endif
                    
                </td>
                <td>
                    @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                        @if($result{0}->flight_type='domestic' AND $base_tax!=0)
                        
                        <span class="currency">
                            {{ $result{0}->base_currency_code }}
                        </span>
                            {{ number_format($base_tax,2) }}
                        
                        @endif
                    @endif
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>TOTAL</td>
                <td></td>
                <td>
                    <span class="currency">
                    {{ $result{0}->order_currency_code }}
                    </span> 
                    {{ number_format($total,2) }}
                </td>
                <td>
                    @if($result{0}->order_currency_code !== $result{0}->base_currency_code)
                    <span class="currency">
                            {{ $result{0}->base_currency_code }}
                        </span> 
                        {{ number_format($base_total,2) }}
                    @endif
                </td>
                <td>
                </td>
                <td></td>
            </tr>
            @endif
            </tbody>
            </table>
            <div class="invoice-footer">
            @if($carrier_code!='Z2')
                <div class="total-qty bold">Total Quantity: 
                    <span>{{ number_format($total_items_qty) }}</span>
                </div>
                <table class="gst-summary">
                    <tbody>
                        <tr>
                            <th width="30%" colspan="2"></th>
                            <th width="30%"></th>
                            <th width="30%"></th>
                        </tr>
                        <tr>
                            <td>{{ $tax_label }} Summary</td>
                            @if($carrier_code == 'QZ' || $carrier_code == 'XT')
                            <td>Amount/ {{ $result{0}->order_currency_code }}</td>
                            <td>Tax/ {{ $result{0}->order_currency_code }}</td>
                            @else 
                            <td>Amount/ {{ $result{0}->base_currency_code }}</td>
                            <td>Tax/ {{ $result{0}->base_currency_code }}</td>
                            @endif
                        </tr>
                        <tr>
                            <td>S={{ $tax_rate_label_s }}%</td>
                            <td>{{ number_format($total_dom_base_cur,2) }}</td>
                            <td>{{ number_format($tax_dom_base_cur,2) }}</td>
                        </tr>
                        <tr>
                            <td>Z={{ $tax_rate_label_z }}%</td>
                            <td>{{ number_format($total_intl_base_cur,2) }}</td>
                            <td>{{ number_format($tax_intl_base_cur,2) }}</td>
                        </tr>
                    </tbody>
                </table>
            @endif
            <p>More information about your order: Please be informed that we are not able to make changes to your final order but you may cancel the order within 30 minutes from placing the order. A cancellation fee may be charged.<br>
                For more information, please visit www.ourshop.com. Any customs and duties applicable are separate and in addition to the price. </p>
            </div>
            <div class="row">
                <div class="col-lg-12 remittance">
                </div>
            </div>
            <div class="alert alert-danger alert-inbox" style="display: none; position: absolute; top: 40%;"></div>
            <div class="loader-darkener"></div>
            <div class="fa-spin dummy-loader"></div>
            <div class="alert alert-danger alert-inbox" style="display: none; position: absolute; top: 40%;"></div>
            <div class="loader-darkener"></div>
            <div class="fa-spin dummy-loader"></div>
            </div>
            </div>
            </div>
            </body>
            </html>
            