@inject('request', 'Illuminate\Http\Request')
@inject('StatusPresenter', 'App\Presenters\StatusPresenter')
@extends('layouts.app')
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    {{--<link href="{{ asset('css/tables.css') }}" rel="stylesheet" type="text/css" />--}}
    <link href="{{ asset('assets/vendors/sweetalert/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> 
                        Warehouse Reports
                    </h4>
                </div>
                <br />
                <div class="panel-body">
					<div class="row">
					@include('admin.report.partials.filter2') 
					</div>
					<div class="col-md-12"><hr></div>
					<div class="row">
                        <div class="panel-body">
						<table class="table table-bordered table-striped table-hover" id="reports_table">
							<thead>
							<tr>
                                <th width="5%" data-field="increment_id">Order No.</th>
								<th width="5%" data-field="pnr">PNR</th>
								<th width="5%" data-field="flight_number">Flight No.</th>
								<th width="10%" data-field="sales_date">Sales Date</th>
								<th width="10%" data-field="flight_date">Flight Date</th>
								<th width="20%" data-field="recipient_name">Recipient Name</th>
								<th width="10%">Status</th>
								<th width="30%">Actions</th>
							</tr>
							</thead>
							<tbody>
                            @if($order_result)
                                @php
                                    $orderId = '';
                                    $pnr = '';
                                    $flight = '';
                                    $recipient = '';
                                    $status = '';
                                @endphp

                            @foreach($order_result as $key => $order)
                                @php
                                    $raw_sales_date = date('Y-m-d H:i:s', strtotime('+8 hours', strtotime($order->sales_date)));
                                    $raw_sales_day = date('d', strtotime('+8 hours', strtotime($order->sales_date)));

                                    if($raw_sales_day > $s_end_date && !is_null($s_end_date)) continue;
                                    if($raw_sales_day < $s_start_date && !is_null($s_start_date)) continue;

                                    $orderId = $order->order_id;
                                    $increment_id = $order->increment_id;
                                    $pnr = $order->pnr;
                                    $flight = $order->carrier_code.$order->flight_number;
                                    $sales_date = date_format(date_create($raw_sales_date),"Y/m/d");
                                    $flight_date = date_format(date_create($order->flight_date),"Y/m/d");
                                    $recipient = $order->first_name.' '.$order->last_name;
                                    $status = empty($order->seller_order_status) ? 'Paid' : $order->seller_order_status;
                                    $sellerOrderStatus = $status;
                                   
                                    if(Auth::user()->hasRole('administrator')){
                                        
                                         $status = empty($order->order_status) ? 'Paid' : $order->order_status;
                                    }

                                @endphp
                            <tr>
                                <td>{{ $increment_id }}</td>
                                <td>{{ $pnr }}</td>
                                <td>{{ $flight }}</td>
                                <td>{{ $sales_date }}</td>
                                <td>{{ $flight_date }}</td>
                                <td>{{ $recipient }}</td>
                                <td>{{ $StatusPresenter->getStatusDesc($status) }}</td>
                                <td>
                                    <div class="text-right">
                                        <a class="btn btn-primary" target="_blank" href="{{ route('wreport2.warehouse.paxslip', [$order->seller_id, $order->order_id]) }}" data-toggle="tooltip" data-placement="top" data-title="rePrint pax slip" ><i class="fa fa-print"></i></a> 
                                        <a class="btn btn-warning" href="{{ route('wreport2.warehouse.detail',[$order->seller_id, $order->order_id]) }}" data-toggle="tooltip" data-placement="top" data-title="View order" ><i class="fa fa-eye"></i></a>
                                        @if($status!='delivered' && $status!='complete' && $status!='canceled' && $status!='req_refund' )
                                            <span data-toggle="modal" class="updateStatus" id="{{$order->order_id}}" data-target="#myModal-{{ $order->order_id }}">
                                                <a class="btn btn-success" data-toggle="tooltip" data-title="Update status"><i class="fa fa-exchange"></i></a>
                                            </span>
                                        @endif
                                        <a class="btn btn-danger" href="{{ route('wreport2.warehouse.invoice',[$order->seller_id, $order->increment_id, $order->order_id]) }}" data-toggle="tooltip" data-placement="top" data-title="View Invoice" ><i class="glyphicon glyphicon-barcode"></i></a>
                                    </div>
                                    <div class="modal fade" class="myModal" id="myModal-{{ $order->order_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        @if(Auth::user()->hasRole('administrator'))
                                            <form method="post" action="{{ route('wreport2.warehouse.magento-order-update-status',[$order->seller_ids, $order->order_id]) }}" class="form-horizontal" id="update-status-{{ $order->order_id }}">
                                        @else
                                             <form method="post" action="{{ route('wreport2.warehouse.seller-order-update-status',[$order->seller_id, $order->order_id]) }}" class="form-horizontal" id="update-status-{{ $order->order_id }}">
                                        @endif
                                        {{ csrf_field() }}
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <!--<span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>-->
                                            </button>
                                            <h4 class="modal-title modal-update-title" id="myModalLabel"><strong>Update</strong> - Order ID : {{ $order->order_id }}</h4>
                                            
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                                <label class="col-lg-4 control-label">Status</label>
                                                <div class="col-lg-5">
                                                    {!! Form::select('update_status', $StatusPresenter->getNextOrderStatus($sellerOrderStatus), null, ['class' => 'form-control', 'width' => '100%' ]) !!}
                                                </div>
                                            </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default modal-cancel-button" data-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary modal-update-button" id="submitUpdate" data-id="{{ $order->order_id }}">Update</button>
                                          </div>
                                        </div>
                                        </form>
                                      </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @endif           
                            </tbody>
						</table>
                        </div>
					</div>
                </div>
            </div>
        </div>  
    </section>
@stop
@section('javascript')
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>--}}
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/moment.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/airasia_orders.js') }}"></script>
    <script>
        $(function() {
            
            var whDatatable = $('#reports_table').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [6,7] }
                ],
                "searching": false, 
                "paging": true, 
                "info": false,
                "bLengthChange": false,
                "order": [
                    [ 0, "desc" ]
                ],
                "language": {
                    "emptyTable": "No Orders Available"
                }
            });

            $('#btn_whExport').click(function(){
            
                var queryString = $('#new_warehouse_filter').serialize();

                var order = whDatatable.order();
                var title = whDatatable.column(order[0][0]).header();

                var sort_field = $(title).data('field');
                var sort_order = order[0][1];

                var url = '{{ route("wreport2.export") }}';
                
               if (url.indexOf('?') == -1) {
                
                   url += '?'+'sort='+sort_field+'&sort_order='+sort_order+'&export=true'+'&'+queryString;                
                } else {

                    url +='&'+'sort='+sort_field+'&sort_order='+sort_order+'&export=true'+'&'+queryString;
                }

                location.href = url;
            });
        });
    </script>
    @include('layouts.flash')
@stop

