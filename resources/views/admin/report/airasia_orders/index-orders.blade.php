@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    {{--<link href="{{ asset('css/tables.css') }}" rel="stylesheet" type="text/css" />--}}
    <link href="{{ asset('assets/vendors/sweetalert/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
<section class="content paddingleft_right15">
    <div class="row">
            <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="sitemap" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    AirAsia Orders
                </h4>
            </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4 pull-right">
                            <h4>File Upload</h4>
                            <form method="POST" role="form" action="orders/upload" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="file" class="form-control-file" style="width:230px;display:inline-block;" name="spreadsheet[]" accept=".csv" multiple>
                                    <input type="submit" value="upload" style="margin-top:-5px;display:inline-block;" class="btn btn-success btn-sm">
                            </form>
                        </div>
                    </div>
                    <div class="row">
                    @include('admin.report.airasia_orders.partials.filter') 
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover sortable dataTable" id="table">
                                <thead>
                                    <tr>
                                        <th data-sort="DESC" data-sortby="purchase_date" class="sort {{ $css_sort_purchase_date }}" data-class="{{ $css_sort_purchase_date }}">Purchase Date</th>
                                        <th data-sort="DESC" data-sortby="pnr" class="sort {{ $css_sort_pnr }}" data-class="{{ $css_sort_pnr }}">PNR</th>
                                        <th data-sort="DESC" data-sortby="flight_number" class="sort {{ $css_sort_flight_number }}" data-class="{{ $css_sort_flight_number }}">Flight Number</th>
                                        <th data-sort="DESC" data-sortby="currency_code" class="sort {{ $css_sort_currency_code }}" data-class="{{ $css_sort_currency_code }}">Currency Code</th>
                                        <th data-sort="DESC" data-sortby="item_code" class="sort {{ $css_sort_item_code }}" data-class="{{ $css_sort_item_code }}">Item Code</th>
                                        <th data-sort="DESC" data-sortby="item_description" class="sort {{ $css_sort_item_description }}" data-class="{{ $css_sort_item_description }}">Item Description</th>
                                        <th data-sort="DESC" data-sortby="quantity" class="sort {{ $css_sort_quantity }}" data-class="{{ $css_sort_quantity }}">Quantity</th>
                                        <th data-sort="DESC" data-sortby="charge_amount" class="sort {{ $css_sort_charge_amount }}" data-class="{{ $css_sort_charge_amount }}">Charge Amount</th>
                                        <th data-sort="DESC" data-sortby="departure_date" class="sort {{ $css_sort_departure_date }}" data-class="{{ $css_sort_departure_date }}">Departure Date</th>
                                        <th data-sort="DESC" data-sortby="email_address" class="sort {{ $css_sort_email_address }}" data-class="{{ $css_sort_email_address }}">Email Address</th>
                                        <th data-sort="DESC" data-sortby="carrier_code" class="sort {{ $css_sort_carrier_code }}" data-class="{{ $css_sort_carrier_code }}">Carrier Code</th>
                                        <th data-sort="DESC" data-sortby="first_name" class="sort {{ $css_sort_first_name }}" data-class="{{ $css_sort_first_name }}">First Name</th>
                                        <th data-sort="DESC" data-sortby="last_name" class="sort {{ $css_sort_last_name }}" data-class="{{ $css_sort_last_name }}">Last Name</th>
                                        <th data-sort="DESC" data-sortby="amount_in_myr" class="sort {{ $css_sort_amount_in_myr }}" data-class="{{ $css_sort_amount_in_myr }}">Amount in MYR</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sheet_records as $row )
                                    <tr>
                                        <td>{{ date('m-d-Y',strtotime($row->purchase_date)) }}</td>
                                        <td>{{ $row->pnr }}</td>
                                        <td>{{ $row->flight_number }}</td>
                                        <td>{{ $row->currency_code }}</td>
                                        <td>{{ $row->item_code }}</td>
                                        <td>{{ $row->item_description }}</td>
                                        <td>{{ $row->quantity }}</td>
                                        <td>{{ $row->charge_amount }}</td>
                                        <td>{{ date('m-d-Y',strtotime($row->departure_date)) }}</td>
                                        <td>{{ $row->email_address }}</td>
                                        <td>{{ $row->carrier_code }}</td>
                                        <td>{{ $row->first_name }}</td>
                                        <td>{{ $row->last_name }}</td>
                                        <td>{{ $row->amount_in_myr }}</td>
                                        <td><a class="btn btn-primary" target="_blank" href="{{ route('aa_orders.orders.paxslip',$row->id) }}" data-toggle="tooltip" data-placement="top" data-title="Print Packing Slip" ><i class="fa fa-print"></i></a></td>
                                    </tr>
                                @endforeach     
                                </tbody>
                            </table>
                        </div>
                        <div class="row">

                        <div class="col-md-12 text-right">
                            <!--{{ $sheet_records->render() }}-->
                            {{ $links }}
                        </div>
                    </div>   
                </div>
            </div>
        </div>       
    </div>
</div>
    
       
@endsection
{{-- page level scripts --}}
@section('javascript')
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>--}}
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/moment.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/airasia_orders.js') }}"></script>
    @include('layouts.flash')
@stop

