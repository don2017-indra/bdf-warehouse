<form method="get" action="" class="form-horizontal">
    <input type="hidden" name="search-filter" value="1">
	
						
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('flight_date', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="flight_date">Flight Date</label>
                                    <div class="col-md-9">
                                        <input id="flight_date" name="flight_date" type="text" placeholder="" class="form-control select_date" value="{{ old('flight_date',$request->has('flight_date') != null ? $request->input('flight_date') : '') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('sales_date', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="sales_date">Sales Date</label>
                                    <div class="col-md-9">
                                        <input id="sales_date" name="sales_date" type="text" placeholder="" class="form-control select_date" value="{{ old('sales_date',$request->has('sales_date') != null ? $request->input('sales_date') : '') }}">
                                    </div>
                                </div>
                            </div>
                    <div class="col-md-6">
                        <div class="form-actions pull-left ">
                            <button type="submit" class="btn btn-primary product_submit">Search</button>
                                        &nbsp;
                            <a type="reset" class="btn btn-default" value="Reset" onclick="reset();" href="{{ route('aa_orders.index') }}">Reset</a>
                    	</div>
                    </div>                
</form>     
<script type="text/javascript">
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script> 
