<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
	<title>Air Asia Order - Pax Slip</title>
</head>
<style>
                html{
                    font-family: 'Gotham Rounded', sans-serif;
                    font-size: 13px;
                    font-weight: 400;
                    color: #555;
                    height: 100%;
                    width: 100%!important;
                    margin: 0;
                    padding: 0;
                }
                .page-header{
                    font-size: 1.7em;
                    font-weight: 700;
                    padding: 10px 0;
                    margin: 0;
                    color: #dc2118;
                    text-transform: uppercase;
                    letter-spacing: -1px;
                    border-bottom: 8px solid #f1f1f1;
                }

                .pnr{
                    font-size: 0.75em;
                }

                .invoice-block{
                    padding: 15px;
                }

                .well{
                    margin-top:30px;
                }

                .flight-details, .details{
                    
                }

                .flight-details{
                    text-transform: uppercase;
                    color: #999;
                    font-weight: 700;
                    font-size: 1.5em;
                    top: 0;
                    left: 15px;
                }

                .details{

                }

                                
                .table-relative{
                    margin: 140px -15px 0px;
                    border-top: 1px solid #cbd2b0;
                    border-bottom: 1px solid #cbd2b0;
                    padding: 20px 15px 0px;
                    
                }

                .pre{
                    font-family: 'Gotham Rounded', sans-serif;
                    font-weight: 700;
                    line-height: 1em;
                    text-shadow: 0 0 1px rgba(51,51,51,.2);
                    color: inherit;
                }

                .table{
                    border-collapse: collapse;
                    border-spacing: 0;
                    background: #fff;
                    margin: 10px 0;
                }

                tr:nth-child(odd) {background-color: #f9f9f9;}

                .table > tbody{
                    border-bottom: 5px solid #cbd2b0;
                }
                    
                .table > thead > tr > th {
                    background: #DDD;
                    color: #000;
                    font-size: 12px;
                    letter-spacing: 1px;
                    text-transform: uppercase;
                    font-weight: 700;
                    padding: 12px 5px;
                    border: none;
                    text-align: left;
                }

                .table > tbody > tr > td{
                    padding-left:5px;
                    height: 55px;
                    font-size:13px;
                    
                }
                tr:not(:first-child) {
                    border-top: 1px solid #ddd;
                }

                .product-num, .name{
                    display:inline;
                }
                .row{
                    width: 100%;
                }
                .col1{
                    float: left;
                    width: 50%;
                }
                @media screen{
                    .product-num{
                        background: #333333;
                        font-size: 12px;
                        padding: 7px;
                        width: 20px;
                        text-align: center;
                        border-radius: 35px;
                        color: #fff;
                        font-weight: 700;
                        position:absolute;
                        margin-top: -7px;
                    }

                    .name{
                        padding-left: 25%;
                    }
                }
</style>
<body>
    <div class="inner-spacer print_area">
        @if($order_result)
        @foreach($order_result as $key => $order)
        <div class="invoice-block" style="border:1px dashed black;">
            <div class="page-header">
                <div class="pnr">PNR :  {{ $order->pnr }}</div>
            </div>
            <div class="well">
                <div class="row">
                    <div class="col1">
                        <div class="flight-details">Flight Details</div>
                        <div class="details">
                            <strong><span style="width: 100px; float: left;">Carrier Code </span>: {{ $order->carrier_code }}</strong><br>
                            <strong><span style="width: 100px; float: left;">Flight Number </span>: {{ $order->flight_number }}</strong><br>
                            <strong><span style="width: 100px; float: left;">Flight Date </span>: {{ date('Y-m-d',strtotime($order->departure_date)) }}</strong><br>
                        </div>
                    </div>
                    <div class="col1">
                        <div class="flight-details">Passenger Details</div>
                        <div class="details">
                            <strong><span style="width: 100px; float: left;">Name </span>: {{ $order->first_name }} {{ $order->last_name }}</strong><br>
                            <strong><span style="width: 100px; float: left;">Seat No </span>: {{ $order->seat_number }}</strong><br>
                        </div>
                    </div>                       
                </div>
            </div>
            <div class="table-relative">
                <span class="pre">ITEMS</span>
                <table class="table table-striped" style="width: 100%">
                    <thead>
                        <tr>
                            <th width="10%">SI. No.</th>
                            <th width="10%">SKU</th>
                            <th width="30%">Description</th>
                            <th width="10%">Quantity</th>
                            <th width="10%">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<!--foreach($order->items as $itemkey => $item)
                        php
                        $price = $item->price_per_item;
                        $qty = number_format($item->qty,0);
                        endphp--> 
				        <tr>
				            <td>
				                <div class="product-name">
				                    <div class="product-num">1</div>
				                </div>
				            </td>
				            <td> {{ $order->item_code }} </td>
				            <td> {{ $order->item_description }} </td>
				            <td> {{ $order->quantity }} </td>
				            <td> {{ number_format(($order->amount_in_myr * $order->quantity),2) }} </td>
				        </tr>
				        <!--endforeach-->
					</tbody>
				</table>
	            <div class="row">
	                <div class="col-lg-12 remittance">
	                </div>
	            </div>
            	<div class="alert alert-danger alert-inbox" style="display: none; position: absolute; top: 40%;"></div>
            	<div class="loader-darkener"></div>
            	<div class="fa-spin dummy-loader"></div>
	            <div class="alert alert-danger alert-inbox" style="display: none; position: absolute; top: 40%;"></div>
	            <div class="loader-darkener"></div>
	            <div class="fa-spin dummy-loader"></div>
            </div>
    	</div>
    	@endforeach
        @endif
	</div>
</body>
</html>