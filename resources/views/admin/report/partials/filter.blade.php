<form method="get" action="" class="form-horizontal">
    <input type="hidden" name="search-filter" value="1">
	<div class="col-md-12">
						<div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('flight_date', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="flight_date">Flight Date</label>
                                    <div class="col-md-9">
                                        <input id="flight_date" name="flight_date" type="text" placeholder="" class="form-control select_date" value="{{ old('flight_date',$request->has('flight_date') != null ? $request->input('flight_date') : '') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('sales_date', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="sales_date">Sales Date</label>
                                    <div class="col-md-9">
                                        <input id="sales_date" name="sales_date" type="text" placeholder="" class="form-control select_date" value="{{ old('sales_date',$request->has('sales_date') != null ? $request->input('sales_date') : '') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('flight_type', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="flight_type">Flight Type : </label>
                                    <div class="col-md-9">
                                        {!! Form::select('flight_type', $f_type, old('flight_type') ? old('flight_type') : '', ['class' => 'form-control select2']) !!}
                                        <!-- <select name="flight_type" id="flight_type" class="form-control select2">
                                            <option value="">Select</option>
                                            <option value="all" {{ $request->input('flight_type') == 'all' ? 'selected' : ''  }}>All</option>
                                            <option value="domestic" {{ $request->input('flight_type') == 'domestic' ? 'selected' : ''  }}>Domestic</option>
                                            <option value="international" {{ $request->input('flight_type') == 'international' ? 'selected' : ''  }}>International</option>
                                        </select> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('increment_id', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="order_id">Order No. : </label>
                                    <div class="col-md-9">
                                        <input id="increment_id" name="increment_id" type="text" placeholder="" class="form-control" value="{{ old('increment_id',$request->has('increment_id') != null ? $request->input('increment_id') : '') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('flight_number', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="flight_no">Flight No. : </label>
                                    <div class="col-md-9">
                                        <input id="flight_number" onkeypress="return isNumberKey(event)" name="flight_number" type="text" placeholder="" class="form-control" value="{{ old('flight_number',$request->has('flight_number') != null ? $request->input('flight_number') : '') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('aoc', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="aoc">AOC : </label>
                                    <div class="col-md-9">
                                        {!! Form::select('carrier_code', $aoc, old('carrier_code') ? old('carrier_code') : '', ['class' => 'form-control select2']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->first('pnr', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="pnr">PNR : </label>
                                    <div class="col-md-9">
                                        <input id="pnr" name="pnr" type="text" placeholder="" class="form-control" value="{{ old('pnr',$request->has('pnr') != null ? $request->input('pnr') : '') }}">
                                    </div>
                                </div>
                            </div>
							<div class="col-md-4">
                                <div class="form-group {{ $errors->first('order_status', 'has--error') }}">
                                    <label class="col-md-3 control-label" for="order_status">Status : </label>
                                    <div class="col-md-9">
                                        {!! Form::select('order_status[]', $order_status, old('order_status') ? old('order_status') : '', ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="row">
							
                        </div>
					</div>
					
                    <div class="col-md-6">
                        <div class="form-actions pull-left ">
                            <button type="submit" class="btn btn-primary product_submit">Search</button>
                                        &nbsp;
                            <a type="reset" class="btn btn-default" value="Reset" onclick="reset();" href="{{ route('report.warehouse') }}">Reset</a>
                            &nbsp;
                            <a class="btn btn-success" href="{{ route('report.export',array_merge(request()->all())) }}">Export</a>
                        </div>
                    </div>
</form>
<script type="text/javascript">
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script> 