@inject('request', 'Illuminate\Http\Request')
@inject('StatusPresenter', 'App\Presenters\StatusPresenter')
@extends('layouts.app')

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    {{--<link href="{{ asset('css/tables.css') }}" rel="stylesheet" type="text/css" />--}}
    <link href="{{ asset('assets/vendors/sweetalert/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="sitemap" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Custom Reports
                    </h4>
                </div>
                <br />
                <div class="panel-body">
					<div class="row">
					@include('admin.report.partials.filter-customreport') 
					</div>
					<div class="col-md-12"><hr></div>
					<div class="row">
                        <div class="panel-body">
						<table class="table table-bordered table-striped table-hover dataTable" id="table">
							<thead>
							<tr>
                                <th width="5%" data-sortby="flight_number" data-sort="ASC" class="sort {{ $flight_no_css_class }}" data-class="{{ $flight_no_css_class }}">Flight No.</th>
    							<th width="12%" data-sortby="flight_date" data-sort="ASC" class="sort {{ $flight_date_css_class }}" data-class="{{ $flight_date_css_class }}">Flight Date</th>
    					       	<th width="1%">Actions</th>
							</tr>
							</thead>
							<tbody>
                            @if($order_result)
                                       @foreach($order_result->rows as $group )
                                       @php
                                           $flight_number = $group->carrier_code.$group->flight_number;
                                           $flight_date = empty($group->flight_date) ? '' : date_format(date_create($group->flight_date),"Y/m/d");
                                       @endphp
                                       <tr>
                                           <td>{{ $flight_number }}</td>
                                           <td>{{ $flight_date }}</td>
                                           <td>
                                                <div class="text-right">
                                                    <a class="btn btn-primary" target="_blank" href="{{ route('customreport.export',array_merge(['flight_date' => $flight_date,'flight_no' => $group->flight_number,'carrier_code' => $group->carrier_code,'departure_station' =>$group->departure_station, 'arrival_station' => $group->arrival_station,'seller_id' => $group->seller_id ])) }}" data-toggle="tooltip" data-placement="top" data-title="Download PDF" ><i class="glyphicon glyphicon-circle-arrow-down"></i></a> 
                                                </div>
                                           </td>
                                       </tr>
                                       @endforeach
                                @endif           
                            </tbody>
						</table>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                @if($order_result)
                                <?php
                                $list_class = 'pagination pagination';
                                $this->_total = $order_result->total;
                                $this->_limit = $order_result->limit;
                                $this->_page = $request->input('page') ? $request->input('page') : 1;
                                $links = $order_result->limit;
                                $this->_row_start = ( ( $this->_page - 1 ) * $this->_limit );
                                //return empty result string, no links necessary
                                if ( $order_result->limit == 'all' ) {
                                    return '';
                                }

                                //get the last page number
                                $last = ceil( $this->_total / $this->_limit );
                                
                                //calculate start of range for link printing
                                $start = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
                                
                                //calculate end of range for link printing
                                $end = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;
                                
                                //debugging
                                // echo '$total: '.$this->_total.' | '; //total rows
                                // echo '$row_start: '.$this->_row_start.' | '; //total rows
                                // echo '$limit: '.$this->_limit.' | '; //total rows per query
                                // echo '$start: '.$start.' | '; //start printing links from
                                // echo '$end: '.$end.' | '; //end printing links at
                                // echo '$last: '.$last.' | '; //last page
                                // echo '$page: '.$this->_page.' | '; //current page
                                // echo '$links: '.$links.' <br /> '; //links 

                                //ul boot strap class - "pagination pagination-sm"
                                $html = '<ul class="' . $list_class . '">';

                                $class = ( $this->_page == 1 ) ? "disabled" : ""; //disable previous page link <<<
                                
                                //create the links and pass limit and page as $_GET parameters

                                //$this->_page - 1 = previous page (<<< link )
                                if($this->_page == 1){
                                    $html .= '<a href=""><li class="' . $class . '">&laquo;</a></li>';
                                } else {
                                    $request->request->add(['page' => ($this->_page - 1)]);
                                    $html .= '<li class="' . $class . '"><a href="' . route('customreport.index',array_merge(request()->all())) . '">&laquo;</a></li>';
                                }

                                if ( $start > 1 ) { //print ... before (previous <<< link)
                                    $request->request->add(['page' => 1]);
                                    $html .= '<li><a href="'.route('customreport.index',array_merge(request()->all())).'">1</a></li>'; //print first page link
                                    $html .= '<li class="disabled"><span>...</span></li>'; //print 3 dots if not on first page
                                }

                                //print all the numbered page links
                                for ( $i = $start ; $i <= $end; $i++ ) {
                                    $request->request->add(['page' => $i]);
                                    $class = ( $this->_page == $i ) ? "active" : ""; //highlight current page
                                    $html .= '<li class="' . $class . '"><a href="' . route('customreport.index',array_merge(request()->all())) . '">' . $i . '</a></li>';
                                }

                                if ( $end < $last ) { //print ... before next page (>>> link)
                                    $request->request->add(['page' => $last]);
                                    $html .= '<li class="disabled"><span>...</span></li>'; //print 3 dots if not on last page
                                    $html .= '<li><a href="' . route('customreport.index',array_merge(request()->all())) . '">' . $last . '</a></li>'; //print last page link
                                }

                                $class = ( $this->_page == $last ) ? "disabled" : ""; //disable (>>> next page link)
                                
                                //$this->_page + 1 = next page (>>> link)
                                if($this->_page == $last){
                                    $html .= '<a href=""><li class="' . $class . '">&raquo;</a></li>';
                                } else {
                                    $request->request->add(['page' => ($this->_page + 1)]);
                                    $html .= '<li class="' . $class . '"><a href="' . route('customreport.index',array_merge(request()->all())) . '">&raquo;</a></li>';
                                }

                                $html .= '</ul>';

                                echo $html;
                                ?>
                                @endif
                            </div>
                        </div>
                        
                        </div>
					</div>
                    
                </div>
            </div>
        </div>    <!-- row-->
    </section>
@stop

{{-- page level scripts --}}
@section('javascript')
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>--}}
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/moment.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/airasia_orders.js') }}"></script>

    @include('layouts.flash')
@stop

