@inject('request', 'Illuminate\Http\Request')
@inject('StatusPresenter', 'App\Presenters\StatusPresenter')
@extends('layouts.app')

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    {{--<link href="{{ asset('css/tables.css') }}" rel="stylesheet" type="text/css" />--}}
    <link href="{{ asset('assets/vendors/sweetalert/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Order Detail</h1>
        <ol class="breadcrumb">
            <li>
                <a href="">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li><a href="#"> Reports</a></li>
            <li class="active">Order Detail</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
                <div class="panel-body">
                    <div class="row">
                    @if($order_result)
                    @foreach($order_result->rows as $key => $order)
                    <div class="panel panel-primary ">
                        <div class="panel-heading clearfix">
                            <h4 class="panel-title pull-left"> <i class="livicon" data-name="sitemap" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                Order no : {{ $order->increment_id }}
                            </h4>
                        </div>
                        <br />
                        <div class="panel-body">
                            <div class="row">
                                <div class="panel-body">
                                <table class="table table-bordered " id="table">
                                    <tr>
                                        <th>Order No.</th>
                                        <td>{{ $order->increment_id }}</td>
                                    </tr>
                                    <tr>
                                        <th>PNR</th>
                                        <td>{{ $order->pnr }}</td>
                                    </tr>
                                    <tr>
                                        <th>Flight</th>
                                        <td>{{ $order->carrier_code }}{{ $order->flight_number }}&nbsp;&nbsp;&nbsp;{{ $order->departure_station }} - {{ $order->arrival_station }}</td>
                                    </tr>
                                    <tr>
                                        <th>Sales Date</th>
                                        <td>{{ date('Y-m-d',strtotime($order->sales_date)) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Flight Date</th>
                                        <td>{{ date('Y-m-d',strtotime($order->flight_date)) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Recipient Name</th>
                                        <td>{{ $order->first_name }} {{ $order->last_name }}</td>
                                    </tr>                
                                </table>
                                <hr>
                                <table class="table table-bordered " id="table">
                                    <thead>
                                    <tr class="filters">
                                        <th>Sku</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($order->items as $key => $item)
                                    @php
                                    $total_price = $item->final_discounted_price;
                                    $qty = number_format($item->qty,0);
                                    @endphp    
                                    <tr>
                                        <td>{{ $item->product_sku }}</td>
                                        <td>{{ $item->product_name }}</td>
                                        <td>{{ number_format($item->price_per_item,2) }}</td>
                                        <td> {{ number_format($item->qty,0) }} </td>
                                        <td>{{ number_format(($total_price * $qty),2) }}</td>
                                        
                                    </tr>
                                    @endforeach 
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    @endforeach
                    @endif
                    </div>
                </div>
        </div>    <!-- row-->
        <!-- row-->
    </section>
@stop

{{-- page level scripts --}}
@section('javascript')
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>--}}
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/moment.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script>
        $(function() {

          $('.select_date').daterangepicker({
              autoUpdateInput: false,
              locale: {
                  cancelLabel: 'Clear'
              }
          });

          $('.select_date').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
          });

          $('.select_date').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });

        });
    </script>



    @include('layouts.flash')
@stop

