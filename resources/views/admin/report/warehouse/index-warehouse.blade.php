@inject('request', 'Illuminate\Http\Request')
@inject('StatusPresenter', 'App\Presenters\StatusPresenter')
@extends('layouts.app')

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    {{--<link href="{{ asset('css/tables.css') }}" rel="stylesheet" type="text/css" />--}}
    <link href="{{ asset('assets/vendors/sweetalert/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
    <!--<section class="content-header">
        <h1>Warehouse Reports</h1>
        <ol class="breadcrumb">
            <li>
                <a href="">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li><a href="#"> Reports</a></li>
            <li class="active">Warehouse Reports</li>
        </ol>
    </section>-->

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="sitemap" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Warehouse Reports
                    </h4>
                </div>
                <br />
                <div class="panel-body">
					<div class="row">
					@include('admin.report.partials.filter') 
					</div>
					<div class="col-md-12"><hr></div>
					<div class="row">
                        <div class="panel-body">
						<table class="table table-bordered table-striped table-hover sortable dataTable" id="table">
							<thead>
							<tr>
                                <th width="5%" data-sortby="order_id" data-sort="ASC" class="sort {{ $order_css_class }}" data-class="{{ $order_css_class }}">Order No.</th>
								<th width="5%" data-sortby="pnr" data-sort="ASC" class="sort {{ $pnr_css_class }}" data-class="{{ $pnr_css_class }}">PNR</th>
								<th width="5%" data-sortby="flight_number" data-sort="ASC" class="sort {{ $flight_no_css_class }}" data-class="{{ $flight_no_css_class }}">Flight No.</th>
								<th width="10%" data-sortby="sales_date" data-sort="ASC" class="sort {{ $sales_date_css_class }}" data-class="{{ $sales_date_css_class }}">Sales Date</th>
								<th width="10%" data-sortby="flight_date" data-sort="ASC" class="sort {{ $flight_date_css_class }}" data-class="{{ $flight_date_css_class }}">Flight Date</th>
								<th width="20%">Recipient Name</th>
								<th width="10%" data-sortby="order_status" data-sort="ASC" class="sort {{ $status_css_class }}" data-class="{{ $status_css_class }}">Status</th>
								<th width="15%">Actions</th>
							</tr>
							</thead>
							<tbody>
                            @if($order_result)
                            @php
                            $orderId = '';
                            $pnr = '';
                            $flight = '';
                            $sales_date = '';
                            $flight_date = '';
                            $recipient = '';
                            $status = '';
                            @endphp
                            @foreach($order_result->rows as $key => $order)
                            @php
                            $orderId        = $order->order_id;
                            $increment_id   = $order->increment_id;
                            $pnr            = isset($order->pnr) ? $order->pnr : '';
                            $flight         = (isset($order->carrier_code) ? $order->carrier_code : '' ) . (isset($order->flight_number) ? $order->flight_number : '' );
                            $sales_date     = date_format(date_create($order->sales_date),"m/d/Y");
                            $flight_date    = isset($order->flight_date) ? date_format(date_create($order->flight_date),"m/d/Y") : '';
                            $recipient      = (isset($order->first_name) ? $order->first_name : '' ) . (isset($order->last_name) ? $order->last_name : '' );
                            $status         = $StatusPresenter->getStatusDesc($order->order_status)
                            @endphp
                            <tr>
                                <td>{{ $increment_id }}</td>
                                <td>{{ $pnr }}</td>
                                <td>{{ $flight }}</td>
                                <td>{{ $sales_date }}</td>
                                <td>{{ $flight_date }}</td>
                                <td>{{ $recipient }}</td>
                                <td>{{ $status }}</td>
                                <td>
                                    <div class="text-right">
                                        <a class="btn btn-primary" target="_blank" href="{{ route('report.warehouse.paxslip',$order->order_id) }}" data-toggle="tooltip" data-placement="top" data-title="rePrint pax slip" ><i class="fa fa-print"></i></a> 
                                        <a class="btn btn-warning" href="{{ route('report.warehouse.detail',$order->order_id) }}" data-toggle="tooltip" data-placement="top" data-title="View order" ><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-success" data-toggle="modal" data-target="#myModal-{{ $order->order_id }}" data-placement="top" data-title="Update status"><i class="fa fa-exchange"></i></a>
                                    </div>
                                    <!-- Modal notify winner -->
                                    <div class="modal fade" class="myModal" id="myModal-{{ $order->order_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <form method="post" action="{{ route('report.warehouse.update-status',$order->order_id) }}" class="form-horizontal" id="update-status-{{ $order->order_id }}">
                                        {{ csrf_field() }}
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Update - Order ID : {{ $order->order_id }}</h4>
                                            
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                                <label class="col-lg-3 control-label">Status</label>
                                                <div class="col-lg-9">   
                                                    {!! Form::select('update_status', $order_status,$order->order_status, ['class' => 'form-control', 'width' => '100%' ]) !!}
                                                </div>
                                            </div>
                                            &nbsp;
                                            <div class="row">
                                                <label class="col-md-3 control-label" for="order_status">Comment : </label>
                                                <div class="col-md-9">
                                                    <textarea name="comment" id="comment" rows="10" class="form-control"></textarea>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary" id="submitUpdate" data-id="{{ $order->order_id }}">Submit</button>
                                          </div>
                                        </div>
                                        </form>
                                      </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @endif           
                            </tbody>
						</table>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                @if($order_result)
                                <?php
                                $list_class = 'pagination pagination';
                                $this->_total = $order_result->total;
                                $this->_limit = $order_result->limit;
                                $this->_page = $request->input('page') ? $request->input('page') : 1;
                                $links = $order_result->limit;
                                $this->_row_start = ( ( $this->_page - 1 ) * $this->_limit );
                                //return empty result string, no links necessary
                                if ( $order_result->limit == 'all' ) {
                                    return '';
                                }

                                //get the last page number
                                $last = ceil( $this->_total / $this->_limit );
                                
                                //calculate start of range for link printing
                                $start = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
                                
                                //calculate end of range for link printing
                                $end = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;
                                
                                //debugging
                                // echo '$total: '.$this->_total.' | '; //total rows
                                // echo '$row_start: '.$this->_row_start.' | '; //total rows
                                // echo '$limit: '.$this->_limit.' | '; //total rows per query
                                // echo '$start: '.$start.' | '; //start printing links from
                                // echo '$end: '.$end.' | '; //end printing links at
                                // echo '$last: '.$last.' | '; //last page
                                // echo '$page: '.$this->_page.' | '; //current page
                                // echo '$links: '.$links.' <br /> '; //links 

                                //ul boot strap class - "pagination pagination-sm"
                                $html = '<ul class="' . $list_class . '">';

                                $class = ( $this->_page == 1 ) ? "disabled" : ""; //disable previous page link <<<
                                
                                //create the links and pass limit and page as $_GET parameters

                                //$this->_page - 1 = previous page (<<< link )
                                if($this->_page == 1){
                                    $html .= '<a href=""><li class="' . $class . '">&laquo;</a></li>';
                                } else {
                                    $request->request->add(['page' => ($this->_page - 1)]);
                                    $html .= '<li class="' . $class . '"><a href="' . route('report.warehouse',array_merge(request()->all())) . '">&laquo;</a></li>';
                                }

                                if ( $start > 1 ) { //print ... before (previous <<< link)
                                    $request->request->add(['page' => 1]);
                                    $html .= '<li><a href="'.route('report.warehouse',array_merge(request()->all())).'">1</a></li>'; //print first page link
                                    $html .= '<li class="disabled"><span>...</span></li>'; //print 3 dots if not on first page
                                }

                                //print all the numbered page links
                                for ( $i = $start ; $i <= $end; $i++ ) {
                                    $request->request->add(['page' => $i]);
                                    $class = ( $this->_page == $i ) ? "active" : ""; //highlight current page
                                    $html .= '<li class="' . $class . '"><a href="' . route('report.warehouse',array_merge(request()->all())) . '">' . $i . '</a></li>';
                                }

                                if ( $end < $last ) { //print ... before next page (>>> link)
                                    $request->request->add(['page' => $last]);
                                    $html .= '<li class="disabled"><span>...</span></li>'; //print 3 dots if not on last page
                                    $html .= '<li><a href="' . route('report.warehouse',array_merge(request()->all())) . '">' . $last . '</a></li>'; //print last page link
                                }

                                $class = ( $this->_page == $last ) ? "disabled" : ""; //disable (>>> next page link)
                                
                                //$this->_page + 1 = next page (>>> link)
                                if($this->_page == $last){
                                    $html .= '<a href=""><li class="' . $class . '">&raquo;</a></li>';
                                } else {
                                    $request->request->add(['page' => ($this->_page + 1)]);
                                    $html .= '<li class="' . $class . '"><a href="' . route('report.warehouse',array_merge(request()->all())) . '">&raquo;</a></li>';
                                }

                                $html .= '</ul>';

                                echo $html;
                                ?>
                                @endif
                            </div>
                        </div>
                        
                        </div>
					</div>
                    
                </div>
            </div>
        </div>    <!-- row-->
    </section>
@stop

{{-- page level scripts --}}
@section('javascript')
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>--}}
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/moment.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/airasia_orders.js') }}"></script>

    @include('layouts.flash')
@stop

