@extends('emails.app')
@section('title', 'Merchant Notice') 
@section('content') 
<div class="container">
    <div class="row"> 
        <div class="col-md-8 col-md-offset-2"> 
             <div> 
                <p> 
                    Merchant Notification
                    <br>
                    ---------------------------
                </p>
				<p> 
                    Dear {{ $merchant }}
                </p>
				<p> 
                     You have order coming in. 
                </p>
                <div class="box-body">
                    <dl class="dl-horizontal">
                    <dt>Reference No:</dt>
                    <dd>{{ $order->ReferenceNo }}</dd>
                    <dt>Order Number:</dt>
                    <dd>{{ $order->OrderNumber }}</dd>
                    @foreach($order->order_detail as $key => $customers)
                    @if($customers->product_merchant)      
                        <dt>Order by :</dt>
                        <dd>{{ $customer_info->name }}</dd>
                        <dt>Email :</dt>
                        <dd>{{ $customer_info->email }}</dd>
                        <dt>Phone :</dt>
                        <dd>{{ $customer_info->shipping_phone }}</dd>
                    @endif
                    @endforeach 
                    </dl>
                </div>
				<table class="table">
                    <tr>
                        <th class="col-md-3">Name</th>
                        <th class="col-md-1">Quantity</th>
                        <th class="col-md-1">Unit Price</th>
                        <th class="col-md-2">Subtotal</th>
                    </tr>
                    @foreach($order->order_detail as $key => $products)
                        @php
                        $subTotal = $products['price'] * $products['quantity']
                        @endphp
                        @if($products->product_merchant)
                        <tr>
                            <td>{{ $products->product_merchant['name_en'] }}</td>
                            <td>{{ $products['quantity'] }}</td>
                            <td>{{ $products['price'] }}</td>
                            <td>{{ number_format($subTotal, 2) }}</td>
                        </tr>
                    @endif
                    @endforeach
                </table> 
             </div> 
        </div>
    </div>
</div>  
@endsection 