@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu" style="padding-top:15px">

            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a class="sidebar-menu-tab" href="{{ url('/') }}">
                    <i class="fa fa-home sidebar-icon-hover"></i>
                    <span class="title parent-title">@lang('global.app_dashboard')</span>
                </a>
            </li>
            @can('manage_accounts')
            <li class="treeview">
                <a class="sidebar-menu-tab" href="#">
                    <i class="fa fa-user-circle-o sidebar-icon-hover"></i>
                    <span class="title parent-title">@lang('global.user-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                        <a  class="sidebar-menu-childtab" href="{{ route('admin.users.index') }}">
                            <i class="fa fa-users sidebar-icon-hover"></i>
                            <span class="title">
                                Users
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a class="sidebar-menu-childtab" href="{{ route('admin.roles.index') }}">
                            <i class="fa fa-user-md sidebar-icon-hover"></i>
                            <span class="title">
                                Roles
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                        <a class="sidebar-menu-childtab" href="{{ route('admin.permissions.index') }}">
                            <i class="fa fa-unlock-alt sidebar-icon-hover"></i>
                            <span class="title">
                                Permissions
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            @endcan
            @can('manage_reports')
            <li class="treeview">
                <a class="sidebar-menu-tab" href="#">
                    <i class="fa fa-bar-chart-o sidebar-icon-hover"></i>
                    <span class="title parent-title">Reports</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $request->segment(2) == 'warehouse' ? 'active active-sub' : '' }}">
                        <a class="sidebar-menu-childtab" href="{{ route('report.warehouse') }}">
                            <i class="fa fa-list-alt sidebar-icon-hover"></i>
                            <span class="title">
                                Warehouse Reports
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'orders' ? 'active active-sub' : '' }}">
                        <a class="sidebar-menu-childtab" href="{{ route('aa_orders.index') }}">
                            <i class="glyphicon glyphicon-shopping-cart sidebar-icon-hover"></i>
                            <span class="title">Airasia Orders</span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(1) == 'wreport2' ? 'active active-sub' : '' }}">
                        <a class="sidebar-menu-childtab" href="{{ route('wreport2.warehouse') }}">
                            <i class="glyphicon glyphicon-list sidebar-icon-hover"></i>
                            <span class="title">New Warehouse Reports</span>
                        </a>
                    </li>
                    @if(!Auth::user()->hasRole('bigdutyfree_seller'))
                    <li class="{{ $request->segment(1) == 'customreport' ? 'active active-sub' : '' }}">
                        <a class="sidebar-menu-childtab" href="{{ route('customreport.index') }}">
                            <i class="glyphicon glyphicon-blackboard sidebar-icon-hover"></i>
                            <span class="title">Custom Report</span>
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @endcan
            @can('manage_accounts','manage_reports')
            <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                <a class="sidebar-menu-tab" href="{{ route('auth.change_password') }}">
                    <i class="fa fa-key sidebar-icon-hover"></i>
                    <span class="title parent-title">Change password</span>
                </a>
            </li>
            @endcan
        </ul>
    </section>
    <div id="side-footer" style="color:#253135;text-align:center;font-weight:bold;padding:2px"></div>
</aside>

{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('global.logout')</button>
{!! Form::close() !!}

