<?php

	return [

		'address' =>[
			'AK_D7'=>'<p class="company-name">AirAsia Berhad (284669-W)</p>
                        <p class="details">REDQ, Jalan Pekeliling 5, Lapangan Terbang<br>Antarabangsa Kuala Lumpur (KLIA2)<br>
                        64000 Sepang, Selangor, Malaysia<br>
                        Tel no: 03- 8660 4333   Fax no: 03- 8660 7777<br>
                        GST Reg. No. : 001062207488
                        </p>',
			'I5'=>'<p class="company-name">AirAsia India</p>
                        <p class="details">Alpha 3 building, Ground Floor<br>
                        Kempagowda International airport, Devanahalli<br>
                        Bangalore , Karnataka - 560300<br>
                        CIN: U62200KA2013PLC086204<br>
						GST No: 29AALCA4699P1ZJ<br>
						PAN NO. AALCA4699P<br>
                        </p>',
			'Z2'=>'<p class="company-name">PHILIPPINES AIRASIA INC.</p>
                        <p class="details">Facility 7233 General Aviation CSEZ<br>
                        Clarkfield Pampanga, Philippines<br>
                        VAT REG. TIN: 005-059-838-0000
                        </p>',
			'QZ_XT'=>'<p class="company-name">PT Jasa Raharja (Persero)</p>
                        <p class="details">Mr. H. Dedy Sudrajat, SH, MM.<br>
                        Jalan Kali Besar Timur No. 10<br>
                        Jakarta 11110<br>
                        Indonesia<br>
                        </p>'
		]
	];