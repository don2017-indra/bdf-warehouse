<?php

namespace App\Presenters;
use \App\Http\Controllers\Agent\AgentReportsController;

class BookingApiPresenter
{  

	public function getFlight($pnr){
        $flights = new AgentReportsController;
        $flightData = $flights->getFlight($pnr) ;
        $html = '<select name="flight_detail" id="flight_detail" class="form-control" width="100%">';
        
        if($flightData) {
            //print_r($flightData);
            foreach ($flightData as $key => $flight) {
                 //foreach ($flight_data as $key => $flight) {
                    $date = date_create($flight['departure_date']);
                    $departure_date = date_format($date,'d M');
                    $item = $flight['departure'].'-'.$flight['arrival'].' '.$departure_date;
                    $params = $flight['bookingId'].'|'.$flight['flight_date'].'|'.$flight['std'].'|'.$flight['sta'].'|'.$flight['departure'].'|'.$flight['arrival'].'|'.$flight['carrier_code'].'|'.$flight['flight_type'].'|'.$flight['email'];
                     $html .= '<option value="'.$params.'">'.$item.'</option>';
                 //}
                
            } 
        }

        $html .= '</select>';
        return $html;
	}

    public function getPassenger($pnr){
        $paxs = new AgentReportsController;
        $pax_data = $paxs->getPassenger($pnr);

        return $pax_data;
    }

}