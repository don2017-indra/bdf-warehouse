<?php

namespace App\Presenters;
 
class StatusPresenter
{ 
	public function getStatusDesc($type){
		
        switch ($type) {
            case "paid":
                $desc = "Paid";
                break;
          case "reserved":
                $desc = "Reserved";
                break;
            case "reprint_pack_slip":
                $desc = "Re-print Packing Slip";
                break;
            case "cart_loaded":
                $desc = "Loaded on Cart";
                break;
            case "delivered":
                $desc = "Delivered";
                break;
            case "not_delivered":
                $desc = "Not Delivered";
                break;
            case "order_cancelled":
                $desc = "Order Cancelled";
                break;
            case "canceled":
                $desc = "Canceled";
                break;
            case "req_refund":
                $desc = "Requested Refund";
                break;
            case "processing":
                $desc = "Processing";
                break;
            case "partial_complete":
                $desc = "Partial Complete";
                break;
            case "complete":
                $desc = "Complete";
                break;
            case "ready_packing":
                $desc = "Ready for packing";
                break; 
            case "partial_canceled":
                $desc = "Partial Canceled";
                break;
            case "canceled":
                $desc = "Canceled";
                break;  
            default:
                $desc = "Paid";
                break;
        }

        return $desc;
	}

    public function getNextOrderStatus($status) {

        $statuses = [
            'delivered' => 'Delivered',
            'ready_packing' => 'Ready for packing',
            'reserved' => 'Reserved',
            'reprint_pack_slip' => 'Re-print Packing Slip',
            'cart_loaded' => 'Loaded on Cart',
            'not_delivered' => 'Not Delivered',
            'redeliver' => 'Redelivery Request',
            'order_cancelled' => 'Order Cancelled',
            'req_refund' => 'Requested Refund'
        ];

        $next = [];
        switch ($status) {
            case 'complete': 
            case 'delivered': 
            case 'req_refund':
                return $next;
                break;

            case 'cart_loaded':
                $next = [
                    'delivered',
                    'order_cancelled',
                    'not_delivered',
                    'req_refund'
                ];
                break;

            case 'paid':
            case 'ready_packing':
            case 'reserved':
            case 'reprint_pack_slip':

                $next = [
                    'ready_packing',
                    'reserved',
                    'reprint_pack_slip',
                    'cart_loaded',
                    'order_cancelled',
                    'not_delivered'
                ];

                break;

            case 'not_delivered':
                $next = ['redeliver'];

            case 'order_cancelled':
                $next[] = 'req_refund';
                break;

            default:
                $next = [];
        }

        return array_intersect_key($statuses, array_flip($next));
    }
}