<?php
namespace App;

use App\Model\Merchant;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Hash;
use App\Notifications\SellerResetPasswordNotif;

/**
 * Class Seller
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
*/
class Seller extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $fillable = ['name', 'email', 'password', 'remember_token'];
    protected $guard_name = 'seller';
    protected $table = 'users';
    
    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function accounts()
    {
        return $this->hasOne(UserSellers::class, 'user_id', 'id');
    }

    //Send password reset notification
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new SellerResetPasswordNotif($token));
    }
    
}
