<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class ProductAttributeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $form_type = $this->form_type;

        if($form_type=='dropdown'){
            return [
                'variant_name' => 'required',
                'form_type' =>'required',

            ];
        }else if($form_type=='textfield'){
            return [
                'variant_name' => 'required',
                'form_type' =>'required',
                'max_length' =>'required',
                'min_length' =>'required'
            ];
        }else{
            return [
                'variant_name' => 'required',
                'form_type' =>'required',
            ];
        }

    }
}
