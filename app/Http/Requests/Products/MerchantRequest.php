<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MerchantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //echo $this->merchant_id;exit;
        if($this->hasFile('vendor_banner_img')){
            return [
                'vendor_name' => 'required',
                'vendor_code' => 'required|unique:vendor_info,vendor_code,'.$this->merchant_id,
                'vendor_description' => 'required',
                'tnc' => 'required',
                'refund_policy' => 'required',
                'shipping_domestic' => 'required',
                'shipping_international' => 'required',
                //'vendor_banner_img' => 'required|image|mimes:jpeg,bmp,png,jpg|dimensions:max_width=600,max_height=224,min_width=600,min_height=224|max:100'

            ];
        }else{
            return [
                'vendor_name' => 'required',
                'vendor_code' => 'required|unique:vendor_info,vendor_code,'.$this->merchant_id,
                'vendor_description' => 'required',
                'tnc' => 'required',
                'refund_policy' => 'required',
            ];
        }

    }

    public function messages()
    {
        return [
            'size' => 'One of the image size bigger than 100kb. Please reduce the image size',
            'vendor_banner_img.dimensions' => 'Merchant Banner Image must be 600px X 224px dimensions'
        ];
    }
}
