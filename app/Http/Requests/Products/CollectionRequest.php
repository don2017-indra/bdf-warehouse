<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class CollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('patch')) {
            return [
                'name' => 'required',
                //'playlist_image' => 'required|image|mimes:jpeg,bmp,png,jpg|dimensions:max_width=600,max_height=307,min_width=600,min_height=307|max:100'
            ];
        } else {
            return [
                'name' => 'required',
                'product_id.*' => 'required',
                'collection_image' => 'required|image|mimes:jpeg,bmp,png,jpg|dimensions:max_width=600,max_height=304,min_width=600,min_height=304|max:100'
            ];
        }
        
    }

    public function messages()
    {
        return [
            'size' => 'Image size bigger than 100kb. Please reduce the image size',
            'collection_image.dimensions' => 'Please use image with 600px X 304px dimensions',
        ];
    }
}
