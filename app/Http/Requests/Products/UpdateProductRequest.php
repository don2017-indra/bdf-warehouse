<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->hasFile('product_image') && $this->hasFile('gallery_image')){
            return [
                'sku' => 'required',
                'name_en' => 'required',
                'description_en' => 'required',
                'category_id' => 'required',
                'sub_category_id' => 'required',
                'delivery' => 'required',
                'type' => 'required',
                'product_price' => 'required',
                'product_sale_price' => 'required',
                'quantity' => 'required',
                'product_image' => 'required|image|mimes:jpeg,bmp,png,jpg|dimensions:max_width=600,max_height=480,min_width=600,min_height=480|max:100',
                'gallery_image.*' => 'required|image|mimes:jpeg,bmp,png,jpg|dimensions:max_width=600,max_height=480,min_width=600,min_height=480|max:100'

            ];
        }else if($this->hasFile('product_image')){
            return [
                'sku' => 'required',
                'name_en' => 'required',
                'description_en' => 'required',
                'category_id' => 'required',
                'sub_category_id' => 'required',
                'delivery' => 'required',
                'type' => 'required',
                'product_price' => 'required',
                'product_sale_price' => 'required',
                'quantity' => 'required',
                'product_image' => 'required|image|mimes:jpeg,bmp,png,jpg|dimensions:max_width=600,max_height=480,min_width=600,min_height=480|max:100',

            ];
        }else if($this->hasFile('gallery_image')){
            return [
                'sku' => 'required',
                'name_en' => 'required',
                'description_en' => 'required',
                'category_id' => 'required',
                'sub_category_id' => 'required',
                'delivery' => 'required',
                'type' => 'required',
                'product_price' => 'required',
                'product_sale_price' => 'required',
                'quantity' => 'required',
                'gallery_image.*' => 'required|image|mimes:jpeg,bmp,png,jpg|dimensions:max_width=600,max_height=480,min_width=600,min_height=480|max:100'

            ];
        }else{
            return [
                'sku' => 'required',
                'name_en' => 'required',
                'description_en' => 'required',
                'category_id' => 'required',
                'sub_category_id' => 'required',
                'delivery' => 'required',
                'type' => 'required',
                'product_price' => 'required',
                'product_sale_price' => 'required',
                'quantity' => 'required',
            ];
        }

    }

    public function messages()
    {
        return [
            'max' => 'Image size bigger than 100kb. Please reduce the image size',
            'product_image.dimensions' => 'Product image must be 600px X 480px dimensions',
            'gallery_image.dimensions'  => 'Gallery images must be 600px X 480px dimensions',
        ];
    }
}
