<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class ProductCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $form_type = $this->cat_type;

        if($form_type=='maincategory'){
            return [
                'name' => 'required',
                'icon_image' => 'required|image|mimes:jpeg,bmp,png,jpg|dimensions:max_width=114,max_height=92,min_width=114,min_height=92|max:100'
            ];
        } else {
            return [
                'name' => 'required'
            ];
        }

        
    }

     public function messages()
    {
        return [
            'size' => 'Image size bigger than 100kb. Please reduce the image size',
            'icon_image.dimensions' => 'Please use image with 114px X 92px dimensions',
        ];
    }
}
