<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use App\Helpers\WarehousePortalHelper;
use App\Helpers\APIHelper;
use App\Helpers\OrderHelper;
use App\Magento\BDFMplaceOrderStatus as bdf_mplace_order_status;
use App\Magento\SalesOrder as sales_order;
use App\Magento\MagentoCustomercustomattributesSalesFlatOrderAddress as mcsfoa;
use Mail;
use Config;
use DB;
use Excel;
use DateTime;
use Carbon;

class WarehouseReports2Controller extends Controller
{
	private $wh_helper;
	
	public function __construct()
	{
		$this->middleware('auth');
   		$this->middleware('warehouse');
   		$this->wh_helper = new WarehousePortalHelper;
	}

	public function index(Request $request)
	{
	    if(Auth::user()->hasRole('administrator'))
        {
            $this->wh_helper->setOrderStatusSearch('processing','Processing');
            $this->wh_helper->setOrderStatusSearch('partial_complete','Partial Complete');
            $this->wh_helper->setOrderStatusSearch('complete','Complete');
        }

        $order_status_search = $this->wh_helper->order_status_search;
	    $update_statuses = $this->wh_helper->update_statuses;
	    $aoc = $this->wh_helper->aoc;
	    $f_type = $this->wh_helper->f_type;
	     
		$order_result = $this->filterField($request);

		$s_start_date = null;
		$s_end_date = null;

		if($request->has('sales_date')){
			$s_date = explode(' - ', $request->input('sales_date'));
			$s_start_date = date('d',strtotime($s_date[0]));
			$s_end_date = date('d',strtotime($s_date[1]));
		}

		if($order_result == FALSE && !empty($order_result)){

			return view('admin.report.warehouse2.index-warehouse', 
				compact(
					'order_result',
					'order_status_search',
					'aoc',
					'f_type',
					'update_statuses',
					's_start_date',
					's_end_date'
				)
			)->withErrors('There was a problem loading orders. Please try again later and reload the page');
		}

		return view('admin.report.warehouse2.index-warehouse', 
			compact(
				'order_result',
				'order_status_search',
				'aoc',
				'f_type',
				'update_statuses',
				's_start_date',
				's_end_date'
			)
		);
	}

	public function filterField($request)
	{
		$params = $this->getFilterInput($request);
		$apiHelper = new APIHelper;
		$result = $apiHelper->callApi(
			 env('NEW_ROKKISHOP_DOMAIN'),
			 'api/warehouse/order'.$params,
			 'GET',
			 [
			 	'headers' => [
					'X-PORTAL-TOKEN' => env('LUMEN_API_TOKEN')
				]
			 ]
		);

		return $result;
		
	}

	public function exportReport(Request $request) 
	{
	    ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');

        $order_result=[];
		$order_status_search = $this->wh_helper->order_status_search;
	    $update_statuses = $this->wh_helper->update_statuses;
	    $aoc = $this->wh_helper->aoc;
	    $f_type = $this->wh_helper->f_type;
		$bigItems = [];

		$order_result = $this->filterField($request);
		
		if(empty($order_result)){

			if($request->has('export') && $request->input('export') == TRUE){

				return redirect()->back()->withErrors('There is nothing to export');
			}

			return view('admin.report.warehouse2.index-warehouse', 
				compact(
					'order_result',
					'order_status_search',
					'aoc',
					'f_type',
					'update_statuses'
				)
			)->withErrors('There is nothing to export');

		} elseif($order_result == FALSE){

			if($request->has('export') && $request->input('export') == TRUE){

				return redirect()->back()->withErrors('Exporting Error: An internal error occured while trying to export orders. Please try again later');
			}
	
			return view('admin.report.warehouse2.index-warehouse', 
				compact(
					'order_result',
					'order_status_search',
					'aoc',
					'f_type',
					'update_statuses'
				)
			)->withErrors('Exporting Error: An internal error occured while trying to export orders. Please try again later');
		}

		$qty = 0;
		$count = 1;
		if($order_result){
			foreach($order_result as $key => $order) {

				$order_items = $this->getOrderItems($order->seller_id,$order->order_id);
				$PLP =  $order->paid_loyalty_points;	
				$paid =  $order->grand_total;
				$total_paid = $order->grand_total + $order->paid_loyalty_points;
				$big_points_redeemed = $order->big_points_total_redeemed;
				$big_points_earned = intval($order->big_points);
				$sales_date = date('Y-m-d H:i:s', strtotime('+8 hours', strtotime($order->sales_date)));

				
				if($request->has('sales_date')){
					$s_date = explode(' - ', $request->input('sales_date'));
					$s_start_date = date('d',strtotime($s_date[0]));
					$s_end_date = date('d',strtotime($s_date[1]));

					$raw_sales_day = date('d', strtotime('+8 hours', strtotime($order->sales_date)));
					if($raw_sales_day > $s_end_date) continue;
                	if($raw_sales_day < $s_start_date) continue;
				}

				foreach ($order_items as $key => $item) {
	
					$base_catalog_discount = $item->base_original_price - $item->base_price;
					$catalog_discount = $item->original_price - $item->price;
					$item_total_paid = ($item->row_total - $item->discount_amount) + $order->tax_amount;
					$base_total_paid =($item->base_row_total - $item->base_discount_amount) + $order->base_tax_amount;
					$qty = number_format($item->qty_ordered,0);
					$product_options = json_decode($item->product_options,true);

					$is_free_gift = isset($product_options['info_buyRequest']['is_free_gift']);

					$data = [
						'Order No' => $order->increment_id,
						'Carrier Code'=>$order->carrier_code,
						'Flight No' => $order->flight_number,
						'Departure' => $order->departure_station,
						'Arrival' => $order->arrival_station,
						'Sales Date' => date_format(date_create($sales_date),"Y/m/d"),
						'Flight Date' => date_format(date_create($order->flight_date),"Y/m/d"),
						'Flight Type' => $order->flight_type,
						'PNR' => $order->pnr,
						'Big Member ID' => $order->big_id,
						'Recipient Name' => $order->first_name.' '.$order->last_name,
						'Parent ID' =>  $item->parent_sku ?? null,
						'SKU' => $item->sku,
						'Manufacturer' =>  $item->manufacturer ?? null,
						'Category' => $item->categories,
						'Product Name' => $item->name,
						'Item Currency' => $order->base_currency_code,
						'Base Original Price' => floatval($item->base_original_price),
						'Base Catalog Discount' => $base_catalog_discount,
						'Base Final Price' => floatval($item->base_price),
						'Base Sub Total' => $item->base_row_total,
						'Paid Currency' => $order->order_currency_code,
						'Original Price' => floatval($item->original_price),
						'Final Price' => floatval($item->price),
						'Quantity' => intval($qty),
						'Sub Total' => $item->row_total,
						'Cart Discount' => $item->discount_amount,
						'Paid Loyalty Points' => $is_free_gift == false ? $PLP : '',
						'Paid' => $is_free_gift == false ? $paid : '' ,
						'Total Paid' => $total_paid,
						'Base Total Paid' => $base_total_paid,
						'Exchange Rate' => floatval($order->base_to_order_rate),
						'BIG Points Total Redeemed' => $is_free_gift == false ? $big_points_redeemed : '' ,
						'Big Points Total Amount Earned'=> $is_free_gift == false ? $big_points_earned : '',
						'Promo Code' => $order->coupon_code,
						'Status' => $this->getStatusDesc($item->seller_order_status),
						'Invoice' =>  $order->invoice_id ?? '0',
						'Invoice Date' =>  $order->invoice_date ?? null,
						'PGRN' => $order->pgrn ?? null
					];

					$PLP = '';
					$paid = '';
					$total_paid ='';
					$big_points_redeemed ='';
					$big_points_earned ='';

					array_push($bigItems, $data);
					$count++;
				}
			}
		}
		
		$filename = 'Warehouse_Report 2-'.date("m-d-Y");

		Excel::create($filename, function($excel) use($bigItems,$count) {

            $excel->sheet('Warehouse_Report', function($sheet) use($bigItems,$count) {

                $sheet->fromArray($bigItems);

            	$sheet->setColumnFormat(
            		array(
            				'A'.'1'.':'.'A'.$count => '000000000',
            				'R'.'1'.':'.'R'.$count => '0.00',
            				'S'.'1'.':'.'S'.$count => '0.00',
            				'T'.'1'.':'.'T'.$count => '0.00',
            				'U'.'1'.':'.'U'.$count => '0.00',
            				'W'.'1'.':'.'W'.$count => '0.00',
            				'X'.'1'.':'.'X'.$count => '0.00',
            				'Y'.'1'.':'.'Y'.$count => '0',
            				'Z'.'1'.':'.'Z'.$count => '0.00',
            				'AA'.'1'.':'.'AA'.$count => '0.00',
            				'AB'.'1'.':'.'AB'.$count => '0.00',
            				'AC'.'1'.':'.'AA'.$count => '0.00',
            				'AD'.'1'.':'.'AD'.$count => '0.00',
					        'AE'.'1'.':'.'AE'.$count => '0.00',
						)
            		); 	
            });

        })->download('xls');
	}

	public function getStatusDesc($type)
	{
		switch ($type) {
			case "paid":
                $desc = "Paid";
                break;
            case "reserved":
                $desc = "Reserved";
                break;
            case "reprint_pack_slip":
                $desc = "Re-print Packing Slip";
                break;
            case "cart_loaded":
                $desc = "Loaded on Cart";
                break;
            case "delivered":
                $desc = "Delivered";
                break;
            case "not_delivered":
                $desc = "Not Delivered";
                break;
            case "order_cancelled":
                $desc = "Order Cancelled";
                break;
            case "req_refund":
                $desc = "Requested Refund";
                break;
            case "processing":
           		$desc = "Processing";
            	break;
            case "partial_complete":
                $desc = "Partial Complete";
                break;
            case "complete":
                $desc = "Complete";
                break;
            case "ready_packing":
                $desc = "Ready for packing";
                break;   
            default:
                $desc = "Paid";
                break;
        }

        return $desc;
	}

	public function getFilterInput($request)
	{		
		$search = [];
		if(empty(array_filter($request->all())) || (count($request->all()) == 3 &&
			($request->has('sort') && $request->has('sort_order') && $request->has('export')))
		)
		{
			$start_date = '';
			$end_date = '';

			$now = Carbon::now();
			$yesterday = Carbon::now();
			
			$minus_8 = $now->subHours(8);
			$yesterday = $yesterday->subDay(1);

			if($minus_8->format('d') == $yesterday->format('d')){

				$dt = Carbon::now();
				$dt = $dt->subDay(1);

				$start_date = $dt->startOfDay()->format('Y/m/d');
				$end_date = $dt->endOfDay()->format('Y/m/d');

			} else {

				$result_now = Carbon::now();
				$start_date = $result_now->startOfDay()->format('Y/m/d');
				$end_date = $result_now->endOfDay()->format('Y/m/d');
			}

			$search['sales_date'] = $start_date.'-'.$end_date;
		}

		if($request->has('sort')){
			$search['sort'] = $request->input('sort');
		}
		
		if($request->has('sort_order')){
			$search['sort_order'] = $request->input('sort_order');
		}

		if($request->has('flight_date')){

			$f_date = explode(' - ', $request->input('flight_date'));
			
				$f_start_date = date("Y/m/d", strtotime($f_date[0]));
				$f_end_date = date("Y/m/d", strtotime($f_date[1]));
			
			$search['flight_date'] =  $f_start_date.'-'.$f_end_date;
		}

		if($request->has('sales_date')){

			$s_date = explode(' - ', $request->input('sales_date'));
			$s_start_date = date('Y/m/d H:i:s', strtotime('-8 hours', strtotime($s_date[0].' 00:00:00')));
			$s_end_date = date('Y/m/d H:i:s', strtotime('-8 hours', strtotime($s_date[1].' 23:59:59')));

			$search['sales_date'] = $s_start_date.'-'.$s_end_date;
		}

		if($request->has('flight_type')){

			$search['flight_type'] = $request->input('flight_type');
		}

		if($request->has('pnr')){

			$search['pnr'] = $request->input('pnr');
		}

		if($request->has('flight_number')){

			$search['flight_number'] = $request->input('flight_number');
		}

		if($request->has('order_id')){

			array_push($search['order_id'], $request->input('order_id'));
		}

		if($request->has('increment_id')){

			$search['increment_id'] = $request->input('increment_id');
		}

		if($request->has('carrier_code')){

			$search['carrier_code'] = $request->input('carrier_code');
		}

		if($request->has('recipient')){

			$search['recipient'] = $request->input('recipient');
		}

		$all_order_status = [
			'paid',
			'reserved',
			'reprint_pack_slip',
			'cart_loaded',
			'delivered',
			'not_delivered',
			'order_cancelled',
			'req_refund',
			'ready_packing',
			'processing',
			'complete',
			'canceled',
			'partial_complete',
			'partial_canceled'
		];

		if($request->has('order_status')) {
			$orderStatusesRequest = $request->input('order_status');
			if (in_array('order_cancelled', $orderStatusesRequest))
				$orderStatusesRequest[] = 'canceled';
			
			if(Auth::user()->hasRole('administrator')) {
				$search['order_status'] =  implode(',', $orderStatusesRequest);
			} else {
				$search['seller_order_status'] = implode(',', $orderStatusesRequest);
				$search['order_status'] = implode(',', $all_order_status);
			}

		} else {
			$search['order_status'] = implode(',', $all_order_status);
		}

		if(!Auth::user()->hasRole('administrator')) {

			$seller_id = Auth::user()->seller_account()->getResults()->seller_id;
			$search['seller_id'] = $seller_id;
		}

		$parameter_output = implode('&', array_map(
		    function ($v, $k) { 
		    	return sprintf("%s=%s", $k, $v); 
		    },
		    $search,
		    array_keys($search)
		));

		return '?'.$parameter_output;
	}

	public function orderDetail($seller_id,$order_id) 
	{
		$order_result = $this->apiOrderDetail($order_id);
		if($order_result == FALSE){

			 return redirect()->back()->withErrors('There was a problem loading the View Order page. Please try again later');
		}
	
		$order_items = $this->getOrderItems($seller_id,$order_id);
		if($order_items == FALSE){

			 return redirect()->back()->withErrors('There was a problem loading Order Items. Please try again later');
		}

		return view('admin.report.warehouse2.order-warehouse-detail', compact('order_result','order_items'));
	}

	public function paxSlip($seller_id,$order_id) {
		
		$order_result = $this->apiOrderDetail($order_id);
		if($order_result == FALSE){

			 return redirect()->back()->withErrors('There was a problem loading the Pax Slip page. Please try again later');
		}

		$order_items = $this->getOrderItems($seller_id,$order_id);
		if($order_items == FALSE){

			 return redirect()->back()->withErrors('There was a problem loading Order Items. Please try again later');
		}

		return view('admin.report.warehouse2.pax_slip', compact('order_result','order_items'));
	}

	public function apiOrderDetail($order_id) 
	{
		$apiHelper = new APIHelper;
		$result = $apiHelper->callApi(
			 env('NEW_ROKKISHOP_DOMAIN'),
			 'api/warehouse/order?order_id='.$order_id,
			 'GET',
			 [
			 	'headers' => [
					'X-PORTAL-TOKEN' => env('LUMEN_API_TOKEN')
				]
			 ]
		);

		return $result;
	}

	public function updateSellerOrderStatus(Request $request,$seller_id,$order_id)
	{
		$apiHelper = new APIHelper;
		$orderHelper = new OrderHelper;
		$this->validate($request,
			array(
				'update_status' => ['required', Rule::in($orderHelper->getAvailableStatuses())]
			),
			$messages = array(
				'update_status.required' => 'Status is required',
				'update_status.in' => $request['update_status'] .  " is an Invalid Status"
			)
		);

		$set_magento_status = $apiHelper->callApi(
			env('NEW_ROKKISHOP_DOMAIN'),
			'api/warehouse/update-seller-order-status/'.$order_id.'/'.$seller_id.'/'.$request->input('update_status'),
			'PUT',
			[
				'headers' => [
					'X-PORTAL-TOKEN' => env('LUMEN_API_TOKEN')
				]
			]
		);

		if (isset($set_magento_status->code) && $set_magento_status->code == 200) {

			$result = $apiHelper->callApi(
				env('NEW_ROKKISHOP_DOMAIN'),
				'rest/V1/bigdutyfree-portal/order/'.$order_id.'/status/'.$set_magento_status->response,
				'PUT',
				[
					'headers' => [
						'Authorization' => 'Bearer '.env('NEW_API_TOKEN')
					]
				]
			);

			if ($result)
				flash_msg()->success('Update Success','Your order has been updated!');
			else
				flash_msg()->error('Error','Update Incomplete. Please re-update the order');

		} else {

			flash_msg()->error('Error','Order status cannot be updated');
		}
		
        return redirect()->back();
	}

	public function updateAdminStatus(Request $request ,$seller_ids, $order_id)
	{

		$seller_ids = explode(',', $seller_ids);
		$apiHelper = new APIHelper;
		$set_magento_status;

		foreach ($seller_ids as $key => $id) {

			$set_magento_status = $apiHelper->callApi(
				env('NEW_ROKKISHOP_DOMAIN'),
				 'api/warehouse/update-seller-order-status/'.$order_id.'/'.$id.'/'.$request->input('update_status'),
				 'PUT',
				 [
				 	'headers' => [
						'X-PORTAL-TOKEN' => env('LUMEN_API_TOKEN')
					]
				 ]
			);
		}

		if (isset($set_magento_status->code) && $set_magento_status->code == 200) {
		
			$result = $apiHelper->callApi(
				env('NEW_ROKKISHOP_DOMAIN'),
				'rest/V1/bigdutyfree-portal/order/'.$order_id.'/status/'.$set_magento_status->response,
				'PUT',
				array(
					'headers' => ['Authorization' => 'Bearer ' . env('NEW_API_TOKEN')]
				)
			);

			if ($result)
				flash_msg()->success('Update Success','Your order has been updated!');
			else
				flash_msg()->error('Error','Order status cannot be updated');

		} else {

			flash_msg()->error('Error','Order status cannot be updated');
		}

        return redirect()->back();
	}

	public function generateInvoice($seller_id,$increment_id,$order_id)
	{
		$apiHelper = new APIHelper;
		$result = $apiHelper->callApi(
			 env('NEW_ROKKISHOP_DOMAIN'),
			 'api/warehouse/order?increment_id='.sprintf('%09d',$increment_id),
			 'GET',
			 array(
			 	'headers' => [
					'X-PORTAL-TOKEN' => env('LUMEN_API_TOKEN')
				]
			 )
		);

		if ($result == FALSE) {

			return redirect()->back()->withErrors('There was a problem loading the Invoice page. Please try again later');
		}

		$order_items = $this->getOrderItems($seller_id,$order_id);
		if ($order_items == FALSE) {

			return redirect()->back()->withErrors('There was a problem loading Invoice Order Items. Please try again later');
		}

		$exchange_rate = $result{0}->base_to_order_rate;

		$address = '';
		$tax_rate = 0;
		$tax_rate_label_s = 0;
		$tax_rate_label_z = 0;
		$tax_label = '';
		$carrier_code = $result{0}->carrier_code;

		if($carrier_code == 'AK' || $carrier_code == 'D7') {

			$tax_rate = env('TAXRATE_AK_D7') / 100;
			$tax_label = 'GST';
			$address = Config::get('defaults.address.AK_D7');

		} elseif($carrier_code == 'I5') {

			$tax_rate = env('TAXRATE_I5') / 100;
			$tax_label = 'GST';
			$address = Config::get('defaults.address.I5');

		} elseif($carrier_code == 'Z2') {

			$tax_rate = env('TAXRATE_Z2') / 100;
			$tax_label = 'VAT';
			$address = Config::get('defaults.address.Z2');

		} elseif($carrier_code == 'QZ' || $carrier_code == 'XT') {

			$tax_rate = env('TAXRATE_QZ_XT') / 100;
			$tax_label = 'VAT';
			$address = Config::get('defaults.address.QZ_XT');
		}

        $sub_totals = 0;
        $base_sub_totals = 0;
        $total_discount = 0;
        $base_total_discount = 0;
        $total_items_qty = 0;
        foreach ($order_items as $key => $item) {

        	$total_items_qty += $item->qty_ordered;
        	$sub_totals += ($item->price * $item->qty_ordered);
        	$base_sub_totals += ($item->base_price * $item->qty_ordered);
        	$total_discount += $item->discount_amount ;
        	$base_total_discount += $item->base_discount_amount;
        }

        $shipping_fees = 0;
        $base_shipping_fees = 0;
        $total = ($sub_totals + $shipping_fees) - $total_discount;
        $base_total = ($base_sub_totals + $base_shipping_fees) - $base_total_discount;
       
        $total_dom_base_cur = 0;
        $tax_dom_base_cur = 0;
        $total_intl_base_cur = 0;
        $tax_intl_base_cur = 0;

        $tax_code = '';
        $tax_rate_label = $tax_rate * 100;
        $base_tax_amount = $result{0}->base_tax_amount;
        $tax_amount = $result{0}->tax_amount;

        if ($carrier_code == 'QZ' || $carrier_code == 'XT') {
        	$tax_in_base_cur = $tax_amount;
	        $total_in_base_cur = ($total) - $tax_amount;
	        $total_final = $total;
	    } else {
	    	$tax_in_base_cur = $base_tax_amount;
	    	$total_in_base_cur = ($base_total) - $base_tax_amount;
	    	$total_final = $base_total;
	    }
        
        $flight_type = $result{0}->flight_type;

        $tax_rate_label_s = $tax_rate_label;

        if ($flight_type == 'domestic') {

        	$tax_dom_base_cur = $tax_in_base_cur;
            $total_dom_base_cur = $total_in_base_cur;
            $tax_code = 'S';

        } elseif ($flight_type == 'international') {

			$total_intl_base_cur = $total_final;
            $tax_code = 'Z';
        }

        $flight_bound_z2 = ($result{0}->arrival_station==='MNL') ? 'INBOUND' : 'OUTBOUND';
        
        $tax = $tax_amount;
        $base_tax = $base_tax_amount;

	    return view('admin.report.warehouse2.invoice',
		        compact(
		        'result',
		        'order_items',
		        'total_items_qty',
		    	'address',
		    	'tax_code',
		    	'tax_rate',
		    	'tax_label',
		    	'sub_totals',
		    	'base_sub_totals',
		    	'total_discount',
		    	'base_total_discount',
		    	'shipping_fees',
		    	'base_shipping_fees',
		    	'total',
		    	'base_total',
		    	'total_dom_base_cur',
		    	'tax_dom_base_cur',
		    	'total_intl_base_cur',
		    	'tax_intl_base_cur',
		    	'tax_rate_label_s',
		    	'tax_rate_label_z',
		    	'exchange_rate',
		    	'carrier_code',
		    	'flight_bound_z2',
		    	'tax',
		    	'base_tax'
		        )
		    );
	}

	public function getOrderItems($seller_id, $order_id)
	{
		$apiHelper = new APIHelper;

		if (Auth::user()->hasRole('administrator')) {
        	$seller_id = 0;
        }
        
		$result = $apiHelper->callApi(
			env('NEW_ROKKISHOP_DOMAIN'),
			'api/warehouse/get-order-items/' . $order_id . '/' . $seller_id,
			'GET',
			array(
				'headers' => [
					'X-PORTAL-TOKEN' => env('LUMEN_API_TOKEN')
				]
			)
		);

		return $result;
	}
}
