<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Mail;
use Config;
use DB;
use Carbon;
use Excel;


class SalesReportsController extends Controller
{	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 public function index(Request $request){

	 	$order_status = [
	 		'Paid' => 'Ready for packing',
	 		'reserved' => 'Reserved',
	 		'reprint_pack_slip' => 'Re-print Packing Slip',
	 		'cart_loaded' => 'Loaded on Cart',
	 		'delivered' => 'Delivered',
	 		'not_delivered' => 'Not Delivered',
	 		'order_cancelled' => 'Order Cancelled'

	 	];

	 	$export = false;
		$order_result = $this->filterField($request,$export);

		return view('admin.report.sales.index-sales', compact('order_result','order_status'));
	 
	 }


	public function filterField($request,$export)
	{
		
		try {
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			//http://stg-mg.bigdutyfree.com/rest/V1/bdf/report_preorder_flights/portal-search?limit=2&page=1&order=desc&field=order_status|carrier_code&value=unsuccess|D7
			$method = 'GET';
			//$domain	= 'http://bdfapp.bigdutyfree.com/';
			//$api	= 'rest/V1/bdf/report_preorder_flights/search';

			$domain	= 'http://stg-mg.bigdutyfree.com/';
			$api	= 'rest/V1/bdf/report_preorder_flights/portal-search';

			/* params request */
			$limit = 10;
			if($request->has('page'))
				$page = $request->input('page');
			else
				$page = 1;

			$field = '';
			$value = '';
			$condition = '';
			$arrSearch = $this->getFilterInput($request);
			
			$i = 1;
			foreach($arrSearch as $search_field){
				if($i < count($arrSearch)){
					$field .= $search_field['field'].'|';
					if($search_field['condition'] == 'eq'){
						$value .= "'".$search_field['value']."'|";
					} else {
						$value .= $search_field['value'].'|';
					}
					
					$condition .= $search_field['condition'].'|';
				} else {
					$field .= $search_field['field'];
					if($search_field['condition'] == 'eq'){
						$value .= "'".$search_field['value']."'";
					} else {
						$value .= $search_field['value'];
					}
					
					$condition .= $search_field['condition'];
				}
				$i++;
			}

			$order_by = 'desc';
			$sort_by = $request->input('sort_by');
			$yesterday = date('Y-m-d H:i:s',strtotime("yesterday"));

			if($export) {
				$params = "?order=desc&sort=sales_date&field=".$field."&value=".$value."&condition=".$condition;
			} else {
				$params = "?limit=".$limit."&page=".$page."&order=desc&sort=sales_date&field=".$field."&value=".$value."&condition=".$condition;
			}
			
			echo $domain.$api.$params;

			$token = env('API_TOKEN');
			$res = $client->request($method, $domain.$api.$params, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				]
			]);

			$result = json_decode($res->getBody());

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		} 
			
		return $result;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function exportReportCSV(Request $request) {
		
		$export = true;
		$bigItems = [];
		$order_result = $this->filterField($request,$export);

		if($order_result){
			foreach($order_result->rows as $key => $order) {
				$data = [
					'Order No' => $order->increment_id,
					'PNR' => $order->pnr,
					'Flight No' => $order->carrier_code.$order->flight_number,
					'Sales Date' => $order->sales_date,
					'Flight Date' => $order->flight_date,
					'Recipient Name' => $order->first_name.' '.$order->last_name,
					'SKU' => $order->product_sku,
					'Product Name' => $order->product_name,
					'Price' => $order->price_per_item,
					'Discounted Price' => $order->discounted_price,
					'Final Price' => $order->final_discounted_price,
					'Status' => $this->getStatusDesc($order->order_status),
				];

				array_push($bigItems, $data);
			}
		}

		$filename = 'Sales_Report-'.date('now');

		Excel::create($filename, function($excel) use($bigItems) {

            $excel->sheet('Sales_Report', function($sheet) use($bigItems) {

                $sheet->fromArray($bigItems);

            });

        })->download('csv');
	}

	public function getStatusDesc($type){
		switch ($type) {
            case "reserved":
                $desc = "Reserved";
                break;
            case "reprint_pack_slip":
                $desc = "Re-print Packing Slip";
                break;
            case "cart_loaded":
                $desc = "Loaded on Cart";
                break;
            case "delivered":
                $desc = "Delivered";
                break;
            case "not_delivered":
                $desc = "Not Delivered";
                break;
            case "order_cancelled":
                $desc = "Order Cancelled";
                break;
            default:
                $desc = "Ready for packing";
                break;
        }

        return $desc;
	}

	public function getFilterInput($request)
	{
		//flight_date,sales_date,flight_type,flight_no,order_no,aoc,order_status
			
			$arrSearch = [];
			if($request->has('flight_date')){

				$f_date = explode(' - ', $request->input('flight_date'));
				$f_start_date = date("Y-m-d 00:00:00", strtotime($f_date[0]));
				$f_end_date = date("Y-m-d 00:00:00", strtotime($f_date[1]));

				$data = [
					'field' => 'flight_date',
					'value' => $f_start_date.','.$f_end_date,
					'condition' => 'between'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('sales_date')){
				$s_date = explode(' - ', $request->input('sales_date'));
				$s_start_date = date("Y-m-d 00:00:00", strtotime($s_date[0]));
				$s_end_date = date("Y-m-d 00:00:00", strtotime($s_date[1]));

				$data = [
					'field' => 'sales_date',
					'value' => $s_start_date.','.$s_end_date,
					'condition' => 'between'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('flight_type')){

				$data = [
					'field' => 'flight_type',
					'value' => $request->input('flight_type'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('pnr')){

				$data = [
					'field' => 'pnr',
					'value' => $request->input('pnr'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('flight_number')){

				$data = [
					'field' => 'flight_number',
					'value' => $request->input('flight_number'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('order_id')){

				$data = [
					'field' => 'order_id',
					'value' => $request->input('order_id'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('aoc')){

				$data = [
					'field' => 'aoc',
					'value' => $request->input('aoc'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('increment_id')){

				$data = [
					'field' => 'increment_id',
					'value' => $request->input('increment_id'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('company_code')){

				$data = [
					'field' => 'company_code',
					'value' => $request->input('company_code'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('carrier_code')){

				$data = [
					'field' => 'carrier_code',
					'value' => $request->input('carrier_code'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('seat_number')){

				$data = [
					'field' => 'seat_number',
					'value' => $request->input('seat_number'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('recipient')){

				$data = [
					'field' => 'recipient',
					'value' => $request->input('recipient'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			// if($request->has('order_status')){

			// 	$data = [
			// 		'field' => 'order_status',
			// 		'value' => $request->input('order_status'),
			// 		'condition' => 'in'
			// 	];

			// 	array_push($arrSearch, $data);
			// }

			if($request->has('sku')){

				$data = [
					'field' => 'sku',
					'value' => $request->input('sku'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			return $arrSearch;
	}

	public function getSortCssClass($column, $sortBy, $sort){
        
        if($sortBy == 'flight_date' && $column == 'flight_date'){
            if($sort == 'asc')
                return "sorting_asc";
            else if($sort == 'desc')
                return "sorting_desc";
            else
                return "sorting";
        }else if($sortBy == 'sales_date' && $column == 'sales_date'){
            if($sort == 'asc')
                return "sorting_asc";
            else if($sort == 'desc')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'flight_type' && $column == 'flight_type'){
            if($sort == 'asc')
                return "sorting_asc";
            else if($sort == 'desc')
                return "sorting_desc";
            else
                return "sorting";
        }else if($sortBy == 'pnr' && $column == 'pnr'){
            if($sort == 'asc')
                return "sorting_asc";
            else if($sort == 'desc')
                return "sorting_desc";
            else
                return "sorting";
        }else if($sortBy == 'flight_number' && $column == 'flight_number'){
            if($sort == 'asc')
                return "sorting_asc";
            else if($sort == 'desc')
                return "sorting_desc";
            else
                return "sorting";
        }else if($sortBy == 'order_id' && $column == 'order_id'){
            if($sort == 'asc')
                return "sorting_asc";
            else if($sort == 'desc')
                return "sorting_desc";
            else
                return "sorting";
        }else if($sortBy == 'increment_id' && $column == 'increment_id'){
            if($sort == 'asc')
                return "sorting_asc";
            else if($sort == 'desc')
                return "sorting_desc";
            else
                return "sorting";
        }else{
            return "sorting";
        }
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function orderDetail(Request $request,$order_id) {
		$order_status = [
	 		'Paid' => 'Ready for packing',
	 		'reserved' => 'Reserved',
	 		'reprint_pack_slip' => 'Re-print Packing Slip',
	 		'cart_loaded' => 'Loaded on Cart',
	 		'delivered' => 'Delivered',
	 		'not_delivered' => 'Not Delivered',
	 		'order_cancelled' => 'Order Cancelled'

	 	];

		$export = false;
		$request->request->add(['increment_id' => $order_id]);
		$order_result = $this->filterField($request,$export);
		//dd($request);
		return view('admin.report.sales.order-sales-detail', compact('order_result','order_status'));
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function paxSlip(Request $request,$order_id) {
		$export = false;
		$request->request->add(['increment_id' => $order_id]);
		$order_result = $this->filterField($request,$export);
		//dd($request);
		return view('admin.report.sales.pax_slip', compact('order_result'));
	}

	public function updateStatus(Request $request){

		try {
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			//http://stg-mg.bigdutyfree.com/rest/V1/bdf/report_preorder_flights/order-status
			$method = 'PUT';
			//$domain	= 'http://bdfapp.bigdutyfree.com/';
			//$api	= 'rest/V1/bdf/report_preorder_flights/search';

			$domain	= 'http://stg-mg.bigdutyfree.com/';
			$api	= 'rest/V1/bdf/report_preorder_flights/order-status';

			/* params request */

			
			//echo $domain.$api.$params;

			$token = env('API_TOKEN');
			$res = $client->request($method, $domain.$api, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				],
				'form_params'=> [
					'order_id' => $request->input('order_id'),
					'status' => $request->input('order_status'),
					'comment' => $request->input('comment'),
					'user_id' => Auth::user()->id,
				]
			]);

			$result = json_decode($res->getBody());

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		} 

	}

}
