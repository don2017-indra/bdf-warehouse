<?php
namespace App\Http\Controllers\Admin\Reports\Helpers;
use Dompdf\Dompdf;



class CustomInFlightReportExport extends Dompdf
{

	private $html = '';
	private $carrier_code = '';
	private $items = array();

	private $station_code = '';
	private $serial_number = '';

	private $flight_number ='';
	private $sector = '';
	private $date = '';

	public function __construct()
	{
		parent::__construct();
	}

	public function setHeaderData(
		$station_code,
		$serial_number,
		$carrier_code,
		$outbound = array()
	)
	{
		$this->carrier_code = $carrier_code;

		$this->station_code = $station_code;
		$this->serial_number = $serial_number;

		$this->flight_number = $outbound['flight_number'];
		$this->sector = $outbound['sector'];
		$this->date = $outbound['date'];

	}

	public function addItem(
		$info = array(),
		$action_outbound = array(),
		$action_inbound = array(),
		$action_store = array(),
		$remark
	){

	$item_code = empty($info['item_code']) ? '<label style="visibility: hidden">N/A</label>' : $info['item_code'];
	$description = empty($info['description']) ? '<label style="visibility: hidden">N/A</label>' : $this->breakSentences($info['description'],30);
	$vol_cl = empty($info['vol_cl']) ? '<label style="visibility: hidden">N/A</label>' : $info['vol_cl'];
	$unit_price = empty($info['unit_price']) ? '<label style="visibility: hidden">N/A</label>' : $info['unit_price'];
	$total_price = empty($info['total_price']) ? '<label style="visibility: hidden">N/A</label>' : $info['total_price'];
	$standard_opening = empty($info['standard_opening']) ? '<label style="visibility: hidden">N/A</label>' : $info['standard_opening'];

	$outbound_opening = empty($action_outbound['opening']) ? '<label style="visibility: hidden">N/A</label>' : $action_outbound['opening'];
	$outbound_sold = empty($action_outbound['sold']) ? '<label style="visibility: hidden">N/A</label>' : $action_outbound['sold'];
	$outbound_closing = empty($action_outbound['closing']) ? '<label style="visibility: hidden">N/A</label>' : $action_outbound['closing'];
	$outbound_amountsold = empty($action_outbound['amount_sold']) ? '<label style="visibility: hidden">TEXT</label>' : $action_outbound['amount_sold'];

	$inbound_opening = empty($action_inbound['opening']) ? '<label style="visibility: hidden">N/A</label>' : $action_inbound['opening'];
	$inbound_closing = empty($action_inbound['closing']) ? '<label style="visibility: hidden">N/A</label>' : $action_inbound['closing'];

	$store_opening = empty($action_store['opening']) ? '<label style="visibility: hidden">N/A</label>' : $action_store['opening'];
	$store_closing = empty($action_store['closing']) ? '<label style="visibility: hidden">N/A</label>' : $action_store['closing'];
	$store_topup = empty($action_store['topup']) ? '<label style="visibility: hidden">N/A</label>' : $action_store['topup'];
	$store_retrieval = empty($action_store['retrieval']) ? '<label style="visibility: hidden">N/A</label>' : $action_store['retrieval'];

	$remark = empty($remark) ? '<label style="visibility: hidden">N/A</label>' : $remark;
	 $item ='<tr>'.'
			<td style="width:100.5px;word-break: break-all;word-wrap: break-word">'.$item_code.'</td>
			<td style="width:120.5px;word-break: break-all">'.$description.'</td>
			<td style="width:20.5px;word-break: break-all;word-wrap: break-word">'.$vol_cl.'</td>
			<td style="width:60.5px;word-break: break-all;word-wrap: break-word">'.$unit_price.'</td>
			<td style="width:60.5px;word-break: break-all;word-wrap: break-word">'.$total_price.'</td>
			<td style="width:100px;word-break: break-all;word-wrap: break-word;text-align: center">'.$standard_opening.'</td>
			<td style="width:45.5px;word-break: break-all;word-wrap: break-word;text-align: center">'.$outbound_opening.'</td>
			<td style="width:32px;word-break: break-all;word-wrap: break-word">'.$outbound_sold.'</td>
			<td style="width:44px;word-break: break-all;word-wrap: break-word"'.$outbound_closing.'</td>
			<td style="width:40.5px;word-break: break-all;word-wrap: break-word">'.$outbound_amountsold.'</td>
			<td style="width:58px;word-break: break-all;word-wrap: break-word;text-align: center">'.$inbound_opening.'</td>
			<td style="width:59px;word-break: break-all;word-wrap: break-word;text-align: center">'.$inbound_closing.'</td>
			<td style="width:57px;word-break: break-all;word-wrap: break-word">'.$store_opening.'</td>
			<td style="width:55px;word-break: break-all;word-wrap: break-word">'.$store_closing.'</td>
			<td style="width:22px;word-break: break-all;word-wrap: break-word">'.$store_topup.'</td>
			<td style="word-break: break-all;word-wrap: break-word">'.$store_retrieval.'</td>
			<td style="width:40px;word-break: break-all;word-wrap: break-word">'.$remark.'</td>'
			.'</tr>';

			array_push($this->items, $item);
	}

	public function downloadPDF($filename)
	{

	$template_file = storage_path('app/files/php/custom_report.php');

	$carrier_code = $this->carrier_code;
	$station_code = $this->station_code;
	$serial_number = $this->serial_number;
	$flight_number = $this->flight_number;
	$sector = $this->sector;
	$date = $this->date;
	$logo = $this->getLogo($carrier_code);

	$item_rows ='';

	foreach ($this->items as $key => $item) {
		$item_rows .= $item;
	}

	ob_start();
    require_once $template_file;
    $htmlBody = ob_get_clean();

    parent::getOptions()->setIsFontSubsettingEnabled(true);
    parent::loadHtml($htmlBody);
    parent::setPaper('A4', 'landscape'); // (Optional) Setup the paper size and orientation
    @parent::render(); // Render the HTML as PDF
    // Output the generated PDF to Browser
    @parent::stream($filename);

	}

	public function savePDFToPath($filepath)
	{
		$template_file = storage_path('app/files/php/custom_report.php');

		$carrier_code = $this->carrier_code;
		$station_code = $this->station_code;
		$serial_number = $this->serial_number;
		$flight_number = $this->flight_number;
		$sector = $this->sector;
		$date = $this->date;
		$logo = $this->getLogo($carrier_code);
		$item_rows ='';

		foreach ($this->items as $key => $item) {
			$item_rows .= $item;
		}
		
		ob_start();
	    include $template_file;
	    $htmlBody = ob_get_contents();
	    ob_clean();

	    parent::getOptions()->setIsFontSubsettingEnabled(true);
	    parent::loadHtml($htmlBody);
	    parent::setPaper('A4', 'landscape'); // (Optional) Setup the paper size and orientation
	     // Render the HTML as PDF
	    @parent::render();

		file_put_contents($filepath, parent::output());
	}

	private function getLogo($carrier_code)
	{
		switch (strtoupper($carrier_code)) {
			case 'AK':
				return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEkAAABECAYAAADN7RgCAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUATWljcm9zb2Z0IE9mZmljZX/tNXEAABV1SURBVHhe7VwHmJTl1b1Td2Z3dmcrSMcOioVofruCLcZuVBSNLYkGFQS7fyxAQGkKCjxWRCWINRZA1CCoFCsQBVQE7GKBrVN2+sx/zv2+GYdlezGrf97nGUH2+95y3lvPvbP2FIb8dzSKgP3nxCcVi0nK75fEDz9I9OOPJfbllxL/+mtJBQIiyZSkolGxJJOSstrE4sTWrFaxuNxi79VL7H37iGOvvcTeo4dYPfn495yfbesdDlIqHJHIe+9KbMOnEnn3HYmsXiMJACXxuEgkLKlIVCSRELFY8LHiY56dYEHILQQqJ8f4OBwibrc4991PXIceIo5+e0rOAQeItaioQwHrGJBw6NjmzRJ48imJrlkj0Q8/lMS2bWLJ84jFbhOx28Vig7RAIsSL/ydADQ1aA8yXImiUxHBYwktek9oF88VaUCDOAQPEsf/+kn/eeeLo3w8S6Gx3wNoXJBwo/OabEnj6aQktXizJikqBOIi1sFDVREc2IAQg/WnsaJQmgCqQJIWTc3q9koI0RlathoS+J7XPPCM5hx0qnnOHivuYo6GOrnYDq91ACi9bJv7ZsxWcVG0IUpMnttJSER4OUqBgmAds1e7r8S8WSKStDGvgZ6lQSEILF0lo0SviOnqweC68QHJPPNEAt42jzSDR+PruvVcC854Qga2hfbB2gRqlAaG96cjBC6CAwlbZ8BGoZHjxa8JLyz31FPGOGAEbtm+bdtAmkILPPCvVkyZKbONGsRUWiaVbt+apT5u23MDLaUmjdHXbSW1X8KmnJfLmMvHecIN4Lrm41VLVKpCScNk1d90pvpn3wsRYxN6tu7Fz81Y7AoMWzQnppQG3d+8uyRqfVF5/vYRXvS+FN94ojp13btFUfLjFIEXhyqtuvllC//qX2EpKDAPZWcDJPr7pEKzeApFctwQfnwcvu1ZKJk0S15FHtAioFoEUeettKR81SmKffCL2nXbSYK9TApQNAS8QKkipisMsbPvzn6Vk2lTJPfnkZgPVbJDCK1dK+eVXSOLbb3XBNnmqZm+vnR407ZWtrEwSVVVSfuWVUgIDn3fGGc1aoFkgRT/6SCqGj1CAbF26dH7paejokCobvG+iqloqRo5CYGuX3FNOaRKoJkGKb9ki5SNHSvzzz+E1TO/V5LSd+AEFqlASFRVSedNNYuvdW3L226/RDTcKUqK8XCpGXCVRRLT2HlCx5kTHnRifzNYIFJxO4rvvpfKaa6Tskdli792nwZ03CpLvnnvgxV6FkaYEYY5fE6uCs9B0RN55VypvuVXKHnywwVSmQZBqX3pJfA89JNbiEvUOnd6LtVSCeeGI8Wxdu0rt/AUSGDRP8v/0p3pnqRckqln1xIngd2LQ32KDyvg1DlIxoGCsiPVq7rxL4yfHbrvvcNJ6QfLPflhioDc0FuqMgWJ7XhgEgIwCPXfNlLukFHmo2BD/ZY0dQFIeaM4/DK4HDGGrQCId8guzX6RzQgsWSO1ZZ0nuccc2DpL/4Ycl/g3iIehqqwCifScNS4qCn44Ai5dAZpNSTpKtrWtQ7cAgJL7/XgKPPiruQUcZLKg5tpOkyLq1UrvoZbHm5mri2qLFzeeTNdWgWHP1EKRe231gzmRtLeZHEutG3hiJtFv+aIX9jbz1loTxcR91VP0ghRcvkfimzSDee7YKoPiPP4p78CApGD5cAs8/L7XznjQi9JTB+bR5EKCaGpWe4kkTJAmirXr87SKgRWiAW3SpdTdDaUJxgdIUevllcR1ySIYKzkhSYutWCb/2GnhjkzBryYmwQIIAgREsRbzBHMnet69EQa0mUQ2xgItu0wG4FwLk86lRLbn7buRdp+sOWVgIPf+CoR6NceXNOY9JCwcXLJT8Sy8Vx6676lsZkGKbNkn4/ffFCtq1RQfC5knyk4QvQfBJgHRiROj27t0kjHltLVXdugcyVSwFC1A6Y0YGID7m2HUXCVlN09AQSKr2fCaLRm4ANILN88TWrq0DEuKq8PKVaoeY9DV7mOJPF1o0aTJC+97KCJJjCi19XQl6eo0WgV53cR4cGXuyuloKb7pRM/cUbBINbRJrKafOOK6+gNe8HB46FY2Y/Bco3sbCGlZysAdKU+5pp6kEKyIs1YSXvaFlm2ZH13iZgDDQLJ4yGYbuSIn8+9+6eWe/flILvU4FwHmzqoFDJVkccNgNSeXNtsAjxXHIvCFDxHvtdVqqomq7DjtMal98UaLr1qNY6dlxPgKEvdHIu353vDj69JHQkqWS/HEr7FcjZSeeC3uLfvyRJMnZY/8KUqJ8GxbGy82tLHADAJSZtBfcTN6ZZ0oEquqfN0+Kx43TBXgApjSpYFDrbQUgu5J4PviCYT+a5fmoyrCVOb8ZCEM9Ee9YtCLjPuYYBaAWcY1wfkprXenAQUkzu0/4nZQ+8IAadhKGtWAoFaT0JdVVUTMKT/n8Evv4E8k55GADpBj4Ipaam+0hMDE9mWvwIPHe/DcV9+rpM8SNqipvNfjifJR4oHYMJWx2KZpwh+SdfpoWKVU9gnDhLFNr1dYAfAfJoirzJiGZRWPHqqpUT5sm0U8/laJbb5XwatTbVr4ltmKkTWmA0gfOAoDr0yOGkchGYAIo6Sx5JSHlzNqt+V4EzpDudOplgpSoroKUrs0C6bPPJQHC3MqHmxrcPOyDraRYiv8+DovkS82MmRJCCbtk4gR9O7RkiSEp/DjhdRBcsvbvm8VA9Rv9mRWHY60sCUngHKzGZh+WhUcermj8OHEhZol98YX4IBEFw4bpQaMffAANKDdYUlO1UoiZGMBmX3bwlVckgmfpGan2DBdsUL3C8X/XgJkhBOeiPU0DRUlP/eCTGDi0jHeLf/2VNjJYCr2NGzVshkkvbVHR2DHi3H8/3OxGlJUmIZQ/Tqu0UXiFxMZNqmYpPo8qbjkORkI+WV2jTQ/OAXujVL1UHHvsIY599lHOPA4vyMPrwHtJmAA3bAldMUclykIMHD1Q7WSoVgsRcDO6X9odSq69dy/VCF4iLygHku0ZOlTIaNQ+97z2GuQgmi65cwoS2d0kBeAsE26XmqnTJLYG9jRd9TUNPufMgKQ3kESY31icYapGsqJc3CefpJvnZiqg59Et30qpSayHl6/QAqXryCPFhqDUjhIO+wECqIHZUW0lCZ/z299KEH0CtBcMGfxz5kjV9TdAAhBBw7gnwUOTBS0aM1YNfQ3iIv+zz0o+AGKHSRxdKXFIka1XD0kQ+F12Fu/VV4sTDGOyslJqpt0t4ZUrpGj0aC1MpgJB8SHdyD97iJTcf59K8lb83TV4sBRccTnA8svW8/8odsZa6VQKf6e6M2xQm2RB24uqRn1DKyJoWIAEJcEN2yAthX+7WZ8Pr1olFkhIMWhQFwwcR/6lf5F8FAK1bk+3DHAZ5rN5wnnQQegGOVSf81zwRwSbqyTwDPoGXpivxjTFdar8KimFo28T5z4DwB5+J9H1H4n3ssuk4GLMi0F2ovurr4pSy1i7BKGBAOAIVNoDt03p5EURoPhXX0klSmDuwUdLyXTEcVDzrfCU3FPx1LtU5X33w7Az8ydIpm3i3ilp7IpRkJJMSC11klGCw/gEt6qspBu8S9cyKbwNm997L92s+/DDJffYnzJmhhKRd96BeC9Sw1z0vzdpf1EAtohq4b3qKn2P9qUKxjcCqaOBtAjWQrcJ1c2SD0+IOn7BZYaa2WBzymY9tN31RSCZ4dffkNB770kBukmcA/aR71D3Dy9bLkGoPdOV0gcf0HfIYwvm7PLoI2IDuD54Rz88bFdIFqWyZupURO2rpQj8WeAfcyWx+TM1O5Q27WKJxbOKk9ndL6Zx5iK0ITmQgNzTThXH7rsbuRgGb4BVlDA26kAKwuYEhgFbL7gIjVlfSuGYMQpQ7cKF4geHXHTHBFUzGmPyyqGXX1GPZbU7xAZ14UFz4QFdBx+cabTgJmOQIq6TDIfEM+QcTZuq77hDgv/8p9h79hLHuLGayzn79Zfo27igRS9JyV1T1eYEnnxSQsuXS1f8ScPMrKJy9BjJ/8OZaqsY9vgemyPeG28Qe5++yilJdnhgcNamutGa040qUDCaEDPGJvmXXiYutLGo58kaEWy66tbbUCB4V21A+tYSEH8C5DrgQPGiwkJvUoW4yQ17VXDVCJ3BByqGQR1vlRsqGDFcPOeco7Yre1CCK28bLSEY3dgXn0vBlcPFesklageTP/wIqguRMQNT07gWXj0KIQGyBti0vPPPkzgO7J81S8MHFySeIUYAcVwOnE0pJZNOJRGXspkzVFPKIeUpXITVW2hsA3hYcIGcz8hBNAJmps4oNaY0RO7ZZ0uumUTSjdohQTSMXKwatxGCSllBVfDfXAzuMHgzHB4cxoYArxoGl00UJNlpgKPr1klg9iMaS5Fz4oYLqQ4Y4bffljjUMA/r0gUH0G/kh8rYSsuwTh6k8EA1quyYSzAE2bmvVmRp9Itvv13sSEZZlU2ie46uPQavW3jd9WJF/kjvR/on79xzxYP5419+pR5Wq7roiqnABWhNsRR8PoWFqgYMhJE8bJ1huNmfyFY8BmGMM2AfSBfQiAXBDMQ+2SBd5s7ViDz43HPa1mLv2UNVx4KJVCrwrnaXlJRqkMlB9Sw79VTYGYNZiCJtiW/epOUbqlIc+l8zeYpWVf1zHoOHukYBYtrhRxGC/DqDSe1DYs2PcyBcoJQXjLxKfNPukSDabLzXXqsHZlCbAynWtffcA9L0jWyDR/TCc+YNAfhQSzZPxNauk7yh52ozBc0Bwx/tpcoKSlOxKC7asE0Kkr1HT406GcAZbXoeCSM5jaxYITGIdskdt0sOPA1BYVrA4FADNlYbABAnSgYDqhZOtOZpjQ4j76ST1FCyk8MNdxvF5piiGBcDb4aYxjdjOg7znTZeFVw+TH8WmPs47Ad4LYQHlALSLvae4LgwYhs/FQe8FsEofexRSYCKIUC0jcGnntI9xQ8+SEIw7DUwxgnEadUIcnkJ9M5Mpbhf3733qWMijaM9l9ulNWbDmRk3GSBBVG3QxXTEyn/jDSb9ATW2HrPUElq61EgFYHApZRo6YCEdbG2EujHliG/5Tmz4uR+utRqBWu6xx+iNhV9dDPuGgFWfxwsEOgrfVlwEgC5XlWRnLhvCLA7kVyaFSqmzOM1uW2zcN3Om2LvuJO7fn6AXWz1lCoCdq5fItkC9XKQv1Ao6Grpxgq4SiYQ1kwql48K6eR88GrMAjcKJD//jhAej9NBga9TL9jqmBUgovecNNRcKSwCeQIsD3LzJ5FHFtD8SAVkKRpxJ5TYEZox7Yl99LTbYgvAbb2rsQ/XZrlUHKk5DnHP4YRpdqxRBGmJUSXpRVjLwPu1G8NlnND2hi06VV6D95xYtA1EttGkVe2JgSltHL6Zsg0mfcC+2dOafXYWuj4mgQUdwzZ6BHDStZkCivvM2mZ4YugADDjF3wDiSouAIvf66BmDKXJoL0dbQC227BEU91vqwEfJHtCkUXxttEdUXwGd4pexbY6QPafKcdbbeHA9bu/AlhAW4uzSdgj9tUAkfEmj/ffcb8yK+EfBDiW0Gc8EuOy0DUbq5HtdtCgzjpDsOgkRODAbfsffeP4FE+0B+hgZNI07NwH2Sf8ZwdLHBYFJ1HpplkPvZ1Qm6Sf4/D0TCzmwi5YEzKpUGpe6tMT+DHXPs1V/jI47wipUSWbNa7CyIpofpTChRNMxqC7lHhAD1rsP3WsBV7YCS+a59F5ggUjAYRgiADbsGDUJ4fr9hvPGgrXsPg7fBoOuObdhgqEo9VOwObGZzNmk2uOed8QeDmENwyjjGQnWuW4rifLiIHTio5qzTkMQ09O8sVWFok5fJ0ma4Wueee6ptiiO+SDHvYoCJYIujFtEx8yR7G2px9e6Jog0jycGkNboauSAl5T84VEhgn50D98/sIgMScyT30cdIDVw/e3YYSVeNH6+um1SsGsJ6pKjV5zF5nwBK6ikY9diGT6BGSa3Lt0ldWr0hQ6PoSDwXXbhdA+p2rL8L1IX/iSfUyzEViX3wobb4WpljZZNibdlI+l2zako1q5k8WcHp6O+INLlthhqwfe7jj/+J28rYJPNtF2IiuuIgA0ZIjjKGaSPcEY0TNPxQ607Rg0k+HdrDMIP2OXvsUD/Kv/BCCc2fj3iHtCqi444Ap+6VdoQBblJssh5Iu314ap5fqy9ZYweQcgYORF4zFOnCTNgh1PT/nwzGe2RK+VWLuqPeSqT3iiuUODMyYyR+v9YmLqJhVmUsiLCZCKdToUYliT+kpyu87jopR+ODRp/t0d7SGSWS3lqrMqgOgyTMOfCAenfZYE3bAyohvHyZNnSR5mxX998ZADMLG1o/PAoMRAP9ktxqw4V/iGHhLbdI7LPPJPr+qgxn1BnO1y57AEis25F3KkFBgHXAhkaj3RHkcEqmT5et5IO/2WJ8Ae/n8HbtgkIjk5B5JOOB6J70LoFqbDTZQuLs3x/f7Jks5X8dpgyiJn3/aZfdFhBNQ02OqngyiqogBpsaTYLECRhgltw9FV+fGKXUiIrmL1GiCBBraQrQZKM+2IzRLJA4T+7pp0spqgcVqEpQl5Wd/CVJFAFCRC0gEmlC6JiaO5oNkgKF8jZJt0rEE2yAUPaQLGVnliqzayUBrt7WpUyK0QeQi+JES0aLQOLErPGXgU+uRjsNmxBY+9L8LpsJbMkOOurZdO8CGAYWAPjLFopQAyRn39LRYpC4gGP33bQxyv8/B2kGz45VMgUZ1rClu2jv50HD0O6waYzUMdsItdDQyt9E0SqQeCZm7wXD/qq/BqOG32ZCA4OwaEijTkbv55YsrXwYzaO0maR6XfhlCt6RV4v7OINhbe1oNUjpBVlZLQO1wvY/H8rKEZS+SeeSpMv0G3WkzTLVihUONoSRn2JXSf7FF4nn/PMNsrCNo80gqVShmy3v7LPEhfoaK7+Bxx/X1r/kD99rmVirJmaxILPf1njG7P4pU1JZwtJWRoDBX/DiQZdJ7kknGw377TTaBaT0Xlir0k3Ce0TQexTEtwKiaEaPrV9vVFNY0yOhTw49/d2T9MEbqIHp3Cyogm/XVmQkpPonJCeBjxPAOKHyuSecgO+EDGq13WkMz3YFKb0QSStumJ84CpTsSYyuX6e93Wyq0sxbVQM9jKwCs3fabPjKJNJmgTRD04DeZbWGtT1Wb11HHCE5A3+D33izr7CI0ZGjQ0DK3rC9T2/0/vTW/qYC/N4QRuzRj/ALpkD8syeb7TnaYAomNIWOEAUNlV2qMEk/Vi4IDvsBHOhBYveb9jWZNbGOBCc9d4eDlH0IFhP4saP7VU78/XbnU7BQotbAlB0uBAmkfJu/L9IOKP4fIyvLUkTQNVUAAAAASUVORK5CYII=';
			case 'D7':
				return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD4AAABECAYAAADZeIbjAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUATWljcm9zb2Z0IE9mZmljZX/tNXEAABcPSURBVHhe5VsJvE3l+n7WWns+e59zkBLRjTJUlHuTW5FmQjS4lVuUSiMi1VVShqRbSob8KUOoRGnUQEVIgyEJEaHIPJxp7332tNa+z/vtc44zOns7R/f+fv/vl1/nnL32Wt/7vdPzPu+7bHEu/D9ctlRljgeDiAf90Gw2aJk1U/36/8z1SQlu7vgN0R/XwNp7ELGfViO+bwfg9sBo3gqOtm3gaH0hoGn/M0Ils5EKBbcOHEDkm6WILP0a5paNiG1cB0Tj0OJRwKB3WBoii5cg/NZM2K++Bq7rboD97ObQnK5knvtfv6aM4PG8XITeexf5s1+Hue1XIJgHzW6H4aFAabxcNCv/VGiII+7PQXjWFETmfwD7BRfC+8hgGA0b/tcFq2wDJQQ3d+xA3mOPIPrV59AcNugeD7TatRKCWtaRexXFQw1aGk3e40Y8lIfwnNcRP5iFzLnzqs/0TRPhLxci/MViGHXrwNbibLpWG/VcWVZuLi0xxp+4R53/ZWbwR/5QySoSPLZhHXL7PYDY+jWw1TkRYPBSWi38V9GNCg5B86TBVs+G2NoViHy9lL7frrJnV/457x37Yycia1Yj9sm7CB/YB3gzYGvZGp677oK5fSvCH7wDKy9M9zN4vxgc558He7sOsJ/bHEb9Uyt8hhLcyj6EvAEPwNxAoRvUA0xqt7iGK99i4nq7Axq/m3v/XfC9NBHOy69M5ptlr5F76TqsXX8g+MqrAPen1a7B0ML4QuszVy1DzvLF1LFFy6TVOd3q7/FoGKHNG5A/800YzRrDde0/4P7nLdBPqlPmGUrw0Lx3qOkfaEon89DMY9usfIsa0lwuxLOzkPdof1j0d9eNN9PyKje90g8Nf/opQnMZZ6hxc/NG6DUyKKRDCajXtPOATd6XWtYl3iS+rSENqCH7sGDt2Ibgv4cjtOBTpPXtB1fHa0q4nw3RKH36y8SpiZ8U3uVYxafwes2aiOdlwT/oIVj79iLtwYcqvxu/F8/P5z6csHKyEXr7DWXGxqn1YdSiNBT0SFAVNxbTFnMthb/Ur3I48h0ewMa18D94D6zdu+ge9xYJb7Nys3mqvyVOs7pAHB+oeb2MNX7kj3kWxmmnw9Wly9GF57PN37fDOKUBwh++B3PXTtjq0+2U2UvgStFqCr6nn1ADcR5kcMKLcFx8KWxNmqp9MBrxJKl1pezqXNSECI9YLvwjh0KvVROOi9qU+4S4mK2YcN1TEGMKDc9/F/FD+1XA0qoCqFVgpmheH5CTxfS8pZjgzNGam+motMlUxyGI8PTN+KHdyLu/N9Knvw77X/9W5s7Wrl3Inz4FcTNM0PQN4rtogSI091YtSw7PRtcIh4puZ9PS09VJm1u3qFOvNnNXrsb7hcIKD1iH9iDvXw/BN34y7E2bUkhqmcJZ/jyYO39H5LOPGMQ2QKtZC5IaNYOmXV2uJ/GDSFPLJCYpWDbNZoftjKaILvq8ynGthHYkvUTCsExaVFyHfsIJsLZsQG7v25AxbRZz/TKEFi6Eo1lTRL78kqmQgKluXcYaZ8Kvq0toCdfUtFavQZGZJ3ycy3F5e4RmTKE/0tcFuFR1idChEPeuw/fieMSzDiEwdJBCVdbvW5A35HGFDGNffYb4yuUSmhF3OaFLgE0VP1S2V9lLfhiu9p2hn1z3iMaV9H9rBVu7qxD5fD5TB0vNqp4282g8OxvufoPguvpq4vk8hGa9AmvrrwnNr/4OcR6w7eSTeNiEm8VRYmWCpPK5uFowwMBaG+6bupf4plKvpDJP34cQ/WEVa+0s5WPHLDxTj3XgEAuWS+Dpn8jf1uFD3EAwkZKkxvG6af4FcFigpgCfVARK9lre18rJIXq7HcbpZ5QVXP5iP6cFPPf2QWDUkzCcTHEFG0r2Geo6dcJBaOm1kDZkBNNkBHEKG/1uucrRWnoGzKwcRmsH9DQCpqpa1tE2x+AopbXtrBZw39OnzJUlHNrd605EfmBB8M4b0BuckpLM6mKauBkIwTtsGIyzzmRp+yZcnTojsuCzhJCsooxzWtPn/ExZ28jiVFO6Kr1THnY8z0/l2eF56hlidULxUquE4JI3fUOGImfLZlibfmIwYJUmBUsyS4qKQ4dha3M55AADE8bQhYgP8gOIrlkhpwL7hRchfeps5L/5GgKPD2SOJ6wsZG6qAlSK70+CGaO4FYxQASNZJV5S7u7LhHCjXj2kT5mO3L73w1r7PfTaJySX5iIRsXVVEFhZhxF+ey4yJk8jc/MzD+QgPxEvNhBe/CXC77KUzKXPUyPxKPM5wYWemV4Nps9nEB9Y/hDcAwbBfcfdFaqs3NxlO60h0l8Yi5we/6Cf7FFFx1HTDLVtEg872neF44KL4B81Uj3QaNoMwdcmM53Rr+udivBXy0goLARYwXnu64/oxvX0wXMRD+QguuD9RL1QlTBHfGtmcR9dbqQCHjyqnVaYtG1NmjAHT1BoyzqwG7qPeLd0jlVmStMKkHWtXRdpjzyO6Lr1CI4fw4qsf0L4k+rB3f9h2NteDkRC8Pe9k8XCZfA+PZIkwy4WIvURemcOIh/NpeYpeIq1SJF04tfE48YZZ8H7L+IEAqKjraN+6mhzMdKGjuJm7yYgkZKRvFthJBZISThqBQI0rSA8Pe+DrXFjhbW9jz2h6nC51tm9J8yD++A4uwWrQLKzrjQ4O7FSIz0U37MLeZMnIbqMpAIBjKrCjmUpkBIgDNbhHfQEmZf6ld6lUpjmuvJKxHrdjeDo4aSWiHx0oaQYvfcdUDW83vhsuDt0grtnL/Uwx99bMyjWUaxHbNPPyLm/L6mic+B44UXkz5gJvX4jOK7pitj27Ywj98JixQTW1nqtTH6Zh5kqchOrk7r7YDY8/QbCeeVVlQotF1QquFyU1u9B0lJrEVn4iSL8BI7aLmgHd4/b4bj0cuhSfnIJ9g69PosmdwAZcz5A7BfS0ksXwXPbLQxwB5A/awbSR49WxYl/5HBYe3awMnTCdvFVsPYfQPz3TSQRUwRPUscfPATnTT3geXhQUkInLbjG5oFv7EQysI8iPGMq01JrZM6cTY07EF2/Hjr5dGvfPgSeeBSxNWvhuedO6C4HzJ/WwN68EVzXd0OAB+JofwWcnbsi/PFHiC38kOhNg+3Cdoz+pKeXfY28vndBEwiriMMklvBvBw/C0fE6eJ95ju4iLFJyKymNy630GjXh+/doQtoQYiu/Ifaegcjy5dCaNKZwLRCaQ03vJoNSLxNGY2E5uCmWm0bL86H50mE75RS4u5D34hIkF8/NgXFeG/heGEcte2Fr1FDxavCTLi5XcEn0xWIAhRY4qjdsCu9wghQJvimspAVXwmdkIn3SKwiMHIbcQf15wmmo8dBAFdXDn36cIBrpo1qGMH4MBXl5sLe5RP1sb3EOuzJL4OzQETG6Ddw+ZoFBPKhTFMsXpXVYZF10YUsKl8hJMsM8nC0QQB0+w5jCFXHCYaSlwzvkaRiMKamulARXwpNUkH5Z/tQJqvISgUKfzodJf9YzvKzKchLlrQhO3G7+ugWxnzcg8PwoxFidRVevRPTbZXB2u5VprR0i33/DOuGvCDN+KJRoFPD5cgMeYtzS4ejcjYWNC9GF8+WuiWtIl+lkhe3Nm6cqs7o+ZcGjK1fAP2wIs5ENzutuVDeJfPQe9yOFDTuo9PvwJx9TI9TU3r0UfCuiK1YyBuxUDGp41nToJ56ItIGPMj78BOuPPxCv34BcOVFiqcAmB2ec3RIZEycxQ2xEzsfvSdOKlhRgUHTB2snOzzMjkD7iaeVOqayUBJdoHpz6CrX7M7XUEo52lyC243dq8Gv6KQOLmDkjvLn+RwRWr4Lh4990loYUWvcyWgs4YT3gvm8AjL80hH/cS0jrfR/L4RWKrNDoSkUUsuR01ukWc31w2nREGVfirK3jtHmjSXPGkz8IUkxE5r0Jv5M1xrOjU2pbpSR4cOLLiHwwF3q6D44r2iv2MvruHMQP7CWmZ0GjzJO1NVOUzs0UBim5Xn1EOClFjOeeB5A/bSosuoCtWTMGSiEaidl5cMqfo6SKeAjSiLDCJgLPDCFRSGxvJ9TtOwDu2+9CcOYMhF4aBSMjDeHZr/FaC55HhiTgdRIracFD785D8NUJvDG1QhO0UeOyoovYjBB4WJyo5M9SeJToTUhDgOkrrU9fhQL9I5+E+7beqokg/JvlDzDgZcDRoQu59boMhEuVhWTOmM1O7Dym0kHwTngZ7muZDr/4XGELLcjGg+yJ2SA49iVoJxI2D3gkCbGT9PHojz8gNP89aiRCbXqoHYsV1jxSSgEGrnVkbAhgSpMKKvsUpB8zBnP3Xjiu6kxuvS2CrM40UlCuazqryQrv4GFs9SxQEd7ZrRuME4n8arysuDKDDHDow/lwde8FT/fuyBvYX2UOl6C/33cylsQUxjfq1yXmf4tu0Iztos6VCl+pxqUFFJo1DeYPK1VAUQQ9I3tUhgaWL2VFpaugVR6bolhWfz6jvEVioiWR1ePqMJwU3jnnffr5aURs+0le5BPKNoD7lluQ+/AA8vCHSYQ0gG/wkwyU80ld5SJj5tO0jCVsLc1Bzc8XIfQ+uy2rV8DZ5QZEF7OtzcBosSbwD+yjMomHUFkOtaJVqeAmNxZd+yOBBUtLcvCFbWONyExqX3Xz0touZFkjJvP4ZXBcdiWcbduycmpMP89iK/kHRvzd8PzlLoTfn4ecPn1IgAwmXifv7Wct/8kC+EYxUjP6B6e/Bu/jTyrfDY4fD9cNNyqt+ify5x69EPvtN9UllSJH99IaWTQFxzxHgJTLYomxQYqpctZRBRdfDM19i5Tw9gQyKi6g+LF0OsoTOsyNZNYgQHkSTgmCvgSWtwgvc+6/B9ElS+Dqdi1wG3vcW3+BFmW+5uYlHXrIj5mbN8PZsRMiS75igdOSMLcTBdwOMGWlkSGSlUYiM7zgczWJoVytMLCSKNWNMPInjVUpTmKKyvulVoWCi+/kDR6ECLuWKqCV12WpiCyk4AbJDNe11yO0eBEPbhs8vXpzsuELxBZ/qooUG0GL2mv2YV5bhwXM69Rmd+L5znCuYsu6YSOA5KQ9FFDlrI1u4e7ZkwhvLZv/rXg4vyJ/ykTeSzoyxVxNGFu6npGpIfjis+z3NyDeuCE5wYUHzxv6FMLkxgw25BUtnCwjKg+mv5mbNyGn+3UIr9mA9GHD2VWJIDz3TWqH1DJdRrqWgtd1BrSMdz4kEpzCQ2DndttWbpIBi6lSIwoMjhgKg7M1rq43saydgth3SxVVbG7dyhRGoYUKL13Kyl4pvMbsE5z0f7BfdDFBU+0Swper8YjMnEydyN40mdZUhC68NTUqgS3y8QdwtudE1M3dWX0tZeW2ggKxLyYcm2B0bswiLWWc2gjpL42HydJUUGFs7SrmGye/wzS3l4MB899nUGOv3C3I0EaAtEZR1UJmVli/8zCkcxPbvA45vW9H+sTJKmsUrjKCW/v30a9nJ4iBYxFa7ixalwGcE04iA3Orelb4vbcZDKlJO/E805SQkLYzTkeEOD/7l19gb9YEkXUkJrdvgW7XlZkK2tNrU1MkPgxfYjxEKjRJqfK3MkMB5QQxQ4T/djH8QwcjfczLidY1VwnBzd1s5w7oy7LzaxYcVWA9JaoLyKnPvHxle8LOPYnUJ+lQNk84Ghw7hodbg78Srf24Ata6lTRPNwzZGK+xkfAoMW0lB6ko6IIOTDlClvlTwfXGSSfSehYn4gMp7hKCW4c4ADTkMebEzxKzMFVpGauxjiDsl16hTFImHOJ7iNcFThbOyfyxlVNLUcQdLmgnnwZkSUkqeL5YPEk2rlR2CGxcaGRyw599XFbw2PZtiLJCEpwYj0QLwMoxsvxi0iQXpAsrK7JoQeIghWBQLWD+yjk1c89e2Fu3g+fBgYSeEwlQvlBlb7UvlXo57rNoIaw+/VV1WGTqUldLFyX68yZEv2D7dv8uFYGTjubFd0tMrhhTM8rGQQ4DDHO1Qn2F40ksWfcdgtHqInhHPEP25XQytGcgq2tHWL+sS7SvYnLtMR586ZNTVuYmTb6X3P5iuG+86YjgQua7ut0MmUQN//0CYuL7FKGglZP8K9WIFC2kqALPDIfe6ExCUJacEpAKlrlnP4zz2yJj0qtkTxJ9LWFXBLwERj9Lymon8TpRnNynukZUCvrkgujKBLfCjRmkkXVCQPFToXxSXiqX+9TkYWzjRmIB+rYQ/tLe2bsPdnZcfM+/QOHYHy+23Lfcypx7ESHnGGaBt1j+cqS0OqaxRGhCWdu5rcj331Sx4PmzZyq6V82KHeuSPCp1uBAjBdjdzPLDeWMP+IaO4L2PzKMUf4QgtHRy8MEzz6T2yaeJuct4SFUCHWOOvI/gvv127inB1JQFMDIVKCS/IR38qg4DSRTjaXMi2iIN52Zjwvs4aauCAdwKz5SFj+ceNhtYyIQmMe0xHRUN5lII9bMa8EsyBogcwt/R/QpXGcHDCz7hMP5aRtcUif0KpBBkpmWewCbeQyw7exQ0BpMzI0+/AYgJOfn9Mhh1TuL4CEfAfLVIUx1mquAAQtI8uiiQ2YR9+XIFlzpWqGOIb2eQ466KeVEb1oHD0Bo2Zon5QlH+TE7kxFU6++fe52j2417kOBgbEOT1tJrSebkcJmGtxZlZVSFWZpkFoElAjLvnnQrHl9B4cNI4mJvWwWhwauo9rEKJCjZh7tnHhsGF9NdxLEiapCJviWttbDX7Rj2PrE3ruTfOwfFT05fJDsylxAekpFn8CPOaGOitoJiiAsWCo999Rzr7W9LalxwRPMYaOMpCQqaSjlnThULv2g37Nd3gGz6yRGFwrNIL8eh9aiRyH+Awbg5T44YfESHg0lwcLBAOP25LkJUxNhnE/x20guIVmwJPZGwP7FKM7hHBGTDyx79AJMX8KUVBqh1LkUhuzk3E9h+G4+quLAjGF0XQYxW4+Pcc7S7jGMnr8D/2MExOJOsWycsYTZ9ma7+0A2nubeTmWdXRl7UYIarQYYUWIAE7PwRHp2vJDnc4EtWFvDPJc0n1lMCTYlD8v4CHQsLwaLuXHE22xuQIhuvmHvAOfbpahS58tKN1a2TM4gzN1MmsIElr5zCG+Eya8DIhcOEm9SyFTnDcWEB4uzTWAYTOCAVV+vRxJsZolBj7Uj4u41eeAY8id80qntwu0kwsHU2pgpiL+RKODPkWrYLUkBinpllJ7U2hrZwgZ07uJWPKdCWnfZyW1NTeJ0fw9YsrOIo9FrEVbGZkb6a1U8uE2J477uOgcCsWRnyVg7RUnJyhRo4/np0Hi4MMhXisKLg5WrViEBmNyIrVnEljTXxaY6U1c/P6xM1lNEvwN01GNQblBRxp4eUQApJF9XDcw8MC4HgKXfwsne3a8XWvs1VPLrLgQyIzjpmw525yuNDHURDHeecpvi84aYIaXHQKX1/nyNhXiajuvPYGyD+hlAvf48jtw4DCsWoJMBZvrvn4MkyLFqpzIRMPmldIRY5u/pOEQxJv/1SnIQgr62VhFbvpZiLNQ4h8tVB5pry842j1dw4tXMa9NmckZ0A7n5MawuIWrHKpp6KXV+jzep26qnEnrV+HvFl4QWtSwNnIe6Sfwr/pL79Ksr9ndcqT0r3Eh+0tz1PfcV7FMRAGPenxFS6ZY3WRsS29js6rMwV4n3gK5h1M+vQPRQDS/HPvl/kxN2mlbiT0r09po8f14oK0pQJaJavShoJ8X9o4MogbWbKYr1tuIuH/ETKmv8ETJtGQ7NhGZTv5kz9PSnDZk5AJgdHM9Qf3c/RiJJxXd/yTt1q9j0tecLKi8maQTDy6bihL0Ffvto7/3ZISPC5vKbESypz2GvtgVxz/Xf0JT0hKcHlJx812UAkg8yds7ng+IinBZSTjaC3X47nB43Xv/wCqN4m4c3aOmwAAAABJRU5ErkJggg==';
			default:
				return '';
		}
	}

	private function breakSentences($paragraph,$length_per_line)
	{
		if(strlen($paragraph) >= $length_per_line)
		{
			$result = '';
			for($position = 0; $position <= strlen($paragraph)-1 ; $position++)
			{
				if(($position % ($length_per_line-1)) == 0)
				{
					$result .= '<br>';
				}

				$result .= $paragraph[$position];
			}
			return $result;
		}

		return $paragraph;
	} 
}



