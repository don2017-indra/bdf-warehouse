<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use App\Helpers;
use App\CustomSerialNumber;
use Config;
use DB;
use Response;
use File;

class CustomReportController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
   		$this->middleware('warehouse');
	}
	
	public function index(Request $request)
	{
		$sortby = 'flight_date';
	    $sort = $request->get('order');
	if ($request->has('sortby') && ($request->get('sortby') == 'flight_number')) {
	    $sortby = 'flight_number';
	    $sort = $request->get('order');
	} else if ($request->has('sortby') && ($request->get('sortby') == 'flight_date')) {
	    $sortby = 'flight_date';
	    $sort = $request->get('order');
	}

	$flight_no_css_class = $this->getSortCssClass($sortby, $sort);
	$flight_date_css_class = $this->getSortCssClass($sortby, $sort);
	$order_result = $this->filterField($request);

	return view('admin.report.custom_report.index-customreport', 
		compact(
			'order_result',
			'order_status',
			'flight_no_css_class',
			'flight_date_css_class'
			)
		);
	}

	public function filterField(Request $request)
	{
		try {
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();
			$method = 'GET';

			$domain	= env('NEW_ROKKISHOP_DOMAIN');
			$api	= 'rest/V1/bigdutyfree-portal/flights/search';

			/* params request */
			$limit = $request->input('export_all_bulk') !== null ? 99999 : 10;


			$page = $request->input('page') ?? 1;
			$sort = $request->input('sortby') ?? 'flight_date';
			$order = $request->input('order') ?? 'DESC';

			$filter_input = $this->getFilterInput($request);

			$fields = implode('|', array_column($filter_input, 'field'));
			$values = implode('|', array_column($filter_input, 'value'));
			$conditions = implode('|', array_column($filter_input, 'condition'));

			$params = 
			'?page='.$page.
			'&limit='.$limit.
			'&sort='.$sort.
			'&order='.$order.
			'&field='.$fields.
			'&value='.$values.
			'&condition='.$conditions;

			$token = env('NEW_API_TOKEN');
			$res = $client->request($method, $domain.$api.$params, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				]
			]);
			$result = json_decode($res->getBody());

		} catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];dd($e);
		} catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];dd($e);
		} catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];dd($e);
		} 

		if(!$success){
			flash_msg()->error('Error',$message);
		}
			
		return $result;
	}

	public function getFilterInput(Request $request)
	{
		$url_params = array();

			$seller_id = Auth::user()->seller_account()->getResults()->seller_id;
			
			if(!Auth::user()->hasRole('administrator'))
			{
				$data = 
				[
					'field'=> 'seller_id',
					'value' => $seller_id,
					'condition' => 'eq'
				];
					array_push($url_params, $data);
			}

			if(!empty($request->input('flight_date')))
			{
				$date = explode('-', $request->input('flight_date'));

				$start = date_format(date_create($date[0]),'Y-m-d');
				$end = date_format(date_create($date[1]),'Y-m-d'); 
				
				$start_date_range = 
				[
					'field'=> 'flight_date',
					'value' => $start.' 00:00:00',
					'condition' => 'gteq'
				];
				array_push($url_params, $start_date_range);

				$end_date_range = 
				[
					'field'=> 'flight_date',
					'value' => $end.' 23:59:59',
					'condition' => 'lteq'
				];
				array_push($url_params, $end_date_range);
			}

			if(!empty($request->input('flight_number')))
			{
				$data = 
				[
					'field'=> 'flight_number',
					'value' => $request->input('flight_number'),
					'condition' => 'eq'
				];
				array_push($url_params, $data);
			}

			if(!empty($request->input('carrier_code')))
			{
				$data = 
				[
					'field'=> 'carrier_code',
					'value' => $request->input('carrier_code'),
					'condition' => 'eq'
				];
				array_push($url_params, $data);
			
			} else {

				$data = 
				[
					'field'=> 'carrier_code',
					'value' => 'AK,D7',
					'condition' => 'in'
				];
				array_push($url_params, $data);

			}

			return $url_params;
	}

	public function generateCustomReportExcel(Request $request)
	{
		$station_code = '';
		$serial_number = '';

		$carrier_code = $request->input('carrier_code');
		$flight_number = $request->input('carrier_code').$request->input('flight_no');
		$flight_no = $request->input('flight_no');
		$sector = $request->input('departure_station').' '.$request->input('arrival_station');
		$flight_date = $request->input('flight_date');

		$last_inserted_serial = CustomSerialNumber::where('id',1)->first();
		$raw_serial_number = 0;
		if(empty($last_inserted_serial))
		{
			 CustomSerialNumber::create(['last_serial_number' => 1]);
			 $raw_serial_number = sprintf('%08d', 1);
		} else {

			$raw_serial_number = intval($last_inserted_serial['last_serial_number']);
			$raw_serial_number++;

			CustomSerialNumber::where('id',1)->update(['last_serial_number' => $raw_serial_number]);
			$raw_serial_number = sprintf('%08d', $raw_serial_number);
		}

		if($carrier_code == 'AK')
		{
			$serial_number = ' DF '.$raw_serial_number;
			$station_code = 'WA8';
		} else if($carrier_code == 'D7') {
			$serial_number = ' DF '.$raw_serial_number;
			$station_code = 'W2T';
		}
		
		$writer = new \App\Http\Controllers\Admin\Reports\Helpers\CustomInFlightReportExport();

		$writer->setHeaderData(
			$station_code,
			$serial_number,
			$carrier_code,
			['flight_number' => $flight_number ,'sector' => $sector ,'date' => $flight_date ]
		);

		$seller_id = $request->input('seller_id');
		$items = $this->getOrderItems($seller_id,$flight_no,$flight_date,$carrier_code);
		$PDF = $this->insertReportItems($writer,$items);

		return $PDF;
	}

	public function insertReportItems($writer,$items)
	{
		foreach ($items as $key => $item) {

			$total_price = $item->price * $item->qty;

			$sold = 0;
			if($item->order_status == 'delivered')
			{
				$sold = $item->qty;
			}

			$closing = $item->qty - $sold;

			$amount_sold = 0;
			if($closing == 0)
			{
				$amount_sold = $total_price;
			}

			$remark = '';
			if($item->order_status == 'delivered' || $item->order_status == 'not_delivered')
			{	 
				$remark = str_replace('_', ' ',ucwords($item->order_status));
			}

			$writer->addItem(
				[
				'item_code' => $item->product_sku,
				'description' => $item->product_name,
				'vol_cl' => '',
				'unit_price' => number_format($item->price,2),
				'total_price' => number_format($total_price,2),
				'standard_opening' => number_format($item->qty,0)
				],
				['opening' => number_format($item->qty,0),'sold' => $sold,'closing' => $closing,'amount_sold' => $amount_sold],
				['opening' => '','closing' => $closing],
				['opening' => '','closing' => '','topup' => '','retrieval' => ''],
				$remark
			);
		}
			return $writer;
	}

	public function exportCustomReportExcel(Request $request) 
	{	
		$export_filename = 'Customs in-Flight Stock Report-'.date('now');
		$this->generateCustomReportExcel($request)->downloadPDF($export_filename);
		/*
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment; filename="'.$export_filename.'"');
		header('Cache-Control: max-age=0');
		$this->generateCustomReportExcel($request)->save('php://output');
		*/
	}

	public function exportAllCustomReports(Request $request)
	{
		//$temp_path = public_path().'/tmp_downloadall';

		/*
		if(!File::exists($temp_path)) {
		   	
		   	$newDir = File::makeDirectory($temp_path, 0775);

	        if(!$newDir) {
	        	
	        	flash_msg()->error('Error','The files cannot be downloaded!');
	        	return;
	        }
		}
		*/

        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');
		
		$temp_storage = base_path().'/public/tmp_downloadall';
		File::cleanDirectory($temp_storage);
		$order_result = $this->filterField($request);
	
		foreach($order_result->rows as $key => $group) {
					
			$group_data = 
			[
				'seller_id' => $group->seller_id,
				'flight_no' => $group->flight_number,
				'carrier_code' => $group->carrier_code,
				'flight_date' => date_format(date_create($group->flight_date),'Y-m-d'),
				'departure_station' => $group->departure_station,
				'arrival_station' => $group->arrival_station
			];

				$group_request = new Request();
				$group_request->replace($group_data);

				$flight_number = $group->flight_number ?? '0000';
				$flight_date = date_format(date_create($group->flight_date),'Y-m-d');

				$filename =  ($key+1).' - '.'Custom Reports - '.$flight_number.' - '.$flight_date.'.pdf'; 
				$doc = $this->generateCustomReportExcel($group_request);
				$doc->savePDFToPath($temp_storage.'/'.$filename);
		}
			
		$file_names = scandir($temp_storage);

        foreach ($file_names as $key => $value) {
            if($value=='.' || $value =='..')
            {
                unset($file_names[$key]);
            }
        }

		$public_dir = $temp_storage;
        $zipFileName = 'Custom Reports Bulk - '.date('now').'.zip';
        
        $zip = new \ZipArchive();
        if ($zip->open($public_dir . '/'. $zipFileName, \ZipArchive::CREATE) == TRUE) {
           foreach ($file_names as $key => $filename) {
            $filesource = $public_dir. '/' .$filename;
            $zip->addFile($filesource,$filename);
           }    

            $zip->close();

            foreach ($file_names as $key => $filename) {
            	File::delete($public_dir.'/'.$filename);
            }
        }
        
        $filetopath = $public_dir . '/' .$zipFileName;
        
        $headers = 
        	[
            'Content-Type : application/octet-stream'
            ];

        if(file_exists($filetopath)){
            return Response::download($filetopath, $zipFileName, $headers);
        }
	}

	public function getSortCssClass($sortBy, $sort)
	{
        if($sortBy == 'flight_date'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        } else if($sortBy == 'flight_number'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }
        
        return "sorting_desc";
    }
    
    public function getOrderItems($seller_id,$flight_no,$flight_date,$carrier_code)
	{	
			
			if(Auth::user()->hasRole('administrator'))
			{
				$seller_id = 0;
			}

		try {
			
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();
			$method = 'GET';

			$domain	= env('NEW_ROKKISHOP_DOMAIN');
			$api	= 'rest/V1/bigdutyfree-portal';

			$params = 
				'/flight-items/'.$seller_id.'/search'.
				'?flight_no='.$flight_no.
				'&flight_date='.str_replace('/', '-', $flight_date).
				'&carrier_code='.$carrier_code;
			
			$token = env('NEW_API_TOKEN');
			$res = $client->request($method, $domain.$api.$params, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				]
			]);

			$result = json_decode($res->getBody());

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];
		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		}

		return $result;
	}
}
