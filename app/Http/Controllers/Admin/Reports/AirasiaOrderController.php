<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use App\AirasiaOrder;
use App\AirasiaOrdersUploadFiles as OrdersUpload;

class AirasiaOrderController extends Controller
{

    private $sheet_model;
    private $fileupload;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        $this->middleware('auth');
        $this->sheet_model = new AirasiaOrder();
        $this->fileupload = new OrdersUpload();
    }

    public function index(Request $request)
    {
        
        $data['css_sort_purchase_date'] = "sorting_desc";
        $data['css_sort_pnr'] = "sorting_desc";
        $data['css_sort_flight_number'] = "sorting_desc";
        $data['css_sort_currency_code'] = "sorting_desc";
        $data['css_sort_item_code'] = "sorting_desc";
        $data['css_sort_item_description'] = "sorting_desc";
        $data['css_sort_quantity'] = "sorting_desc";
        $data['css_sort_charge_amount'] = "sorting_desc";
        $data['css_sort_departure_date'] = "sorting_desc";
        $data['css_sort_email_address'] = "sorting_desc";
        $data['css_sort_carrier_code'] = "sorting_desc";
        $data['css_sort_first_name'] = "sorting_desc";
        $data['css_sort_last_name'] = "sorting_desc";
        $data['css_sort_amount_in_myr'] = "sorting_desc";


        if($request->exists('order') && $request->exists('sortby'))
        {
           $key = "css_sort_".$request->get('sortby');
           $data[$key] = $this->getSortCssClass($request->get('sortby'),$request->get('order'));
           
        }

        $records = $this->getRecords($request);

        $data['sheet_records'] = $records;
        $data['links'] = $records->appends(['sortby' => $request->get('sortby'), 'order' => $request->get('order')])->links();
        return view('admin.report.airasia_orders.index-orders',$data);
    }


    public function upload(Request $request)
    {
     
    	$files = $request->file('spreadsheet');

        if($request->hasFile('spreadsheet'))
        {

            foreach ($files as $file) {
                
                //echo 'File Name: '.$file->getClientOriginalName();
                //echo 'File Extension: '.$file->getClientOriginalExtension();
                //echo 'File Real Path: '.$file->getRealPath();
                //echo 'File Mime Type: '.$file->getMimeType();

                $filetype = $file->getMimeType();
                if($filetype != "text/plain" && $filetype !="application/vnd.ms-excel"
                    && $filetype != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    flash_msg()->error('Upload Error','Sorry the file '.$file->getClientOriginalName().' is not supported for uploading');
                    continue;
                }

                $from = $file->getRealPath();
                Storage::putFileAs('files/airasia_orders', new File($from),$file->getClientOriginalName());

               
                $path = storage_path('app/files/airasia_orders');
                Excel::load(
                    $path.'/'.$file->getClientOriginalName(), 
                    function($reader) use ($file) {

                        $file_id = $this->fileupload->saveFileName($file->getClientOriginalName());
                        $results = $reader->toArray();
                        $this->sheet_model->savedata($file_id,$results);
                });
                
            }

        }else{

           flash_msg()->error('No selected files','Please choose files');
        }	

        return redirect('airasia/orders');

    }

    public function getRecordFromFiles()
    {

        $server = env("FTP_SERVER");
        $ftp_user = env("FTP_USER");
        $ftp_password = env("FTP_PASS");
        $temp_file = storage_path("app/tmp/")."temp_orders.csv";

        $connection_id = ftp_connect($server);
        $login_result = ftp_login($connection_id, $ftp_user, $ftp_password);

        ftp_pasv($connection_id, false);


        if ((!$connection_id) || (!$login_result))
        {
            Log::error('FTP connection has failed!');
        }else
        {
            $contents = ftp_nlist($connection_id, "airasia_orders/");

            if($contents)
            {
              foreach ($contents as $key => $file) {

                    $dir = explode("/", $file);
                    $filename = $dir[count($dir)-1];

                    if (ftp_get($connection_id, $temp_file, $file, FTP_BINARY))
                    {
                        Excel::load(
                            $temp_file,
                            function($reader) use ($filename) {

                                $file_id = $this->fileupload->saveFileName($filename);
                                $results = $reader->toArray();
                                $this->sheet_model->savedata($file_id,$results);
                            });

                        ftp_delete($connection_id,$file);
                    }else
                    {
                        Log::info("Failed to download ".$filename);
                    }
                }
            }
        }

        ftp_close($connection_id);
    }

    public function getRecords($request)
    {

        //default
        $sortCol = "purchase_date";
        $sortDirection = "ASC";


        if(($request->has('order') && $request->has('sortby')) &&
        (!empty($request->has('order')) && !empty($request->has('sortby')
        )))
        {

            $sortCol = $request->input('sortby');
            $sortDirection = $request->input('order');

        }


        if( !$request->has('flight_date')  && 
            !$request->has('sales_date') )
        {
            return $this->sheet_model->orderBy($sortCol,$sortDirection)->paginate(10);
        }


        $filter_query = array();
        $sort_query = array();

       if($request->has('flight_date'))
       {
            $fields = explode('-',$request->input('flight_date'));
            $filter_query['flight_date']['from'] = $fields[0];
            $filter_query['flight_date']['to'] = $fields[1];
       }

       if($request->has('sales_date'))
       {
            $fields = explode('-',$request->input('sales_date'));
            $filter_query['sales_date']['from'] = $fields[0];
            $filter_query['sales_date']['to'] = $fields[1];
       }
        
        //array_walk_recursive($filter_query, 'trim');
        return $this->sheet_model->search(
                $filter_query,
                $sortCol,
                $sortDirection
                );

    }

    public function getSortCssClass($sortBy, $sort){
        
        if($sortBy == 'purchase_date'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";
        }else if($sortBy == 'pnr'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'flight_number'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";
        }else if($sortBy == 'currency_code'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";
        }else if($sortBy == 'item_code'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";
        }else if($sortBy == 'item_description'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";
        }else if($sortBy == 'quantity'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";
        
        }else if($sortBy == 'charge_amount'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";

        }else if($sortBy == 'departure_date'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";

        }else if($sortBy == 'email_address'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";

        }else if($sortBy == 'first_name'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";

        }else if($sortBy == 'last_name'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";

        }else if($sortBy == 'amount_in_myr'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting";

        }else{
            return "sorting";
        }
    }

    public function paxSlip($order_id)
    {
        //$request->request->add(['order_id'=>$order_id]);
        $order_result = (object) $this->sheet_model->getOrderDetails($order_id);
        return view('admin.report.airasia_orders.paxslip-print',compact('order_result'));
    }


}