<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Mail;
use Config;
use DB;
use Excel;

class WarehouseReportsController extends Controller
{
	public function __construct(){
		
		$this->middleware('auth');
   		$this->middleware('warehouse');
	}
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 public function index(Request $request)
     {

	 	$order_status = [
	 		'paid' => 'Ready for packing',
	 		'reserved' => 'Reserved',
	 		'reprint_pack_slip' => 'Re-print Packing Slip',
	 		'cart_loaded' => 'Loaded on Cart',
	 		'delivered' => 'Delivered',
	 		'not_delivered' => 'Not Delivered',
	 		'order_cancelled' => 'Order Cancelled',
	 		'req_refund' => 'Requested Refund'

	 	];

	 	$aoc = [
	 		'' => 'All',
	 		'AK' => 'AK',
	 		'D7' => 'D7',
	 		'Z2' => 'Z2',
	 		'QZ' => 'QZ',
	 		'XT' => 'XT',
	 		'FD' => 'FD',
	 		'XJ' => 'XJ',
	 		'DJ' => 'DJ'
	 	];

	 	$f_type = [
	 		'' => 'All',
	 		'international' => 'International',
	 		'domestic' => 'Domestic'
	 	];
  
	    if ($request->has('sortby') && ($request->get('sortby') == 'order_id')) {
	        $sortby = 'order_id';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'pnr')) {
	        $sortby = 'pnr';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'flight_number')) {
	        $sortby = 'flight_number';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'sales_date')) {
	        $sortby = 'sales_date';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'flight_date')) {
	        $sortby = 'flight_date';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'order_status')) {
	        $sortby = 'order_status';
	        $sort = $request->get('order');
	    } else {
	        $sortby = 'sales_date';
	        $sort = 'DESC';
	    }  

	 	$export = false;
		$order_result = $this->filterField($request,$export,$sortby, $sort);

        $order_css_class = $this->getSortCssClass($sortby, $sort);
        $pnr_css_class = $this->getSortCssClass($sortby, $sort);
        $flight_no_css_class = $this->getSortCssClass($sortby, $sort);
        $sales_date_css_class = $this->getSortCssClass($sortby, $sort);
        $flight_date_css_class = $this->getSortCssClass($sortby, $sort);
        $status_css_class = $this->getSortCssClass($sortby, $sort);

		return view('admin.report.warehouse.index-warehouse', compact('order_result','order_status','aoc','f_type','order_css_class','pnr_css_class','flight_no_css_class','sales_date_css_class','flight_date_css_class','status_css_class'));
	 
	 }


	public function filterField($request,$export,$sortby, $sort)
	{
		try {
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			//http://stg-mg.bigdutyfree.com/rest/V1/bdf/report_preorder_flights/portal-search?limit=2&page=1&order=desc&field=order_status|carrier_code&value=unsuccess|D7
			$method = 'GET';

			$domain	= env('ROKKISHOP_DOMAIN');
			$api	= env('API_ORDER_SEARCH');

			/* params request */
			$params = '';
			$limit = 10;
			$yesterday = date('Y-m-d 00:00:00',strtotime("-1 days"));
			$today = date('Y-m-d 00:00:00');

			$defaultdate = $yesterday.','.$today;

			if($request->has('page'))
				$page = $request->input('page');
			else
				$page = 1;

			if($request->has('search-filter')){
				$field = '';
				$value = '';
				$condition = '';
				$arrSearch = $this->getFilterInput($request);
				//dd($arrSearch);
				$i = 1;
				foreach($arrSearch as $search_field){
					if($i == 1){
						$field .= $search_field['field'];
						
					 	if($search_field['field'] == 'order_status'){
							$status = $search_field['value'];
						 	$sts = '';
						 	foreach($status as $key => $status_value){
						 		if($key == 0){
						 			$sts = $status_value; 
						 		} else {
						 			$sts .= ','.$status_value;
						 		}
						 	}
							$value .= $sts;
						} elseif($search_field['field'] == 'flight_type' || $search_field['field'] == 'pnr' || $search_field['field'] == 'carrier_code'){
							//$value .= "'".$search_field['value']."'";
							$value .= $search_field['value'];
						} else {
							$value .= $search_field['value'];
						}

						$condition .= $search_field['condition'];

						
					} else {
						$field .= '|'.$search_field['field'];

					 	if($search_field['field'] == 'order_status'){
							$status = $search_field['value'];
						 	$sts = '';
						 	foreach($status as $key => $status_value){
						 		if($key == 0){
						 			$sts = $status_value; 
						 		} else {
						 			$sts .= ','.$status_value;
						 		}
						 	}
							$value .= '|'.$sts;
						} elseif($search_field['field'] == 'flight_type' || $search_field['field'] == 'pnr' || $search_field['field'] == 'carrier_code'){
							//$value .= "|'".$search_field['value']."'";
							$value .= '|'.$search_field['value'];
						} else {
							$value .= '|'.$search_field['value'];
						}

						$condition .= '|'.$search_field['condition'];
					}
					$i++;
				}

				if($export) {
					$params = "?limit=99999&order=".$sort."&sort=".$sortby."&field=".$field."&value=".$value."&condition=".$condition;
				} else {
					if(empty($field) && empty($value) && empty($condition)){
							$params = "?limit=".$limit."&page=".$page."&order=".$sort."&sort=".$sortby."&field=sales_date|order_status&value=".$defaultdate."|paid,reserved,reprint_pack_slip,cart_loaded,delivered,not_delivered,order_cancelled,req_refund&condition=between|in";
					} else {
							$params = "?limit=".$limit."&page=".$page."&order=".$sort."&sort=".$sortby."&field=".$field."&value=".$value."&condition=".$condition;
					}
				}
			} else {
				if($export){
					$params = "?limit=99999&order=".$sort."&sort=".$sortby."&field=order_status&value=paid,reserved,reprint_pack_slip,cart_loaded,delivered,not_delivered,order_cancelled,req_refund&condition=in";
				} else {
					$params = "?limit=".$limit."&page=".$page."&order=".$sort."&sort=".$sortby."&field=order_status&value=paid,reserved,reprint_pack_slip,cart_loaded,delivered,not_delivered,order_cancelled,req_refund&condition=in";
				}
			}
		

			$token = env('API_TOKEN');

			$res = $client->request($method, $domain.$api.$params, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				]
			]);

			$result = json_decode($res->getBody());
		

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];

		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		} 

		return $result;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function exportReportCSV(Request $request) {

		//for sorting   
	    if ($request->has('sortby') && ($request->get('sortby') == 'order_id')) {
	        $sortby = 'order_id';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'pnr')) {
	        $sortby = 'pnr';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'flight_number')) {
	        $sortby = 'flight_number';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'sales_date')) {
	        $sortby = 'sales_date';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'flight_date')) {
	        $sortby = 'flight_date';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'order_status')) {
	        $sortby = 'order_status';
	        $sort = $request->get('order');
	    } else {
	        $sortby = 'sales_date';
	        $sort = 'DESC';
	    }    
		
		$export = true;
		$bigItems = [];
		$order_result = $this->filterField($request,$export,$sortby, $sort);

		$price = 0;
		$qty = 0;
		$count = 1;
		if($order_result){
			foreach($order_result->rows as $key => $order) {
				foreach ($order->items as $key => $item) {
					$price = $item->price_per_item;
					$final_discounted_price = $item->final_discounted_price;
					$qty = number_format($item->qty,0);
					$final_total_price = $final_discounted_price * $qty;
					$data = [
						'Order No' => $order->increment_id,
						'PNR' => $order->pnr,
						'Carrier Code' =>$order->carrier_code,
						'Flight No' => $order->flight_number,
						'Departure' => $order->departure_station,
						'Arrival' => $order->arrival_station,
						'Sales Date' => date_format(date_create($order->sales_date),"m/d/Y"),
						'Flight Date' => date_format(date_create($order->flight_date),"m/d/Y"),
						'Recipient Name' => $order->first_name.' '.$order->last_name,
						'SKU' => $item->product_sku,
						'Product Name' => $item->product_name,
						'Original Price' => $item->price_per_item,
						'Discounted Price' => $item->discounted_price,
						'Final Discounted Price' => $final_discounted_price,
						'Quantity' => number_format($item->qty,0),
						'Final Total Price' => $final_total_price,
						'Status' => $this->getStatusDesc($order->order_status),
					];

					array_push($bigItems, $data);
					$count++;
				}
			}
		}

		//dd($bigItems);
		$filename = 'Warehouse_Report-'.date("m-d-Y");

		Excel::create($filename, function($excel) use($bigItems,$count) {

            $excel->sheet('Warehouse_Report', function($sheet) use($bigItems,$count) {

                $sheet->fromArray($bigItems);

                for($column = 2; $column <= $count; $column++) {
                	
                	$sheet->getStyle('A'.$column)->getNumberFormat()->setFormatCode('000000000');
            	}

            });

        })->download('xls');
	}

	public function getStatusDesc($type){
		switch ($type) {
            case "reserved":
                $desc = "Reserved";
                break;
            case "reprint_pack_slip":
                $desc = "Re-print Packing Slip";
                break;
            case "cart_loaded":
                $desc = "Loaded on Cart";
                break;
            case "delivered":
                $desc = "Delivered";
                break;
            case "not_delivered":
                $desc = "Not Delivered";
                break;
            case "order_cancelled":
                $desc = "Order Cancelled";
                break;
            case "req_refund":
                $desc = "Refund Requested";
                break;
            default:
                $desc = "Ready for packing";
                break;
        }

        return $desc;
	}

	public function getFilterInput($request)
	{
		//flight_date,sales_date,flight_type,flight_no,order_no,aoc,order_status
			
			$arrSearch = [];
			if($request->has('flight_date')){

				$f_date = explode(' - ', $request->input('flight_date'));
				
				if($f_date[0] == $f_date[1]){
					$f_start_date = date("Y-m-d 00:00:00", strtotime($f_date[0]));
					$f_end_date = date("Y-m-d 00:00:0", strtotime($f_date[1].'+1 days'));
				} else {
					$f_start_date = date("Y-m-d 00:00:00", strtotime($f_date[0]));
					$f_end_date = date("Y-m-d 00:00:00", strtotime($f_date[1].'+1 days'));
				}
				$data = [
					'field' => 'flight_date',
					'value' => $f_start_date.','.$f_end_date,
					'condition' => 'between'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('sales_date')){
				$s_date = explode(' - ', $request->input('sales_date'));
				if($s_date[0] == $s_date[1]){
					$s_start_date = date("Y-m-d 00:00:00", strtotime($s_date[0]));
					$s_end_date = date("Y-m-d 00:00:0", strtotime($s_date[1].'+1 days'));
				} else {
					$s_start_date = date("Y-m-d 00:00:00", strtotime($s_date[0]));
					$s_end_date = date("Y-m-d 00:00:0", strtotime($s_date[1].'+1 days'));
				}

				$data = [
					'field' => 'sales_date',
					'value' => $s_start_date.','.$s_end_date,
					'condition' => 'between'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('flight_type')){

				$data = [
					'field' => 'flight_type',
					'value' => $request->input('flight_type'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('pnr')){

				$data = [
					'field' => 'pnr',
					'value' => $request->input('pnr'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('flight_number')){

				$data = [
					'field' => 'flight_number',
					'value' => $request->input('flight_number'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('order_id')){

				$data = [
					'field' => 'order_id',
					'value' => $request->input('order_id'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('aoc')){

				$data = [
					'field' => 'aoc',
					'value' => $request->input('aoc'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('increment_id')){

				$data = [
					'field' => 'increment_id',
					'value' => $request->input('increment_id'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('company_code')){

				$data = [
					'field' => 'company_code',
					'value' => $request->input('company_code'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('carrier_code')){

				$data = [
					'field' => 'carrier_code',
					'value' => $request->input('carrier_code'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('seat_number')){

				$data = [
					'field' => 'seat_number',
					'value' => $request->input('seat_number'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('recipient')){

				$data = [
					'field' => 'recipient',
					'value' => $request->input('recipient'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('order_status')){

				$data = [
					'field' => 'order_status',
					'value' => $request->input('order_status'),
					'condition' => 'in'
				];

				array_push($arrSearch, $data);

			} else {
				$data = [
					'field' => 'order_status',
					'value' => ['paid','reserved','reprint_pack_slip','cart_loaded','delivered','not_delivered','order_cancelled','req_refund'],
					'condition' => 'in'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('sku')){

				$data = [
					'field' => 'sku',
					'value' => $request->input('sku'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			return $arrSearch;
	}

	public function getSortCssClass($sortBy, $sort){
        
        if($sortBy == 'flight_date'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'sales_date'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'flight_type'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'pnr'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'flight_number'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'order_id'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'increment_id'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else{
            return "sorting_desc";
        }
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function orderDetail(Request $request,$order_id) {
		$request->request->add(['order_id' => $order_id]);
		$order_result = $this->apiOrderDetail($request);
		//dd($request);
		return view('admin.report.warehouse.order-warehouse-detail', compact('order_result'));
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function paxSlip(Request $request,$order_id) {
		$request->request->add(['order_id' => $order_id]);
		$order_result = $this->apiOrderDetail($request);
		//dd($request);
		return view('admin.report.warehouse.pax_slip', compact('order_result'));
	}

	public function apiOrderDetail($request) {
		try {
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			$method = 'GET';

			$domain	= env('ROKKISHOP_DOMAIN');
			$api	= env('API_ORDER_SEARCH');

			/* params request */
			$limit = 10;
			$order_by = 'DESC';
			$sort_by = 'sales_date';
			$page = 1;
			$field = 'order_id';
			$value = $request->input('order_id');
			$condition = 'eq';
			$params = "?limit=".$limit."&page=".$page."&order=DESC&sort=".$sort_by."&field=".$field."&value=".$value."&condition=".$condition;
			//echo $domain.$api.$params;
			$token = env('API_TOKEN');
			$res = $client->request($method, $domain.$api.$params, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				]
			]);

			$result = json_decode($res->getBody());

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		}

		return $result;
	}

	public function updateStatus(Request $request,$order_id){

		try {
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			$status = $request->input('update_status');
			$comment = $request->input('comment');
			$user_id = Auth::user()->id;

			//http://stg-mg.bigdutyfree.com/rest/V1/bdf/report_preorder_flights/order-status?order_id=5017&status=reprint_pack_slip&comment=testing for update status&user_id=1
			$method = 'PUT';

			$domain	= env('ROKKISHOP_DOMAIN');
			$api	= env('API_ORDER_STATUS');

			/* params request */
			$params = "?order_id=".$order_id."&status=".$status."&comment=".$comment."&user_id=".$user_id;

			$token = env('API_TOKEN');
			$res = $client->request($method, $domain.$api.$params, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				]
			]);

			$result = json_decode($res->getBody());

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		}

		//dd($domain.$api.$params);
		if($result){
			flash_msg()->success('Updated!','Your order has been updated!');
		} else {
			flash_msg()->error('Updated!','Updated error!');
		}
		
        
        return redirect()->back();
		
	}

}
