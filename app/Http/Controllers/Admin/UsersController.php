<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\UserSellers;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;

class UsersController extends Controller
{
   
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('manage_accounts')) {
            return redirect()->to('/');
        }

        $users = User::whereHas('roles', function($query) {
            $primary_roles = [
                'warehouse_user',
                'Warehouse',
                'administrator'
            ];

            $query->whereIn('name',$primary_roles)
            ->where('guard_name','web');
        })
        ->get();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('manage_accounts')) {
            return redirect()->to('/');
        }
        $roles = Role::get()->pluck('name', 'name');

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsersRequest $request)
    {
        if (! Gate::allows('manage_accounts')) {
            return abort(404);
        }
        $user = User::create($request->all());
        $seller = UserSellers::create(
            [
            'seller_id' => $request->input('seller_id'),
            'user_id' => $user->id
            ]);
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        return redirect()->route('admin.users.index');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('manage_accounts')) {
            return redirect()->to('/');
        }
        $roles = Role::get()->pluck('name', 'name');

        $user = User::findOrFail($id);
        $seller_account = UserSellers::where('user_id',$id)->first();

        return view('admin.users.edit', compact('user', 'roles','seller_account'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsersRequest $request, $id)
    {
        if (! Gate::allows('manage_accounts')) {
            return abort(404);
        }
        $user = User::findOrFail($id);
        $user->update($request->all());

        $seller = UserSellers::where('user_id',$id)->update(
            ['seller_id' => $request->input('seller_id')
            ]);

        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);

        return redirect()->route('admin.users.index');
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('manage_accounts')) {
            return abort(404);
        }
        $user = User::findOrFail($id);
        $user->seller_account()->delete();
        $user->delete();

        return redirect()->route('admin.users.index');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('manage_accounts')) {
            return abort(404);
        }
        if ($request->input('ids')) {
            $entries = User::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
