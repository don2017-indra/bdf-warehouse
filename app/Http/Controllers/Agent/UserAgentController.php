<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;
use Illuminate\Session\Store;

class UserAgentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('agent');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $agent_name = Session::has('AgentName') ? Session::get('AgentName') : '';
            $success = true;
            $message = 'Found matching user';
            $client = new Client();

            $method = 'GET';

            //$domain = 'http://stg-mg.bigdutyfree.com/';
            $domain = env('ROKKISHOP_DOMAIN');
            $api    = env('API_CUSTOMER_SEARCH');

            /* params request */
            $params = '?searchCriteria[filterGroups][0][filters][0][field]=agent_name&searchCriteria[filterGroups][0][filters][0][value]='.$agent_name;

            //echo $domain.$api.$params;

            $token = env('API_TOKEN');
            $res = $client->request($method, $domain.$api.$params, [
                'headers' => [
                    'Authorization'      => 'Bearer '.$token
                ]
            ]);

            $result = json_decode($res->getBody());
            $status_code = 200;

        }catch(\GuzzleHttp\Exception\ClientException $e) {
            $status_code = $e->getResponse()->getStatusCode();
            $success = false;
            if($status_code == 422){
                $message = 'Please complete all required fields';
            }
            $result = [];

        }catch(\GuzzleHttp\Exception\ConnectException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently offline';
            $result = [];
        }catch(\GuzzleHttp\Exception\ServerException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently encountered a problem';
            $result = [];
        }

        
        if(!empty($result)){
            
            return $this->getAgentId($result);   
        }

        
    }

    public function getAgentId($result) {

        if (is_array($result) || is_object($result))
        {
            foreach($result->items as $key => $user) {
                $agentID = $user->id;
            }
        }
        
        return $agentID;
    }
}
