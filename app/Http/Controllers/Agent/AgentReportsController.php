<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Agent\UserAgentController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use Mail;
use Config;
use DB;
use Carbon;
use Excel;
use Session;

use nusoap_client;

class AgentReportsController extends Controller
{
	public function __construct(){

    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 public function index(Request $request){

	 	$order_status = [
	 		'delivered' => 'Delivered',
            'ready_packing' => 'Ready for packing',
            'reserved' => 'Reserved',
            'reprint_pack_slip' => 'Re-print Packing Slip',
            'cart_loaded' => 'Loaded on Cart',
            'not_delivered' => 'Not Delivered',
            'order_cancelled' => 'Order Cancelled',
            'req_refund' => 'Requested Refund',
            'processing'=> 'Processing',
            'partial_complete' => 'Partial Complete',
            'complete' => 'Complete'
	 	];

	 	$aoc = [
	 		'' => 'All',
	 		'AK' => 'AK',
	 		'D7' => 'D7',
	 		'Z2' => 'Z2',
	 		'QZ' => 'QZ',
	 		'XT' => 'XT',
	 		'FD' => 'FD',
	 		'XJ' => 'XJ',
	 		'DJ' => 'DJ'
	 	];

	 	$f_type = [
	 		'' => 'All',
	 		'international' => 'International',
	 		'domestic' => 'Domestic'
	 	];

	 	//for sorting   
	    if ($request->has('sortby') && ($request->get('sortby') == 'order_id')) {
	        $sortby = 'order_id';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'pnr')) {
	        $sortby = 'pnr';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'flight_number')) {
	        $sortby = 'flight_number';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'sales_date')) {
	        $sortby = 'sales_date';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'flight_date')) {
	        $sortby = 'flight_date';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'order_status')) {
	        $sortby = 'order_status';
	        $sort = $request->get('sort');
	    } else {
	        $sortby = 'sales_date';
	        $sort = 'DESC';
	    }  

	 	$export = false;

	 	$agentID = Session::has('AgentID') ? Session::get('AgentID') : '';
	 	$order_result = $this->filterField($request,$export,$sortby, $sort);
       
		//Get sorting CSS class
        $order_css_class = $this->getSortCssClass($sortby, $sort);
        $pnr_css_class = $this->getSortCssClass($sortby, $sort);
        $flight_no_css_class = $this->getSortCssClass($sortby, $sort);
        $sales_date_css_class = $this->getSortCssClass($sortby, $sort);
        $flight_date_css_class = $this->getSortCssClass($sortby, $sort);
        $status_css_class = $this->getSortCssClass($sortby, $sort);

		return view('agent.report.index-agent', compact('order_result','order_status','aoc','f_type','order_css_class','pnr_css_class','flight_no_css_class','sales_date_css_class','flight_date_css_class','status_css_class'));
	 
	}


	public function filterField($request,$export,$sortby, $sort)
	{
		$success = true;
			$message = 'Found matching list of order';
			$client = new Client();
			$method = 'GET';
			$domain	= env('ROKKISHOP_DOMAIN');
			$api	= env('API_ORDER_SEARCH');

			/* params request */
			$params = '';
			$limit = 10;
			$order_by = 'DESC';
			$sort_by = 'sales_date';
			$yesterday = date('Y-m-d 00:00:00',strtotime("-1 days"));

			if($request->has('page')) {
				$page = $request->input('page');
			} else {
				$page = 1;
			}
					
				$groupID = 4;
				if($request->has('search-filter')){
					$field = '';
					$value = '';
					$condition = '';
					$arrSearch = $this->getFilterInput($request);

					$i = 1;
					foreach($arrSearch as $search_field){
						if($i == 1){
							$field .= $search_field['field'];
							
						 	if($search_field['field'] == 'order_status'){
								$status = $search_field['value'];
							 	$sts = '';
							 	foreach($status as $key => $status_value){
							 		if($key == 0){
							 			$sts = $status_value; 
							 		} else {
							 			$sts .= ','.$status_value;
							 		}
							 	}
								$value .= $sts;
							} elseif($search_field['field'] == 'flight_type' || $search_field['field'] == 'pnr' || $search_field['field'] == 'carrier_code'){
								//$value .= "'".$search_field['value']."'";
								$value .= $search_field['value'];
							} else {
								$value .= $search_field['value'];
							}

							$condition .= $search_field['condition'];

							
						} else {
							$field .= '|'.$search_field['field'];

						 	if($search_field['field'] == 'order_status'){
								$status = $search_field['value'];
							 	$sts = '';
							 	foreach($status as $key => $status_value){
							 		if($key == 0){
							 			$sts = $status_value; 
							 		} else {
							 			$sts .= ','.$status_value;
							 		}
							 	}
								$value .= '|'.$sts;
							} elseif($search_field['field'] == 'flight_type' || $search_field['field'] == 'pnr' || $search_field['field'] == 'carrier_code'){
								//$value .= "|'".$search_field['value']."'";
								$value .= '|'.$search_field['value'];
							} else {
								$value .= '|'.$search_field['value'];
							}

							$condition .= '|'.$search_field['condition'];
						}
						$i++;
					}

					 if(!empty(Session::get('AgentID')))
                    {
                        $field .='|customer_id';
                        $value .='|'.Session::get('AgentID');
                        $condition .='|eq';
                    }
            

					if($export) {
						
						$params = "?limit=99999&order=".$sort."&sort=".$sortby."&field=".$field."|group_id&value=".$value."|".$groupID."&condition=".$condition."|eq";
						
					} else {

						if(empty($field) && empty($value) && empty($condition)){
						
								$params = "?limit=".$limit."&page=".$page."&order=".$sort."&sort=".$sortby."&field=group_id|order_status&value=".$groupID."|paid,reserved,reprint_pack_slip,cart_loaded,delivered,not_delivered,order_cancelled,req_refund&condition=eq|in";
						
						} else {
								
								$params = "?limit=".$limit."&page=".$page."&order=".$sort."&sort=".$sortby."&field=".$field."|group_id&value=".$value."|".$groupID."&condition=".$condition."|eq";
						
						}
					}

				} else {

					$field ='group_id|order_status';
                    $value= $groupID.'|paid,reserved,reprint_pack_slip,cart_loaded,delivered,not_delivered,order_cancelled,req_refund';
                    $condition='eq|in';

	                if(!empty(Session::get('AgentID')))
	                {
	                    $field .='|customer_id';
	                    $value .='|'.Session::get('AgentID');
	                    $condition .='|eq';
	                }

					if($export) {
	                    
	                    $params = "?limit=99999&page=".$page."&order=".$sort."&sort=".$sortby."&field=".$field."&value=".$value."&condition=".$condition;
	        
	                }else{
	                
	                    $params = "?limit=".$limit."&page=".$page."&order=".$sort."&sort=".$sortby."&field=".$field."&value=".$value."&condition=".$condition;
	                }
					
				}

			$token = env('API_TOKEN');
			$res = $client->request($method, $domain.$api.$params, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				]
			]);

			$result = json_decode($res->getBody());

		return $result;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function exportReportCSV(Request $request) 
	{

		//for sorting   
	    if ($request->has('sortby') && ($request->get('sortby') == 'order_id')) {
	        $sortby = 'order_id';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'pnr')) {
	        $sortby = 'pnr';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'flight_number')) {
	        $sortby = 'flight_number';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'sales_date')) {
	        $sortby = 'sales_date';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'flight_date')) {
	        $sortby = 'flight_date';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'order_status')) {
	        $sortby = 'order_status';
	        $sort = $request->get('order');
	    } else {
	        $sortby = 'sales_date';
	        $sort = 'DESC';
	    }  
		
		$export = true;
		$bigItems = [];
		
		$order_result = $this->filterField($request,$export,$sortby, $sort);

		$price = 0;
		$qty = 0;
		$count = 1;
		if($order_result){
			foreach($order_result->rows as $key => $order) {
				foreach ($order->items as $key => $item) {
					$price = $item->price_per_item;
					$final_discounted_price = $item->final_discounted_price;
					$qty = number_format($item->qty,0);
					$final_total_price = $final_discounted_price * $qty;
					$data = [
						'Order No' => $order->increment_id,
						'PNR' => $order->pnr,
						'Carrier Code' =>$order->carrier_code,
						'Flight No' => $order->flight_number,
						'Departure' => $order->departure_station,
						'Arrival' => $order->arrival_station,
						'Sales Date' => date('Y-m-d',strtotime($order->sales_date)),
						'Flight Date' => date('Y-m-d',strtotime($order->flight_date)),
						'Recipient Name' => $order->first_name.' '.$order->last_name,
						'SKU' => $item->product_sku,
						'Product Name' => $item->product_name,
						'Original Price' => $item->price_per_item,
						'Discounted Price' => $item->discounted_price,
						'Final Discounted Price' => $final_discounted_price,
						'Quantity' => number_format($item->qty,0),
						'Final Total Price' => $final_total_price,
						'Status' => $this->getStatusDesc($order->order_status)
					];

						$data['Agent First Name'] = isset($order->agent->firstname) ? $order->agent->firstname : '';
						$data['Agent Last Name'] = isset($order->agent->lastname) ? $order->agent->lastname : '';
						$data['Organization Code'] = isset($order->agent->custom_attributes->organization_code) ? $order->agent->custom_attributes->organization_code : '';
						
						$domain_code = isset($order->agent->custom_attributes->domain_code) ? $order->agent->custom_attributes->domain_code : '';
                        if($domain_code === 'EXT'){

                            $data['Agent Type'] = 'Travel Agent';

                        }else if($domain_code === 'DEF'){

                            $data['Agent Type'] = 'ATSC Agent';

                        } else {

                        	  $data['Agent Type'] = '';
                        }
                           
                        $data['Payment Method'] = isset($order->agent->payment->payment_method) ? $order->agent->payment->payment_method : '';
                        $data['Agent Collected Currency'] = isset($order->agent->payment->agent_collected_currency) ? $order->agent->payment->agent_collected_currency : '';
                        $data['Agent Collected Amount'] = isset($order->agent->payment->agent_collected_amount) ? $order->agent->payment->agent_collected_amount : '';
                        

					array_push($bigItems, $data);
					$count++;
				}
			}
		}
		//dd($bigItems);
		$filename = 'Agent_Report-'.date('now');

		Excel::create($filename, function($excel) use($bigItems,$count) {

            $excel->sheet('Agent_Report', function($sheet) use($bigItems,$count) {

                $sheet->fromArray($bigItems);

                for($column = 2; $column <= $count; $column++) {
                	
                	$sheet->getStyle('A'.$column)->getNumberFormat()->setFormatCode('000000000');
            	}

            });

        })->download('csv');
	}

	public function getStatusDesc($type){
		switch ($type) {
			case "paid":
                $desc = "Paid";
                break;
            case "reserved":
                $desc = "Reserved";
                break;
            case "reprint_pack_slip":
                $desc = "Re-print Packing Slip";
                break;
            case "cart_loaded":
                $desc = "Loaded on Cart";
                break;
            case "delivered":
                $desc = "Delivered";
                break;
            case "not_delivered":
                $desc = "Not Delivered";
                break;
            case "order_cancelled":
                $desc = "Order Cancelled";
                break;
            case "req_refund":
                $desc = "Requested Refund";
                break;
            case "processing":
                $desc = "Processing";
                break;
            case "partial_complete":
                $desc = "Partial Complete";
                break;
            case "complete":
                $desc = "Complete";
                break;    
            default:
                $desc = "Paid";
                break;
        }

        return $desc;
	}

	public function getFilterInput($request)
	{
		//flight_date,sales_date,flight_type,flight_no,order_no,aoc,order_status
			
			$arrSearch = [];
			if($request->has('flight_date')){

				$f_date = explode(' - ', $request->input('flight_date'));
				
				if($f_date[0] == $f_date[1]){
					$f_start_date = date("Y-m-d 00:00:00", strtotime($f_date[0]));
					$f_end_date = date("Y-m-d 00:00:0", strtotime($f_date[1].'+1 days'));
				} else {
					$f_start_date = date("Y-m-d 00:00:00", strtotime($f_date[0]));
					$f_end_date = date("Y-m-d 00:00:0", strtotime($f_date[1].'+1 days'));
				}
				$data = [
					'field' => 'flight_date',
					'value' => $f_start_date.','.$f_end_date,
					'condition' => 'between'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('sales_date')){
				$s_date = explode(' - ', $request->input('sales_date'));
				if($s_date[0] == $s_date[1]){
					$s_start_date = date("Y-m-d 00:00:00", strtotime($s_date[0]));
					$s_end_date = date("Y-m-d 00:00:0", strtotime($s_date[1].'+1 days'));
				} else {
					$s_start_date = date("Y-m-d 00:00:00", strtotime($s_date[0]));
					$s_end_date = date("Y-m-d 00:00:00", strtotime($s_date[1].'+1 days'));
				}

				$data = [
					'field' => 'sales_date',
					'value' => $s_start_date.','.$s_end_date,
					'condition' => 'between'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('flight_type')){

				$data = [
					'field' => 'flight_type',
					'value' => $request->input('flight_type'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('pnr')){

				$data = [
					'field' => 'pnr',
					'value' => $request->input('pnr'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('flight_number')){

				$data = [
					'field' => 'flight_number',
					'value' => $request->input('flight_number'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('order_id')){

				$data = [
					'field' => 'order_id',
					'value' => $request->input('order_id'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('aoc')){

				$data = [
					'field' => 'aoc',
					'value' => $request->input('aoc'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('increment_id')){

				$data = [
					'field' => 'increment_id',
					'value' => $request->input('increment_id'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('company_code')){

				$data = [
					'field' => 'company_code',
					'value' => $request->input('company_code'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('carrier_code')){

				$data = [
					'field' => 'carrier_code',
					'value' => $request->input('carrier_code'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('seat_number')){

				$data = [
					'field' => 'seat_number',
					'value' => $request->input('seat_number'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('recipient')){

				$data = [
					'field' => 'recipient',
					'value' => $request->input('recipient'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('order_status')){

				$data = [
					'field' => 'order_status',
					'value' => $request->input('order_status'),
					'condition' => 'in'
				];

				array_push($arrSearch, $data);

			} else {
				$data = [
					'field' => 'order_status',
					'value' => ['paid','reserved','reprint_pack_slip','cart_loaded','delivered','not_delivered','order_cancelled','req_refund'],
					'condition' => 'in'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('sku')){

				$data = [
					'field' => 'sku',
					'value' => $request->input('sku'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			return $arrSearch;
	}

	public function getSortCssClass($sortBy, $sort){
        
        if($sortBy == 'flight_date'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'sales_date'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'flight_type'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'pnr'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'flight_number'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'order_id'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else if($sortBy == 'increment_id'){
            if($sort == 'ASC')
                return "sorting_asc";
            else if($sort == 'DESC')
                return "sorting_desc";
            else
                return "sorting_desc";
        }else{
            return "sorting_desc";
        }
    }

    public function apiOrderDetail($request) {
		try {
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			$method = 'GET';

			$domain	= env('ROKKISHOP_DOMAIN');
			$api	= env('API_ORDER_SEARCH');

			/* params request */
			$limit = 10;
			$order_by = 'DESC';
			$sort_by = 'sales_date';
			$page = 1;
			$field = 'order_id';
			$value = $request->input('order_id');
			$condition = 'eq';
			$params = "?limit=".$limit."&page=".$page."&order=DESC&sort=".$sort_by."&field=".$field."&value=".$value."&condition=".$condition;
			//echo $domain.$api.$params;
			$token = env('API_TOKEN');
			$res = $client->request($method, $domain.$api.$params, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				]
			]);

			$result = json_decode($res->getBody());

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		}

		return $result;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function orderDetail(Request $request,$order_id) {
		$request->request->add(['order_id' => $order_id]);
		$order_result = $this->apiOrderDetail($request);
		//dd($request);
		return view('agent.report.order-detail', compact('order_result'));
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function paxSlip(Request $request,$order_id) {
		$request->request->add(['order_id' => $order_id]);
		$order_result = $this->apiOrderDetail($request);
		//dd($request);
		return view('agent.report.pax_slip', compact('order_result'));
	}

	public function getPnr($order_id) {

	}

	public function getSessionBooking(){
		try {
				$success = true;
				$message = 'Found matching user';
				$client = new Client();

				$method = 'GET';

				//$api	= 'getsession';
				$domain = env('NAVITAIRE_DOMAIN');
				$api = env('NAVITAIRE_GETSESSION');

				/* params request */

				$res = $client->request($method, $domain.$api, [
					'headers' => [
						'agentName'		=> env('NAVITAIRE_AGENT_USERNAME'),
						'agentPassword' => env('NAVITAIRE_AGENT_PASSWORD'),
                        'domainCode' => env('NAVITAIRE_DOMAIN_CODE'),
						'Authorization' => 'Bearer '.env('NAVITAIRE_TOKEN')
					]
				]);

				$result = $res->getBody()->getContents();
				Session::put('AccessSession', $result);

			}catch(\GuzzleHttp\Exception\ClientException $e) {
				$status_code = $e->getResponse()->getStatusCode();
				$success = false;
				if($status_code == 422){
					$message = 'Please complete all required fields';
				}
				$this->session->put('error','Please complete all required fields');
				$result = [];

			}catch(\GuzzleHttp\Exception\ConnectException $e) {
				$success = false;
				$message = 'We are sorry to inform that server is currently offline';
				$this->session->put('error',$message);
				$result = [];
			}catch(\GuzzleHttp\Exception\ServerException $e) {
				$success = false;
				$message = 'We are sorry to inform that server is currently encountered a problem';
				$this->session->put('error',$message);
				$result = [];
			}

			if(empty($result)){
				return false;
			} else {
				if(Session::has('AccessSession')) {
			        return true;
			    } else {
			        return false;
			    }
			}
	}

	public function apiGetBooking($pnr) {
		try {
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			$method = 'GET';

			$domain = env('NAVITAIRE_DOMAIN');
			$api = env('NAVITAIRE_GETBOOKING');

			/* params request */
			$params = $pnr;
			//echo $domain.$api.$params;

			$access_session = '';
			if($this->getSessionBooking()){
				$access_session = Session::has('AccessSession') ? Session::get('AccessSession') : '';
			
			}

			$res = $client->request($method, $domain.$api.$params, [
				'headers' => [
					'authToken'	=> $access_session,
					'Authorization' => 'Bearer '.env('NAVITAIRE_TOKEN')
				]
			]);

			$result = json_decode($res->getBody());

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
			
		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
			
		}
	
		$arrData  = $result;
		
		$flightData = [];
		$passengerData = [];
		$fullselectData = [];
		$seat_arr = [];
		$segmentsData ='';
		//Flight
		if(!empty($arrData)){
			
			//flight
			if(isset($arrData->GetBookingResponse->Booking->Journeys)) {
				$journeys = $arrData->GetBookingResponse->Booking->Journeys;
				foreach($journeys->Journey as $journeyKey => $journey){

					if(isset($journey->Segments)){
						//echo count($journey->Segments);
						$segmentsData = $journey->Segments->Segment;
						//print_r($segmentsData);
						if(is_array($segmentsData)){
							//echo 'more than 2';
							foreach($segmentsData as $segmentKey => $segment){
								$arrival = $segment->ArrivalStation;
								$arrival_time = explode("T",$segment->STA);
								$departure = $segment->DepartureStation;
								$departure_time = explode("T",$segment->STD);
								$carrier_code = $segment->FlightDesignator->CarrierCode;
								$flight_no = $segment->FlightDesignator->FlightNumber;
								$flight_type = $segment->International == 'true' ? 'international' : 'domestic';
								$emails = '';
								if(isset($segment->PaxSeats->PaxSeat)){
									$seats = $segment->PaxSeats->PaxSeat;
									foreach ($seats as $seatkey => $value) {
										$seat = $value->UnitDesignator;
										if($value->ArrivalStation == $arrival){
											array_push($seat_arr, $seat);
										}
										
									}
								}
								
								$data = [
									'bookingId' => $arrData->GetBookingResponse->Booking->RecordLocator,
									'flight_date' => $segment->STD,
									'flight_no' => $flight_no,
									'std' => $segment->STD,
									'sta' => $segment->STA,
									'departure' => $departure,
									'departure_date' => $departure_time[0],
									'departure_time' => $departure_time[1],
									'arrival' => $arrival,
									'arrival_date' => $arrival_time[0],
									'arrival_time' => $arrival_time[1],
									'carrier_code' => $carrier_code,
									'flight_type' => $flight_type,
									'email' => $emails
								];
								array_push($flightData, $data);
							}
						} else {
							//echo 'only 2';
							$arrival = $segmentsData->ArrivalStation;
							$arrival_time = explode("T",$segmentsData->STA);
							$departure = $segmentsData->DepartureStation;
							$departure_time = explode("T",$segmentsData->STD);
							$carrier_code = $segmentsData->FlightDesignator->CarrierCode;
							$flight_no = $segmentsData->FlightDesignator->FlightNumber;
							$flight_type = $segmentsData->International == 'true' ? 'international' : 'domestic';
							$emails = '';
							if(isset($segmentsData->PaxSeats->PaxSeat)){
								$seats = $segmentsData->PaxSeats;
								//print_r($seats);
								foreach ($seats as $seatkey => $values) {
									//print_r($values);
									foreach ($values as $key => $value) {
										if(isset($value->UnitDesignator)){
											$seat = $value->UnitDesignator;
											if($value->ArrivalStation == $arrival){
													array_push($seat_arr, $seat);
											}
										}
										
									}	
								}
							}
							$data = [
								'bookingId' => $arrData->GetBookingResponse->Booking->RecordLocator,
								'flight_date' => $segmentsData->STD,
								'flight_no' => $flight_no,
								'std' => $segmentsData->STD,
								'sta' => $segmentsData->STA,
								'departure' => $departure,
								'departure_date' => $departure_time[0],
								'departure_time' => $departure_time[1],
								'arrival' => $arrival,
								'arrival_date' => $arrival_time[0],
								'arrival_time' => $arrival_time[1],
								'carrier_code' => $carrier_code,
								'flight_type' => $flight_type,
				 				'email' => $emails
							];
							array_push($flightData, $data);
						}
						
						
					} else {
						//echo 'only 1';
						$segmentArr = $journeys->Journey->Segments->Segment;
							
							if(is_array($segmentArr)){

								$segment = $segmentArr[0];
								
							} else {

								$segment = $segmentArr;

							}
							$arrival = $segment->ArrivalStation;
							$arrival_time = explode("T",$segment->STA);
							$departure = $segment->DepartureStation;
							$departure_time = explode("T",$segment->STD);
							$carrier_code = $segment->FlightDesignator->CarrierCode;
							$flight_no = $segment->FlightDesignator->FlightNumber;
							$flight_type = $segment->International == 'true' ? 'international' : 'domestic';
							$emails = '';
							if(isset($segment->PaxSeats->PaxSeat)){
								$seats = $segment->PaxSeats->PaxSeat->UnitDesignator;
							// 	foreach ($seats as $seatkey => $value) {
							// 		$seat = $value->UnitDesignator;
								array_push($seat_arr,$segment->PaxSeats->PaxSeat->UnitDesignator);
							// 	}
							}
							$data = [
								'bookingId' => $arrData->GetBookingResponse->Booking->RecordLocator,
								'flight_date' => $segment->STD,
								'flight_no' => $flight_no,
								'std' => $segment->STD,
								'sta' => $segment->STA,
								'departure' => $departure,
								'departure_date' => $departure_time[0],
								'departure_time' => $departure_time[1],
								'arrival' => $arrival,
								'arrival_date' => $arrival_time[0],
								'arrival_time' => $arrival_time[1],
								'carrier_code' => $carrier_code,
								'flight_type' => $flight_type,
								'email' => $emails
							];
							array_push($flightData, $data);
						break;
					}

				}
				
				//Passenger
				//print_r($seats);
				//die();
				//print_r($arrData->GetBookingResponse->Booking->Passengers);
				foreach($arrData->GetBookingResponse->Booking->Passengers as $keyarr => $ArrData){
					foreach($ArrData as $passKey => $passengers){
						if(is_int($passKey)){

							$i = 0;
							foreach ($passengers->Names as $key => $customer) {
								$seatNo = '';
								if(!empty($seat_arr)){
									$seatNo = $seat_arr[$i];
								}
								if(isset($seats->PaxSeat)){
									foreach($seats->PaxSeat as $skey => $value) {
										if($passKey == $skey){
											$data = [
												'firstName' => $customer->FirstName,
												'lastName' => $customer->LastName,
												'seatNo' => $value->UnitDesignator
											];
											array_push($passengerData, $data);
										}
									}
								} else {
									if(!empty($seats)){
										foreach($seats as $skey => $value) {
											if($passKey == $skey){
												$data = [
													'firstName' => $customer->FirstName,
													'lastName' => $customer->LastName,
													'seatNo' => $value->UnitDesignator
												];
												array_push($passengerData, $data);
											}
										}
									} else {
										$data = [
													'firstName' => $customer->FirstName,
													'lastName' => $customer->LastName,
													'seatNo' => ''
										];
										array_push($passengerData, $data);
									}
									
								}
								

								
								$i++;
							}
						} else {
							$customers = $arrData->GetBookingResponse->Booking->Passengers;
							$i = 0;
							foreach ($customers->Passenger->Names as $key => $customer) {
								$seatNo = '';
								if(!empty($seat_arr)){
									$seatNo = $seat_arr[$i];
								}
								
								$data = [
									'firstName' => $customer->FirstName,
									'lastName' => $customer->LastName,
									'seatNo' => $seatNo
								];
								array_push($passengerData, $data);
								$i++;
							}
							break;
						}		
					}
				}

				$fullselectData = [
					'flight' => $flightData,
					'passenger' => $passengerData
				];
				//dd($fullselectData);
			}
			
		} else {
			$fullselectData = [
				'flight' => '',
				'passenger' => ''
			];
		}
		
		return $fullselectData;
	}

	public function getUpdateBooking(Request $request,$pnr,$order_id){
		$agent_type = Session::has('AgentType') ? Session::get('AgentType') : '';

	 	if($agent_type == 'Agent_Super_Admin'){
	 		$agentID = 4;
	 	} else {
	 		$agent = new UserAgentController;
		 	$agentID = $agent->index();
	 	}

	 	$order_status = [
	 		'req_refund' => 'Request Refund'

	 	];

		$request->request->add(['order_id' => $order_id]);
		$order_result = $this->apiOrderDetail($request);

		$apiData = $this->apiGetBooking($pnr);
		//dd($order_result);
		return View::make('agent.layouts.iframe', array('page_title' => "Update Flight"))
                        ->nest('content', 'agent.report.update-content', array(
                            'page_title' => "Update Flight",
                            'pnr' => $pnr,
                            'flightData' => $apiData['flight'],
                            'paxData' => $apiData['passenger'],
                            'agentID' => $agentID,
                            'order_status' => $order_status,
                        	'order_result' => $order_result));
	}


	public function getFlight($pnr) {

		$flights = $this->apiGetBooking($pnr);                       
		if($flights){

			return $flights['flight'];
		} else {
			return [];
		}
	}

	public function getPassenger($pnr) {

		$passengers = $this->apiGetBooking($pnr);

		if($passengers){
			return $passengers['passenger'];
		} else {
			return [];
		}
	}

	public function getChooseUpdate() {
		return view('agent.report.choose-update');
	}

	public function getChangePnr($pnr) {
		$flights = $this->apiGetBooking($pnr);
		$passengers = $this->apiGetBooking($pnr);

		$html = '<div class="col-md-12">
            <div class="form-group">
                <label class="col-md-3 control-label" for="order_status">Flight : </label>
                <div class="col-md-9">
                    <select name="flight" id="flight" class="form-control" width="100%">';

        foreach ($flights['flight'] as $key => $flight) {
        	$date = date_create($flight['departure_date']);
            $departure_date = date_format($date,'d M');
            $item = $flight['departure'].'-'.$flight['arrival'];
            $params = $flight['bookingId'].'|'.$flight['flight_date'].'|'.$flight['std'].'|'.$flight['sta'].'|'.$flight['departure'].'|'.$flight['arrival'].'|'.$flight['carrier_code'].'|'.$flight['flight_type'].'|'.$flight['email'].'|'.$flight['flight_no'];
            $html .= '<option value="'.$params.'">'.$item.' '.$departure_date.'</option>';
        }

        $html .= '</select>
                </div>
            </div>
        </div>';

        $html .= '<div class="col-md-12">
            <div class="form-group">
                <label class="col-md-3 control-label" for="order_status">Pax : </label>
                <div class="col-md-9">
                    <select name="pax" id="pax" class="form-control" width="100%">';

        foreach ($passengers['passenger'] as $key => $passenger) {
        	$fullname = $passenger['firstName'].' '.$passenger['lastName'];
        	$params = $passenger['firstName'].'|'.$passenger['lastName'].'|'.$passenger['seatNo'];
        	$html .= '<option value="'.$params.'">'.$fullname.'</option>';
        }

        $html .= '</select>
                </div>
            </div>
        </div>';

        return $html;
		//return view('agent.report.change-pnr');
	}

	public function getChangeFlight() {
		return view('agent.report.change-flight');
	}

	public function getChangePax() {
		return view('agent.report.change-pax');
	}

	public function getChangeStatus() {
		return view('agent.report.change-status');
	}

	public function updateFlight(Request $request,$order_id) {

		$flight_details = explode('|', $request->input('flight_detail'));
		$flight_date = $request->input('flight_date');
		$carrier_code = $request->input('carrier_code');
		$input_arr = [
				"booking_no" => $flight_details[0],
				"flight_date" => $flight_details[1],
				"flight_no" => $flight_details[9],
				"std" => $flight_details[1],
				"sta" => $flight_details[3],
				"departure" => $flight_details[4],
				"arrival" => $flight_details[5],
				"carrier_code" => $carrier_code,
				"flight_type" => $flight_details[7],
				"email" => $flight_details[8],
				'user_id' => $request->input('user_id'),
				'order_id' => $order_id
		];

		if($this->updateFlightValidation($carrier_code,$flight_date)){
			//echo 'updated error! Please check your flight date';
			flash_msg()->error('Updated error!','Please check your flight date');
			return redirect()->back()->with('flash_error', 'Updated error!.Please check your flight date');

		} else {

			$result = $this->postUpdateFlight($input_arr);
			
			if($result){
				//echo 'updated success';
				flash_msg()->success('Updated!','Your order has been updated!');
				return redirect()->back()->with('flash_success', 'Your order has been updated!');
			} else {
				//echo 'updated error';
				flash_msg()->error('Updated!','Updated error!');
				return redirect()->back()->with('flash_error', 'Updated error!');
			}
		}

		
		
	}

	public function updatePax(Request $request,$order_id) {
		
		//$pax_details = explode(' ', $request->input('pax'));
		$pax_details = explode('|', $request->input('pax'));
		$flight_date = $request->input('flight_date');
		$carrier_code = $request->input('carrier_code');
		$input_arr = [
				"firstname" => $pax_details[0],
				"lastname" => $pax_details[1],
				'seat_number' => $pax_details[2],
				'user_id' => $request->input('user_id'),
				'order_id' => $order_id
		];

		//dd($input_arr);

		if($this->updateFlightValidation($carrier_code,$flight_date)){
			flash_msg()->error('Updated error!','Please check your flight date');
			return redirect()->back()->with('flash_error', 'Updated error!.Please check your flight date');
			
		} else {
			$result = $this->postUpdatePax($input_arr);
			if($result){
				flash_msg()->success('Updated!','Your order has been updated!');
				return redirect()->back()->with('flash_success', 'Your order has been updated!');
			} else {
				flash_msg()->error('Updated!','Updated error!');
				return redirect()->back()->with('flash_error', 'Updated error!');
			}
				
		}
	}

	public function updatePnr(Request $request,$order_id) {

		$flight_details = explode('|', $request->input('flight'));
		$flight_date = $request->input('flight_date');
		$carrier_code = $request->input('carrier_code');
		//dd($flight_details);
		$input_arr_flight = [
				"booking_no" => $flight_details[0],
				"flight_date" => $flight_details[1],
				"flight_no" => $flight_details[9],
				"std" => $flight_details[1],
				"sta" => $flight_details[3],
				"departure" => $flight_details[4],
				"arrival" => $flight_details[5],
				"carrier_code" => $request->input('carrier_code'),
				"flight_type" => $flight_details[7],
				"email" => $flight_details[8],
				'user_id' => $request->input('user_id'),
				'order_id' => $order_id
		];
		
		$pax_details = explode('|', $request->input('pax'));
		$input_arr_pax = [
				"firstname" => $pax_details[0],
				"lastname" => $pax_details[1],
				'seat_number' => $pax_details[2],
				'user_id' => $request->input('user_id'),
				'order_id' => $order_id
		];

		if($this->updateFlightValidation($carrier_code,$flight_date)){
			flash_msg()->error('Updated error!','Please check your flight date');
			return redirect()->back()->with('flash_error', 'Updated error!.Please check your flight date');

		} else {
			//echo 'Updated success!';
			$resultFlight = $this->postUpdateFlight($input_arr_flight);
			$resultPax = $this->postUpdatePax($input_arr_pax);

			if($resultFlight && $resultPax){
				flash_msg()->success('Updated!','Your order has been updated!');
				return redirect()->back()->with('flash_success', 'Your order has been updated!');
			} else {
				flash_msg()->error('Updated!','Updated error!');
				return redirect()->back()->with('flash_error', 'Updated error!');
			}
		}

		return redirect()->back();
	}

	public function updateFlightValidation($carrier_code,$flight_date){

		date_default_timezone_set('Asia/Kuala_Lumpur');
		$timestamp = strtotime($flight_date); //1373673600

		if($carrier_code == 'AK'){

			if($timestamp < time() + 86400) 
			{
			  return true;
			}
			else
			{
			  return false;
			}
		}

		if($carrier_code == 'D7'){

			if($timestamp < time() + 86400 + 86400) 
			{
			  return true;
			}
			else
			{
			  return false;
			}
		}
	}

	public function postUpdateFlight($requests){

		try {
			
			$booking_no = $requests['booking_no'];
			$flight_date = $requests['flight_date'];
			$flight_no = $requests['flight_no'];
			$std = $requests['std'];
			$sta = $requests['sta'];
			$departure = $requests['departure'];
			$arrival = $requests['arrival'];
			$carrier_code = $requests['carrier_code'];
			$flight_type = $requests['flight_type'];
			$email = '';
			$user_id = $requests['user_id'];
			$order_id = $requests['order_id'];

			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			$method = 'PUT';

			$domain	= env('ROKKISHOP_DOMAIN');
			$api	= env('API_ORDER_FLIGHT');

				/* params request */
			$params = '?arrival_station='.$arrival.'&flight_date='.$flight_date.'&booking_no='.$booking_no.'&carrier_code='.$carrier_code.'&departure_station='.$departure.'&flight_type='.$flight_type.'&sta='.$sta.'&std='.$std.'&flight_no='.$flight_no.'&user_id='.$user_id.'&order_id='.$order_id;

			$token = env('API_TOKEN');
			$res = $client->request($method, $domain.$api.$params, [
					'headers' => [
						'Authorization'      => 'Bearer '.$token
					]
			]);

			$result = json_decode($res->getBody());

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		}

		if($result){
			return $result;
		} else {
			return false;
		}

	}

	public function postUpdatePax($requests){

		try {
			
			$first_name = $requests['firstname'];
			$last_name = $requests['lastname'];
			$seat_number = $requests['seat_number'];
			$user_id = $requests['user_id'];
			$order_id = $requests['order_id'];

			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			$method = 'PUT';
			$domain	= env('ROKKISHOP_DOMAIN');
			$api	= env('API_ORDER_PAX');

				/* params request */
			$params = '?first_name='.$first_name.'&last_name='.$last_name.'&seat_number='.$seat_number.'&user_id='.$user_id.'&order_id='.$order_id;

			$token = env('API_TOKEN');
			$res = $client->request($method, $domain.$api.$params, [
					'headers' => [
						'Authorization'      => 'Bearer '.$token
					]
			]);

			$result = json_decode($res->getBody());

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		}

		if($result){
			return $result;
		} else {
			return false;
		}

		
	}

	public function updateStatus(Request $request,$order_id){
		try {
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			$status = $request->input('new_order_status');
			$comment = $request->input('comment');
			$user_id = $request->input('user_id');

			//http://stg-mg.bigdutyfree.com/rest/V1/bdf/report_preorder_flights/order-status?order_id=5017&status=reprint_pack_slip&comment=testing for update status&user_id=1
			$method = 'PUT';

			$domain	= env('ROKKISHOP_DOMAIN');
			$api	= env('API_ORDER_STATUS');

			/* params request */
			$params = "?order_id=".$order_id."&status=".$status."&comment=".$comment."&user_id=".$user_id;

			$token = env('API_TOKEN');
			//echo $domain.$api.$params;
			//die();
			$res = $client->request($method, $domain.$api.$params, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				]
			]);
			
			$result = json_decode($res->getBody());

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		}

		//dd($domain.$api.$params);
		if($result){
			flash_msg()->success('Updated!','Your order has been updated!');
		} else {
			flash_msg()->error('Updated!','Updated error!');
		}
		
        return redirect()->back();
 
	}

}
