<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Hash;

class AgentLoginController extends Controller
{
	public function __construct(Store $session){
        $this->session = $session;
    }

	public function showLoginForm(){
		if(Session::has('AgentSession')) {
            return redirect()->to('agent/home');
        } else {
        	Session::flush();
            return view('agent.auth.login');
        }	
		
	}

	public function agentLogin(Request $request) {

		if(config('agentConfig.agent_admin_authorized')->contains([$request->input('agent_name'),$request->input('password')])){

			$result = Hash::make(str_random(8));

			$this->session->put('AgentSession', $result);
			$this->session->put('AgentName', $request->input('agent_name'));
			$this->session->put('AgentType', 'Agent_Super_Admin');

		} else {
			try {
				$success = true;
				$message = 'Found matching user';
				$client = new Client();

				$method = 'GET';

			
				$domain	= env('NAVITAIRE_DOMAIN');
				$api	= env('NAVITAIRE_GETSESSION');

				/* params request */

				$res = $client->request($method, $domain.$api, [
					'headers' => [
						'domainCode'	=> $request->input('type'),
						'agentName'		=> $request->input('agent_name'),
						'agentPassword' => $request->input('password'),
						'Authorization' => 'Bearer '.env('NAVITAIRE_TOKEN')	
					]
				]);

				$result = $res->getBody()->getContents();
				$status_code = 200;
				$this->session->put('AgentSession', $result);
				$this->session->put('AgentName', $request->input('agent_name'));
				$this->session->put('AgentType', $request->input('type'));

				$agent_name = Session::has('AgentName') ? Session::get('AgentName') : '';
				$this->session->put('AgentID', $this->getSessionUserID($agent_name));

			}catch(\GuzzleHttp\Exception\ClientException $e) {
				$status_code = $e->getResponse()->getStatusCode();
				$success = false;
				if($status_code == 422){
					$message = 'Please complete all required fields';
				}
				$this->session->put('error','Please complete all required fields');
				$result = [];

			}catch(\GuzzleHttp\Exception\ConnectException $e) {
				$success = false;
				$message = 'We are sorry to inform that server is currently offline';
				$this->session->put('error',$message);
				$result = [];
			}catch(\GuzzleHttp\Exception\ServerException $e) {
				$success = false;
				$message = 'We are sorry to inform that server is currently encountered a problem';
				$this->session->put('error',$message);
				$result = [];
			}

		}

		if(empty($result)){
				//$this->session->put('error','Username or password invalid. Please try again');
				Session::put('error', 'Username or password invalid. Please try again');
				return redirect()->back();
		} else {
			if($this->session->has('AgentSession')) {
		            return redirect()->to('agent/home');
		    } else {
		            //$this->session->flush();
		        //$this->session->put('error','Username or password invalid. Please try again');
		        Session::put('error', 'Username or password invalid. Please try again');
		        return redirect()->to('/agent/login')->with('error', 'Username or password invalid. Please try again');
		    }
		}
		

	}

	public function agentLogout(Request $request) {
		$this->session->flush();
		return redirect()->to('agent/login');
	}

	public function getSessionUserID($agent_name) {
		try {
            $success = true;
            $message = 'Found matching user';
            $client = new Client();

            $method = 'GET';

            $domain	= env('NEW_ROKKISHOP_DOMAIN');
            $api    = env('NEW_API_CUSTOMER_SEARCH');

            /* params request */
            $params = '?searchCriteria[filterGroups][0][filters][0][field]=agent_name&searchCriteria[filterGroups][0][filters][0][value]='.$agent_name;

            //echo $domain.$api.$params;

            $token = env('NEW_API_TOKEN');
            $res = $client->request($method, $domain.$api.$params, [
                'headers' => [
                    'Authorization'      => 'Bearer '.$token
                ]
            ]);

            $result = json_decode($res->getBody());
            $status_code = 200;

        }catch(\GuzzleHttp\Exception\ClientException $e) {
            $status_code = $e->getResponse()->getStatusCode();
            $success = false;
            if($status_code == 422){
                $message = 'Please complete all required fields';
            }
            $result = [];

        }catch(\GuzzleHttp\Exception\ConnectException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently offline';
            $result = [];
        }catch(\GuzzleHttp\Exception\ServerException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently encountered a problem';
            $result = [];
        }

        
        if(!empty($result)){
            return $this->getAgentId($result);
        }
	}

	public function getAgentId($result) {

        if (is_array($result) || is_object($result))
        {
            foreach($result->items as $key => $user) {
                $agentID = $user->id;
            }
        }
        
        return $agentID;
    }

}
