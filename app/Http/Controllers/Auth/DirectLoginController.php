<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserSellers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Hash;
use Auth;

class DirectLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | DirectLogin Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation.
    |
    */
    use RedirectsUsers,ThrottlesLogins;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function createAccount(Request $request)
    {
      
        $password = $request->input('luma_password');
        $user = User::create([
                    'name' => $request->input('firstname'),
                    'email' =>  $request->input('email'),
                    'password' => Hash::make($password),
                ]);
                
            if($user){

                $user->assignRole([$request->input('role')]);
                
                $seller = UserSellers::create(
                [
                'seller_id' => $request->input('user_id'),  //Luma Seller ID
                'user_id' => $user->id                      //local User ID
                ]);

            } else {

                return redirect()->route('auth.login');
            }

            $login_request = new Request();
            $login_request->replace(
                [
                'email' => $request->input('email'),
                'password' => $password 
                ]
            );

        if ($this->attemptLogin($login_request)) {
            return $this->sendLoginResponse($login_request);
        }

        return $this->sendFailedLoginResponse($login_request);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {

        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );

    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }


    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

}
