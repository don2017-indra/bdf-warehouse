<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Socialite;
use Auth;
use App\User;
use Session;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use RedirectsUsers,ThrottlesLogins;

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        $user = User::where('email', '=', $request->input('email'))->first();

        if (is_null($user)) {

            $token = $this->getAccountToken(
                $request->input('email'),
                $request->input('password')
            );
            
             $api_user_account = $this->getUserAccountDetails($token);

            if(is_null($api_user_account) || empty($api_user_account))    
            {
                
                //Try with admin
                $admin_token = $this->getAdminAccountToken(
                    $request->input('email'),
                    $request->input('password')
                );

                //check if token is valid
                if(!empty($admin_token))
                {
                    if($this->isAdminTokenValid($admin_token))
                    {
                        $account_name = substr($request->input('email'), 0, strpos($request->input('email'), '@'));

                        $user_id = 0;
                        $email = $request->input('email');
                        $luma_password = $request->input('password');
                        $firstname = empty($account_name) ? $request->input('email') : $account_name;
                        $lastname = '';
                        $token = '';
                        $role = 'administrator';
                       
                       return view('auth.passwords.directlogin_password', 
                            compact(
                            'user_id',
                            'email',
                            'luma_password',
                            'firstname',
                            'lastname',
                            'token',
                            'role'
                            )
                        );
                    }
                }
            
                return redirect()->route('auth.login')->withErrors(['Account does not exist']);
            
            } else if(isset($api_user_account->message)) {
            
                return redirect()->route('auth.login')->withErrors(['Something went wrong. Please try again later']);

            } else {

                $user_id = $api_user_account->id;
                $email = $api_user_account->email;
                $luma_password = $request->input('password');
                $firstname = $api_user_account->firstname;
                $lastname = $api_user_account->lastname;
                $token = '';
                $role = 'warehouse_user';
               
               return view('auth.passwords.directlogin_password', 
                    compact(
                    'user_id',
                    'email',
                    'luma_password',
                    'firstname',
                    'lastname',
                    'token',
                    'role'
                    )
                );

            }
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ],$messages = array('email.required' => 'Email Error','password.required'=>'Password Error'));
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {

        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );

    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    public function getAdminAccountToken($email,$password) 
    {

        try {
            $success = true;
            $message = 'Found matching list of order';
            $client = new Client();

            $method = 'POST';

            $domain = env('NEW_ROKKISHOP_DOMAIN');
            $api    = 'rest/V1/integration/admin/token';
            $params = 
            [
                'username' => $email,
                'password' => $password
            ];

            $res = $client->request($method, $domain.$api ,[
                'headers' => [
                    'content-type' => 'application/json'
                ],
                'json' =>$params
            ]);

            $result = json_decode($res->getBody());

        } catch(\GuzzleHttp\Exception\ClientException $e) {
            $status_code = $e->getResponse()->getStatusCode();
            $success = false;
            if($status_code == 422){
                $message = 'Please complete all required fields';
            }
            $result = '';
        } catch(\GuzzleHttp\Exception\ConnectException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently offline';
            $result = '';
        } catch(\GuzzleHttp\Exception\ServerException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently encountered a problem';
            $result = '';
        }

        if(!$success){
            flash_msg()->error('Error',$message);
        }

        return $result;
    }

    public function getAccountToken($email,$password) 
    {

        try {
            $success = true;
            $message = 'Found matching list of order';
            $client = new Client();

            $method = 'POST';

            $domain = env('NEW_ROKKISHOP_DOMAIN');
            $api    = 'rest/V1/integration/customer/token';
            $params = 
            [
                'username' => $email,
                'password' => $password
            ];

            $res = $client->request($method, $domain.$api ,[
                'headers' => [
                    'content-type' => 'application/json'
                ],
                'json' =>$params
            ]);

            $result = json_decode($res->getBody());

        } catch(\GuzzleHttp\Exception\ClientException $e) {
            $status_code = $e->getResponse()->getStatusCode();
            $success = false;
            if($status_code == 422){
                $message = 'Please complete all required fields';
            }
            $result = '';
        } catch(\GuzzleHttp\Exception\ConnectException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently offline';
            $result = '';
        } catch(\GuzzleHttp\Exception\ServerException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently encountered a problem';
            $result = '';
        }

        if(!$success){
            flash_msg()->error('Error',$message);
        }

        return $result;
    }

    public function getUserAccountDetails($token) 
    {
        try {

            $success = true;
            $message = 'Found matching list of order';
            $client = new Client();

            $method = 'GET';

            $domain = env('NEW_ROKKISHOP_DOMAIN');
            $api    = 'rest/V1/customers/me';

            $res = $client->request($method, $domain.$api, [
                'headers' => [
                    'Authorization'      => 'Bearer '.$token
                ]
            ]);

            $result = json_decode($res->getBody());

        } catch(\GuzzleHttp\Exception\ClientException $e) {
            $status_code = $e->getResponse()->getStatusCode();
            $success = false;
            if($status_code == 422){
                $message = 'Please complete all required fields';
            }
            $result = [];
        } catch(\GuzzleHttp\Exception\ConnectException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently offline';
            $result = [];
        } catch(\GuzzleHttp\Exception\ServerException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently encountered a problem';
            $result = [];
        }

        if(!$success){
            flash_msg()->error('Error' , $message);
        }
    
        return $result;
    }

    public function isAdminTokenValid($token)
    {
        if(strlen($token) == 32)
        {
            return true;
        }

        return false;
    }
}
