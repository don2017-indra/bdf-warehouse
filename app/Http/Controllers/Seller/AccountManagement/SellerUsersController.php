<?php

namespace App\Http\Controllers\Seller\AccountManagement;

use App\User;
use App\Seller;
use App\UserSellers;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use Auth;
use Illuminate\Support\Facades\Session;

class SellerUsersController extends Controller
{


    private $guard_name = 'seller';

    public function __construct(){

        $this->middleware('seller');
    }
    
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('manage_accounts', Auth::user())) {
            return redirect()->to('/seller');
        }
        
        $users = Seller::has('accounts')->get();
        
        return view('seller.account-management.users.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('manage_accounts')) {
            return redirect()->to('/seller');
        }
        $roles = Role::where('guard_name',$this->guard_name)->get()->pluck('name', 'name');

        return view('seller.account-management.users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsersRequest $request)
    {
        if (! Gate::allows('manage_accounts')) {
            return abort(401);
        }
        $user = Seller::create($request->all());

        $seller = UserSellers::create(
            [
            'seller_id' => $request->input('seller_id'),
            'user_id' => $user->id
            ]);
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        return redirect()->route('seller.users.index');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('manage_accounts')) {
            return redirect()->to('/seller');
        }
        $roles = Role::where('guard_name',$this->guard_name)->get()->pluck('name', 'name');

        $user = Seller::findOrFail($id);
        $seller_acc = UserSellers::where('user_id',$id)->first();

        return view('seller.account-management.users.edit', compact('user', 'roles','seller_acc'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsersRequest $request, $id)
    {
        if (! Gate::allows('manage_accounts')) {
            return abort(401);
        }
        $user = Seller::findOrFail($id);
        $user->update($request->all());

        $seller = UserSellers::where('user_id',$id)->update(
            ['seller_id' => $request->input('seller_id')
            ]);

        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);

        return redirect()->route('seller.users.index');
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('manage_accounts')) {
            return abort(401);
        }
        $user = Seller::findOrFail($id);
        $user->accounts()->delete();
        $user->delete();

        return redirect()->route('seller.users.index');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('manage_accounts')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Seller::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
