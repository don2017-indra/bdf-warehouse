<?php

namespace App\Http\Controllers\Seller\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Session;
use Excel;
use App\helpers;
use App\Presenters\StatusPresenter;

class SellerSalesReportController extends Controller
{

	public function index(Request $request)
	{	
		
		$order_status = [
			'paid' => 'Paid',
			'complete' => 'Complete',
			'canceled' => 'Canceled',
			'partial_complete' => 'Partial Complete',
			'partial_canceled' => 'Partial Canceled'
		];

		if(Auth::user()->hasRole('administrator'))
		{	
			$order_status['processing'] = 'Processing';
			$order_status['cart_loaded'] = 'Loaded on Cart';
			$order_status['reserved'] = 'Reserved';
			$order_status['delivered'] = 'Delivered';
			$order_status['reprint_pack_slip'] = 'Re-print Packing slip';
			$order_status['not_delivered'] ='Not Delivered';
			$order_status['canceled'] ='Order Cancelled';
			$order_status['req_refund'] ='Request Refund';
			$order_status['ready_packing'] ='Ready Packing';
			$order_status['canceled'] ='Canceled';
		}
			
		if ($request->has('sortby') && ($request->get('sortby') == 'order_no')) {
		    $sortby = 'order_no';
		    $sort = $request->get('order');
		} else if ($request->has('sortby') && ($request->get('sortby') == 'sales_date')) {
		    $sortby = 'sales_date';
		    $sort = $request->get('sales_date');
		} else if ($request->has('sortby') && ($request->get('sortby') == 'collection_date')) {
		    $sortby = 'pickup_datetime_from';
		    $sort = $request->get('order');
		} else if ($request->has('sortby') && ($request->get('sortby') == 'sales_date')) {
		    $sortby = 'sales_date';
		    $sort = $request->get('order');
		} else if ($request->has('sortby') && ($request->get('sortby') == 'flight_date')) {
		    $sortby = 'flight_date';
		    $sort = $request->get('order');
		} else if ($request->has('sortby') && ($request->get('sortby') == 'order_status')) {
		    $sortby = 'order_status';
		    $sort = $request->get('order');
		} else if ($request->has('sortby') && ($request->get('sortby') == 'recipient')) {
		    $sortby = 'first_name';
		    $sort = $request->get('order');
		} else if ($request->has('sortby') && ($request->get('sortby') == 'shop')) {
		    $sortby = 'shop_title';
		    $sort = $request->get('order');
		} else if ($request->has('sortby') && ($request->get('sortby') == 'updated_at')) {
            $sortby = 'updated_at';
            $sort = $request->get('updated_at');
        } else {
		    $sortby = 'sales_date';
		    $sort = 'DESC';
		}  

		$export = false;
		$order_result = $this->filterField($request, $export, $sortby, $sort);

		$store_location = [];
		if (isset($order_result->available_stores))
		foreach ($order_result->available_stores as $key => $availableStores) {
			$store_location[$availableStores->value] = $availableStores->value;
		}

		$sortingColumn = $this->getSortingColumn($sortby, $sort);

		$s_start_date = null;
		$s_end_date = null;

		if($request->has('sales_date')){
			$s_date = explode(' - ', $request->input('sales_date'));
			$s_start_date = date('d',strtotime($s_date[0]));
			$s_end_date = date('d',strtotime($s_date[1]));
		}

		return view('seller.sales.report.index-seller', 
		   compact('order_result'
				,'order_status'
				,'sortingColumn'
				,'store_location'
				,'s_start_date'
				,'s_end_date'
				)
				);
	}

	public function filterField($request, $export, $sortby, $sort)
	{
		try {
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			$seller_id = Session::get('SellerID');

			if (Auth::user()->hasRole('administrator')) {	
				$seller_id = 0;
			}

			$domain	= env('NEW_ROKKISHOP_DOMAIN');
			$api 	= env('NEW_API_SELLER') . '/' . $seller_id . '/getsellerorders';

			$queryValues = [
				'limit'	=> 10,
				'page' 	=> $request->has('page') ? $request->page : 1,
				'order'	=> $sort,
				'sort'	=> $sortby
			];

			if ($request->has('search-filter')) {

				$arrSearch = $this->getFilterInput($request);
				
				$value = array_map(function($v) {
					return is_array($v) ? implode(',', $v) : $v;
				}, array_column($arrSearch, 'value'));

				$queryValues = array_merge($queryValues, [
					'field' 	=> implode('|', array_column($arrSearch, 'field')),
					'value'		=> implode('|', $value),
					'condition'	=> implode('|', array_column($arrSearch, 'condition')),
				]);

			}

			if ($export) {
				unset($queryValues['page']);
				$queryValues['limit'] = 99999;
			}
			
			$token = env('NEW_API_TOKEN');
			$res = $client->request('GET', $domain . $api, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				],
				'query' => $queryValues
			]);
			
			$result = json_decode($res->getBody());
			
		} catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];
		} catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		} catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		} 

		return $result;
	}

	public function getFilterInput($request)
	{
			$arrSearch = [];
			if($request->has('flight_date')){

				$f_date = explode(' - ', $request->input('flight_date'));
				
				if($f_date[0] == $f_date[1]){
					$f_start_date = date("Y-m-d 00:00:00", strtotime($f_date[0]));
					$f_end_date = date("Y-m-d 23:59:00", strtotime($f_date[1].'+1 days'));
				} else {
					$f_start_date = date("Y-m-d 00:00:00", strtotime($f_date[0]));
					$f_end_date = date("Y-m-d 23:59:00", strtotime($f_date[1].'+1 days'));
				}
				$data = [
					'field' => 'flight_date',
					'value' => $f_start_date.','.$f_end_date,
					'condition' => 'between'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('sales_date')){
				$s_date = explode(' - ', $request->input('sales_date'));
				$s_start_date = date("Y-m-d 00:00:00", strtotime($s_date[0]));
				$s_end_date = date("Y-m-d 23:59:00", strtotime($s_date[1]));

				$data = [
					'field' => 'sales_date',
					'value' => $s_start_date . ',' . $s_end_date,
					'condition' => 'between'
				];
				array_push($arrSearch, $data);
			}

			if($request->has('flight_type')){

				$data = [
					'field' => 'flight_type',
					'value' => $request->input('flight_type'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('pnr')){

				$data = [
					'field' => 'pnr',
					'value' => $request->input('pnr'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('flight_number')){

				$data = [
					'field' => 'flight_number',
					'value' => $request->input('flight_number'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('order_id')){

				$data = [
					'field' => 'order_id',
					'value' => $request->input('order_id'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('aoc')){

				$data = [
					'field' => 'aoc',
					'value' => $request->input('aoc'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('increment_id')){

				$data = [
					'field' => 'increment_id',
					'value' => $request->input('increment_id'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('company_code')){

				$data = [
					'field' => 'company_code',
					'value' => $request->input('company_code'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('carrier_code')){

				$data = [
					'field' => 'carrier_code',
					'value' => $request->input('carrier_code'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('seat_number')){

				$data = [
					'field' => 'seat_number',
					'value' => $request->input('seat_number'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('recipient')){

				$data = [
					'field' => 'recipient',
					'value' => $request->input('recipient'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('store_location')) {

				$data = [
					'field' => 'store_location',
					'value' => $request->input('store_location'),
					'condition' => 'in'
				];

				array_push($arrSearch, $data);

			}

			if($request->has('order_status')) {

				$data = [
					'field' => 'order_status',
					'value' => $request->input('order_status'),
					'condition' => 'in'
				];

				array_push($arrSearch, $data);

			} elseif($request->has('seller_order_status')) {

				$data = [
					'field' => 'seller_order_status',
					'value' => $request->input('seller_order_status'),
					'condition' => 'in'
				];

				array_push($arrSearch, $data);

			} else {

				$all_order_status =
				 [
					'paid',
					'reserved',
					'reprint_pack_slip',
					'cart_loaded',
					'delivered',
					'not_delivered',
					'order_cancelled',
					'req_refund',
					'ready_packing',
					'complete',
					'partial_complete',
					'canceled',
					'partial_canceled'
				];

				$data = [
					'field' => 'seller_order_status',
					'value' =>  $all_order_status,
					'condition' => 'in'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('sku')){

				$data = [
					'field' => 'sku',
					'value' => $request->input('sku'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('collection_date')){

				$c_date = explode(' - ', $request->input('collection_date'));
				$start_date = date_format(date_create($c_date[0]),'Y-m-d');
				$end_date = date_format(date_create($c_date[1]),'Y-m-d');

				$data = [
					'field' => 'collection_date',
					'value' => $start_date.' '.'00:00:00'.','.$end_date.' '.'23:59:59',
					'condition' => 'between'
				];

				array_push($arrSearch, $data);
			}

			if($request->has('shop_name')){

				$data = [
					'field' => 'shop_title',
					'value' => $request->input('shop_name'),
					'condition' => 'eq'
				];

				array_push($arrSearch, $data);
			}

            if($request->has('updated_at')){
                $s_date = explode(' - ', $request->input('updated_at'));
                $s_start_date = date("Y-m-d 00:00:00", strtotime($s_date[0]));
                $s_end_date = date("Y-m-d 23:59:00", strtotime($s_date[1]));

                $data = [
                    'field' => 'updated_at',
                    'value' => $s_start_date . ',' . $s_end_date,
                    'condition' => 'between'
                ];
                array_push($arrSearch, $data);
            }


			return $arrSearch;
	}

	public function getSortingColumn($sortBy, $sort)
	{

		$displayColumn = [
			'order_no' => 0,
			'sales_date' => 1,
			'pickup_datetime_from' => 2,
			'pickup_datetime_from' => 3,
			'updated_at' => 4,
			'first_name' => 5,
			'shop' => 6,
			'order_status' => 7,
		];

        return [
        	'column' => isset($displayColumn[$sortBy]) ? $displayColumn[$sortBy] : 0,
        	'sort' => ($sort=='ASC') ? 'asc' : 'desc'
        ];
    }

	public function orderDetail($order_id,$seller_id) 
	{
		$order_status = [
			'' => '<Select Status>',
	 		'complete' => 'Complete',
	 		'canceled' => 'Cancel'
 		];

 		$update_seller_status = 
 		[
	 		'delivered' => 'Delivered',
	        'ready_packing' => 'Ready for packing',
	        'reserved' => 'Reserved',
	        'reprint_pack_slip' => 'Re-print Packing Slip',
	        'cart_loaded' => 'Loaded on Cart',
	        'not_delivered' => 'Not Delivered',
	        'order_cancelled' => 'Order Cancelled',
	        'req_refund' => 'Requested Refund'
 		];

		$order_result = $this->apiOrderDetail($order_id,$seller_id);
		$order_items = $this->getSellerOrderItems($order_id,$seller_id);
		$real_status = empty($order_result->seller_order_status) ? 'paid' : $order_result->seller_order_status; 
		
		return view('seller.sales.report.order-seller-detail', 
			compact('order_result',
				'order_status',
				'update_seller_status',
				'order_id',
				'seller_id',
				'real_status',
				'order_items')
			);
	}

	public function apiOrderDetail($order_id,$seller_id) 
	{
		try {
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();

			$domain	= env('NEW_ROKKISHOP_DOMAIN');
			$api	= env('NEW_API_SELLER');
			
			$params = '/'.$seller_id.'/getsellerorder/'.$order_id;
			
			$token = env('NEW_API_TOKEN');
 			
			$res = $client->request('GET', $domain . $api . $params, [
				'headers' => [
					'Authorization'	=> 'Bearer '.$token
				]
			]);

			$result = json_decode($res->getBody());

		} catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];
		} catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		} catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		}

		if(!$success){
			flash_msg()->error('Error',$message);
		}

		return $result;
	}

	public function getSellerOrderItems($order_id,$seller_id)
	{
		try {
			
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();
			
			$method = 'GET';
			$domain	= env('NEW_ROKKISHOP_DOMAIN');
			$api	= env('NEW_API_SELLER_ORDER_ITEMS');

			$params = '/' . $seller_id . '/order-items/' . $order_id;

			$token = env('NEW_API_TOKEN');
			$res = $client->request($method, $domain . $api . $params, [
				'headers' => [
					'Authorization' => 'Bearer '.$token
				]
			]);

			$result = json_decode($res->getBody());

		} catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422) {
				$message = 'Please complete all required fields';
			}
			$result = [];

		} catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		} catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		}

		return $result;
	}

	public function updateOrderStatus(Request $request,$order_id,$seller_id,$status){

		try {
			
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();
			
			$method = 'PUT';
			$domain	= env('NEW_ROKKISHOP_DOMAIN');
			$api	= env('NEW_API_SELLER');
			$status = $status == 0 ? $request->input('update_status') : $status;
			$params = '/'.$seller_id.'/'.$order_id.'/updatesellerorderstatus/'.$status;

			$token = env('NEW_API_TOKEN');
			$res = $client->request($method, $domain.$api.$params, [
				'headers' => [
					'Authorization'      => 'Bearer '.$token
				]
			]);

			$result = json_decode($res->getBody());

		}catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];
		}catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		}

		if($result){
			flash_msg()->success('Updated!','Your order has been updated!');
		} else {
			flash_msg()->error('Updated!','Updated error!');
		}
		        
        return redirect()->back();
	}

	public function updateItemStatus(Request $request){

		$order_id = $request->input('orderID');
		$item_id = $request->input('itemID');
		$seller_id = $request->input('sellerID');
		
		$sales_mrktp_id = $request->input('sales_market_id');
		$status = $request->input('update_status');

		if(empty($item_id))
		{
			flash_msg()->error('No Items','There are no items selected!');
			return redirect()->back();
		}

		if(empty($status))
		{
			flash_msg()->error('No Status Selected','Please select a status');
			return redirect()->back();
		}

		try {
			
			$success = true;
			$message = 'Found matching list of order';
			$client = new Client();
			
			$method = 'PUT';
			$domain	= env('NEW_ROKKISHOP_DOMAIN');
			$api	= env('NEW_API_SELLER');

			$failures = 0;
			$items = explode(',',$item_id);
			foreach ($items as $key => $itemID) {

				if(empty($itemID)) continue;
			
				$params = '/'.$itemID.'/'.$sales_mrktp_id.'/updatesellerorderitemstatus/'.$status;

				$token = env('NEW_API_TOKEN');
				$res = $client->request($method, $domain.$api.$params, [
					'headers' => [
						'Authorization'      => 'Bearer '.$token
					]
				]);

				$result = json_decode($res->getBody());

				if(!$result) $failures++;
			}
			
			if ($failures >= 1) {
				flash_msg()->error('Error', $failures.' item(s) failed to update!');
			}

		} catch(\GuzzleHttp\Exception\ClientException $e) {
			$status_code = $e->getResponse()->getStatusCode();
			$success = false;
			if($status_code == 422){
				$message = 'Please complete all required fields';
			}
			$result = [];

		} catch(\GuzzleHttp\Exception\ConnectException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently offline';
			$result = [];

		} catch(\GuzzleHttp\Exception\ServerException $e) {
			$success = false;
			$message = 'We are sorry to inform that server is currently encountered a problem';
			$result = [];
		}

		if ($success) {
			flash_msg()->success('Updated!','The item has been updated!');
		} else {
			flash_msg()->error('Updated!','Updated error!');
		}
		        
        return redirect()->back();
	}

	public function exportReport(Request $request) 
	{
		ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');

        $airAsiaMerchants = ['AirAsia', 'AirAsia Merchandise'];

		if ($request->has('sortby') && ($request->get('sortby') == 'order_no')) {
	        $sortby = 'order_no';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'sales_date')) {
	        $sortby = 'sales_date';
	        $sort = $request->get('sales_date');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'collection_date')) {
	        $sortby = 'pickup_datetime_from';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'sales_date')) {
	        $sortby = 'sales_date';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'flight_date')) {
	        $sortby = 'flight_date';
	        $sort = $request->get('order');
	    } else if ($request->has('sortby') && ($request->get('sortby') == 'order_status')) {
	        $sortby = 'order_status';
	        $sort = $request->get('order');
        } else if ($request->has('sortby') && ($request->get('sortby') == 'recipient')) {
	        $sortby = 'first_name';
	        $sort = $request->get('order');
        } else if ($request->has('sortby') && ($request->get('sortby') == 'seller')) {
	        $sortby = 'shop_name';
	        $sort = $request->get('order');
	    } else {
	        $sortby = 'sales_date';
	        $sort = 'DESC';
	    }
		
		$bigItems = [];
		$order_result = $this->filterField($request,true,$sortby, $sort);
		
		$count = 1;
		if ($order_result) {

			$raw_airports = file_get_contents(storage_path('app/files/json/airports.json'));
			$raw_terminals = file_get_contents(storage_path('app/files/json/terminals.json'));
			$raw_zones = file_get_contents(storage_path('app/files/json/zones.json'));

			$airports = json_decode($raw_airports, true);
			$terminals = json_decode($raw_terminals, true);
			$zones = json_decode($raw_zones, true);

			$status_presenter = new StatusPresenter;

			foreach($order_result->rows as $key => $order) {

				$order_items = $this->getSellerOrderItems($order->order_id,$order->seller_id);
				$sales_date = date('Y-m-d H:i:s', strtotime('+8 hours', strtotime($order->sales_date)));

				foreach ($order_items as $key => $item) {

					$base_catalog_discount = $item->base_original_price - $item->base_price;
					$base_catalog_discount = number_format($base_catalog_discount, 4);
					$catalog_discount = $item->original_price - $item->price;
					$total_paid = ($item->row_total - $item->discount_amount) + $order->tax_amount;
					$base_total_paid =($item->base_row_total - $item->base_discount_amount) + $order->base_tax_amount;
					$qty = number_format($item->qty_ordered,0);
					$item_status = $item->item_status;

					if(!is_null($order->flight_no)){
						$item_status = $order->seller_order_status;
					} 

					$data = [];
					$airport = '';
					$terminal ='';
					$zone ='';
					$collect_on = '';

					$is_free_gift = $item->is_free_gift;
					
					$item_store_location = $item->store_location;
					$collection_areas = config('store_locations.area');

					$raw_store_location = $item->options->store_location;
					$store_location = explode('-', $raw_store_location);

					if (count($store_location) > 2) {
						
						$airport = $airports[$store_location[0]]['name'] ?? null;
						$terminal = $terminals[$store_location[1]] ?? null;
						$zone = $zones[$store_location[2]] ?? null;
						$collect = $store_location[3] ?? null;
						$collect_on = !is_null($collect) ? $collection_areas[$collect] : 'N/A';

						if (empty($item->store_location)) {
							$item_store_location = 'AC';
						}
					}

					if (Session::get('SellerID') != 0) {

						if($item_store_location == 'AC') {
							
							$data = [

									'Order No.'                         => $order->increment_id ?? null,
	                                'Product Name'                      => $item->name ?? null,
	                                'SKU'                               => $item->sku ?? null,
	                                'Quantity'                          => intval($qty) ?? null,
	                                'Base Currency'                     => $item->base_currency_code ?: ($order->base_currency_code ?: null),
	                                'Base Price per Unit'               => floatval($item->base_original_price) ?? null,
	                                'Base Catalog Discount'             => floatval($base_catalog_discount) ?? null,
	                                'Base After-Disc Price per unit'    => floatval($item->base_price) ?? null,
	                                'Base Sub Total'                    => floatval($item->base_row_total) ?? null,
	                                'Total Paid in base currency'       => floatval($base_total_paid) ?? null,
	                                'Sales Date'                        => date_format(date_create($order->sales_date),'Y/m/d'),
	                                'Status'                            => is_null($item_status) ? 'Paid' : $status_presenter->getStatusDesc($item_status),
	                                'Airport'                           => $airport,
									'Terminal'                          => $terminal,
									'Zone'                              => $zone === 'Common' ? 'Public Concourse': $zone,
									'Collect on'                        => $collect_on,
	                                'Collection Date From'              => (!in_array($order->shop_title, $airAsiaMerchants)) ? date_format(date_create($order->pickup_datetime_from),'Y/m/d'): '',
									'Collection Date to'                => (!in_array($order->shop_title, $airAsiaMerchants)) ? date_format(date_create($order->pickup_datetime_to),'Y/m/d') : '',
                                    'Collection Time'                   => (!in_array($order->shop_title, $airAsiaMerchants)) ? date_format(date_create($order->pickup_datetime_from),'h:i a') : '',
                                    //'Seller Name'                     => $order->first_name.' '.$order->last_name,
									//'Passport Number'                 => '',
									'Recepient Name'                    => $order->first_name.' '.$order->last_name ?? null,
		                            'Email'                   	 		=> $order->email ?? null,
		                            'Telephone'              			=> $order->telephone ?? null,
									'Parent ID'                         => $item->parent_sku ?? null,
	                                'Manufacturer'                      => $item->manufacturer ?? null,
									'Category'                          => $item->category ?? null,
	                                'Paid Currency'                     => $order->order_currency_code ?? null,
									'Price paid per Unit'               => floatval($item->original_price) ?? null,
	                                'Final Price Paid per Unit'         => floatval($item->price) ?? null,
	                                'Paid Sub Total'                    => floatval($item->row_total) ?? null,

	                                'Cart Discount in Paid Currency'    => floatval($item->discount_amount) ?? null,
	                                'Total Price in paid currency'      => $total_paid,
	                                'Exchange Rate'                     => floatval($item->base_to_order_rate) ?: (floatval($order->base_to_order_rate) ?: null),
	                                'Promo Code'                        => $order->coupon_code ?? null,
	                                'Status Change Date'                => date_format(date_create($order->updated_at),'Y/m/d'),

									'PGRN'                              => $order->pgrn ?? null
							];
									//$seller_name = $item->seller_name ?? null;
									//array_splice($data, 8, 0, ['Shop Name' => $seller_name]);
					
						} elseif($item_store_location == 'INFLIGHT') {
							continue;
						}

					} else {
						$data = [

	                            'Order No.'                         => $order->increment_id ?? null,
	                            'Product Name'                      => $item->name ?? null,
	                            'SKU'                               => $item->sku ?? null,
	                            'Quantity'                          => intval($qty) ?? null,
	                            'Base Currency'                     => $item->base_currency_code ?: ($order->base_currency_code ?: null),
	                            'Base Price per Unit'               => floatval($item->base_original_price) ?? null,
	                            'Base Catalog Discount'             => floatval($base_catalog_discount) ?? null,
	                            'Base After-Disc Price per unit'    => floatval($item->base_price) ?? null,
	                            'Base Sub Total'                    => floatval($item->base_row_total) ?? null,
	                            'Total Paid in base currency'       => floatval($base_total_paid) ?? null,
	                            'Flight No'                         => $order->flight_no ?? null,
	                            'Departure'                         => $order->departure_station ?? null,
	                            'Arrival'                           => $order->arrival_station ?? null,
	                            'Sales Date'                        => date_format(date_create($order->sales_date),'Y/m/d'),
	                            'Status'                            => is_null($item_status) ? 'Paid' : $status_presenter->getStatusDesc($item_status),
	                            'Merchant'                          => $order->shop_title,
	                            'Flight Type'                       => $order->flight_type ?? null,
	                            'Airport'                           => $airport,
	                            'Terminal'                          => $terminal,
	                            'Zone'                              => $zone === 'Common' ? 'Public Concourse': $zone,
	                            'Collect on'                        => $collect_on,
	                            'Carrier Code'                      => $order->carrier_code ?? null,

	                            'Collection Date From'              => (!in_array($order->shop_title, $airAsiaMerchants)) ? date_format(date_create($order->pickup_datetime_from),"Y/m/d") : '',
	                            'Collection Date to'                => (!in_array($order->shop_title, $airAsiaMerchants)) ? date_format(date_create($order->pickup_datetime_to),"Y/m/d") : '',
                                'Collection Time'                   => (!in_array($order->shop_title, $airAsiaMerchants)) ? date_format(date_create($order->pickup_datetime_from),'h:i a') : '',

	                            'Flight Date'                       => $order->carrier_code ? date_format(date_create($order->flight_date),'Y/m/d') : null,
	                            'PNR'                               => $order->pnr ?? null,
	                            'Big Member ID'                     => $order->big_id ?? null,
	                            'Recepient Name'                    => $order->first_name.' '.$order->last_name ?? null,
	                            'Email'                   	 		=> $order->email ?? null,
	                            'Telephone'              			=> $order->telephone ?? null,
	                            'Parent ID'                         => $item->parent_sku ?? null,
	                            'Manufacturer'                      => $item->manufacturer ?? null,
	                            'Category'                          => $item->category ?? null,
	                            'Paid Currency'                     => $order->order_currency_code ?? null,
	                            'Price paid per Unit'                    => floatval($item->original_price) ?? null,

	                            'Discount given'                    => floatval($catalog_discount) ?? null,

	                            'Final Price Paid per Unit'         => floatval($item->price) ?? null,
	                            'Paid Sub Total'                    => floatval($item->row_total) ?? null,

	                            'Big points redeemed value'         => isset($order->reward_currency_amount) && $order->reward_currency_amount ? floatval($order->reward_currency_amount) : null,

	                            'Cart Discount in Paid Currency'    => floatval($item->discount_amount) ?? null,
	                            'Total Price in paid currency'      => $total_paid,
	                            'Exchange Rate'                     => floatval($item->base_to_order_rate) ?: (floatval($order->base_to_order_rate) ?: null),
	                            'Big points redeemed'               => isset($order->reward_points_balance) && $order->reward_points_balance && ($is_free_gift == false) ? $order->reward_points_balance : null,

	                            'Big Points Total Amount Earned'    => ($is_free_gift == false) ? $order->big_points : '',
	                            'Promo Code'                        => $order->coupon_code ?? null,
	                            'Status Change Date'                => date_format(date_create($order->updated_at),'Y/m/d'),
	                            'PGRN'                              => $order->pgrn ?? null
	                        ];

					}
					
					array_push($bigItems, $data);
				}
			}
		}
		
		$filename = 'Seller_Report-'.date("Y-m-d");

		Excel::create($filename, function($excel) use($bigItems,$count) {

            $excel->sheet('Seller_Report', function($sheet) use($bigItems,$count) {

                $sheet->fromArray($bigItems);
            });

        })->download('xls');
	}
}
