<?php

namespace App\Http\Controllers\Seller\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Socialite;
use Auth;
use App\User;
use App\UserSellers;
use Illuminate\Session\Store;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;


class SellerLoginController extends Controller
{

 use AuthenticatesUsers;

    public function __construct()
    {

        Auth::shouldUse('seller');

    }


    public function showLoginForm()
    {

        if(Auth::check())
        {
            return redirect()->route('seller.home');
        }


        return view('seller.auth.login');
    }

    public function login(Request $request)
    {

        $email = $request->input('seller_email');
        $password = $request->input('password');

        if($attempt = Auth::attempt([
          'email'   => $email,
          'password' => $password
        ])){

            $is_seller_acc = UserSellers::where('user_id',Auth::user()->id)->first();

            if(!empty($is_seller_acc))
            {
                Session::put('SellerID',$is_seller_acc->seller_id);
                return redirect()->route('seller.home');

            } else {

                Auth::logout();
                Session::put('error', "Account does not exist");
                return redirect()->route('seller.login');
            }

                
        } else {

            Session::put('error', "Username or password invalid. Please try again");
            return redirect()->route('seller.login');
        }
    }

    public function logout()
    {   
            
        Session::flush();
        Auth::logout();
   
        return redirect()->route('seller.login');
    }
}
