<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserSellers;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /*$user_id = Auth::user()->id;
        $user_check = UserSellers::where([
            ['user_id','=',$user_id],
            ['seller_id','!=',0]
        ])->first();

        if(count($user_check) >= 1)
        {
            Auth::logout();
            return redirect()->back()->withErrors(['Account does not exists']);
        }*/

        return view('home');
    }
}
