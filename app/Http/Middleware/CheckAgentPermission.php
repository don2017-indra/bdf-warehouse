<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;
use Session;
use Illuminate\Session\Store;

class CheckAgentPermission
{
    protected $session;
    protected $timeout = 12000;

    public function __construct(Store $session){
        $this->session = $session;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isLoggedIn = $request->path() != 'agent/logout';
        if(! session('lastActivityTime'))
            $this->session->put('lastActivityTime', time());
        elseif(time() - $this->session->get('lastActivityTime') > $this->timeout){
            $this->session->forget('lastActivityTime');
            $cookie = cookie('intend', $isLoggedIn ? url()->current() : 'agent/home');
            $request->session()->flush();

            return redirect('/agent/login')->with('timeout', 'You had not activity in '.$this->timeout/60 .' minutes ago.'); 

        }

        $isLoggedIn ? $this->session->put('lastActivityTime', time()) : $this->session->forget('lastActivityTime');

        if(!$request->session()->has('AgentSession')) {

            $request->session()->flush();
            return redirect()->to('/agent/login');
            
        } 

        return $next($request);
        
    }
}
