<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Auth;
use Session;

class CheckSellerPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Auth::shouldUse('seller');

        if (! Gate::allows('manage_reports')) 
        {
            //flash_msg()->error('Restricted!','You have no permission to access the url.');
            //return redirect()->to('/seller/home');
        }


        if(!Auth::check())
        {   
            Session::flush();
             return redirect()->route('seller.login');
        }

        return $next($request);
    }
}
