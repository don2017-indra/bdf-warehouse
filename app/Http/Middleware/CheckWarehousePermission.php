<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;

class CheckWarehousePermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Gate::allows('manage_reports')) {
            //flash_msg()->error('Restricted!','You have no permission to access the url.');
            return redirect()->to('/');
        }
        
        return $next($request);
        
    }
}
