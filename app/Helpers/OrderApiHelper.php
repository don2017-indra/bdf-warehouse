<?php

namespace App\Helpers;

use GuzzleHttp\Client;

/**
 * Class OrderApiHelper
 * @package App\Helpers
 */
class OrderApiHelper {

    public function apiOrderDetail($orderId) {
        try {
            $success = true;
            $message = 'Found matching list of order';
            $client = new Client();

            $method = 'GET';

            //$domain	= 'http://stg-mg.bigdutyfree.com/';
            $domain	= env('ROKKISHOP_DOMAIN');
            $api	= 'rest/V1/bdf/report_preorder_flights/portal-search';

            /* params request */
            $limit = 10;
            $order_by = 'desc';
            $sort_by = 'sales_date';
            $page = 1;
            $field = 'order_id';
            $value = $orderId;
            $condition = 'eq';
            $params = "?limit=".$limit."&page=".$page."&order=desc&sort=".$sort_by."&field=".$field."&value=".$value."&condition=".$condition;
            //echo $domain.$api.$params;
            $token = env('API_TOKEN');
            $res = $client->request($method, $domain.$api.$params, [
                'headers' => [
                    'Authorization'      => 'Bearer '.$token
                ]
            ]);

            $result = json_decode($res->getBody());

        }catch(\GuzzleHttp\Exception\ClientException $e) {
            $status_code = $e->getResponse()->getStatusCode();
            $success = false;
            if($status_code == 422){
                $message = 'Please complete all required fields';
            }
            $result = [];

        }catch(\GuzzleHttp\Exception\ConnectException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently offline';
            $result = [];
        }catch(\GuzzleHttp\Exception\ServerException $e) {
            $success = false;
            $message = 'We are sorry to inform that server is currently encountered a problem';
            $result = [];
        }

        return $result;
    }
}