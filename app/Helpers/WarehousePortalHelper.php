<?php

namespace App\Helpers;

class WarehousePortalHelper {
	
	public $order_status_search = [
        'delivered' => 'Delivered',
        'ready_packing' => 'Ready for packing',
        'reserved' => 'Reserved',
        'reprint_pack_slip' => 'Re-print Packing Slip',
        'cart_loaded' => 'Loaded on Cart',
        'not_delivered' => 'Not Delivered',
        'order_cancelled' => 'Order Cancelled',
        'req_refund' => 'Requested Refund'
        ];

    public $update_statuses = [
        'delivered' => 'Delivered',
        'ready_packing' => 'Ready for packing',
        'reserved' => 'Reserved',
        'reprint_pack_slip' => 'Re-print Packing Slip',
        'cart_loaded' => 'Loaded on Cart',
        'not_delivered' => 'Not Delivered',
        'order_cancelled' => 'Order Cancelled',
        'req_refund' => 'Requested Refund'
    ];

    public $aoc = [
        '' => 'All',
        'AK' => 'AK',
        'D7' => 'D7',
        'Z2' => 'Z2',
        'QZ' => 'QZ',
        'XT' => 'XT',
        'FD' => 'FD',
        'XJ' => 'XJ',
        'DJ' => 'DJ'
    ];

    public $f_type = [
        '' => 'All',
        'international' => 'International',
        'domestic' => 'Domestic'
    ];

    public function setOrderStatusSearch($index_name,$value)
    {
        $this->order_status_search[$index_name] = $value;
    }
}