<?php
namespace App\Helpers;

use GuzzleHttp\Client;

class OrderHelper
{

    /**
     * MAGENTO additional statuses
     */
    const STATUS_PAID = 'paid';
    const STATUS_READY_PACKING = 'ready_packing';
    const STATUS_RESERVED = 'reserved';
    const STATUS_REPRINT_PACKING_SLIP = 'reprint_pack_slip';
    const STATUS_CART_LOADED = 'cart_loaded';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_NOT_DELIVERED = 'not_delivered';
    const STATUS_ORDER_CANCELLED = 'order_cancelled';
    const STATUS_REQUEST_REFUND = 'req_refund';
    const STATUS_PROCESSING = 'processing';

    /**
     * SELLER order status
     */
    const SELLER_STATUS_COMPLETE = 'complete';
    const SELLER_STATUS_CANCELED = 'canceled';
    const SELLER_STATUS_PARTIAL_COMPLETE = 'partial_complete';
    const SELLER_STATUS_PARTIAL_CANCELED = 'partial_canceled';

    /**
     * Additional magento status
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [
            self::STATUS_READY_PACKING, self::STATUS_RESERVED, self::STATUS_REPRINT_PACKING_SLIP,
            self::STATUS_CART_LOADED, self::STATUS_DELIVERED, self::STATUS_NOT_DELIVERED,
            self::STATUS_ORDER_CANCELLED, self::STATUS_REQUEST_REFUND,self::STATUS_PROCESSING,
            self::SELLER_STATUS_COMPLETE, self::SELLER_STATUS_CANCELED, self::SELLER_STATUS_PARTIAL_COMPLETE
        ];
    }

    public function getOrderStatusFromSellerStatus($sellerOrderStatuses)
    {

        $canceled_state = [self::STATUS_ORDER_CANCELLED];

        $complete_states = [
            self::STATUS_DELIVERED,
            self::STATUS_REQUEST_REFUND,
            self::STATUS_NOT_DELIVERED
        ];

        $processing_states = [
            self::STATUS_PAID,
            self::STATUS_READY_PACKING,
            self::STATUS_RESERVED,
            self::STATUS_REPRINT_PACKING_SLIP,
            self::STATUS_CART_LOADED
        ];

        $sellerOrderStatuses = array_unique($sellerOrderStatuses);

        if(count($sellerOrderStatuses) == 1) {
            switch ($sellerOrderStatuses[0]) {

                case self::STATUS_PAID:
                    $finalState = self::STATUS_PAID;
                    break;

                case self::STATUS_RESERVED:
                    $finalState = self::STATUS_RESERVED;
                    break;

                case self::STATUS_NOT_DELIVERED:
                    $finalState = self::STATUS_NOT_DELIVERED;
                    break;

                case self::STATUS_ORDER_CANCELLED:
                    $finalState = self::SELLER_STATUS_CANCELED;
                    break;

                case self::STATUS_REQUEST_REFUND:
                case self::STATUS_DELIVERED:
                    $finalState = self::SELLER_STATUS_COMPLETE;
                    break;

                default:
                    $finalState = self::STATUS_PROCESSING;    
            }

        } else {

            if (in_array(self::STATUS_REQUEST_REFUND, $sellerOrderStatuses) && 
                in_array(self::STATUS_DELIVERED, $sellerOrderStatuses)) {
                $finalState = self::SELLER_STATUS_COMPLETE;

            } elseif (in_array(self::STATUS_REQUEST_REFUND, $sellerOrderStatuses) && 
                in_array(self::STATUS_NOT_DELIVERED, $sellerOrderStatuses)) {
                $finalState = self::SELLER_STATUS_PARTIAL_COMPLETE;

            } elseif (in_array(self::STATUS_ORDER_CANCELLED, $sellerOrderStatuses) && 
                in_array(self::STATUS_NOT_DELIVERED, $sellerOrderStatuses)) {
                $finalState = self::SELLER_STATUS_PARTIAL_CANCELED;

            } elseif (in_array(self::STATUS_DELIVERED, $sellerOrderStatuses)) {
                $finalState = self::SELLER_STATUS_PARTIAL_COMPLETE;

            } elseif (in_array(self::STATUS_ORDER_CANCELLED, $sellerOrderStatuses)) {
                $finalState = self::SELLER_STATUS_PARTIAL_CANCELED;

            } elseif(count(array_intersect($complete_states, $sellerOrderStatuses)) == 1) {
                $finalState = self::SELLER_STATUS_PARTIAL_COMPLETE;

            } else {
                $finalState = self::STATUS_PROCESSING;
            }
        }

        return $finalState;

    }
}