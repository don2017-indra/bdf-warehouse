<?php

namespace App\Helpers;

use GuzzleHttp\Client;

class APIHelper {
	
	public function callApi($domain, $api_url, $method = 'GET', array $headers = [])
    {
        try {

            $client = new Client();
            $res = $client->request($method, $domain . $api_url, $headers);
            $result = json_decode($res->getBody());
           
        } catch(\GuzzleHttp\Exception\ClientException $e) {

            return false;
            
        } catch(\GuzzleHttp\Exception\ConnectException $e) {

            return false;
            
        } catch(\GuzzleHttp\Exception\ServerException $e) {
         
            return false;
        }

        return $result;
    }
}