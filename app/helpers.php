<?php
/**
 * Created by PhpStorm.
 * User: Fahmi
 * Date: 25/11/2016
 * Time: 11:54 AM
 */

function flash_msg($title = null,$message = null)
{
    $flash = app('App\Http\Utilities\Flash');

    if(func_num_args() == 0){
        return $flash;
    }
    return $flash->info($title,$message);
}



function getSellerOrderStatus($items,$orderStatus,$updateDate,$collectionDateFrom,$collectionDateTo)
{	

	$update_date = strtotime($updateDate);
	$collection_date_from = strtotime($collectionDateFrom);
	$collection_date_to = strtotime($collectionDateTo);
	$collection_status = '';


	if($update_date < $collection_date_from)
	{
		$collection_status = 1;
	
	}elseif($update_date >= $collection_date_from && $update_date <= $collection_date_to)
	{
		$collection_status = 2;
	
	}elseif($update_date > $collection_date_to)
	{
		$collection_status = 3;

	}


	$statusHierchy = [
	'paid',
	'reserved',
	'ready for packing',
	'reprint_pack_slip',
	'delivered',
	'not delivered',
	'req_refund',
	'order cancelled',
	];

	$statuses = array();
	foreach ($items as $key => $item) 
	{
		array_push($statuses, $item->status);
	}

	$statuses = array_unique($statuses);
	
	if($collection_status == 1)
	{

		if(strtolower($orderStatus) == 'order cancelled')
		{
			$valid_statuses = array('reserved');
			$valid_status ='';
			foreach ($items as $key => $value) 
			{
				if(in_array($value->status,$valid_statuses))
				{
					$valid_status =  $value['status'];
				}
			}

			return $valid_status;
		}

		if(count($statuses) == 1)
		{
			return $statuses[0];
		
		} else {

			$status_indexes = array();

			foreach ($statuses as $key => $status) {

				if(empty($status)) continue;

				$key = array_search ($status, $statusHierchy);

				array_push($status_indexes,$key);
			}

			rsort($status_indexes,SORT_NUMERIC);

			$lowest = $statusHierchy[$status_indexes[0]];
			
			return $lowest;
		}

	} elseif ($collection_status == 2) {
		
		if(count($statuses) == 1)
		{
			return $statuses[0];//Complete

		} else {


			if(in_array('delivered',$statuses))
			{

				return 'partial delivery';
			
			} else {

				$status_indexes = array();

				foreach ($statuses as $key => $status) {

					if(empty($status)) continue;

					$key = array_search ($status, $statusHierchy);

					array_push($status_indexes,$key);
				
				}

				if(in_array('order cancelled',$statuses)){
				
					sort($status_indexes,SORT_NUMERIC);
				
				} else {

					rsort($status_indexes,SORT_NUMERIC);

				}

				$lowest = $statusHierchy[$status_indexes[0]];
				
				return $lowest;
			}
		}


	}elseif ($collection_status == 3) {
		
		if(in_array('request refund', $statuses))
		{
			return 'partial refund';

		} else {

			return 'complete';
		}
	}
}