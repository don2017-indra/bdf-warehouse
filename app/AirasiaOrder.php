<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;


class AirasiaOrder extends Model
{


	protected $fillable = [
	                    'file_id',
						'purchase_date',
						'pnr',
						'flight_number',
						'currency_code',
						'item_code',
						'item_description',
						'quantity',
						'charge_amount',
						'departure_date',
						'email_address',
						'carrier_code',
						'first_name',
						'last_name',
						'amount_in_myr'
					];
    

	public function savedata($file_id,$sheetData)
	{

        foreach ($sheetData as $key => $value) {
			
			//remove the milliseconds 2018-02-07 10:32:15.820000000
      		$filtered_purchasedate = substr($value['purchasedate'], 0, strpos($value['purchasedate'], "."));
            $purchasedate = date('Y-m-d H:i:s',strtotime($filtered_purchasedate));

            $departuredate = date('Y-m-d H:i:s',strtotime($value['departuredate']));

		        parent::Create([
		        		'file_id' => $file_id,
						'purchase_date' => $purchasedate,
						'pnr' => $value['pnr'],
						'flight_number' => $value['flightnumber'],
						'currency_code' => $value['currencycode'],
						'item_code' => $value['itemcode'],
						'item_description' => $value['itemdescription'],
						'quantity' => $value['quantity'],
						'charge_amount' => $value['chargeamount'],
						'departure_date' => $departuredate,
						'email_address' => $value['emailaddress'],
						'carrier_code' => $value['carriercode'],
						'first_name' => $value['firstname'],
						'last_name' => $value['lastname'],
						'amount_in_myr' => $value['amountinmyr']
					]);
        }
	}

	public function search($searchFields,$sortField,$sortDirection)
	{
	
		$queryBlock = "";

		if(isset($searchFields['flight_date']))
		{

			$queryBlock .= "("."DATE(departure_date) >= " .
                        "STR_TO_DATE('" . addslashes($searchFields['flight_date']['from']) . "','%m/%d/%Y')" . " AND " . "DATE(departure_date) <= ".
                        "STR_TO_DATE('" . addslashes($searchFields['flight_date']['to']) . "','%m/%d/%Y')" . ")";

		}

		if(isset($searchFields['sales_date']))
		{
			if(isset($searchFields['flight_date']))
			{
				$queryBlock .= " AND ";
			}

			$queryBlock .= "("." DATE(purchase_date) >= " . "STR_TO_DATE('" . addslashes($searchFields['sales_date']['from']) . "','%m/%d/%Y')".
                           " AND "."DATE(purchase_date) <= " . "STR_TO_DATE('" . addslashes($searchFields['sales_date']['to']) . "','%m/%d/%Y')" . ")";
			
		}
         
        $filter_results = parent::select('*')->whereRaw($queryBlock)->orderBy($sortField,$sortDirection)->paginate(10);
        return $filter_results;

	}

    public function getOrderDetails($order_id)
    {
        $order_detail = parent::select('*')->where('id',$order_id)->get();
        return $order_detail;
    }
}