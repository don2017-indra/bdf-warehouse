<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomSerialNumber extends Model
{
    
	protected $fillable = ['last_serial_number'];
	public $timestamps = false;

}
