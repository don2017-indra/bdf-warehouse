<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Products\SalesController;
use App\Model\Orders;
use App\Model\OrderDetails;
use App\Model\CustomerDetails;
use App\Model\Products;
use Mail;
use Config;
use App\User;

class OrderTransactionStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'OrderTransactionStatus:statuscomplete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Order Request Transaction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $url = env('CRON_JOBS_URL');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json_data = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($json_data);
        //dd($data);

        // ID,ReferenceNo,array(ItemPurchase),Price,PortalSessionID,MacAddress,User,array(CustomerInfo),BigShotUID,Status,array(UppResponse),DateModified,DateCreated,OrderNumber
        $orderData = array();
        $itemData = array();
        $cutomerData = array();
        $emailArr = array();

        foreach($data as $key => $value)
        {
            $orderData = [
                'request_transaction_id' => $value->ID,
                'ReferenceNo' => $value->ReferenceNo,
                'ItemPurchase' => $value->ItemPurchase,
                'Price' => $value->Price,
                'PortalSessionID' => $value->PortalSessionID,
                'MacAddress' => $value->MacAddress,
                'User' => $value->User,
                'CustomerInfo' => $value->CustomerInfo,
                'BigShotUID' => $value->BigShotUID,
                'Status' => $value->Status,
                'UppResponse' => $value->UppResponse,
                'DateModified' => $value->DateModified,
                'DateCreated' => $value->DateCreated,
                'OrderNumber' => $value->OrderNumber
            ];

            $ItemPurchase = json_decode($value->ItemPurchase, true);

            $customerInfo = json_decode($value->CustomerInfo, true);

            $checkRequest = Orders::where('request_transaction_id',$value->ID)->first();
            if(!$checkRequest){

                $order = Orders::create($orderData);
                
                $new_qty = 0;
                $new_master_qty = 0;
                foreach ($ItemPurchase as $marchant => $items) {
                    foreach($items as $itemkey => $item)
                    {
                        $product_id = $item['ItemID'];
                        $itemData = [
                                'order_id' => $order->id,
                                'merchant' => $marchant,
                                'product_id' => $product_id,
                                'quantity' => $item['Quantity'],
                                'price' => $item['Price'],
                                'sku' => $item['SKU']
                        ];

                        $orderDetail = OrderDetails::create($itemData);

                        /*
                        update product quantity
                        */
                        $product = Products::find($product_id);
  
                        if($product) {
                            $new_qty = $product->quantity - $item['Quantity'];
                            $new_master_qty = $product->master_quantity - $item['Quantity'];
                            $product->quantity = $new_qty;
                            $product->master_quantity = $new_master_qty;
                            $product->save();
                        }

                        foreach ($customerInfo as $customerkey => $customer) {
                            $shoopingRate = $customer['ShippingRate'];
                            //echo $customer['ShippingRate'];
                            if (is_array($shoopingRate) || is_object($shoopingRate))
                            {
                                $shipping_rate = '';
                                foreach($shoopingRate as $shipping)
                                {
                                   if($shipping['vendor_name'] == $marchant)
                                   {
                                        $shipping_rate = $shipping['shipping_rate'];
                                   } else {
                                        $shipping_rate = '';
                                   }
                                }
                            } else {
                                $shipping_rate = '';
                            }
 
                            $cutomerData = [
                                    'order_id' => $order->id,
                                    'merchant' => $marchant,
                                    'product_id' => $product_id,
                                    'customer_id' => $customer['id'],
                                    'name' => $customer['name'],
                                    'email' => $customer['email'],
                                    'booking_no' => $customer['booking_no'],
                                    'seat_no' => $customer['seat_no'],
                                    'shipping_address1' => $customer['shipping_address1'],
                                    'shipping_address2' => $customer['shipping_address2'],
                                    'shipping_city' => $customer['shipping_city'],
                                    'shipping_postcode' => $customer['shipping_postcode'],
                                    'shipping_state' => $customer['shipping_state'],
                                    'shipping_country' => $customer['shipping_country'],
                                    'shipping_phone' => $customer['shipping_phone'],
                                    'flight_no' => $customer['flight_no'],
                                    'origin_code' => $customer['origin_code'],
                                    'destination_code' => $customer['destination_code'],
                                    'order_type' => $customer['order_type'],
                                    'ShippingRate' => $shipping_rate,
                                    'order_detail_id' => $orderDetail->id,
                                ];

                            $customerDetail = CustomerDetails::create($cutomerData);
                        }


                        /*
                        * get Merchant email
                        */
                        $products = Products::with('merchant_info')->where('product_id',$product_id)->first();
                        $user = User::find($products->created_by);
                        $email_to = $user->email;

                        array_push($emailArr,$email_to);

                    }
                    
                }

                
            }

            /*
            * send email notification order
            */
            $this->sendEmailNotification($order->id,$marchant,array_unique($emailArr));
            
        }

        $apiResponse = array('status' => true, 'message' => 'Import successfull');
        $status_code = 200;
        
        return response()->json($apiResponse,$status_code);

    }

    public function sendEmailNotification($order_id,$merchant,$arrEmail){

        $order = Orders::with('comments','order_detail.product_merchant','order_detail.customer_detail','status_logs.statustype')->find($order_id);

        $to_email = env('NOTIFY_EMAIL_TO');

        $content_var = [
            'order' => $order,  
            'merchant' => $merchant
        ];

        Mail::send('emails.merchant-notification', $content_var, function($message) use ($to_email) {
            $message->to($to_email)->subject("Merchant Notification Order");
        });

        return true;
    }
}
