<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Controllers\Admin\Reports\AirasiaOrderController as Orders;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        '\App\Console\Commands\OrderTransactionStatus',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         
        $type = env('CRON_JOBS_START');

        switch ($type) {
            case "everyminutes":
                $schedule->command('OrderTransactionStatus:statuscomplete')
                ->everyMinute();
                break;
            case "5minutes":
                $schedule->command('OrderTransactionStatus:statuscomplete')
                ->everyFiveMinutes();
                break;
            case "10minutes":
                $schedule->command('OrderTransactionStatus:statuscomplete')
                ->everyTenMinutes();
                break;
            case "30minutes":
                $schedule->command('OrderTransactionStatus:statuscomplete')
                ->everyThirtyMinutes();
                break;
            case "hourly":
                $schedule->command('OrderTransactionStatus:statuscomplete')
                ->hourly();
                break;
            default:
                $schedule->command('OrderTransactionStatus:statuscomplete')
                ->daily();
        }

        $schedule->call(function () {
           $orders = new Orders();
           $orders->getRecordFromFiles();
        })->everyFiveMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
