<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;


class AirasiaOrdersUploadFiles extends Model
{

protected $fillable = [
	                    'file_name'
					  ];


	public function saveFileName($filename)
    {
       $data = parent::Create([
        	'file_name' => $filename
        ]);

        return $data->id;

    }

    public function fileExists($filename)
    {

		if (parent::where('file_name', '=', $filename)->count() > 0) {
		  
		   return TRUE;
		}
        
        return FALSE;

    }

}