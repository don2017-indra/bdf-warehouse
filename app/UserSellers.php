<?php
namespace App;

use App\Model\Merchant;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Hash;

/**
 * Class Seller
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
*/
class UserSellers extends Authenticatable
{
    use HasRoles;

    protected $fillable = ['user_id', 'seller_id'];
    protected $guard_name = 'seller';
    
}
