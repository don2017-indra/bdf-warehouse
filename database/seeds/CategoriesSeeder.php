<?php

use Illuminate\Database\Seeder;
use App\Model\ProductCategories as Category;
use Faker\Factory as Faker;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,2) as $index){
            Category::create([
                'name' => $faker->randomElement($array = array ('Beauty','Fragrance','Food')),
                'created_by' => '1'
            ]);
        }
    }
}
