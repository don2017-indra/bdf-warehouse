<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirasiaOrdersUploadFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airasia_orders_upload_files', function (Blueprint $table) {
            $table->bigIncrements('file_id');
            $table->string('file_name',200);
            $table->timestamps();

        });

        Schema::table('airasia_orders', function (Blueprint $table) {
            
            $table->foreign('file_id')
                ->references('file_id')
                ->on('airasia_orders_upload_files')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('airasia_orders', function (Blueprint $table) {
            $table->dropForeign('airasia_orders_file_id_foreign');
        });
        Schema::drop('airasia_orders_upload_files');

    }
}
