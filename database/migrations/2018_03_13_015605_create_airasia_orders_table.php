<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirAsiaOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //if (!Schema::hasTable('orders')) {}
     
         Schema::create('airasia_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('file_id');
            $table->dateTime('purchase_date');
            $table->string('pnr',50);
            $table->string('flight_number',1000);
            $table->string('currency_code',50);
            $table->string('item_code',100);
            $table->string('item_description',1000);
            $table->integer('quantity');
            $table->integer('charge_amount');
            $table->dateTime('departure_date');
            $table->string('email_address',50);
            $table->string('carrier_code',50);
            $table->string('first_name',50);
            $table->string('last_name',50);
            $table->integer('amount_in_myr');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('airasia_orders');
    }
}
