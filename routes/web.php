<?php
Route::get('/', function () { return redirect('/admin/home'); });

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

//New Password From Direct Login Luma account
//$this->get('login/password/directlogin/new', 'Auth\DirectLoginController@showDirectLoginPasswordForm')->name('directlogin.password');
Route::post('directlogin/account/new', ['uses' => 'Auth\DirectLoginController@createAccount', 'as' => 'directlogin.password']);

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    
});

/*
** AirAsia Orders
*/
Route::group(['middleware' => ['auth'], 'prefix' => 'airasia', 'as' => 'aa_orders.'], function () {
   Route::get('/orders',['as'=>'index','uses'=>'Admin\Reports\AirasiaOrderController@index']);
   Route::get('/orders/{id}/paxslip',['as'=>'orders.paxslip','uses'=>'Admin\Reports\AirasiaOrderController@paxSlip']);
   Route::post('orders/upload',['uses'=>'Admin\Reports\AirasiaOrderController@upload']);
});

/*
** Report
*/
Route::group(['middleware' => ['auth'], 'prefix' => 'report', 'as' => 'report.'], function () {
        Route::get('warehouse',['as'=> 'warehouse', 'uses' => 'Admin\Reports\WarehouseReportsController@index']);
        Route::get('warehouse/export',['as'=> 'export', 'uses' => 'Admin\Reports\WarehouseReportsController@exportReportCSV']);
        Route::get('warehouse/order/{id}',['as'=> 'warehouse.detail', 'uses' => 'Admin\Reports\WarehouseReportsController@orderDetail']);
        Route::get('warehouse/order/{id}/paxslip',['as'=> 'warehouse.paxslip', 'uses' => 'Admin\Reports\WarehouseReportsController@paxSlip']);

        Route::post('warehouse/order/update/{id}',['as' => 'warehouse.update-status', 'uses' => 'Admin\Reports\WarehouseReportsController@updateStatus']);
    });

/*
** New Warehouse Report
*/
Route::group(['middleware' => ['auth'], 'prefix' => 'wreport2', 'as' => 'wreport2.'], function () {

   Route::get('warehouse2',['as'=> 'warehouse', 'uses' => 'Admin\Reports\WarehouseReports2Controller@index']);
   Route::get('warehouse2/export',['as'=> 'export', 'uses' => 'Admin\Reports\WarehouseReports2Controller@exportReport']);
   Route::get('warehouse2/order/{seller_id}/view/{order_id}',['as'=> 'warehouse.detail', 'uses' => 'Admin\Reports\WarehouseReports2Controller@orderDetail']);
   Route::get('warehouse2/order/{seller_id}/paxslip/{order_id}',['as'=> 'warehouse.paxslip', 'uses' => 'Admin\Reports\WarehouseReports2Controller@paxSlip']);
   Route::post('warehouse2/order/{order_id}/updatemagento/{seller_ids}',['as' => 'warehouse.magento-order-update-status', 'uses' => 'Admin\Reports\WarehouseReports2Controller@updateAdminStatus']);
   Route::post('warehouse2/order/{order_id}/updateseller/{seller_id}',['as' => 'warehouse.seller-order-update-status', 'uses' => 'Admin\Reports\WarehouseReports2Controller@updateSellerOrderStatus']);
   Route::get('warehouse2/order/{seller_id}/invoice/{increment_id}/{order_id}',['as'=> 'warehouse.invoice', 'uses' => 'Admin\Reports\WarehouseReports2Controller@generateInvoice']);


});

/*
** Custom Report
*/
Route::group(['middleware' => ['auth'], 'prefix' => 'customreport', 'as' => 'customreport.'], function () {
   Route::get('/',['as'=> 'index', 'uses' => 'Admin\Reports\CustomReportController@index']);
   Route::get('/exportall',['as'=> 'exportAll', 'uses' => 'Admin\Reports\CustomReportController@exportAllCustomReports']);
   Route::get('/order/export',['as'=> 'export', 'uses' => 'Admin\Reports\CustomReportController@exportCustomReportExcel']);
   Route::get('/order/{id}',['as'=> 'detail', 'uses' => 'Admin\Reports\CustomReportController@orderDetail']);
   Route::get('/order/{id}/paxslip',['as'=> 'paxslip', 'uses' => 'Admin\Reports\CustomReportController@paxSlip']);
   Route::post('/order/update/{id}',['as' => 'update-status', 'uses' => 'Admin\Reports\CustomReportController@updateStatus']);

});


//Seller Portal

// Change Password Routes...
$this->get('seller/change_password', 'Seller\Auth\ChangePasswordController@showChangePasswordForm')->name('seller-change_password');
$this->patch('seller/change_password', 'Seller\Auth\ChangePasswordController@changePassword')->name('seller.change_password');

// Password Reset Routes...
$this->get('seller/password/reset', 'Seller\Auth\ForgotPasswordController@showLinkRequestForm')->name('seller-password-reset-link');
$this->post('seller/password/email', 'Seller\Auth\ForgotPasswordController@sendResetLinkEmail')->name('seller-password-reset-email');
$this->get('seller/password/reset/{token}', 'Seller\Auth\ResetPasswordController@showResetForm')->name('password-reset-form');
$this->post('seller/password/reset', 'Seller\Auth\ResetPasswordController@reset')->name('seller-password-reset');

$this->post('seller/logout', 'Seller\Auth\SellerLoginController@logout')->name('seller.post-logout');
$this->get('seller/login', 'Seller\Auth\SellerLoginController@showLoginForm')->name('seller.login');;
$this->post('seller/login','Seller\Auth\SellerLoginController@login')->name('seller.post-login');

Route::group(['middleware' => ['seller'], 'prefix' => 'seller', 'as' => 'seller.'], function () {
    
   
    Route::get('/',['as' => 'index','uses' => 'Seller\Auth\HomeSellerController@index']);
    Route::get('/home',['as' => 'home','uses' => 'Seller\Auth\HomeSellerController@index']);
   
    Route::get('/orders',['as'=> 'orders-index', 'uses' => 'Seller\Reports\SellerSalesReportController@index']);
    Route::get('/order/{orderid}/{sellerid}',['as'=> 'order-detail', 'uses' => 'Seller\Reports\SellerSalesReportController@orderDetail']);

    Route::post('/order/{orderid}/{sellerid}/status/{status}',['as'=> 'update-order-status', 'uses' => 'Seller\Reports\SellerSalesReportController@updateOrderStatus']);
    Route::post('/order/itemstatus',['as'=> 'update-item-status', 'uses' => 'Seller\Reports\SellerSalesReportController@updateItemStatus']);
    Route::get('/sellerorders/export',['as'=> 'export', 'uses' => 'Seller\Reports\SellerSalesReportController@exportReport']);
    
    Route::resource('permissions', 'Seller\AccountManagement\SellerPermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Seller\AccountManagement\SellerPermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Seller\AccountManagement\SellerRolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Seller\AccountManagement\SellerRolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Seller\AccountManagement\SellerUsersController');
    Route::post('users_mass_destroy', ['uses' => 'Seller\AccountManagement\SellerUsersController@massDestroy', 'as' => 'users.mass_destroy']);

});

/*
** Agent
*/
Route::group(['prefix' => 'agent', 'as' => 'agent.'], function () {
    Route::get('login',['as' => 'login','uses' => 'Agent\AgentLoginController@showLoginForm']);
    Route::post('login',['as' => 'post-login','uses' => 'Agent\AgentLoginController@agentLogin']);
    Route::get('logout',['as' => 'post-logout','uses' => 'Agent\AgentLoginController@agentLogout']);
});
Route::group(['middleware' => ['agent'],'prefix' => 'agent', 'as' => 'agent.'], function () {
    Route::get('/home', 'Agent\HomeAgentController@index');
    Route::get('report',['as'=> 'report', 'uses' => 'Agent\AgentReportsController@index']);
    Route::get('report/export',['as'=> 'export', 'uses' => 'Agent\AgentReportsController@exportReportCSV']);
    Route::get('report/order/{id}',['as'=> 'report.detail', 'uses' => 'Agent\AgentReportsController@orderDetail']);
    Route::get('report/order/{id}/paxslip',['as'=> 'report.paxslip', 'uses' => 'Agent\AgentReportsController@paxSlip']);

    Route::get('choose-update','Agent\AgentReportsController@getChooseUpdate');
    Route::get('change-pnr/{pnr}','Agent\AgentReportsController@getChangePnr');
    Route::get('change-flight/{pnr}','Agent\AgentReportsController@getFlight');
    Route::get('change-pax','Agent\AgentReportsController@getChangePax');
    Route::get('change-status','Agent\AgentReportsController@getChangeStatus');
    Route::get('/agentid','Agent\UserAgentController@index');
    Route::get('report/order/{id}',['as'=> 'report.detail', 'uses' => 'Agent\AgentReportsController@orderDetail']);
    Route::get('report/pnr/{pnr}',['as'=> 'report.pnr', 'uses' => 'Agent\AgentReportsController@apiGetBooking']);

    Route::get('report/update/pnr/{pnr}/{id}',['as' => 'update.pnr','uses' => 'Agent\AgentReportsController@getUpdateBooking']);

    Route::post('report/order/update-flight/{id}',['as' => 'report.update-flight', 'uses' => 'Agent\AgentReportsController@updateFlight']);
    Route::post('report/order/update-pax/{id}',['as' => 'report.update-pax', 'uses' => 'Agent\AgentReportsController@updatePax']);
    Route::post('report/order/update-pnr/{id}',['as' => 'report.update-pnr', 'uses' => 'Agent\AgentReportsController@updatePnr']);
    Route::post('report/order/update/{id}',['as' => 'report.update-status', 'uses' => 'Agent\AgentReportsController@updateStatus']);
});

/*
** Agent Reports 2
*/
Route::group(['middleware' => ['agent'],'prefix' => 'agent2', 'as' => 'agent2.'], function () {

    Route::get('report',['as'=> 'report', 'uses' => 'Agent\AgentReports2Controller@index']);
    Route::get('report2/export',['as'=> 'export', 'uses' => 'Agent\AgentReports2Controller@exportReportCSV']);
    Route::get('report2/order/{id}',['as'=> 'report.detail', 'uses' => 'Agent\AgentReports2Controller@orderDetail']);
    Route::get('report2/order/{id}/paxslip',['as'=> 'report.paxslip', 'uses' => 'Agent\AgentReportsController@paxSlip']);

    Route::get('choose-update','Agent\AgentReports2Controller@getChooseUpdate');
    Route::get('change-pnr/{pnr}','Agent\AgentReports2Controller@getChangePnr');
    Route::get('change-flight/{pnr}','Agent\AgentReports2Controller@getFlight');
    Route::get('change-pax','Agent\AgentReports2Controller@getChangePax');
    Route::get('change-status','Agent\AgentReports2Controller@getChangeStatus');
    Route::get('/agentid','Agent\UserAgentController@index');
    Route::get('report/order/{id}',['as'=> 'report.detail', 'uses' => 'Agent\AgentReports2Controller@orderDetail']);
    Route::get('report/pnr/{pnr}',['as'=> 'report.pnr', 'uses' => 'Agent\AgentReports2Controller@apiGetBooking']);

    Route::get('report/update/pnr/{pnr}/{id}',['as' => 'update.pnr','uses' => 'Agent\AgentReports2Controller@getUpdateBooking']);

    Route::post('report/order/update-flight/{id}',['as' => 'report.update-flight', 'uses' => 'Agent\AgentReports2Controller@updateFlight']);
    Route::post('report/order/update-pax/{id}',['as' => 'report.update-pax', 'uses' => 'Agent\AgentReports2Controller@updatePax']);
    Route::post('report/order/update-pnr/{id}',['as' => 'report.update-pnr', 'uses' => 'Agent\AgentReports2Controller@updatePnr']);
    Route::post('report/order/update/{id}',['as' => 'report.update-status', 'uses' => 'Agent\AgentReports2Controller@updateStatus']);
});
